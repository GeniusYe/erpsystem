Introduction
------------

ERPSystem is a C/S desktop warehouse management program based on TCP (Customized higher protocol) with .NET(C#). Salesman & Manager roles & multiple operators supported.

System Requirement
------------------

ERPSystem is developed based on .NET framework

-	.NET 4.0+
-	Windows Operating System
-	Microsoft SQL Server 2008+

is needed.

Initialize System
-----------------

#### Company name ####

*WebRoot/usr/include/include-link.jsp*

#### Company logo ####

*WebRoot/usr/img/logo.png*

#### DB Info ####

*WebRoot/WEB-INF/classes/applicationContext_util.xml*

#### Initialize DB ####

Import *db_\*.sql* into database

#### Initialize System Password ####

Table Sys_User contains all user info, please add a few lines of user for your users

-	**name:** users' name
-	**password:** sha1 encrypted password
-	**role:** choices are *USER\_IN* and *USER\_ADMIN*
	*USER\_IN* is the most widely used user role, it represents all the accountants who have the rights to see and modify the account.
	*USER\_ADMIN* is the admin user, what have the rights to log into lock system to lock system.
-	**username:** the name used to log in the system
-	**certificateID** is used to handle certificate log in, **password** and **certificateID** is not both required, they are not null means that user can support this way of log in.

Main Functionality
------------------

This program provide functionalities in the following aspects.

-	Warehouse management, import/export draft, balance, receipt print.
-	Bonus calculation, automanticlly get transaction from warehouse module, automantically detect transaction type(VIP, normal, after-sale service, etc.) and calculate bonus for servant.
-	Ticket, ticket management, all aspects search.

Monthly Update Process
----------------------

Every month, the database save some old data to another place, when these information are queried, the system will not try to read a lot of data to response. The cycle is set to 29th of last month to 28th of this month (For example, September 29 00:00:00 to October 28 23:59:59 is one cycle, the cycle we called October)

The following 4 procedure will do the save operation when a new cycle begins.

This one stored the balance of all drafts, when we try to read the history balance, the system read from here.
	
	EXEC dbo.[SP_Draft_SaveDataForMonthCheck]

This one stored a lot of data of all vouchers in one month, like total sales amount/ total cost/ total discount we give out. 
	
	EXEC dbo.[SP_Warrant_SaveAmountForMonthCheck]

Every month, we have 5-7 days for warehouse manager and counter to check if there are mistakes between them, after 5th day of the second month, the voucher will be locked and no changes can be made anymore.

	EXEC dbo.[SP_Warrant_FinishForMonthlyResult]

Every month, on the 15th day, the system will lock all the ticket that happens last month, this procedure does that.d

	EXEC dbo.[SP_Ticket_FinishForMonthlyResult]

Bonus
-----

#### 