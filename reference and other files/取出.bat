      @echo.服务停止......
      @echo off
      @sc stop MSSQL$ERPSYSTEM
      @echo off
      @echo.停止完毕！
      @pause
      copy "E:\Program Files\Microsoft SQL Server\MSSQL10.ERPSYSTEM\MSSQL\DATA\ERPSystem.mdf" .\
      copy "E:\Program Files\Microsoft SQL Server\MSSQL10.ERPSYSTEM\MSSQL\DATA\ERPSystem_log.ldf" .\
      @pause
      @echo.服务启动......
      @echo off
      @sc start MSSQL$ERPSYSTEM
      @echo off
      @echo.启动完毕！
      @pause