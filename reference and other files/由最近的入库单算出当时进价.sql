use ERPSystem
select IDDraft, IDWarrant, price, num, currentPriceIn, currentNumAll, 
	(currentPriceIn * currentNumAll - price * num) / (currentNumAll - num) as oriPrice
	from dbo.[Warrant.In.Item] AS wii
	left join dbo.[Warrant.In.Warrant] as wiw on wiw.ID = wii.IDWarrant 
	join dbo.[Warrant.In.OperationRecord] as wio on wio.warrantID = wiw.ID and wio.operation = 8 and DATEDIFF(DAY, wio.operatedTime, '2012-12-29') < 0
	where wiw.state >= 9 and price != currentPriceIn
	order by IDDraft, wio.operatedTime desc
