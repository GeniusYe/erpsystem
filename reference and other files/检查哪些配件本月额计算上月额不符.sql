CREATE TABLE #TTT(DRAFT INT, NOW INT, SHOULD INT, LAST INT, INS INT, OUT INT)
GO

DECLARE @IDDraft int = 2

WHILE @IDDRAFT < 1000
BEGIN
DECLARE @IN INT = (SELECT sum(wii.num * wii.price) from dbo.[Warrant.In.Item] as wii 
	left join dbo.[Warrant.In.Warrant] as wiw on wiw.ID = wii.IDWarrant and 
	exists(select * from dbo.[Warrant.In.OperationRecord] as wio where wio.warrantID = wiw.ID and wio.operation = 8 and wio.operatedTime between '2012-12-29' and '2013-01-29')
	where wiw.state >= 9 and wii.IDDraft = @IDDraft)
DECLARE @OUT INT = (select sum((woi.num - woi.numReturn) * woi.priceCost) from dbo.[Warrant.Out.Item] as woi 
	left join dbo.[Warrant.Out.Warrant] as wow on wow.ID = woi.IDWarrant and 
	exists(select * from dbo.[Warrant.Out.OperationRecord] as woo where woo.warrantID = wow.ID and woo.operation = 8 and woo.operatedTime between '2012-12-29' and '2013-01-29')
	where wow.state >= 9 and woi.IDDraft = @IDDraft)

DECLARE @NOW INT = (SELECT SUM(numAll * priceIn) from [Draft.Draft] WHERE ID = @IDDraft)
DECLARE @LAST INT = (SELECT SUM(numOriginal201212 * priceInOriginal201212) from [Draft.Amount] WHERE IDDraft = @IDDraft)
DECLARE @SHOULD INT = @NOW + @OUT - @IN

INSERT INTO #TTT VALUES(@IDDRAFT, @NOW, @SHOULD, @LAST, @IN, @OUT)

SET @IDDRAFT = @IDDRAFT + 1
END

SELECT * FROM #TTT

SELECT * FROM #TTT WHERE SHOULD != LAST