﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using ServerSystem.Global;
using ServerSystem.ClientsConnection;

namespace ERPSystemServer
{
    static class Program
    {
        static Thread tcpListenerThread1;
        static bool ifListening = true;

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string parameterFileName = Application.StartupPath + "\\parameters.inf";

            int i = 0;
            while (true)
            {
                if (args.Length > i)
                {
                    switch (args[i])
                    {
                        case "-p":
                            if (args.Length > i + 1)
                            {
                                parameterFileName = args[i + 1];
                                i += 1;
                            }
                            else
                                Application.Exit();
                            break;
                    }
                }
                else
                    break;
            }

            try
            {
                using (StreamReader parametersReader = new StreamReader(parameterFileName))
                {
                    string str;
                    string[] pair;
                    while (!parametersReader.EndOfStream)
                    {
                        try
                        {
                            str = parametersReader.ReadLine();
                            pair = str.Split('=');
                            switch (pair[0])
                            {
                                case "port":
                                    GlobalParameters.localPort = Int32.Parse(pair[1]);
                                    break;
                                case "disposePartMessageTimeOut":
                                    GlobalParameters.disposePartMessageTimeOut = Int32.Parse(pair[1]);
                                    break;
                                case "logFileName":
                                    GlobalParameters.logFileName = pair[1];
                                    break;
                                case "connectorTimeOut":
                                    GlobalParameters.connectorTimeOut = Int32.Parse(pair[1]);
                                    break;
                                case "tcpBufferSize":
                                    GlobalParameters.tcpBufferSize = Int32.Parse(pair[1]);
                                    break;
                                case "certificateName":
                                    GlobalParameters.certificateName = pair[1];
                                    break;
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            catch { }
            ParameterizedThreadStart tcpListenerThreadStart = new ParameterizedThreadStart(tcpListenerStart);
            tcpListenerThread1 = new Thread(tcpListenerThreadStart);
            tcpListenerThread1.Start(GlobalParameters.localPort);
            tcpListenerThread1.IsBackground = true;
            Application.Run();
        }

        private static void tcpListenerStart(object obj)
        {
            TcpListener listener = new TcpListener(IPAddress.Any, GlobalParameters.localPort);
            try
            {
                X509Certificate2 serverCertificate = new X509Certificate2(Application.StartupPath + "\\" + GlobalParameters.certificateName + ".pfx", "youlincertificate");

                listener.Start();

                while (ifListening)
                {
                    NetClient c = new NetClient(listener.AcceptTcpClient(), GlobalParameters.localPort, serverCertificate);
                }
            }
            catch (Exception e)
            {
                GlobalParameters.log(e);
            }
                
        }
    }
}
