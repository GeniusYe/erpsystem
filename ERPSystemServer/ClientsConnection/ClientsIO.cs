﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ServerSystem.Connector;
using SharedObject.Main.Transmission;
using SharedObject.Identity;
using SharedObject.Normal;
using TransmissionReceiveMessage;

namespace ServerSystem.ClientsConnection
{
    public class ClientsIO
    {
        private List<Connector.Connector> connectors = new List<Connector.Connector>();

        private Connector.Identity.IdentityConnector.IdentityState identityState = new Connector.Identity.IdentityConnector.IdentityState();
        private NetClientWithCom netClient;

        public ClientsIO(NetClientWithCom netClient)
        {
            this.netClient = netClient;
        }

        /// <summary>
        /// Method Reply is used to handle the received data of receiveStream
        /// </summary>
        /// <param name="instrData"></param>
        /// <param name="receiveStream"></param>
        /// <returns></returns>
        public void Reply(MemoryStream receiveStream)
        {
            byte[] instrData = new byte[16];
            receiveStream.Read(instrData, 0, 16);
            MemoryStream returnStream = null, authorizeExecuteStream = null;
            if (instrData.Length < 16) return;
            Instruction mainInstruction;
            int cookie;
            if (instrData[0] == 0 && instrData[1] == 0 && instrData[2] == 0 && instrData[3] == 0)
                return;
            mainInstruction = (Instruction)instrData[2];
            cookie = ReceiveMessage.getCookie(instrData);
            try
            {
                SystemID systemID = (SystemID)instrData[1];
                foreach (Connector.Connector connector in connectors)
                    if (connector.ifMatch(mainInstruction, cookie))
                        returnStream = connector.execute(instrData, receiveStream);
                if (returnStream == null)
                {
                    Connector.Connector connectorNew = null;// = new CustomerManageServerConnector(instrData, customerManageConnectors, identityState.normalIdentity);
                    switch (mainInstruction)
                    {
                        //Identity
                        case Instruction.askForNormalLogIn:
                        case Instruction.askForSpecialLogIn:
                            connectorNew = new Connector.Identity.IdentityConnector(instrData, connectors, identityState);
                            break;
                        //WareHouseManage
                        case Instruction.draftContainer1List:
                        case Instruction.draftContainer2List:
                        case Instruction.draftContainer3List:
                            connectorNew = new WareHouseManage.Connector.ReadContainerConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.draftDisplayDraftList:
                        case Instruction.draftDisplayDraftListSearch:
                            connectorNew = new WareHouseManage.Connector.ReadDraftListConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.draftSpecialOne:
                            connectorNew = new WareHouseManage.Connector.ReadSpecialOneDraftConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.draftAdd:
                        case Instruction.draftEdit:
                            connectorNew = new WareHouseManage.Connector.CreateOneDraftConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.draftSubmit:
                        case Instruction.draftDel:
                            connectorNew = new WareHouseManage.Connector.ControlOneDraftConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.draftViewAll:
                            connectorNew = new WareHouseManage.Connector.ViewAllConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        //CustomerManage
                        case Instruction.customerInSourceList:
                        case Instruction.customerOutCustomerList:
                        case Instruction.customerAllCustomersList:
                            connectorNew = new CustomerManage.Connector.ReadCustomersConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.customerSpecialOneCustomer:
                            connectorNew = new CustomerManage.Connector.ReadSpecialOneCustomerConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.customerAdd:
                            connectorNew = new CustomerManage.Connector.CreateOneCustomerConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        //WareHouseInOut
                        case Instruction.inWarrantList:
                        case Instruction.outWarrantList:
                            connectorNew = new WareHouseInOut.Connector.ReadWarrantListConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.inWarrantSpecialOne:
                        case Instruction.outWarrantSpecialOne:
                            connectorNew = new WareHouseInOut.Connector.ReadSpecialOneWarrantConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.inWarrantAdd:
                        case Instruction.inWarrantEdit:
                        case Instruction.outWarrantAdd:
                        case Instruction.outWarrantEdit:
                            connectorNew = new WareHouseInOut.Connector.CreateOneWarrantConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.inWarrantSubmit:
                        case Instruction.inWarrantPrint:
                        case Instruction.outWarrantSubmitCustomerPage:
                        case Instruction.outWarrantPrintCustomerPage:
                        case Instruction.outWarrantForcePass:
                        case Instruction.outWarrantTakeOut:
                        case Instruction.outWarrantSubmit:
                        case Instruction.outWarrantPrint:
                            connectorNew = new WareHouseInOut.Connector.ControlOneWarrantConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.outWarrantSpecialPrice:
                            connectorNew = new WareHouseInOut.Connector.GetSpecialPriceConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.outWarrantReturn:
                            connectorNew = new WareHouseInOut.Connector.ReturnOneWarrantConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.warrantViewDetail:
                            connectorNew = new WareHouseInOut.Connector.ViewDetailConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.warrantViewOneCycle:
                            connectorNew = new WareHouseInOut.Connector.ViewOneCycleConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.outWarrantDiscount:
                        case Instruction.outWarrantPayCheck:
                        case Instruction.outWarrantReturnPayCheck:
                            connectorNew = new WareHouseInOut.Connector.MoneyOperationConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.warrantAttendantsList:
                            connectorNew = new WareHouseInOut.Connector.ReadAttendantsConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.rightsAddUser:
                            connectorNew = new RightsControl.Connector.CreateNewUserConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                        case Instruction.test:
                            connectorNew = new Connector.Test.TestConnector(instrData, connectors, identityState.normalIdentity);
                            break;
                    }
                    returnStream = connectorNew.execute(instrData, receiveStream);
                    if (mainInstruction == Instruction.askForSpecialLogIn)
                        authorizeExecuteStream = ((Connector.Identity.IdentityConnector)connectorNew).authorizeExecuteStream;
                }
                returnReturnStream(netClient.netClient, returnStream);
                if (mainInstruction == Instruction.askForSpecialLogIn && authorizeExecuteStream != null)
                {
                    returnReturnStream(netClient.netClient, authorizeExecuteStream);
                }
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        private void returnReturnStream(NetClient netClient, MemoryStream returnStream)
        {
            if (returnStream != null)
            {
                netClient.sendMessage(returnStream);
            }
        }
    }
}
