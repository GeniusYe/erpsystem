﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Collections;
using System.IO;
using SharedObject.Main.Transmission;
using TransmissionReceiveMessage;

namespace ServerSystem.ClientsConnection
{
    public class NetClient
    {
        public static Hashtable allClients = new Hashtable();

        private TcpClient client;
        public string clientIP { get; private set; }
        private int clientPort;
        private ReceiveMessage transmissionReceiveMessage;
        public ClientsIO clientsIO { get; private set; }

        private SslStream sslStream;

        public NetClient(TcpClient client, int clientPort, X509Certificate serverCertificate)
        {
            this.client = client;
            this.clientPort = clientPort;
            clientIP = client.Client.RemoteEndPoint.ToString();
            try
            {
                sslStream = new SslStream(client.GetStream(), false);
                sslStream.AuthenticateAsServer(serverCertificate, false, System.Security.Authentication.SslProtocols.Tls, false);

                Object obj = allClients[clientIP];
                NetClientWithCom netClient;
                netClient = new NetClientWithCom(this, clientPort);
                if (obj != null)
                {
                    allClients.Remove(clientIP);
                }
                allClients.Add(clientIP, netClient);
                clientsIO = new ClientsIO(netClient);

                transmissionReceiveMessage = new ReceiveMessage(sslStream, Global.GlobalParameters.tcpBufferSize);
                transmissionReceiveMessage.DisposePartMessageTimeOut = Global.GlobalParameters.disposePartMessageTimeOut;
                transmissionReceiveMessage.DataReceive += new ReceiveMessage.DataReceiveEventHandler(DataReceive);
                transmissionReceiveMessage.beginRead();
            }
            catch (Exception e)
            {
                Dispose();
                throw e;
            }
        }

        public void sendMessage(Stream stream)
        {
            sendMessage(stream, true);
        }

        public void sendMessage(Stream stream, bool ifFlush)
        {
            try
            {
                lock (sslStream)
                {
                    byte[] buffer = new byte[Global.GlobalParameters.tcpBufferSize];
                    int len;
                    while ((len = stream.Read(buffer, 0, buffer.Length)) > 0)
                        sslStream.Write(buffer, 0, len);
                    if (ifFlush)
                        sslStream.Flush();
                }
            }
            catch
            {
                Dispose();
            }
        }

        private void Dispose()
        {
            lock (allClients)
                allClients.Remove(clientIP);
            lock (sslStream)
                sslStream.Close();
            lock (client)
                client.Close();
        }

        private void DataReceive(MemoryStream returnStream)
        {
            try
            {
                clientsIO.Reply(returnStream);
            }
            catch (Exception e)
            {
                Dispose();
                Global.GlobalParameters.log(e);
            }
        }
    }

    public class NetClientWithNumber
    {
        public NetClient netClient { get; set; }
        public int number { get; set; }
        public NetClientWithNumber(NetClient netClient, int number)
        {
            this.netClient = netClient;
            this.number = number;
        }
    }


    public class NetClientWithCom
    {
        public NetClient netClient { get; set; }
        public int com { get; set; }
        public NetClientWithCom(NetClient netClient, int com)
        {
            this.netClient = netClient;
            this.com = com;
        }
    }
}
