﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using System.Security.Cryptography;

namespace ClientSystem
{
    public class Function
    {
        public static string remoteAddress = "127.0.0.1";
        public static int remotePort = 8000;
        public static int disposePartMessageTimeOut = 10000;
        public static int tcpBufferSize = 2097152;
        public static string certificateName = "ERPSystem";
        public static string certificateFile = "ERPSystem.cer";

        public static string company_name;

        public static String getMd5Hash(string input)
        {

            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static String OperatedStateStringBuilder(OperatedState operatedState)
        {
            StringBuilder sb = new StringBuilder();
            switch (operatedState.state)
            {
                case OperatedState.OperatedStateEnum.success:
                    sb.Append("恭喜您，操作已成功\n");
                    break;
                case OperatedState.OperatedStateEnum.notSuccess:
                    sb.Append("对不起，您的操作因为以下原因失败\n");
                    break;
            }
            IEnumerator<OperatedState.OperatedConfirmingItem> iterator = operatedState.GetEnumerator();
            while (iterator.MoveNext())
            {
                switch (iterator.Current.getConfirmingType())
                {
                    case OperatedState.OperatedConfirmingEnum.exception:
                        sb.Append("服务器处理时发生异常");
                        break;
                    case OperatedState.OperatedConfirmingEnum.authorizeNeed:
                        sb.Append("需要授权");
                        break;
                    case OperatedState.OperatedConfirmingEnum.noRights:
                        sb.Append("您没有权限");
                        break;
                    case OperatedState.OperatedConfirmingEnum.notEnoughRights:
                        sb.Append("您的权限不足");
                        break;
                    case OperatedState.OperatedConfirmingEnum.cookieWrong:
                        sb.Append("在您读取后已经被修改");
                        break;
                    case OperatedState.OperatedConfirmingEnum.stateWrong:
                        sb.Append("状态异常");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantNotFound:
                        sb.Append("未找到单据");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantReturnMoreThanSell:
                        sb.Append(iterator.Current.getConfimingData(0).ToString() + "退货数量比销售数量更多");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantNotEnoughDraft:
                        sb.Append(iterator.Current.getConfimingData(0).ToString() + "配件数量不足");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantLowerPriceAdjustThanPrice:
                        sb.Append(iterator.Current.getConfimingData(0).ToString() + "调整价格低于原价");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantLowerPriceAdjustThanPriceIn:
                        sb.Append(iterator.Current.getConfimingData(0).ToString() + "调整价格低于库值");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantTooMuchPriceChange:
                        sb.Append(iterator.Current.getConfimingData(0).ToString() + "本次进货价与原库值有较大差异");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantNotSubmittedDraft:
                        sb.Append(iterator.Current.getConfimingData(0).ToString() + "还未通过审核");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantPriceAdjusted:
                        sb.Append(iterator.Current.getConfimingData(0).ToString() + "价格调整");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantNotPaid:
                        sb.Append("该订单还未支付");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantSubmitToLate:
                        sb.Append("提交时间在销货72小时以后");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantTakeInAlreadyDeletedDraft:
                        sb.Append("尝试入库已经删除的配件");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantMultipleDraftInOneWarrant:
                        sb.Append("同一张订单中含有重复配件");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantReadException:
                        sb.Append("订单读取异常");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantPermitNotLegal:
                        sb.Append("折让后金额不合法");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantWarrantTooOldToHandleDiscount:
                        sb.Append("订单已经在30天前完成，无法再进行折让");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantNotSoMuchPayCheckToReturn:
                        sb.Append("付款金额没有超过应付款金额，不能退款");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantTypeNotMatched:
                        sb.Append("您选择了不匹配的单据类型");
                        break;
                    case OperatedState.OperatedConfirmingEnum.draftNotFound:
                        sb.Append("未找到该配件，可能该配件已经被删除，请刷新重试");
                        break;
                    case OperatedState.OperatedConfirmingEnum.draftAlreadySubmitted:
                        sb.Append("该配件已经处于审核通过状态");
                        break;
                    case OperatedState.OperatedConfirmingEnum.draftEditSubmitDraft:
                        sb.Append("修改已经审核的配件");
                        break;
                    case OperatedState.OperatedConfirmingEnum.draftEditSubmitDraftConsistentInfo:
                        sb.Append("修改已经审核的配件的固有信息（名称和规格）");
                        break;
                    case OperatedState.OperatedConfirmingEnum.draftEditSubmitDraftPriceChange:
                        sb.Append("修改已经审核的配件的价格");
                        break; 
                    case OperatedState.OperatedConfirmingEnum.draftNumNotZero:
                        sb.Append("配件余额不为零，不能删除");
                        break;
                    case OperatedState.OperatedConfirmingEnum.draftCannotLoadPreviousInformation:
                        sb.Append("错误信息丢失");
                        break;
                    case OperatedState.OperatedConfirmingEnum.customerNotFound:
                        sb.Append("未找到用户");
                        break;
                    case OperatedState.OperatedConfirmingEnum.customerNameExists:
                        sb.Append("姓名重复");
                        break;
                    case OperatedState.OperatedConfirmingEnum.loginInformationWrong:
                        sb.Append("验证信息有误");
                        break;
                    case OperatedState.OperatedConfirmingEnum.canOnlyLogin:
                        sb.Append("该验证方式只支持登录");
                        break;
                    case OperatedState.OperatedConfirmingEnum.canOnlyAuthorize:
                        sb.Append("该验证方式只支持授权");
                        break;
                    case OperatedState.OperatedConfirmingEnum.canNotLoginOrAuthorize:
                        sb.Append("该验证方式不支持登录或授权");
                        break;
                    case OperatedState.OperatedConfirmingEnum.rowCountNotRight:
                        sb.Append("数据行变动数量不正确");
                        break;
                    case OperatedState.OperatedConfirmingEnum.warrantTooMuchDiscount:
                        sb.Append("折让金额超过权限");
                        break;
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }

        public static void log(Exception e)
        {
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.AppendLine(e.Message);
            if (e.InnerException != null)
                bodyBuilder.AppendLine(e.InnerException.Message);
            bodyBuilder.AppendLine(e.StackTrace);
            sendMail(new String[] { "netaccount@geniusye.com" }, "您的ERPSystem程序，在" + DateTime.Now + "时发生错误", bodyBuilder.ToString());
        }

        public static bool sendMail(String[] to, String subject, String body, bool isBodyHtml = false)
        {
            try
            {
                SMTPAssistant.MailSend mail = new SMTPAssistant.MailSend();
                foreach (String str in to)
                    mail.addToAddress(new System.Net.Mail.MailAddress(str));
                mail.subject = subject;
                mail.enableSsl = false;
                mail.hostName = "smtp.ym.163.com";
                mail.hostPort = 25;
                mail.username = "datasender@youlinjixie.com";
                mail.password = "5cteQHGC3t";
                mail.isBodyHtml = isBodyHtml;
                mail.fromAddress = new System.Net.Mail.MailAddress("datasender@youlinjixie.com", "发送者", Encoding.UTF8);
                mail.body = body;
                mail.sendMail(true);
            }
            catch
            {
                return false;
            }
            return true;   
        }

        

    }
}
