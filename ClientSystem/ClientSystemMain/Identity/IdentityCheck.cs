﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Identity.Stuff;

namespace ClientSystem.Identity 
{
    public class IdentityCheck
    {
        public static IdentityMark longTermIdentity;

        public List<SharedObject.Identity.Stuff.IdentityMark> getIdentities()
        {
            List<SharedObject.Identity.Stuff.IdentityMark> identities = new List<SharedObject.Identity.Stuff.IdentityMark>();
            identities.Add(longTermIdentity);
            return identities;
        }
    }
}
