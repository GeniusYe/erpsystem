﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ClientSystem.Identity
{
    public partial class CardReadForm : Form
    {
        public DateTime cardReadStartTime { get; private set; }

        public String cardInfoRead { get; private set; }

        public CardReadForm()
        {
            InitializeComponent();

            this.cardInfoRead = String.Empty;

            this.KeyDown += new KeyEventHandler(IdentityCheckForm_KeyDown);
        }

        private void IdentityCheckForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DateTime now = DateTime.Now;
                if (now < cardReadStartTime.AddMilliseconds(200))
                {
                    this.Close();
                }
                else
                {
                    MessageBox.Show("读卡失败");
                }
                cardInfoRead = String.Empty;
            }
            else if (e.KeyCode == (Keys.LButton | Keys.ShiftKey))
            {
                MessageBox.Show("读卡失败");
                cardInfoRead = String.Empty;
            }
            else
            {
                if (cardInfoRead == String.Empty)
                    cardReadStartTime = DateTime.Now;
                cardInfoRead += e.KeyValue - 48;
            }
        }
    }
}
