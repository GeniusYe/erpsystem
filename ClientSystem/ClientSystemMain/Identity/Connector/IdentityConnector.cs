﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SharedObject.Main.Transmission;
using SharedObject.Identity.Stuff;
using SharedObject.Identity.Transmission;
using TransmissionReceiveMessage;

namespace ClientSystem.Identity.Connector
{
    public class IdentityConnector
    {
        public class ConnectorMainInstructionNotMatchException : Exception
        {
            public ConnectorMainInstructionNotMatchException(string str)
                : base(str)
            { }
        }

        public delegate void MainFormShowEventHandler(SharedObject.Identity.Stuff.IdentityMark identity);
        public delegate void DOperatedState(OperatedState instrState);
        public event DOperatedState EOperatedState = null;
        public event MainFormShowEventHandler mainFormShow = null;

        private IdentityCheckForm identityCheckForm;

        System.Windows.Forms.Timer timer;

        private Transmission.Transmission mainTransmission;
        private List<IdentityConnector> identityConnectors;

        private SharedObject.Normal.TransmissionCookie cookie;
        private Instruction mainInstruction;

        public IdentityConnector(Instruction mainInstruction, Transmission.Transmission identityTransmission, List<IdentityConnector> identityConnectors)
        {
            this.mainInstruction = mainInstruction;
            this.mainTransmission = identityTransmission;
            this.identityConnectors = identityConnectors;
            identityConnectors.Add(this);
            cookie = new SharedObject.Normal.TransmissionCookie();
        }

        public void dispose()
        {
            this.identityConnectors.Remove(this);
            if (this.timer != null)
                this.timer.Dispose();
        }

        public bool DataReceive(byte[] instrData, MemoryStream receiveStream, Object obj)
        {
            int cookie = ReceiveMessage.getCookie(instrData);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            if (instrData.Length >= 16 && instrData[0] == 1)
            {
                if (instrData[1] == (byte)SharedObject.Normal.SystemID.Identity)
                {
                    Instruction mainInstruction = (Instruction)instrData[2];
                    TSCIdentity tSCIdentity = null;
                    if (obj is TSCIdentity)
                        tSCIdentity = (TSCIdentity)obj;
                    if (timer != null)
                    {
                        timer.Dispose();
                    }
                    AWithOperatedState OperatedStateObj = (AWithOperatedState)obj;
                    OperatedState operatedState = OperatedStateObj.operatedState;
                    if (instrData[3] == (int)SecondaryInstruction.sendResponse)
                        if (EOperatedState != null)
                            EOperatedState(operatedState);
                    if (operatedState.state != OperatedState.OperatedStateEnum.normal && operatedState.state != OperatedState.OperatedStateEnum.success)
                    {
                        if (identityCheckForm != null)
                            identityCheckForm.reshow();
                        this.dispose();
                        return true;
                    }
                    switch (mainInstruction)
                    {
                        case Instruction.askForNormalLogIn:
                            string server_version = tSCIdentity.version;
                            if (server_version != SharedObject.Normal.Version.version)
                            {
                                System.Windows.Forms.MessageBox.Show("需要更新");
                                System.Windows.Forms.Application.Exit();
                            }
                            IdentityCheck.longTermIdentity = tSCIdentity.identity;
                            if (mainFormShow != null)
                            {
                                mainFormShow(tSCIdentity.identity);
                                this.dispose();
                            }
                            break;
                        case Instruction.askForSpecialLogIn:
                            if (instrData[3] == (int)SecondaryInstruction.sendResponse)
                            {
                            }
                            break;
                    }
                    return true;
                }
            }
            return false;
        }

        public void getAuthorize(string cardInformation, int cookieToBeRedo, SharedObject.Normal.SystemID systemId, SharedObject.Main.Transmission.Instruction instructionToBeRedo, int cookie, IdentityCheckForm form)
        {
            this.identityCheckForm = form;
            sendRequest(Instruction.askForSpecialLogIn, cardInformation, cookieToBeRedo, systemId, instructionToBeRedo, cookie, new byte[] { });
        }

        public void getAuthorize(string userName, string password, int cookieToBeRedo, SharedObject.Normal.SystemID systemId, SharedObject.Main.Transmission.Instruction instructionToBeRedo, int cookie, IdentityCheckForm form)
        {
            this.identityCheckForm = form;
            sendRequest(Instruction.askForSpecialLogIn, userName, password, cookieToBeRedo, systemId, instructionToBeRedo, cookie, new byte[] { });
        }

        public void getLogIn(string cardInformation, int cookie, IdentityCheckForm form)
        {
            this.identityCheckForm = form;
            startTimer();
            sendRequest(Instruction.askForNormalLogIn, cardInformation, 0, 0, 0, cookie, new byte[] { });
        }

        public void getLogIn(string userName, string password, int cookie, IdentityCheckForm form)
        {
            this.identityCheckForm = form;
            startTimer();
            sendRequest(Instruction.askForNormalLogIn, userName, password, 0, 0, 0, cookie, new byte[] { });
        }

        private void startTimer()
        {
            if (timer != null)
                timer.Dispose();
            timer = new System.Windows.Forms.Timer();
            timer.Interval = 5000;
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        private void sendRequest(Instruction instruction, string cardInformation, int cookieToBeRedo, SharedObject.Normal.SystemID systemId, SharedObject.Main.Transmission.Instruction instructionParameter, int cookie, byte[] para)
        {
            TCSIdentity tCSIdentity = new TCSIdentity();
            tCSIdentity.authorizeType = SharedObject.Identity.Transmission.TCSIdentity.AuthorizeType.card;
            tCSIdentity.cardInformation = cardInformation;
            tCSIdentity.cookieToBeRedo = cookieToBeRedo;
            tCSIdentity.systemNumber = systemId;
            tCSIdentity.instructionNumber = (byte)instructionParameter;
            if (mainTransmission != null)
                mainTransmission.sendRequestObj(SharedObject.Normal.SystemID.Identity, instruction, tCSIdentity, cookie, para);
        }

        private void sendRequest(Instruction instruction, string userName, string password, int cookieToBeRedo, SharedObject.Normal.SystemID systemId, SharedObject.Main.Transmission.Instruction instructionParameter, int cookie, byte[] para)
        {
            TCSIdentity tCSIdentity = new TCSIdentity();
            tCSIdentity.authorizeType = TCSIdentity.AuthorizeType.password;
            tCSIdentity.userName = userName;
            tCSIdentity.password = password;
            tCSIdentity.cookieToBeRedo = cookieToBeRedo;
            tCSIdentity.systemNumber = systemId;
            tCSIdentity.instructionNumber = (byte)instructionParameter;
            if (mainTransmission != null)
                mainTransmission.sendRequestObj(SharedObject.Normal.SystemID.Identity, instruction, tCSIdentity, cookie, para);
        }

        void timer_Tick(object sender, EventArgs e)
        {
            this.dispose();
            ((System.Windows.Forms.Timer)sender).Dispose();
            System.Windows.Forms.MessageBox.Show("连接超时");
        }

    }
}
