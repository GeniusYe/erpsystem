﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.Identity;
using SharedObject.Main.Transmission;

namespace ClientSystem.Identity
{
    public partial class IdentityCheckForm : Form
    {
        private delegate void Show_Invoke();

        private List<Connector.IdentityConnector> identityConnectors = new List<Connector.IdentityConnector>();
        private SharedObject.Normal.TransmissionCookie cookie;
        private int cookieToBeRedo;
        private SharedObject.Main.Transmission.Instruction instructionId;
        private SharedObject.Normal.SystemID systemId;

        private delegate void DOperatedState(OperatedState instrState);
        private delegate void DShow();
        private delegate void DClose();
        private Connector.IdentityConnector.DOperatedState EOperateState;
        public Connector.IdentityConnector.MainFormShowEventHandler EMainFormShow { get; set; }

        private Transmission.Transmission mainTransmission;

        //This is for display the operation we operated Not the operation that we login
        private SharedObject.Main.Transmission.OperatedState _operatedState;
        public SharedObject.Main.Transmission.OperatedState operatedState
        {
            set
            {
                this._operatedState = value;
                warningListBox.Visible = true;
                WarningLabel.Visible = true;
                displayWarning(_operatedState);
                this.Height = 350;
            }
        }

        private CardReadForm cardReadForm; 

        private static char[] passwordChar = new char[] { '!', '@', '#', '$', '%', '^', '&', '*', '_', '-', '+', '=', ',', '.'};

        public IdentityCheckForm(int cookieToBeRedo, SharedObject.Normal.SystemID systemId, SharedObject.Main.Transmission.Instruction instructionId, Transmission.Transmission mainTransmission)
        {
            this.mainTransmission = mainTransmission;
            if (mainTransmission != null)
                mainTransmission.addIdentityConnectors(identityConnectors);
            else
                throw new Transmission.TransmissionIsNullException();
            this.cookieToBeRedo = cookieToBeRedo;
            this.systemId = systemId;
            this.instructionId = instructionId;
            InitializeComponent();
            cookie = new SharedObject.Normal.TransmissionCookie();
            this.Height = 191;
            WarningLabel.Text = "您刚才的操作因为以下原因失败\n如需要继续，请进行授权";
            warningListBox.Visible = false;
            WarningLabel.Visible = false;
            EOperateState = new Connector.IdentityConnector.DOperatedState(operatedState => this.Invoke(new DOperatedState(responseForOperatedState), new Object[] { operatedState }));
            this.FormClosing += new FormClosingEventHandler(IdentityCheckForm_FormClosing);

            userNameTextBox.KeyUp += new KeyEventHandler(password_KeyUp);
            passwordTextBox.KeyUp += new KeyEventHandler(password_KeyUp);
            this.Focus();
        }

        private void password_KeyUp(object sender, KeyEventArgs e)
        {
            if (sender == userNameTextBox)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (userNameTextBox.Text.Length > 0)
                        passwordTextBox.Focus();
                }
                else if (e.KeyValue >= 48 && e.KeyValue <= 57)
                {
                    //userNameTextBox.Text = "";
                }
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    passwordLoginButton_Click(this, e);
                }
            }
        }

        public void IdentityCheckForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            while (identityConnectors.Count > 0)
                identityConnectors[0].dispose();
            this.mainTransmission.removeIdentityConnectors(identityConnectors);
        }

        private void responseForOperatedState(OperatedState operatedState)
        {
            String str = Function.OperatedStateStringBuilder(operatedState);
            if (str.Length > 0)
                MessageBox.Show(str);
            //setAccessiable(null);
        }

        private void passwordLoginButton_Click(object sender, EventArgs e)
        {
            string userName, password;
            userName = userNameTextBox.Text;
            if (!(userName != null && userName != "" && userName.Length > 3))
            {
                MessageBox.Show("用户名长度过短，请输入三位以上的用户名");
                return;
            }
            string tempPassword = passwordTextBox.Text;
            if (!(tempPassword != null && tempPassword != ""))
            {
                MessageBox.Show("密码不得为空");
                return;
            }
            password = Function.getMd5Hash(tempPassword);
            if (cookieToBeRedo != 0)
            {
                Connector.IdentityConnector connector = new Connector.IdentityConnector(Instruction.NotSet, Transmission.Transmission.mainTransmission, identityConnectors);
                connector.EOperatedState += EOperateState;
                connector.getAuthorize(userName, password, this.cookieToBeRedo, this.systemId, this.instructionId, cookie.generateNewCookie(), this);
            }
            else
            {
                Connector.IdentityConnector connector = new Connector.IdentityConnector(Instruction.NotSet, Transmission.Transmission.mainTransmission, identityConnectors);
                connector.EOperatedState += EOperateState;
                if (EMainFormShow != null)
                    connector.mainFormShow += EMainFormShow;
                connector.getLogIn(userName, password, cookie.generateNewCookie(), this);
            }
            this.Hide();
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            passwordTextBox.PasswordChar = passwordChar[(new Random()).Next(passwordChar.Length)];
        }

        public void reshow()
        {
            this.Invoke(new DShow(() => this.Show()));
        }

        public void close()
        {
            this.Invoke(new DClose(() => this.Close()));
        }

        public void displayWarning(OperatedState operatedState)
        {
            if (operatedState.state != OperatedState.OperatedStateEnum.success && operatedState.state != OperatedState.OperatedStateEnum.normal)
            {
                foreach (OperatedState.OperatedConfirmingItem i in operatedState)
                {
                    switch (i.getConfirmingType())
                    {
                        case OperatedState.OperatedConfirmingEnum.exception:
                            warningListBox.Items.Add("服务器处理时发生异常");
                            break;
                        case OperatedState.OperatedConfirmingEnum.authorizeNeed:
                            warningListBox.Items.Add("需要授权");
                            break;
                        case OperatedState.OperatedConfirmingEnum.noRights:
                            warningListBox.Items.Add("您没有权限");
                            break;
                        case OperatedState.OperatedConfirmingEnum.notEnoughRights:
                            warningListBox.Items.Add("您的权限不足");
                            break;
                        case OperatedState.OperatedConfirmingEnum.cookieWrong:
                            warningListBox.Items.Add("在您读取后已经被修改");
                            break;
                        case OperatedState.OperatedConfirmingEnum.stateWrong:
                            warningListBox.Items.Add("状态异常");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantNotFound:
                            warningListBox.Items.Add("未找到单据");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantReturnMoreThanSell:
                            warningListBox.Items.Add(i.getConfimingData(0).ToString() + "退货数量比销售数量更多");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantNotEnoughDraft:
                            warningListBox.Items.Add(i.getConfimingData(0).ToString() + "配件数量不足");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantLowerPriceAdjustThanPrice:
                            warningListBox.Items.Add(i.getConfimingData(0).ToString() + "调整价格低于原价");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantLowerPriceAdjustThanPriceIn:
                            warningListBox.Items.Add(i.getConfimingData(0).ToString() + "调整价格低于库值");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantTooMuchPriceChange:
                            warningListBox.Items.Add(i.getConfimingData(0).ToString() + "本次进货价与原库值有较大差异");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantNotSubmittedDraft:
                            warningListBox.Items.Add(i.getConfimingData(0).ToString() + "还未通过审核");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantPriceAdjusted:
                            warningListBox.Items.Add(i.getConfimingData(0).ToString() + "价格调整");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantNotPaid:
                            warningListBox.Items.Add("该订单还未支付");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantSubmitToLate:
                            warningListBox.Items.Add("提交时间在销货72小时以后");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantTakeInAlreadyDeletedDraft:
                            warningListBox.Items.Add("尝试入库已经删除的配件");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantMultipleDraftInOneWarrant:
                            warningListBox.Items.Add("同一张订单中含有重复配件");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantReadException:
                            warningListBox.Items.Add("订单读取异常");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantPermitNotLegal:
                            warningListBox.Items.Add("折让后金额不合法");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantWarrantTooOldToHandleDiscount:
                            warningListBox.Items.Add("订单已经在30天前完成，无法再进行折让");
                            break;
                        case OperatedState.OperatedConfirmingEnum.warrantNotSoMuchPayCheckToReturn:
                            warningListBox.Items.Add("付款金额没有超过应付款金额，不能退款");
                            break;
                        case OperatedState.OperatedConfirmingEnum.draftNotFound:
                            warningListBox.Items.Add("未找到该配件，可能该配件已经被删除，请刷新重试");
                            break;
                        case OperatedState.OperatedConfirmingEnum.draftAlreadySubmitted:
                            warningListBox.Items.Add("该配件已经处于审核通过状态");
                            break;
                        case OperatedState.OperatedConfirmingEnum.draftEditSubmitDraft:
                            warningListBox.Items.Add("修改已经审核的配件");
                            break;
                        case OperatedState.OperatedConfirmingEnum.draftEditSubmitDraftConsistentInfo:
                            warningListBox.Items.Add("修改已经审核的配件的固有信息（名称和规格）");
                            break;
                        case OperatedState.OperatedConfirmingEnum.draftNumNotZero:
                            warningListBox.Items.Add("配件余额不为零，不能删除");
                            break;
                        case OperatedState.OperatedConfirmingEnum.draftCannotLoadPreviousInformation:
                            warningListBox.Items.Add("错误信息丢失");
                            break;
                        case OperatedState.OperatedConfirmingEnum.customerNotFound:
                            warningListBox.Items.Add("未找到用户");
                            break;
                        case OperatedState.OperatedConfirmingEnum.customerNameExists:
                            warningListBox.Items.Add("姓名重复");
                            break;
                        case OperatedState.OperatedConfirmingEnum.loginInformationWrong:
                            warningListBox.Items.Add("验证信息有误");
                            break;
                        case OperatedState.OperatedConfirmingEnum.canOnlyLogin:
                            warningListBox.Items.Add("该验证方式只支持登录");
                            break;
                        case OperatedState.OperatedConfirmingEnum.canOnlyAuthorize:
                            warningListBox.Items.Add("该验证方式只支持授权");
                            break;
                        case OperatedState.OperatedConfirmingEnum.canNotLoginOrAuthorize:
                            warningListBox.Items.Add("该验证方式不支持登录或授权");
                            break;
                        case OperatedState.OperatedConfirmingEnum.rowCountNotRight:
                            warningListBox.Items.Add("数据行变动数量不正确");
                            break;
                    }
                }
            }
        }

        private void readCardButton_Click(object sender, EventArgs e)
        {
            if (cardReadForm != null)
                cardReadForm.Dispose();
            cardReadForm = new CardReadForm();
            cardReadForm.Show();
            cardReadForm.FormClosed += new FormClosedEventHandler(cardReadForm_FormClosed);
        }

        private void cardReadForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            string cardInformation = String.Empty;
            if (this.cardReadForm.cardInfoRead != String.Empty)
                cardInformation = Function.getMd5Hash(this.cardReadForm.cardInfoRead.ToString());
            else
                return;
            
            if (cookieToBeRedo != 0)
            {
                Connector.IdentityConnector connector = new Connector.IdentityConnector(Instruction.NotSet, Transmission.Transmission.mainTransmission, identityConnectors);
                connector.EOperatedState += EOperateState;
                connector.getAuthorize(cardInformation, this.cookieToBeRedo, this.systemId, this.instructionId, cookie.generateNewCookie(), this);
            }
            else
            {
                Connector.IdentityConnector connector = new Connector.IdentityConnector(Instruction.NotSet, Transmission.Transmission.mainTransmission, identityConnectors);
                connector.EOperatedState += EOperateState;
                if (EMainFormShow != null)
                    connector.mainFormShow += EMainFormShow;
                connector.getLogIn(cardInformation, cookie.generateNewCookie(), this);
            }
            this.Hide();
        }
    }
}

/*private void cardLoginButton_Click(object sender, EventArgs e)
{
d8Control = new D8.D8Control();
d8Control.findCard();
StringBuilder sb = d8Control.readCard();
if (sb.ToString() == "")
{
MessageBox.Show("读卡失败");
return;
}
string cardInformation = getMd5Hash(sb.ToString());
if (cookieToBeRedo != 0)
{
Connector.IdentityConnector connector = new Connector.IdentityConnector(Instruction.NotSet, Transmission.Transmission.mainTransmission, identityConnectors);
connector.EOperatedState += EOperateState;
connector.getAuthorize(cardInformation, this.cookieToBeRedo, this.systemId, this.instructionId, cookie.generateNewCookie(), this);
}
else
{
Connector.IdentityConnector connector = new Connector.IdentityConnector(Instruction.NotSet, Transmission.Transmission.mainTransmission, identityConnectors);
connector.EOperatedState += EOperateState;
if (EMainFormShow != null)
    connector.mainFormShow += EMainFormShow;
connector.getLogIn(cardInformation, cookie.generateNewCookie(), this);
}
d8Control.close();
this.Hide();
}*/

/*   private void button2_Click(object sender, EventArgs e)
   {
       d8Control.findCard();
       //int i = d8Control.writeCard("897946132879658");
       StringBuilder sb = d8Control.readCard();
       //textBox1.Text = sb.ToString();
       //198794679843164
       //894641654798416
       //679841613137896
       //897946132879658

   }*/
