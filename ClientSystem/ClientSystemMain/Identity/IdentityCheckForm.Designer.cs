﻿namespace ClientSystem.Identity
{
    partial class IdentityCheckForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userNameTextBox = new System.Windows.Forms.TextBox();
            this.passwordLoginButton = new System.Windows.Forms.Button();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.WarningLabel = new System.Windows.Forms.Label();
            this.warningListBox = new System.Windows.Forms.ListBox();
            this.readCardButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // userNameTextBox
            // 
            this.userNameTextBox.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.userNameTextBox.Location = new System.Drawing.Point(11, 12);
            this.userNameTextBox.Name = "userNameTextBox";
            this.userNameTextBox.Size = new System.Drawing.Size(178, 26);
            this.userNameTextBox.TabIndex = 1;
            // 
            // passwordLoginButton
            // 
            this.passwordLoginButton.Location = new System.Drawing.Point(199, 12);
            this.passwordLoginButton.Name = "passwordLoginButton";
            this.passwordLoginButton.Size = new System.Drawing.Size(112, 63);
            this.passwordLoginButton.TabIndex = 3;
            this.passwordLoginButton.Text = "登录";
            this.passwordLoginButton.UseVisualStyleBackColor = true;
            this.passwordLoginButton.Click += new System.EventHandler(this.passwordLoginButton_Click);
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.passwordTextBox.Location = new System.Drawing.Point(11, 47);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(178, 26);
            this.passwordTextBox.TabIndex = 2;
            this.passwordTextBox.TextChanged += new System.EventHandler(this.passwordTextBox_TextChanged);
            // 
            // WarningLabel
            // 
            this.WarningLabel.AutoSize = true;
            this.WarningLabel.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.WarningLabel.Location = new System.Drawing.Point(10, 150);
            this.WarningLabel.Name = "WarningLabel";
            this.WarningLabel.Size = new System.Drawing.Size(0, 14);
            this.WarningLabel.TabIndex = 4;
            // 
            // warningListBox
            // 
            this.warningListBox.FormattingEnabled = true;
            this.warningListBox.Location = new System.Drawing.Point(13, 205);
            this.warningListBox.MultiColumn = true;
            this.warningListBox.Name = "warningListBox";
            this.warningListBox.Size = new System.Drawing.Size(299, 95);
            this.warningListBox.TabIndex = 5;
            // 
            // readCardButton
            // 
            this.readCardButton.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.readCardButton.Location = new System.Drawing.Point(13, 81);
            this.readCardButton.Name = "readCardButton";
            this.readCardButton.Size = new System.Drawing.Size(299, 64);
            this.readCardButton.TabIndex = 0;
            this.readCardButton.Text = "点此刷卡";
            this.readCardButton.UseVisualStyleBackColor = true;
            this.readCardButton.Click += new System.EventHandler(this.readCardButton_Click);
            // 
            // IdentityCheckForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 314);
            this.Controls.Add(this.readCardButton);
            this.Controls.Add(this.warningListBox);
            this.Controls.Add(this.WarningLabel);
            this.Controls.Add(this.passwordLoginButton);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.userNameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IdentityCheckForm";
            this.Text = "授权";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox userNameTextBox;
        private System.Windows.Forms.Button passwordLoginButton;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label WarningLabel;
        private System.Windows.Forms.ListBox warningListBox;
        private System.Windows.Forms.Button readCardButton;
    }
}