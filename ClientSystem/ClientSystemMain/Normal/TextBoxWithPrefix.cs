﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ClientSystem.Normal
{
    /// <summary>
    /// automantically add some characters before the number
    /// </summary>
    public class TextBoxWithPrefix : TextBox
    {
        private string front;
        private string _text;

        public new string Text
        {
            set
            {
                _text = value;
                if (value != null)
                {
                    base.Text = front + "" + _text;
                }
                else
                {
                    base.Text = "";
                }
            }
            get
            {
                return _text;
            }
        }

        public TextBoxWithPrefix(string front)
            : base()
        {
            this.front = front;
            this.ReadOnly = true;
            this.Text = "";
        }
    }
}
