﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SharedObject.Main.Transmission;
using TransmissionReceiveMessage;

namespace ClientSystem.Transmission
{

    public abstract class Connector
    {
        protected readonly SharedObject.Normal.SystemID systemID;

        public class ConnectorMainInstructionNotMatchException : Exception
        {
            public ConnectorMainInstructionNotMatchException()
                : this("") { }
            public ConnectorMainInstructionNotMatchException(string str)
                : base(str) { }
        }

        //delegate for receiving somekind of object
        public delegate void DOperatedState(OperatedState instrState);
        public delegate void DIdentityCheck(Identity.IdentityCheckForm identityCheckForm);
        public event DOperatedState EOperatedState = null;
        public event DIdentityCheck EIdentityCheck = null;

        private Transmission mainTransmission;

        private SharedObject.Normal.TransmissionCookie cookie;
        protected Instruction mainInstruction;
        protected int id;

        private List<Connector> connectors;

        public Connector(Instruction mainInstruction, Transmission mainTransmission, SharedObject.Normal.SystemID systemId, List<Connector> connectors)
        {
            systemID = systemId;
            this.mainInstruction = mainInstruction;
            this.mainTransmission = mainTransmission;
            cookie = new SharedObject.Normal.TransmissionCookie();
            this.connectors = connectors;
            this.connectors.Add(this);
        }

        public void Dispose()
        {
            this.connectors.Remove(this);
        }

        public bool DataReceive(byte[] instrData, Object obj)
        {
            int cookie;
            if (instrData.Length >= 16 && instrData[0] == 1)
            {
                cookie = ReceiveMessage.getCookie(instrData);
                if (this.cookie.contains(cookie) || cookie == 0 || this.cookie.getCookie() == 0)
                {
                    if (instrData[1] == (byte)this.systemID)
                    {
                        Instruction mainInstruction = (Instruction)instrData[2];
                        if (mainInstruction != this.mainInstruction)
                            return false;
                        AWithOperatedState OperatedStateObj = (AWithOperatedState)obj;
                        OperatedState operatedState = OperatedStateObj.operatedState;
                        if (instrData[3] == (int)SecondaryInstruction.sendResponse)
                            if (EOperatedState != null)
                                EOperatedState(operatedState);

                        if (operatedState.state == OperatedState.OperatedStateEnum.success || operatedState.state == OperatedState.OperatedStateEnum.normal)
                        {
                            if (instrData[3] == (int)SecondaryInstruction.sendResponse)
                            {
                                if (ResponseMethod(obj, mainInstruction, operatedState) && cookie != 0 && this.cookie.getCookie() != 0)
                                {
                                    this.Dispose();
                                    return true;
                                }
                                else
                                    return false;
                            }
                        }
                        else
                        {
                            int i = 0;
                            IEnumerator<OperatedState.OperatedConfirmingItem> iterator = operatedState.GetEnumerator();
                            while (iterator.MoveNext())
                            {
                                if (!iterator.Current.ifFatal())
                                {
                                    i++;
                                }
                            }
                            bool t = false;
                            if (!(i == operatedState.Count && operatedState.Count > 0 && operatedState.state == OperatedState.OperatedStateEnum.notSuccess))
                                t = true;
                            if (cookie == 0 || this.cookie.getCookie() == 0)
                                t = false;
                            if (t)
                            {
                                this.Dispose();
                                return true;
                            }
                            else
                            {
                                Identity.IdentityCheckForm identityCheckForm = new Identity.IdentityCheckForm(cookie, SharedObject.Normal.SystemID.WareHouseManage, mainInstruction, mainTransmission);
                                identityCheckForm.operatedState = operatedState;
                                if (EIdentityCheck != null)
                                    EIdentityCheck(identityCheckForm);
                                return false;
                            }
                        }
                    }                                           //instr[0] == 4
                }                                               //cookie == cookie
            }                                                   //length > 16
            return false;
        }

        protected abstract bool ResponseMethod(Object obj, Instruction mainInstruction, OperatedState operatedState);

        protected void sendRequestObj(Object obj, int id)
        {
            sendRequestObj(obj, ReceiveMessage.convertIntToBytes(id));
        }

        protected void sendRequestObj(Object obj, byte[] parameter)
        {
            mainTransmission.sendRequestObj(this.systemID, this.mainInstruction, obj, cookie.generateNewCookie(), parameter);
        }

        public void receiveAll()
        {
            this.cookie.setCookie(0);
        }
    }
}
