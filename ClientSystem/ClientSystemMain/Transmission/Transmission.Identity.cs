﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SharedObject.Identity.Stuff;
using SharedObject.Identity.Transmission;

namespace ClientSystem.Transmission
{
    partial class Transmission
    {
        private List<List<Identity.Connector.IdentityConnector>> identityConnectors = new List<List<Identity.Connector.IdentityConnector>>();

        public void addIdentityConnectors(List<Identity.Connector.IdentityConnector> identityConnectors)
        {
            this.identityConnectors.Add(identityConnectors);
        }

        public void removeIdentityConnectors(List<Identity.Connector.IdentityConnector> identityConnectors)
        {
            this.identityConnectors.Remove(identityConnectors);
        }
    }
}
