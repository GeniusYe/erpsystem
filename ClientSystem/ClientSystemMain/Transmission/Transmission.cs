﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SharedObject.Main.Transmission;
using SharedObject.Normal;
using TransmissionReceiveMessage;

namespace ClientSystem.Transmission
{
    public partial class Transmission
    {
        private static Transmission _mainTransmission = null;

        private TcpClient tcpClient = new TcpClient();
        private SslStream sslStream;
        public bool connected { get; private set; }
        private ReceiveMessage transmissionReceiveMessage;

        private Hashtable hashTable = new Hashtable();

        public static Transmission mainTransmission
        {
            get
            {
                if (_mainTransmission == null)
                    _mainTransmission = new Transmission();
                return _mainTransmission;
            }
        }

        private Transmission()
        {

            try
            {
                tcpClient = new TcpClient(Function.remoteAddress, Function.remotePort);
                sslStream = new SslStream(tcpClient.GetStream(), false, new RemoteCertificateValidationCallback(ValidateServerCertificate), null);
                transmissionReceiveMessage = new ReceiveMessage(sslStream, Function.tcpBufferSize);
                transmissionReceiveMessage.DisposePartMessageTimeOut = Function.disposePartMessageTimeOut;
                transmissionReceiveMessage.DataReceive += new ReceiveMessage.DataReceiveEventHandler(DataReceive);

                X509CertificateCollection certs = new X509CertificateCollection();
                X509Certificate cert = X509Certificate.CreateFromCertFile(Function.certificateFile);
                certs.Add(cert);

                sslStream.AuthenticateAsClient(Function.certificateName, certs, System.Security.Authentication.SslProtocols.Tls, false);

                transmissionReceiveMessage.beginRead();

                connected = true;
            }
            catch (Exception e)
            {
                connected = false;
                Disconnection();
                Function.log(e);
            }
            this.hashTable.Add(SystemID.WareHouseManage, new List<List<Connector>>());
            this.hashTable.Add(SystemID.WareHouseInOut, new List<List<Connector>>());
            this.hashTable.Add(SystemID.CustomerManage, new List<List<Connector>>());
            this.hashTable.Add(SystemID.RightsControl, new List<List<Connector>>());
        }

        /// <summary> 
        /// Disconnection is used to mannual shut down the connection to server
        /// </summary>
        private void Disconnection()
        {
            if (connected)
            {
                lock (sslStream)
                    sslStream.Close();
            }
        }

        private static bool ValidateServerCertificate(
                  object sender,
                  X509Certificate certificate,
                  X509Chain chain,
                  SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None || sslPolicyErrors == SslPolicyErrors.RemoteCertificateChainErrors)
                return true;

            Console.WriteLine("Certificate error: {0}", sslPolicyErrors);

            return false;
        }

        public static void ResetMainTransmission()
        {
            if (_mainTransmission != null)
                _mainTransmission.Disconnection();
            _mainTransmission = null;
        }

        private void DataReceive(MemoryStream receiveStream)
        {
            try
            {
                byte[] instrData = new byte[16];
                receiveStream.Read(instrData, 0, 16);
                if (instrData[0] == 1)
                {
                    SystemID systemID = (SystemID)instrData[1];
                    bool t = false;
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    Object obj = null;
                    try
                    {
                        obj = binaryFormatter.Deserialize(receiveStream);
                    }
                    catch { }
                    if (systemID == SystemID.Identity)
                    {
                        foreach (List<Identity.Connector.IdentityConnector> identityConnectors in this.identityConnectors)
                        {
                            foreach (Identity.Connector.IdentityConnector identityConnector in identityConnectors)
                            {
                                if (t = identityConnector.DataReceive(instrData, receiveStream, obj)) break;
                            }
                            if (t) break;
                        }
                    }
                    else
                    {
                        List<List<Connector>> connectors = (List<List<Connector>>)this.hashTable[systemID];
                        List<Connector> inLoopConnectors;
                        if (connectors != null)
                        {
                            int i = 0;
                            while (i < connectors.Count)
                            {
                                inLoopConnectors = (List<Connector>)connectors[i];
                                if (inLoopConnectors != null)
                                {
                                    int j = 0;
                                    while (j < inLoopConnectors.Count)
                                    {
                                        if (!inLoopConnectors[j].DataReceive(instrData, obj))
                                            j++;
                                    }
                                }
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Function.log(e);
            }
        }

        public void addConnectors(SystemID systemId, List<Connector> inLoopConnectors)
        {
            List<List<Connector>> connectors = (List<List<Connector>>)this.hashTable[systemId];
            if (connectors != null)
                connectors.Add(inLoopConnectors);
        }

        public void removeConnectors(SystemID systemId, List<Connector> inLoopConnectors)
        {
            List<List<Connector>> connectors = (List<List<Connector>>)this.hashTable[systemId];
            if (connectors != null)
                connectors.Remove(inLoopConnectors);
        }

        public void sendRequestObj(SystemID systemID, Instruction instruction, Object obj, int cookie, byte[] para)
        {
            if (connected == false)
                return;
            MemoryStream sendStream = new MemoryStream();
            if (obj != null)
            {
                BinaryFormatter binarySerializer = new BinaryFormatter();
                sendStream.Position = 20;
                binarySerializer.Serialize(sendStream, obj);
            }
            setSendStream(systemID, instruction, cookie, para, sendStream);
            lock (sslStream)
            {
                sslStream.Write(sendStream.ToArray(), 0, (int)sendStream.Length);
                sslStream.Flush();
            }
        }

        public void sendBytes(byte[] data)
        {
            lock (sslStream)
            {
                sslStream.Write(data, 0, (int)data.Length);
                sslStream.Flush();
            }
        }

        public void setSendStream(SystemID systemID, Instruction instruction, int cookie, byte[] para, MemoryStream sendStream)
        {
            sendStream.Position = 0;
            int k = (int)sendStream.Length - 4;
            if (k < 16) k = 16;
            sendStream.Write(ReceiveMessage.convertIntToBytes(k), 0, 4);
            sendStream.Write(SharedObject.Main.Transmission.TransmissionFunction.createInstrData(systemID, instruction, SecondaryInstruction.sendRequire, cookie, para), 0, 16);
            sendStream.Position = 0;
        }
    }
}
