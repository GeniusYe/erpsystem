﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ClientSystem.WareHouseInOut.Connector
{
    public class WarrantConnector : Transmission.Connector
    {
        //delegate for receiving somekind of object
        public delegate void DWarrantList(List<DefaultWarrant> lists);
        public delegate void DSpecialOneInWarrant(InWarrant inWarrant);
        public delegate void DSpecialOneOutWarrant(OutWarrant outWarrant);
        public delegate void DPrint(OperatedState instrState);
        public delegate void DOutWarrantPrintCustomerPage(OperatedState instrState);
        public delegate void DOutWarrantSpecialPrice(List<SpecialPriceSC.DraftWithSpecialPrice> specialPrice);
        public delegate void DViewDetail(DetailAccount detailAccount);
        public delegate void DViewOneCycle(ViewOneCycle viewOneCycle);
        public delegate void DDiscountChange(DiscountItem discountItem);
        public delegate void DPayCheck(PayCheckItem payCheckItem);
        public delegate void DAttendantList(List<DefaultAttendant> attendantsList);
        public event DWarrantList EWarrantList = null;
        public event DSpecialOneInWarrant ESpecialOneInWarrant = null;
        public event DSpecialOneOutWarrant ESpecialOneOutWarrant = null;
        public event DPrint EPrint = null;
        public event DOutWarrantPrintCustomerPage EOutWarrantPrintCustomerPage = null;
        public event DOutWarrantSpecialPrice EOutWarrantSpecialPrice = null;
        public event DViewDetail EViewDetail = null;
        public event DViewOneCycle EViewOneCycle = null;
        public event DDiscountChange EDiscountChange = null;
        public event DPayCheck EPayCheck = null;
        public event DAttendantList EAttendantList = null;

        public WarrantConnector(Instruction mainInstruction, Transmission.Transmission mainTransmission, List<Transmission.Connector> warrantConnectors)
            : base(mainInstruction, mainTransmission, SharedObject.Normal.SystemID.WareHouseInOut, warrantConnectors) { }

        protected override bool ResponseMethod(Object obj, Instruction mainInstruction, OperatedState operatedState)
        {
            switch (mainInstruction)
            {
                case Instruction.rights:
                    break;
                case Instruction.warrantViewDetail:
                    if (EViewDetail != null)
                    {
                        TSCViewDetail TSCViewDetail = new TSCViewDetail();
                        if (obj != null && obj is TSCViewDetail)
                            TSCViewDetail = (TSCViewDetail)obj;
                        EViewDetail(TSCViewDetail.detailAccount);
                    }
                    break;
                case Instruction.warrantViewOneCycle:
                    if (EViewOneCycle != null)
                    {
                        TSCViewOneCycle tSCViewOneCycle = new TSCViewOneCycle();
                        if (obj != null && obj is TSCViewOneCycle)
                            tSCViewOneCycle = (TSCViewOneCycle)obj;
                        EViewOneCycle(tSCViewOneCycle.viewOneCycle);
                    }
                    break;
                case Instruction.inWarrantList:
                case Instruction.outWarrantList:
                    if (EWarrantList != null)
                    {
                        TSCDefaultWarrantList transSCDefaultWarrantList = null;
                        if (obj != null && obj is TSCDefaultWarrantList)
                            transSCDefaultWarrantList = (TSCDefaultWarrantList)obj;
                        if (transSCDefaultWarrantList != null)
                            EWarrantList(transSCDefaultWarrantList.defaultWarrantList);
                    }
                    break;
                case Instruction.inWarrantSpecialOne:
                    if (ESpecialOneInWarrant != null)
                    {
                        TSCInWarrant transSCInWarrant = null;
                        if (obj != null && obj is TSCInWarrant)
                            transSCInWarrant = (TSCInWarrant)obj;
                        if (transSCInWarrant != null)
                            ESpecialOneInWarrant(transSCInWarrant.inWarrant);
                    }
                    break;
                case Instruction.outWarrantSpecialOne:
                    if (ESpecialOneOutWarrant != null)
                    {
                        TSCOutWarrant transSCOutWarrant = null;
                        if (obj != null && obj is TSCOutWarrant)
                            transSCOutWarrant = (TSCOutWarrant)obj;
                        if (transSCOutWarrant != null)
                            ESpecialOneOutWarrant(transSCOutWarrant.outWarrant);
                    }
                    break;
                case Instruction.inWarrantAdd:
                case Instruction.inWarrantEdit:
                    if (ESpecialOneInWarrant != null)
                    {
                        TSCInWarrant transSCInWarrant = null;
                        if (obj != null && obj is TSCInWarrant)
                            transSCInWarrant = (TSCInWarrant)obj;
                        if (transSCInWarrant != null && transSCInWarrant.inWarrant != null)
                            ESpecialOneInWarrant(transSCInWarrant.inWarrant);
                    }
                    break;
                case Instruction.outWarrantAdd:
                case Instruction.outWarrantEdit:
                    if (ESpecialOneOutWarrant != null)
                    {
                        TSCOutWarrant transSCOutWarrant = null;
                        if (obj != null && obj is TSCOutWarrant)
                            transSCOutWarrant = (TSCOutWarrant)obj;
                        if (transSCOutWarrant != null && transSCOutWarrant.outWarrant != null)
                            ESpecialOneOutWarrant(transSCOutWarrant.outWarrant);
                    }
                    break;
                case Instruction.inWarrantCancel:
                case Instruction.outWarrantCancel:
                case Instruction.outWarrantSubmitCustomerPage:
                case Instruction.inWarrantSubmit:
                case Instruction.outWarrantSubmit:
                case Instruction.outWarrantForcePass:
                case Instruction.outWarrantTakeOut:
                case Instruction.outWarrantReturn:
                    break;
                case Instruction.outWarrantPrintCustomerPage:
                    if (EOutWarrantPrintCustomerPage != null)
                        EOutWarrantPrintCustomerPage(operatedState);
                    break;
                case Instruction.inWarrantPrint:
                case Instruction.outWarrantPrint:
                    if (EPrint != null)
                        EPrint(operatedState);
                    break;
                case Instruction.outWarrantSpecialPrice:
                    if (EOutWarrantSpecialPrice != null)
                    {
                        TSCSpecialPrice tSCSpecialPrice = null;
                        if (obj != null && obj is TSCSpecialPrice)
                            tSCSpecialPrice = (TSCSpecialPrice)obj;
                        if (tSCSpecialPrice != null && tSCSpecialPrice.specialPrice != null)
                            EOutWarrantSpecialPrice(tSCSpecialPrice.specialPrice.draftIDWithPrice);
                    }
                    break;
                case Instruction.outWarrantDiscount:
                    if (EDiscountChange != null)
                    {
                        DiscountItem discountItem = null;
                        TSCDiscountChange tSCDiscountChange = null;
                        if (obj != null && obj is TSCDiscountChange)
                            tSCDiscountChange = (TSCDiscountChange)obj;
                        if (tSCDiscountChange != null && tSCDiscountChange.discountItem != null)
                        {
                            discountItem = tSCDiscountChange.discountItem;
                            if (discountItem != null)
                                EDiscountChange(discountItem);
                        }
                    }
                    break;
                case Instruction.outWarrantPayCheck:
                case Instruction.outWarrantReturnPayCheck:
                    if (EPayCheck != null)
                    {
                        PayCheckItem payCheckItem = null;
                        TSCPayCheckChange tSCPayCheck = null;
                        if (obj != null && obj is TSCPayCheckChange)
                            tSCPayCheck = (TSCPayCheckChange)obj;
                        if (tSCPayCheck != null && tSCPayCheck.payCheckItem != null)
                        {
                            payCheckItem = tSCPayCheck.payCheckItem;
                            if (payCheckItem != null)
                                EPayCheck(payCheckItem);
                        }
                    }
                    break;
                case Instruction.warrantAttendantsList:
                    if (EAttendantList != null)
                    {
                        TSCAttendantsList tSCAttendantsList = null;
                        List<DefaultAttendant> attendantsList = null;
                        if (obj != null && obj is TSCAttendantsList)
                            tSCAttendantsList = (TSCAttendantsList)obj;
                        if (tSCAttendantsList != null && tSCAttendantsList.attendantsList != null)
                        {
                            attendantsList = tSCAttendantsList.attendantsList;
                            EAttendantList(attendantsList);
                        }
                    }
                    break;
                default:
                    throw new ConnectorMainInstructionNotMatchException();
            }
            if (operatedState.state == SharedObject.Main.Transmission.OperatedState.OperatedStateEnum.success || operatedState.state == SharedObject.Main.Transmission.OperatedState.OperatedStateEnum.normal)
            {
                switch (mainInstruction)
                {
                    case Instruction.inWarrantCancel:
                    case Instruction.inWarrantEdit:
                    case Instruction.inWarrantSubmit:
                    case Instruction.inWarrantPrint:
                        this.mainInstruction = Instruction.inWarrantSpecialOne;
                        getSpecialOneWarrant(WarrantType.In, id);
                        return false;
                    case Instruction.outWarrantCancel:
                    case Instruction.outWarrantEdit:
                    case Instruction.outWarrantSubmitCustomerPage:
                    case Instruction.outWarrantPrintCustomerPage:
                    case Instruction.outWarrantForcePass:
                    case Instruction.outWarrantTakeOut:
                    case Instruction.outWarrantReturn:
                    case Instruction.outWarrantSubmit:
                    case Instruction.outWarrantPrint:
                        this.mainInstruction = Instruction.outWarrantSpecialOne;
                        getSpecialOneWarrant(WarrantType.Out, id);
                        return false;
                }
            }
            return true;
        }

        public void getWarrantCustomersList(WarrantType warrantType)
        {
            switch (warrantType)
            {
                case WarrantType.In:
                    sendRequestObj(null, new byte[] { });
                    break;
                case WarrantType.Out:
                    sendRequestObj(null, new byte[] { });
                    break;
            }
        }

        public void getWarrantList(WarrantType warrantType, DateTime startDate, DateTime stopDate, int target)
        {
            TCSDefaultWarrantsRequirement tCSRequirement = new TCSDefaultWarrantsRequirement();
            tCSRequirement.dateRange = new SharedObject.Normal.TCSDateRange();
            tCSRequirement.dateRange.startDate = startDate;
            tCSRequirement.dateRange.stopDate = stopDate;
            tCSRequirement.target = target;
            switch (warrantType)
            {
                case WarrantType.In:
                    sendRequestObj(tCSRequirement, new byte[] { });
                    break;
                case WarrantType.Out:
                    sendRequestObj(tCSRequirement, new byte[] { });
                    break;
            }
        }

        public void getSpecialOneWarrant(WarrantType warrantType, int id)
        {
            switch (warrantType)
            {
                case WarrantType.In:
                    sendRequestObj(null, id);
                    break;
                case WarrantType.Out:
                    sendRequestObj(null, id);
                    break;
            }
        }

        public void getDetailAccount(int id, DateTime startDate, DateTime stopDate)
        {
            TCSViewDetail tCSViewDetail = new TCSViewDetail();
            tCSViewDetail.ID = id;
            tCSViewDetail.dateRange = new SharedObject.Normal.TCSDateRange();
            tCSViewDetail.dateRange.startDate = startDate;
            tCSViewDetail.dateRange.stopDate = stopDate;
            sendRequestObj(tCSViewDetail, new byte[] { });
        }

        public void getOneCycleWarrant(DateTime date)
        {
            TCSViewOneCycle tCSViewOneCycle = new TCSViewOneCycle();
            tCSViewOneCycle.date = date;
            sendRequestObj(tCSViewOneCycle, new byte[] { });
        }

        public void addInWarrant(InWarrant inWarrant)
        {
            TCSInWarrantCreate tCSInWarrantCreate = new TCSInWarrantCreate();
            tCSInWarrantCreate.inWarrant = new InWarrantCSCreate(inWarrant);
            sendRequestObj(tCSInWarrantCreate, new byte[] { });
        }

        public void editInWarrant(InWarrant inWarrant)
        {
            id = inWarrant.ID;
            TCSInWarrantCreate tCSInWarrantCreate = new TCSInWarrantCreate();
            tCSInWarrantCreate.inWarrant = new InWarrantCSCreate(inWarrant);
            sendRequestObj(tCSInWarrantCreate, new byte[] { });
        }

        public void addOutWarrant(OutWarrant outWarrant)
        {
            TCSOutWarrantCreate tCSOutWarrantCreate = new TCSOutWarrantCreate();
            tCSOutWarrantCreate.outWarrant = new OutWarrantCSCreate(outWarrant);
            sendRequestObj(tCSOutWarrantCreate, new byte[] { });
        }

        public void editOutWarrant(OutWarrant outWarrant)
        {
            id = outWarrant.ID;
            TCSOutWarrantCreate tCSOutWarrantCreate = new TCSOutWarrantCreate();
            tCSOutWarrantCreate.outWarrant = new OutWarrantCSCreate(outWarrant);
            sendRequestObj(tCSOutWarrantCreate, new byte[] { });
        }

        public void cancelWarrant(WarrantType warrantType, AbstractWarrant abstractWarrant)
        {
            id = abstractWarrant.ID;
            TCSWarrantControl tCSWarrantControl = new TCSWarrantControl();
            tCSWarrantControl.warrant = new WarrantCSControl(abstractWarrant);
            switch (warrantType)
            {
                case WarrantType.In:
                    sendRequestObj(tCSWarrantControl, new byte[] { });
                    break;
                case WarrantType.Out:
                    sendRequestObj(tCSWarrantControl, new byte[] { });
                    break;
            }
        }

        public void submitCustomerPageWarrant(WarrantType warrantType, AbstractWarrant abstractWarrant)
        {
            id = abstractWarrant.ID;
            TCSWarrantControl tCSWarrantControl = new TCSWarrantControl();
            tCSWarrantControl.warrant = new WarrantCSControl(abstractWarrant);
            switch (warrantType)
            {
                case WarrantType.Out:
                    sendRequestObj(tCSWarrantControl, new byte[] { });
                    break;
            }
        }

        public void printCustomerPageWarrant(WarrantType warrantType, AbstractWarrant abstractWarrant)
        {
            id = abstractWarrant.ID;
            TCSWarrantControl tCSWarrantControl = new TCSWarrantControl();
            tCSWarrantControl.warrant = new WarrantCSControl(abstractWarrant);
            switch (warrantType)
            {
                case WarrantType.Out:
                    sendRequestObj(tCSWarrantControl, new byte[] { });
                    break;
            }
        }

        public void forcePass(WarrantType warrantType, AbstractWarrant abstractWarrant)
        {
            id = abstractWarrant.ID;
            TCSWarrantControl tCSWarrantControl = new TCSWarrantControl();
            tCSWarrantControl.warrant = new WarrantCSControl(abstractWarrant);
            switch (warrantType)
            {
                case WarrantType.Out:
                    sendRequestObj(tCSWarrantControl, new byte[] { });
                    break;
            }
        }

        public void takeOut(WarrantType warrantType, AbstractWarrant abstractWarrant)
        {
            id = abstractWarrant.ID;
            TCSWarrantControl tCSWarrantControl = new TCSWarrantControl();
            tCSWarrantControl.warrant = new WarrantCSControl(abstractWarrant);
            switch (warrantType)
            {
                case WarrantType.Out:
                    sendRequestObj(tCSWarrantControl, new byte[] { });
                    break;
            }
        }

        public void returnWarrant(WarrantType warrantType, OutWarrant outWarrant)
        {
            id = outWarrant.ID;
            switch (warrantType)
            {
                case WarrantType.Out:
                    List<TransReturnNumberPair> pairs = new List<TransReturnNumberPair>();
                    TransReturnNumberPair pair;
                    foreach (OutWarrantItem item in outWarrant.items)
                    {
                        pair = new TransReturnNumberPair();
                        pair.number1 = item.ID;
                        pair.number2 = item.numReturn;
                        pairs.Add(pair);
                    }
                    TCSOutWarrantReturnNumber tCSOutWarrantReturnNumber = new TCSOutWarrantReturnNumber();
                    tCSOutWarrantReturnNumber.transCSOutWarrantReturnNumberPairs = pairs;
                    tCSOutWarrantReturnNumber.warrant = new WarrantCSControl(outWarrant);
                    tCSOutWarrantReturnNumber.attendants = outWarrant.getAttendants();
                    sendRequestObj(tCSOutWarrantReturnNumber, new byte[] { });
                    break;
            }
        } 

        public void submitWarrant(WarrantType warrantType, AbstractWarrant abstractWarrant)
        {
            id = abstractWarrant.ID;
            TCSWarrantControl tCSWarrantControl = new TCSWarrantControl();
            tCSWarrantControl.warrant = new WarrantCSControl(abstractWarrant);
            switch (warrantType)
            {
                case WarrantType.In:
                    sendRequestObj(tCSWarrantControl, new byte[] { });
                    break;
                case WarrantType.Out:
                    sendRequestObj(tCSWarrantControl, new byte[] { });
                    break;
            }
        }

        public void PrintWarrant(WarrantType warrantType, AbstractWarrant abstractWarrant)
        {
            id = abstractWarrant.ID;
            TCSWarrantControl tCSWarrantControl = new TCSWarrantControl();
            tCSWarrantControl.warrant = new WarrantCSControl(abstractWarrant);
            switch (warrantType)
            {
                case WarrantType.In:
                    sendRequestObj(tCSWarrantControl, new byte[] { });
                    break;
                case WarrantType.Out:
                    sendRequestObj(tCSWarrantControl, new byte[] { });
                    break;
            }
        }

        public void getOutWarrantSpecialPrice(OutWarrant warrant)
        {
            GetSpecialPriceCS getSpecialPriceCS = new GetSpecialPriceCS();
            getSpecialPriceCS.target = warrant.target;
            List<int> draftID = new List<int>();
            foreach (OutWarrantItem item in warrant.items)
                draftID.Add(item.IDdraft);
            getSpecialPriceCS.draftID = draftID;
            TCSGetSpecialPrice tCSGetSpecialPrice = new TCSGetSpecialPrice();
            tCSGetSpecialPrice.getSpecialPriceInfo = getSpecialPriceCS;
            sendRequestObj(tCSGetSpecialPrice, new byte[] { });
        }

        public void changeDiscount(OutWarrant warrant, int? amountPermit, string reason)
        {
            TCSDiscountChange tCSDiscountChange = new TCSDiscountChange();
            tCSDiscountChange.discountItem = new DiscountItem();
            tCSDiscountChange.discountItem.amountPermit = amountPermit;
            tCSDiscountChange.discountItem.reason = reason;
            tCSDiscountChange.discountItem.warrantId = warrant.ID;
            tCSDiscountChange.warrant = new WarrantCSControl(warrant);
            sendRequestObj(tCSDiscountChange, new byte[] { });
        }

        public void payCheck(OutWarrant warrant, int amountPaid)
        {
            TCSPayCheck tCSPayCheck = new TCSPayCheck();
            tCSPayCheck.payCheckItem = new PayCheckItem();
            tCSPayCheck.payCheckItem.amountPaid = amountPaid;
            tCSPayCheck.payCheckItem.warrantId = warrant.ID;
            tCSPayCheck.warrant = new WarrantCSControl(warrant);
            sendRequestObj(tCSPayCheck, new byte[] { });
        }

        public void returnPayCheck(OutWarrant warrant, int amountPaid)
        {
            TCSPayCheck tCSPayCheck = new TCSPayCheck();
            tCSPayCheck.payCheckItem = new PayCheckItem();
            tCSPayCheck.payCheckItem.amountPaid = -amountPaid;
            tCSPayCheck.payCheckItem.warrantId = warrant.ID;
            tCSPayCheck.warrant = new WarrantCSControl(warrant);
            sendRequestObj(tCSPayCheck, new byte[] { });
        }

        public void getAttendantsList()
        {
            sendRequestObj(null, new byte[] { });
        }
    }
}
