﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using ClassObjectToNum.Money;

namespace ClientSystem.WareHouseInOut.Stuff
{
    public partial class ReturnPayCheckInputForm : Form
    {
        private OutWarrant warrant;
        private int amountTotal;
        private int? amountPermitNow;
        private int? amountPaid;

        //transmission
        private Transmission.Transmission mainTransmission;
        private SharedObject.Normal.TransmissionCookie cookie = new SharedObject.Normal.TransmissionCookie();
        private List<Transmission.Connector> warrantConnectors = new List<Transmission.Connector>();

        private delegate void DPayCheck(PayCheckItem payCheckItem);
        private delegate void DOperatedState(OperatedState instrState);
        private delegate void DIdentityCheck(Identity.IdentityCheckForm identityCheckForm);

        //delegates called by transmission
        private Connector.WarrantConnector.DPayCheck EPayCheck = null;
        private Connector.WarrantConnector.DOperatedState EOperateState;
        private Connector.WarrantConnector.DIdentityCheck EIdentityCheck;

        public ReturnPayCheckInputForm(Transmission.Transmission mainTransmission)
        {
            InitializeComponent();

            this.mainTransmission = mainTransmission;

            this.mainTransmission.addConnectors(SharedObject.Normal.SystemID.WareHouseInOut, this.warrantConnectors);

            EPayCheck = new Connector.WarrantConnector.DPayCheck(payCheckItem => this.Invoke(new DPayCheck(responseForPayCheck), new object[] { payCheckItem }));
            EOperateState = new Connector.WarrantConnector.DOperatedState(state => this.Invoke(new DOperatedState(responseForOperatedState), new object[] { state }));
            EIdentityCheck = new Connector.WarrantConnector.DIdentityCheck(identityCheckForm => this.Invoke(new DIdentityCheck(responseForIdentityCheck), new object[] { identityCheckForm }));

        }

        public void init(OutWarrant warrant, int amountTotal, int? amountPermitNow, int? amountPaid)
        {
            this.warrant = warrant;
            this.amountTotal = amountTotal;
            this.amountPermitNow = amountPermitNow;
            this.amountPaid = amountPaid;
            refresh();
        }

        private void refresh()
        {
            int amountShould = this.amountPermitNow == null ? this.amountTotal : this.amountPermitNow.Value;
            this.totalAmountLabel.Text = "需付款金额：" + amountShould.MoneyNumToDisplayString();
            if (this.amountPaid != null)
                this.amountPaidLabel.Text = "已付金额：" + amountPaid.Value.MoneyNumToDisplayString();
            else
                this.amountPaidLabel.Text = "已付金额：0.00";
            this.warrantIdLabel.Text = "单号：" + this.warrant.ID.ToString();
        }

        private void responseForPayCheck(PayCheckItem payCheckItem)
        {
            this.Close();
        }

        ///This method is used for response to server
        private void responseForOperatedState(OperatedState operatedState)
        {
            String str = Function.OperatedStateStringBuilder(operatedState);
            if (str.Length > 0)
                MessageBox.Show(str);
            //setAccessiable(null);
        }
        ///This method is used for response to server
        private void responseForIdentityCheck(Identity.IdentityCheckForm identityCheckForm)
        {
            identityCheckForm.Show();
        }

        private void returnPayCheckOkButton_Click(object sender, EventArgs e)
        {
            int amountPaid;
            try
            {
                amountPaid = ClassMoney.MoneyStringToNum(this.actuallyPaidTextBox.Text);
            }
            catch (ClassMoney.NumberFormatNotRecognizableException)
            {
                MessageBox.Show("数字格式不正确");
                return;
            }
            if (amountPaid < 0)
            {
                MessageBox.Show("退款金额不能为负值");
                return;
            }
            Connector.WarrantConnector connector = new Connector.WarrantConnector(SharedObject.Main.Transmission.Instruction.outWarrantReturnPayCheck, mainTransmission, warrantConnectors);
            connector.EPayCheck += EPayCheck;
            connector.EOperatedState += EOperateState;
            connector.EIdentityCheck += EIdentityCheck;
            connector.returnPayCheck(warrant, amountPaid);
        }
    }
}
