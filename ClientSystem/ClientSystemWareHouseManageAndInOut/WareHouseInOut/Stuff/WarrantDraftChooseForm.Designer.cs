﻿namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class WarrantDraftChooseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addButton = new System.Windows.Forms.Button();
            this.shutWindowsButton = new System.Windows.Forms.Button();
            this.scanIDButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(818, 12);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(34, 118);
            this.addButton.TabIndex = 0;
            this.addButton.Text = "添加";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // shutWindowsButton
            // 
            this.shutWindowsButton.Location = new System.Drawing.Point(818, 380);
            this.shutWindowsButton.Name = "shutWindowsButton";
            this.shutWindowsButton.Size = new System.Drawing.Size(34, 69);
            this.shutWindowsButton.TabIndex = 1;
            this.shutWindowsButton.Text = "关闭子窗口";
            this.shutWindowsButton.UseVisualStyleBackColor = true;
            this.shutWindowsButton.Click += new System.EventHandler(this.shutWindowsButton_Click);
            // 
            // scanIDButton
            // 
            this.scanIDButton.Location = new System.Drawing.Point(818, 136);
            this.scanIDButton.Name = "scanIDButton";
            this.scanIDButton.Size = new System.Drawing.Size(34, 118);
            this.scanIDButton.TabIndex = 2;
            this.scanIDButton.Text = "扫码";
            this.scanIDButton.UseVisualStyleBackColor = true;
            this.scanIDButton.Click += new System.EventHandler(this.scanIDButton_Click);
            // 
            // WarrantDraftChooseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 461);
            this.Controls.Add(this.scanIDButton);
            this.Controls.Add(this.shutWindowsButton);
            this.Controls.Add(this.addButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "WarrantDraftChooseForm";
            this.Text = "选择配件";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WarrantDraftChooseForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button shutWindowsButton;
        private System.Windows.Forms.Button scanIDButton;
    }
}