﻿namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class OneCycleWarrantForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.warrantListTabPage = new System.Windows.Forms.TabPage();
            this.inWarrantGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.outWarrantGridView = new System.Windows.Forms.DataGridView();
            this.amountAdjustOutTextBox = new System.Windows.Forms.TextBox();
            this.amountGiftCostTextBox = new System.Windows.Forms.TextBox();
            this.amountServiceCostTextBox = new System.Windows.Forms.TextBox();
            this.amountServiceFeeTextBox = new System.Windows.Forms.TextBox();
            this.amountServiceTextBox = new System.Windows.Forms.TextBox();
            this.amountPaidTextBox = new System.Windows.Forms.TextBox();
            this.amountCostTextBox = new System.Windows.Forms.TextBox();
            this.amountSellTotalTextBox = new System.Windows.Forms.TextBox();
            this.amountSellTextBox = new System.Windows.Forms.TextBox();
            this.amountAdjustInTextBox = new System.Windows.Forms.TextBox();
            this.amountAdjustOutLabel = new System.Windows.Forms.Label();
            this.amountInTextBox = new System.Windows.Forms.TextBox();
            this.amountGiftCostLabel = new System.Windows.Forms.Label();
            this.amountPaidLabel = new System.Windows.Forms.Label();
            this.amountCostLabel = new System.Windows.Forms.Label();
            this.amountServiceCostLabel = new System.Windows.Forms.Label();
            this.amountSerivceFeeLabel = new System.Windows.Forms.Label();
            this.amountSellTotalLabel = new System.Windows.Forms.Label();
            this.amountServiceLabel = new System.Windows.Forms.Label();
            this.amountAdjustInLabel = new System.Windows.Forms.Label();
            this.amountSellLabel = new System.Windows.Forms.Label();
            this.amountInLabel = new System.Windows.Forms.Label();
            this.sendResultButton = new System.Windows.Forms.Button();
            this.reloadWarrantButton = new System.Windows.Forms.Button();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.warrantTabPage = new System.Windows.Forms.TabPage();
            this.warrantID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.warrantAmountCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.warrantAmountTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.warrantAmountPermit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.warrantAmountPaid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.warrantState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.warrantListTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inWarrantGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.outWarrantGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.warrantListTabPage);
            this.tabControl1.Controls.Add(this.warrantTabPage);
            this.tabControl1.Location = new System.Drawing.Point(21, 21);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(2580, 1433);
            this.tabControl1.TabIndex = 0;
            // 
            // warrantListTabPage
            // 
            this.warrantListTabPage.Controls.Add(this.inWarrantGridView);
            this.warrantListTabPage.Controls.Add(this.outWarrantGridView);
            this.warrantListTabPage.Controls.Add(this.amountAdjustOutTextBox);
            this.warrantListTabPage.Controls.Add(this.amountGiftCostTextBox);
            this.warrantListTabPage.Controls.Add(this.amountServiceCostTextBox);
            this.warrantListTabPage.Controls.Add(this.amountServiceFeeTextBox);
            this.warrantListTabPage.Controls.Add(this.amountServiceTextBox);
            this.warrantListTabPage.Controls.Add(this.amountPaidTextBox);
            this.warrantListTabPage.Controls.Add(this.amountCostTextBox);
            this.warrantListTabPage.Controls.Add(this.amountSellTotalTextBox);
            this.warrantListTabPage.Controls.Add(this.amountSellTextBox);
            this.warrantListTabPage.Controls.Add(this.amountAdjustInTextBox);
            this.warrantListTabPage.Controls.Add(this.amountAdjustOutLabel);
            this.warrantListTabPage.Controls.Add(this.amountInTextBox);
            this.warrantListTabPage.Controls.Add(this.amountGiftCostLabel);
            this.warrantListTabPage.Controls.Add(this.amountPaidLabel);
            this.warrantListTabPage.Controls.Add(this.amountCostLabel);
            this.warrantListTabPage.Controls.Add(this.amountServiceCostLabel);
            this.warrantListTabPage.Controls.Add(this.amountSerivceFeeLabel);
            this.warrantListTabPage.Controls.Add(this.amountSellTotalLabel);
            this.warrantListTabPage.Controls.Add(this.amountServiceLabel);
            this.warrantListTabPage.Controls.Add(this.amountAdjustInLabel);
            this.warrantListTabPage.Controls.Add(this.amountSellLabel);
            this.warrantListTabPage.Controls.Add(this.amountInLabel);
            this.warrantListTabPage.Controls.Add(this.sendResultButton);
            this.warrantListTabPage.Controls.Add(this.reloadWarrantButton);
            this.warrantListTabPage.Controls.Add(this.dateTimePicker);
            this.warrantListTabPage.Location = new System.Drawing.Point(8, 39);
            this.warrantListTabPage.Margin = new System.Windows.Forms.Padding(12);
            this.warrantListTabPage.Name = "warrantListTabPage";
            this.warrantListTabPage.Padding = new System.Windows.Forms.Padding(12);
            this.warrantListTabPage.Size = new System.Drawing.Size(2564, 1386);
            this.warrantListTabPage.TabIndex = 0;
            this.warrantListTabPage.Text = "单据列表查询";
            this.warrantListTabPage.UseVisualStyleBackColor = true;
            // 
            // inWarrantGridView
            // 
            this.inWarrantGridView.AllowUserToAddRows = false;
            this.inWarrantGridView.AllowUserToDeleteRows = false;
            this.inWarrantGridView.ColumnHeadersHeight = 20;
            this.inWarrantGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.inWarrantGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column1,
            this.Column2,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.inWarrantGridView.Location = new System.Drawing.Point(24, 24);
            this.inWarrantGridView.Margin = new System.Windows.Forms.Padding(12);
            this.inWarrantGridView.Name = "inWarrantGridView";
            this.inWarrantGridView.RowHeadersVisible = false;
            this.inWarrantGridView.RowTemplate.Height = 18;
            this.inWarrantGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.inWarrantGridView.Size = new System.Drawing.Size(1934, 526);
            this.inWarrantGridView.TabIndex = 17;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "单号：";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 80;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "提前时间";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "客户";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "类型";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn3.HeaderText = "金额";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "状态";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // outWarrantGridView
            // 
            this.outWarrantGridView.AllowUserToAddRows = false;
            this.outWarrantGridView.AllowUserToDeleteRows = false;
            this.outWarrantGridView.ColumnHeadersHeight = 20;
            this.outWarrantGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.outWarrantGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.warrantID,
            this.Column5,
            this.dataGridViewTextBoxColumn5,
            this.type,
            this.dataGridViewTextBoxColumn6,
            this.Column3,
            this.warrantAmountCost,
            this.warrantAmountTotal,
            this.Column4,
            this.warrantAmountPermit,
            this.warrantAmountPaid,
            this.warrantState});
            this.outWarrantGridView.Location = new System.Drawing.Point(24, 574);
            this.outWarrantGridView.Margin = new System.Windows.Forms.Padding(12);
            this.outWarrantGridView.Name = "outWarrantGridView";
            this.outWarrantGridView.RowHeadersVisible = false;
            this.outWarrantGridView.RowTemplate.Height = 18;
            this.outWarrantGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.outWarrantGridView.Size = new System.Drawing.Size(1934, 765);
            this.outWarrantGridView.TabIndex = 16;
            // 
            // amountAdjustOutTextBox
            // 
            this.amountAdjustOutTextBox.Location = new System.Drawing.Point(2024, 988);
            this.amountAdjustOutTextBox.Margin = new System.Windows.Forms.Padding(12);
            this.amountAdjustOutTextBox.Name = "amountAdjustOutTextBox";
            this.amountAdjustOutTextBox.Size = new System.Drawing.Size(468, 31);
            this.amountAdjustOutTextBox.TabIndex = 15;
            // 
            // amountGiftCostTextBox
            // 
            this.amountGiftCostTextBox.Location = new System.Drawing.Point(2024, 908);
            this.amountGiftCostTextBox.Margin = new System.Windows.Forms.Padding(12);
            this.amountGiftCostTextBox.Name = "amountGiftCostTextBox";
            this.amountGiftCostTextBox.Size = new System.Drawing.Size(468, 31);
            this.amountGiftCostTextBox.TabIndex = 15;
            // 
            // amountServiceCostTextBox
            // 
            this.amountServiceCostTextBox.Location = new System.Drawing.Point(2024, 828);
            this.amountServiceCostTextBox.Margin = new System.Windows.Forms.Padding(12);
            this.amountServiceCostTextBox.Name = "amountServiceCostTextBox";
            this.amountServiceCostTextBox.Size = new System.Drawing.Size(468, 31);
            this.amountServiceCostTextBox.TabIndex = 13;
            // 
            // amountServiceFeeTextBox
            // 
            this.amountServiceFeeTextBox.Location = new System.Drawing.Point(2024, 1068);
            this.amountServiceFeeTextBox.Margin = new System.Windows.Forms.Padding(12);
            this.amountServiceFeeTextBox.Name = "amountServiceFeeTextBox";
            this.amountServiceFeeTextBox.Size = new System.Drawing.Size(468, 31);
            this.amountServiceFeeTextBox.TabIndex = 19;
            // 
            // amountServiceTextBox
            // 
            this.amountServiceTextBox.Location = new System.Drawing.Point(2024, 748);
            this.amountServiceTextBox.Margin = new System.Windows.Forms.Padding(12);
            this.amountServiceTextBox.Name = "amountServiceTextBox";
            this.amountServiceTextBox.Size = new System.Drawing.Size(468, 31);
            this.amountServiceTextBox.TabIndex = 13;
            // 
            // amountPaidTextBox
            // 
            this.amountPaidTextBox.Location = new System.Drawing.Point(2024, 668);
            this.amountPaidTextBox.Margin = new System.Windows.Forms.Padding(12);
            this.amountPaidTextBox.Name = "amountPaidTextBox";
            this.amountPaidTextBox.Size = new System.Drawing.Size(468, 31);
            this.amountPaidTextBox.TabIndex = 11;
            // 
            // amountCostTextBox
            // 
            this.amountCostTextBox.Location = new System.Drawing.Point(2024, 588);
            this.amountCostTextBox.Margin = new System.Windows.Forms.Padding(12);
            this.amountCostTextBox.Name = "amountCostTextBox";
            this.amountCostTextBox.Size = new System.Drawing.Size(468, 31);
            this.amountCostTextBox.TabIndex = 11;
            // 
            // amountSellTotalTextBox
            // 
            this.amountSellTotalTextBox.Location = new System.Drawing.Point(2024, 511);
            this.amountSellTotalTextBox.Margin = new System.Windows.Forms.Padding(12);
            this.amountSellTotalTextBox.Name = "amountSellTotalTextBox";
            this.amountSellTotalTextBox.Size = new System.Drawing.Size(468, 31);
            this.amountSellTotalTextBox.TabIndex = 11;
            // 
            // amountSellTextBox
            // 
            this.amountSellTextBox.Location = new System.Drawing.Point(2024, 431);
            this.amountSellTextBox.Margin = new System.Windows.Forms.Padding(12);
            this.amountSellTextBox.Name = "amountSellTextBox";
            this.amountSellTextBox.Size = new System.Drawing.Size(468, 31);
            this.amountSellTextBox.TabIndex = 11;
            // 
            // amountAdjustInTextBox
            // 
            this.amountAdjustInTextBox.Location = new System.Drawing.Point(2024, 291);
            this.amountAdjustInTextBox.Margin = new System.Windows.Forms.Padding(12);
            this.amountAdjustInTextBox.Name = "amountAdjustInTextBox";
            this.amountAdjustInTextBox.Size = new System.Drawing.Size(468, 31);
            this.amountAdjustInTextBox.TabIndex = 9;
            // 
            // amountAdjustOutLabel
            // 
            this.amountAdjustOutLabel.AutoSize = true;
            this.amountAdjustOutLabel.Location = new System.Drawing.Point(2034, 951);
            this.amountAdjustOutLabel.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.amountAdjustOutLabel.Name = "amountAdjustOutLabel";
            this.amountAdjustOutLabel.Size = new System.Drawing.Size(138, 25);
            this.amountAdjustOutLabel.TabIndex = 14;
            this.amountAdjustOutLabel.Text = "调整出库总额";
            // 
            // amountInTextBox
            // 
            this.amountInTextBox.Location = new System.Drawing.Point(2024, 193);
            this.amountInTextBox.Margin = new System.Windows.Forms.Padding(12);
            this.amountInTextBox.Name = "amountInTextBox";
            this.amountInTextBox.Size = new System.Drawing.Size(468, 31);
            this.amountInTextBox.TabIndex = 9;
            // 
            // amountGiftCostLabel
            // 
            this.amountGiftCostLabel.AutoSize = true;
            this.amountGiftCostLabel.Location = new System.Drawing.Point(2034, 871);
            this.amountGiftCostLabel.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.amountGiftCostLabel.Name = "amountGiftCostLabel";
            this.amountGiftCostLabel.Size = new System.Drawing.Size(96, 25);
            this.amountGiftCostLabel.TabIndex = 14;
            this.amountGiftCostLabel.Text = "折让成本";
            // 
            // amountPaidLabel
            // 
            this.amountPaidLabel.AutoSize = true;
            this.amountPaidLabel.Location = new System.Drawing.Point(2034, 631);
            this.amountPaidLabel.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.amountPaidLabel.Name = "amountPaidLabel";
            this.amountPaidLabel.Size = new System.Drawing.Size(138, 25);
            this.amountPaidLabel.TabIndex = 10;
            this.amountPaidLabel.Text = "销货收款总额";
            // 
            // amountCostLabel
            // 
            this.amountCostLabel.AutoSize = true;
            this.amountCostLabel.Location = new System.Drawing.Point(2034, 554);
            this.amountCostLabel.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.amountCostLabel.Name = "amountCostLabel";
            this.amountCostLabel.Size = new System.Drawing.Size(96, 25);
            this.amountCostLabel.TabIndex = 10;
            this.amountCostLabel.Text = "销货成本";
            // 
            // amountServiceCostLabel
            // 
            this.amountServiceCostLabel.AutoSize = true;
            this.amountServiceCostLabel.Location = new System.Drawing.Point(2034, 791);
            this.amountServiceCostLabel.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.amountServiceCostLabel.Name = "amountServiceCostLabel";
            this.amountServiceCostLabel.Size = new System.Drawing.Size(96, 25);
            this.amountServiceCostLabel.TabIndex = 12;
            this.amountServiceCostLabel.Text = "三包成本";
            // 
            // amountSerivceFeeLabel
            // 
            this.amountSerivceFeeLabel.AutoSize = true;
            this.amountSerivceFeeLabel.Location = new System.Drawing.Point(2034, 1031);
            this.amountSerivceFeeLabel.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.amountSerivceFeeLabel.Name = "amountSerivceFeeLabel";
            this.amountSerivceFeeLabel.Size = new System.Drawing.Size(75, 25);
            this.amountSerivceFeeLabel.TabIndex = 18;
            this.amountSerivceFeeLabel.Text = "服务费";
            // 
            // amountSellTotalLabel
            // 
            this.amountSellTotalLabel.AutoSize = true;
            this.amountSellTotalLabel.Location = new System.Drawing.Point(2034, 474);
            this.amountSellTotalLabel.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.amountSellTotalLabel.Name = "amountSellTotalLabel";
            this.amountSellTotalLabel.Size = new System.Drawing.Size(222, 25);
            this.amountSellTotalLabel.TabIndex = 10;
            this.amountSellTotalLabel.Text = "销货总额（不计折让）";
            // 
            // amountServiceLabel
            // 
            this.amountServiceLabel.AutoSize = true;
            this.amountServiceLabel.Location = new System.Drawing.Point(2034, 711);
            this.amountServiceLabel.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.amountServiceLabel.Name = "amountServiceLabel";
            this.amountServiceLabel.Size = new System.Drawing.Size(96, 25);
            this.amountServiceLabel.TabIndex = 12;
            this.amountServiceLabel.Text = "三包总额";
            // 
            // amountAdjustInLabel
            // 
            this.amountAdjustInLabel.AutoSize = true;
            this.amountAdjustInLabel.Location = new System.Drawing.Point(2034, 254);
            this.amountAdjustInLabel.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.amountAdjustInLabel.Name = "amountAdjustInLabel";
            this.amountAdjustInLabel.Size = new System.Drawing.Size(138, 25);
            this.amountAdjustInLabel.TabIndex = 8;
            this.amountAdjustInLabel.Text = "调整入库总额";
            // 
            // amountSellLabel
            // 
            this.amountSellLabel.AutoSize = true;
            this.amountSellLabel.Location = new System.Drawing.Point(2034, 394);
            this.amountSellLabel.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.amountSellLabel.Name = "amountSellLabel";
            this.amountSellLabel.Size = new System.Drawing.Size(96, 25);
            this.amountSellLabel.TabIndex = 10;
            this.amountSellLabel.Text = "销货总额";
            // 
            // amountInLabel
            // 
            this.amountInLabel.AutoSize = true;
            this.amountInLabel.Location = new System.Drawing.Point(2034, 156);
            this.amountInLabel.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.amountInLabel.Name = "amountInLabel";
            this.amountInLabel.Size = new System.Drawing.Size(96, 25);
            this.amountInLabel.TabIndex = 8;
            this.amountInLabel.Text = "入库总额";
            // 
            // sendResultButton
            // 
            this.sendResultButton.Location = new System.Drawing.Point(2024, 1226);
            this.sendResultButton.Margin = new System.Windows.Forms.Padding(12);
            this.sendResultButton.Name = "sendResultButton";
            this.sendResultButton.Size = new System.Drawing.Size(472, 96);
            this.sendResultButton.TabIndex = 7;
            this.sendResultButton.Text = "发送结果";
            this.sendResultButton.UseVisualStyleBackColor = true;
            this.sendResultButton.Click += new System.EventHandler(this.sendResultButton_Click);
            // 
            // reloadWarrantButton
            // 
            this.reloadWarrantButton.Location = new System.Drawing.Point(2024, 94);
            this.reloadWarrantButton.Margin = new System.Windows.Forms.Padding(12);
            this.reloadWarrantButton.Name = "reloadWarrantButton";
            this.reloadWarrantButton.Size = new System.Drawing.Size(472, 50);
            this.reloadWarrantButton.TabIndex = 7;
            this.reloadWarrantButton.Text = "重新加载";
            this.reloadWarrantButton.UseVisualStyleBackColor = true;
            this.reloadWarrantButton.Click += new System.EventHandler(this.reloadWarrantButton_Click);
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.CustomFormat = "yyyy年MM月";
            this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker.Location = new System.Drawing.Point(2024, 39);
            this.dateTimePicker.Margin = new System.Windows.Forms.Padding(12);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(460, 31);
            this.dateTimePicker.TabIndex = 6;
            // 
            // warrantTabPage
            // 
            this.warrantTabPage.Location = new System.Drawing.Point(8, 39);
            this.warrantTabPage.Margin = new System.Windows.Forms.Padding(12);
            this.warrantTabPage.Name = "warrantTabPage";
            this.warrantTabPage.Padding = new System.Windows.Forms.Padding(12);
            this.warrantTabPage.Size = new System.Drawing.Size(2564, 1386);
            this.warrantTabPage.TabIndex = 1;
            this.warrantTabPage.Text = "单个单据查看";
            this.warrantTabPage.UseVisualStyleBackColor = true;
            // 
            // warrantID
            // 
            this.warrantID.HeaderText = "单号：";
            this.warrantID.Name = "warrantID";
            this.warrantID.ReadOnly = true;
            this.warrantID.Width = 80;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "销货时间 ";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "提交时间";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // type
            // 
            this.type.HeaderText = "类型";
            this.type.Name = "type";
            this.type.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "客户姓名";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "服务人员";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // warrantAmountCost
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.warrantAmountCost.DefaultCellStyle = dataGridViewCellStyle2;
            this.warrantAmountCost.HeaderText = "成本";
            this.warrantAmountCost.Name = "warrantAmountCost";
            this.warrantAmountCost.ReadOnly = true;
            // 
            // warrantAmountTotal
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.warrantAmountTotal.DefaultCellStyle = dataGridViewCellStyle3;
            this.warrantAmountTotal.HeaderText = "销售金额";
            this.warrantAmountTotal.Name = "warrantAmountTotal";
            this.warrantAmountTotal.ReadOnly = true;
            // 
            // Column4
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column4.HeaderText = "服务费";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // warrantAmountPermit
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.warrantAmountPermit.DefaultCellStyle = dataGridViewCellStyle5;
            this.warrantAmountPermit.HeaderText = "折让后金额";
            this.warrantAmountPermit.Name = "warrantAmountPermit";
            this.warrantAmountPermit.ReadOnly = true;
            // 
            // warrantAmountPaid
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.warrantAmountPaid.DefaultCellStyle = dataGridViewCellStyle6;
            this.warrantAmountPaid.HeaderText = "已付款金额";
            this.warrantAmountPaid.Name = "warrantAmountPaid";
            this.warrantAmountPaid.ReadOnly = true;
            // 
            // warrantState
            // 
            this.warrantState.HeaderText = "状态";
            this.warrantState.Name = "warrantState";
            this.warrantState.ReadOnly = true;
            // 
            // OneCycleWarrantForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(2619, 1464);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "OneCycleWarrantForm";
            this.Text = "单周期单据查询";
            this.tabControl1.ResumeLayout(false);
            this.warrantListTabPage.ResumeLayout(false);
            this.warrantListTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inWarrantGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.outWarrantGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage warrantListTabPage;
        private System.Windows.Forms.Button reloadWarrantButton;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.TabPage warrantTabPage;
        private System.Windows.Forms.TextBox amountServiceTextBox;
        private System.Windows.Forms.TextBox amountSellTextBox;
        private System.Windows.Forms.TextBox amountInTextBox;
        private System.Windows.Forms.Label amountServiceLabel;
        private System.Windows.Forms.Label amountSellLabel;
        private System.Windows.Forms.Label amountInLabel;
        private System.Windows.Forms.DataGridView outWarrantGridView;
        private System.Windows.Forms.DataGridView inWarrantGridView;
        private System.Windows.Forms.TextBox amountPaidTextBox;
        private System.Windows.Forms.Label amountPaidLabel;
        private System.Windows.Forms.Button sendResultButton;
        private System.Windows.Forms.TextBox amountGiftCostTextBox;
        private System.Windows.Forms.TextBox amountServiceCostTextBox;
        private System.Windows.Forms.TextBox amountCostTextBox;
        private System.Windows.Forms.Label amountGiftCostLabel;
        private System.Windows.Forms.Label amountCostLabel;
        private System.Windows.Forms.Label amountServiceCostLabel;
        private System.Windows.Forms.TextBox amountSellTotalTextBox;
        private System.Windows.Forms.Label amountSellTotalLabel;
        private System.Windows.Forms.TextBox amountAdjustOutTextBox;
        private System.Windows.Forms.TextBox amountAdjustInTextBox;
        private System.Windows.Forms.Label amountAdjustOutLabel;
        private System.Windows.Forms.Label amountAdjustInLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.TextBox amountServiceFeeTextBox;
        private System.Windows.Forms.Label amountSerivceFeeLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn warrantID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn type;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn warrantAmountCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn warrantAmountTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn warrantAmountPermit;
        private System.Windows.Forms.DataGridViewTextBoxColumn warrantAmountPaid;
        private System.Windows.Forms.DataGridViewTextBoxColumn warrantState;
    }
}