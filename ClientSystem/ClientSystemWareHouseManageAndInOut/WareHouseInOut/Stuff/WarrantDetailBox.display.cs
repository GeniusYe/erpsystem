﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClassObjectToNum;
using ClassObjectToNum.DateTime;
using SharedObject.WareHouseInOut.Stuff;
using ClassObjectToNum.Money;

namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class WarrantDetailBox
    {
        private void displayWarrant()
        {
            if (_warrant != null)
            {
                numberTextBox.Text = string.Format("{0:d13}", _warrant.number).PadLeft(4);

                switch (_warrant.type)
                {
                    case DefaultWarrant.SpecificWarrantType.withoutReceipt:
                        typeRadioButtonWithoutReceipt.Checked = true;
                        break;
                    case DefaultWarrant.SpecificWarrantType.withReceipt:
                        typeRadioButtonWithReceipt.Checked = true;
                        break;
                    case DefaultWarrant.SpecificWarrantType.Service:
                        typeRadioButtonService.Checked = true;
                        break;
                    case DefaultWarrant.SpecificWarrantType.AdjustOut:
                        typeRadioButtonAdjustOut.Checked = true;
                        break;
                    case DefaultWarrant.SpecificWarrantType.Gift:
                        typeRadioButtonGift.Checked = true;
                        break;
                    case DefaultWarrant.SpecificWarrantType.In:
                        typeRadioButtonIn.Checked = true;
                        break;
                    case DefaultWarrant.SpecificWarrantType.AdjustIn:
                        typeRadioButtonAdjustIn.Checked = true;
                        break;
                }

                CommentTextBox.Text = _warrant.comment;
                dateAndOperator.Rows.Clear();
                if (_warrant.ID > 0)
                {
                    foreach (WarrantOperationRecord record in _warrant.operationRecord)
                    {
                        string mark, mark2 = string.Empty;
                        switch (record.operation)
                        {
                            case WarrantOperationEnum.create:
                                mark = "制单";
                                break;
                            case WarrantOperationEnum.edit:
                                mark = "编辑";
                                break;
                            case WarrantOperationEnum.cancel:
                                mark = "作废";
                                break;
                            case WarrantOperationEnum.submitCustomerPage:
                                mark = "销货提交";
                                break;
                            case WarrantOperationEnum.printCustomerPage:
                                mark = "打印销货单";
                                break;
                            case WarrantOperationEnum.forcePass:
                                mark = "强制出库";
                                break;
                            case WarrantOperationEnum.takeOut:
                                mark = "出库";
                                break;
                            case WarrantOperationEnum.returnOperation:
                                mark = "退件";
                                break;
                            case WarrantOperationEnum.submit:
                                if (warrant is OutWarrant)
                                    mark = "出库提交";
                                else if (warrant is InWarrant)
                                    mark = "提交";
                                else
                                    mark = "未知操作";
                                break;
                            case WarrantOperationEnum.print:
                                if (warrant is OutWarrant)
                                    mark = "打印出库单";
                                else if (warrant is InWarrant)
                                    mark = "打印入库单";
                                else
                                    mark = "未知操作";
                                break;
                            case WarrantOperationEnum.finish:
                                mark = "完结";
                                break;
                            case WarrantOperationEnum.payCheck:
                                if (record.parameters[0] != null)
                                {
                                    int amount = (int)record.parameters[0];
                                    if (amount > 0)
                                        mark = "付款" + amount.MoneyNumToDisplayString() + "元";
                                    else
                                        mark = "退款" + (-amount).MoneyNumToDisplayString() + "元";
                                }
                                else
                                {
                                    mark = "资金流动";
                                }
                                break;
                            case WarrantOperationEnum.discount:
                                if (record.parameters[0] != null)
                                {
                                    if (!(record.parameters[0] is DBNull))
                                        mark = "修改下浮后金额为：" + ((int)record.parameters[0]).MoneyNumToDisplayString() + "元";
                                    else
                                        mark = "取消下浮";
                                    if (record.parameters[1] != null)
                                        mark2 = record.parameters[1].ToString();
                                }
                                else
                                {
                                    mark = "资金流动";
                                }
                                break;
                            default:
                                mark = "";
                                break;
                        }
                        dateAndOperator.Rows.Add(new object[] { mark, record.operationOperatorName, record.operatedTime.ToString(), mark2 });
                    }
                }
                setVisible(_warrant.state);
                refreshItem();
                setAccessiable();
            }
            else
            {
                numberTextBox.Text = "0";
                setVisible();
                refreshItem();
                setAccessiable();
            }
        }
        
        /// <summary>
        /// Method displayOneItem is used to create a DataGridViewRow
        /// </summary>
        /// <param name="i">the number on the left</param>
        /// <param name="row">a row to display</param>
        /// <returns></returns>
        private DataGridViewRow displayOneItem(int i, InWarrantItem row, out int amount)
        {
            DataGridViewRow viewRow = displayOneItem(i, (AbstractWarrantItem)row);
            viewRow.Cells[4].Value = 0;
            viewRow.Cells[5].Value = 0;
            viewRow.Cells[7].Value = 0;
            viewRow.Cells[8].Value = 0;
            amount = row.num * row.price;
            viewRow.Cells[9].Value = ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(amount);
            return viewRow;
        }
        /// <summary>
        /// Method displayOneItem is used to create a DataGridViewRow
        /// </summary>
        /// <param name="i">the number on the left</param>
        /// <param name="row">a row to display</param>
        /// <returns></returns>
        private DataGridViewRow displayOneItem(int i, OutWarrantItem row, out int amount, out int amountCost)
        {
            DataGridViewRow viewRow = displayOneItem(i, (AbstractWarrantItem)row);
            viewRow.Cells[4].Value = row.numReturn;
            viewRow.Cells[5].Value = ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(row.priceCost);
            if (_warrant.state == DefaultWarrant.WarrantState.Normal)
            {
                if (((OutWarrant)_warrant).type == OutWarrant.SpecificWarrantType.withoutReceipt)
                    row.price = row.priceOutL;
                else
                    row.price = row.priceOutH;
            }
            viewRow.Cells[6].Value = ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(row.price);
            viewRow.Cells[7].Value = ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(row.priceAdjustMannual == null ? 0 : row.priceAdjustMannual.Value);
            amountCost = row.priceCostValue;
            viewRow.Cells[8].Value = ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(amountCost);
            int num, price;
            num = row.num - row.numReturn;
            if (row.priceAdjustMannual == null || row.priceAdjustMannual == 0)
                price = row.price;
            else
                price = row.priceAdjustMannual.Value;
            amount = num * price;
            viewRow.Cells[9].Value = ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(amount);
            return viewRow;
        }
        /// <summary>
        /// Method displayOneItem is used to create a DataGridViewRow
        /// </summary>
        /// <param name="i">the number on the left</param>
        /// <param name="row">a row to display</param>
        /// <returns></returns>
        private DataGridViewRow displayOneItem(int i, AbstractWarrantItem row)
        {
            DataGridViewRow viewRow;
            viewRow = new DataGridViewRow();
            DataGridViewTextBoxCell[] DataCell = new DataGridViewTextBoxCell[12];
            Int16 j;
            for (j = 0; j < 12; j++)
                DataCell[j] = new DataGridViewTextBoxCell();
            DataCell[0].Value = i;
            DataCell[1].Value = row.name;
            DataCell[2].Value = row.specification;
            DataCell[3].Value = row.num + "(" + row.unit + ")";
            DataCell[6].Value = ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(row.price);
            DataCell[10].Value = row.remark;
            DataCell[11].Value = row.hiddenSpecification;
            viewRow.Cells.AddRange(DataCell);
            return viewRow;
        }

        /// <summary>
        /// Method refreshItem is used to mannual refresh the Items
        /// </summary>
        public void refreshItem()
        {
            itemGridView.Rows.Clear();
            long totalAmount = 0, totalAmountCost = 0;
            int amount, amountCost;
            if (_warrant != null)
            {
                int i = 0;
                if (_warrant is InWarrant)
                {
                    foreach (InWarrantItem item in ((InWarrant)_warrant).items)
                    {
                        i++;
                        itemGridView.Rows.Add(displayOneItem(i, item, out amount));
                        totalAmount += amount;
                    }
                    totalAmountLabel.Text = "总金额：" + ClassMoney.MoneyNumToDisplayString(totalAmount) + "";
                }
                else if (_warrant is OutWarrant)
                {
                    foreach (OutWarrantItem item in ((OutWarrant)_warrant).items)
                    {
                        i++;
                        itemGridView.Rows.Add(displayOneItem(i, item, out amount, out amountCost));
                        totalAmount += amount;
                        totalAmountCost += amountCost;
                    }
                    int amountPermit, amountPaid;
                    if (((OutWarrant)_warrant).amountPermit != null)
                    {
                        amountPermit = ((OutWarrant)_warrant).amountPermit.Value;
                        amountPermitLabel.Text = "下浮后金额：" + amountPermit.MoneyNumToDisplayString();
                    }
                    if (((OutWarrant)_warrant).amountPaid != null)
                    {
                        amountPaid = ((OutWarrant)_warrant).amountPaid.Value;
                        amountPaidLabel.Text = "已付金额：" + amountPaid.MoneyNumToDisplayString();
                    }
                    totalAmountLabel.Text = "总金额：" + totalAmount.MoneyNumToDisplayString();
                    totalAmountCostLabel.Text = "成本总额：" + totalAmountCost.MoneyNumToDisplayString();
                }
            }
            else
            {
                amountPaidLabel.Text = "--------";
                amountPermitLabel.Text = "--------";
                totalAmountCostLabel.Text = "--------";
                totalAmountLabel.Text = "--------";
            }
        }

        public void setStyle(WarrantType warrantType)
        {
            switch (warrantType)
            {
                case WarrantType.In:
                    typeGroupBoxOut.Visible = false;
                    typeGroupBoxIn.Visible = true;
                    break;
                case WarrantType.Out:
                    typeGroupBoxOut.Visible = true;
                    typeGroupBoxIn.Visible = false;
                    break;
            }
        }

        private void typeRadioButtonWithReceipt_Click(object sender, EventArgs e)
        {
            if (_warrant != null && _warrant is OutWarrant)
            {
                OutWarrant outWarrant = (OutWarrant)_warrant;
                outWarrant.type = DefaultWarrant.SpecificWarrantType.withReceipt;
                refreshItem();
            }
        }

        private void typeRadioButtonWithoutReceipt_Click(object sender, EventArgs e)
        {
            if (_warrant != null && _warrant is OutWarrant)
            {
                _warrant.type = DefaultWarrant.SpecificWarrantType.withoutReceipt;
                refreshItem();
            }
        }

        private void typeRadioButtonService_Click(object sender, EventArgs e)
        {
            if (_warrant != null && _warrant is OutWarrant)
            {
                _warrant.type = DefaultWarrant.SpecificWarrantType.Service;
                refreshItem();
            }
        }

        private void typeRadioButtonGift_Click(object sender, EventArgs e)
        {
            if (_warrant != null && _warrant is OutWarrant)
            {
                _warrant.type = DefaultWarrant.SpecificWarrantType.Gift;
                refreshItem();
            }
        }

        private void typeRadioButtonAdjustOut_Click(object sender, EventArgs e)
        {
            if (_warrant != null && _warrant is OutWarrant)
            {
                _warrant.type = DefaultWarrant.SpecificWarrantType.AdjustOut;
                refreshItem();
            }
        }

        private void typeRadioButtonIn_Click(object sender, EventArgs e)
        {
            if (_warrant != null && _warrant is InWarrant)
            {
                _warrant.type = DefaultWarrant.SpecificWarrantType.In;
                refreshItem();
            }
        }

        private void typeRadioButtonAdjustIn_Click(object sender, EventArgs e)
        {
            if (_warrant != null && _warrant is InWarrant)
            {
                _warrant.type = DefaultWarrant.SpecificWarrantType.AdjustIn;
                refreshItem();
            }
        }

        private void CommentTextBox_TextChanged(object sender, EventArgs e)
        {
            if (_warrant != null)
                _warrant.comment = CommentTextBox.Text.Trim();
        }
    }
}
