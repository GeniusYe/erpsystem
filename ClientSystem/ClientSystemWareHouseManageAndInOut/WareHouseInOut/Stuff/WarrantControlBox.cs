﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.CustomerManage.Stuff;

namespace ClientSystem.WareHouseInOut.Stuff
{
    public partial class WarrantControlBox : UserControl
    {
        //transmission
        protected Transmission.Transmission mainTransmission;
        protected SharedObject.Normal.TransmissionCookie cookie = new SharedObject.Normal.TransmissionCookie();
        protected List<Transmission.Connector> warrantConnectors = new List<Transmission.Connector>();

        //identity
        protected Identity.IdentityCheck identityCheck = new Identity.IdentityCheck();
        //the draft chosen form
        protected WarrantDraftChooseForm warrantDraftChooseForm;
        //the warrantDetailBox
        protected WarrantDetailBox warrantDetailBox;

        //show warrantlist for thread safety
        protected delegate void DWarrantList(List<DefaultWarrant> lists);
        protected delegate void DOperatedState(OperatedState instrState);
        protected delegate void DIdentityCheck(Identity.IdentityCheckForm identityCheckForm);

        //delegates called by transmission
        protected Connector.WarrantConnector.DOperatedState EOperateState;
        protected Connector.WarrantConnector.DIdentityCheck EIdentityCheck;

        public new event WarrantDetailBox.DDoubleClick DoubleClick;

        protected bool _addNew;

        public bool allowUserToAddNew
        {
            get { return _addNew; }
            set
            {
                _addNew = value;
                addNewButton.Enabled = _addNew;
            }
        }

        public enum EditingState
        {
            Normal,
            Editing,
            Returning
        }
        //local state
        protected EditingState editingState = EditingState.Normal;
        protected bool addEditing;

        public WarrantControlBox()
            : this(Transmission.Transmission.mainTransmission) { }

        public WarrantControlBox(Transmission.Transmission mainTransmission)
        {
            InitializeComponent(); 
            
            this.mainTransmission = mainTransmission;
            if (mainTransmission != null)
                mainTransmission.addConnectors(SharedObject.Normal.SystemID.WareHouseInOut, warrantConnectors);
            else
                throw new Transmission.TransmissionIsNullException();

            EOperateState = new Connector.WarrantConnector.DOperatedState(state => this.Invoke(new DOperatedState(responseForOperatedState), new object[] { state }));
            EIdentityCheck = new Connector.WarrantConnector.DIdentityCheck(identityCheckForm => this.Invoke(new DIdentityCheck(responseForIdentityCheck), new object[] { identityCheckForm }));
            
            this.warrantDetailBox = new WarrantDetailBox(this.mainTransmission);
            this.warrantDetailBox.Location = new System.Drawing.Point(19, 66);
            this.warrantDetailBox.Name = "warrantDetailBox";
            this.warrantDetailBox.Size = new System.Drawing.Size(872, 364);
            this.warrantDetailBox.TabIndex = 4;
            this.warrantDetailBox.visiblePriceIn = false;
            this.warrantDetailBox.warrant = null;
            this.warrantDetailBox.visiblePriceIn = true;
            this.warrantDetailBox.DoubleClick += new WarrantDetailBox.DDoubleClick(warrantDetailBox_DoubleClick);
            this.Controls.Add(this.warrantDetailBox);
        }

        private void warrantDetailBox_DoubleClick(int draftId)
        {
            if (DoubleClick != null)
                DoubleClick(draftId);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            while (warrantConnectors.Count > 0)
                warrantConnectors[0].Dispose();
            if (mainTransmission != null)
                mainTransmission.removeConnectors(SharedObject.Normal.SystemID.WareHouseInOut, warrantConnectors);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        ///This method is used for response to server
        private void responseForOperatedState(OperatedState operatedState)
        {
            String str = Function.OperatedStateStringBuilder(operatedState);
            if (str.Length > 0)
                MessageBox.Show(str);
            //setAccessiable(null);
        }
        ///This method is used for response to server
        private void responseForIdentityCheck(Identity.IdentityCheckForm identityCheckForm)
        {
            identityCheckForm.Show();
        }

        //This method is called draftChooseForm
        protected virtual void warrantDraftChooseForm_Add(DefaultDraft draft) { }

        protected virtual void editOkButton_Click(object sender, EventArgs e)
        {
            if (warrantDraftChooseForm != null)
                warrantDraftChooseForm.Close();
            editingState = EditingState.Normal;
            warrantDetailBox.readOnly = true;
        }

        protected virtual void editCancelButton_Click(object sender, EventArgs e)
        {
            if (warrantDraftChooseForm != null)
                warrantDraftChooseForm.Close();
            editingState = EditingState.Normal;
            warrantDetailBox.readOnly = true;
        }

        private void addItemButton_Click(object sender, EventArgs e)
        {
            if (warrantDraftChooseForm != null)
            {
                warrantDraftChooseForm.Close();
                warrantDraftChooseForm.Dispose();
            }
            warrantDraftChooseForm = new WarrantDraftChooseForm(mainTransmission);
            warrantDraftChooseForm.Add += new WarrantDraftChooseForm.AddEventHandler(warrantDraftChooseForm_Add);
            warrantDraftChooseForm.Show();
        }

        //制单人按钮
        protected virtual void addNewButton_Click(object sender, EventArgs e) { }

        protected virtual void editButton_Click(object sender, EventArgs e) { }

        protected virtual void cancelButton_Click(object sender, EventArgs e) { }

        protected string find_customer_name(List<DefaultCustomer> list, int? id)
        {
            foreach (DefaultCustomer customer in list)
            {
                if (customer.id == id)
                {
                    return customer.name;
                }
            }
            return string.Empty;
        }
    }
}
