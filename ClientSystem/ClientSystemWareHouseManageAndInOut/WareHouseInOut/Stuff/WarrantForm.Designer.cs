﻿namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class WarrantForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.warrantsDisplayListBox = new System.Windows.Forms.ListBox();
            this.warrantsDisplayListBoxGroupBox = new System.Windows.Forms.GroupBox();
            this.viewOneCycleFormButton = new System.Windows.Forms.Button();
            this.targetCustomerPicker = new ClientSystem.CustomerManage.Stuff.CustomerPicker();
            this.typeCheckBoxGift = new System.Windows.Forms.CheckBox();
            this.typeCheckBoxService = new System.Windows.Forms.CheckBox();
            this.typeCheckBoxWithoutReceipt = new System.Windows.Forms.CheckBox();
            this.typeCheckBoxWithReceipt = new System.Windows.Forms.CheckBox();
            this.reloadWarrantListButton = new System.Windows.Forms.Button();
            this.stopDatePicker = new System.Windows.Forms.DateTimePicker();
            this.startDatePicker = new System.Windows.Forms.DateTimePicker();
            this.typeCheckBoxAdjustOut = new System.Windows.Forms.CheckBox();
            this.warrantsDisplayListBoxGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // warrantsDisplayListBox
            // 
            this.warrantsDisplayListBox.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.warrantsDisplayListBox.FormattingEnabled = true;
            this.warrantsDisplayListBox.Location = new System.Drawing.Point(11, 22);
            this.warrantsDisplayListBox.Name = "warrantsDisplayListBox";
            this.warrantsDisplayListBox.Size = new System.Drawing.Size(210, 303);
            this.warrantsDisplayListBox.TabIndex = 1;
            // 
            // warrantsDisplayListBoxGroupBox
            // 
            this.warrantsDisplayListBoxGroupBox.Controls.Add(this.viewOneCycleFormButton);
            this.warrantsDisplayListBoxGroupBox.Controls.Add(this.targetCustomerPicker);
            this.warrantsDisplayListBoxGroupBox.Controls.Add(this.typeCheckBoxAdjustOut);
            this.warrantsDisplayListBoxGroupBox.Controls.Add(this.typeCheckBoxGift);
            this.warrantsDisplayListBoxGroupBox.Controls.Add(this.typeCheckBoxService);
            this.warrantsDisplayListBoxGroupBox.Controls.Add(this.typeCheckBoxWithoutReceipt);
            this.warrantsDisplayListBoxGroupBox.Controls.Add(this.typeCheckBoxWithReceipt);
            this.warrantsDisplayListBoxGroupBox.Controls.Add(this.reloadWarrantListButton);
            this.warrantsDisplayListBoxGroupBox.Controls.Add(this.stopDatePicker);
            this.warrantsDisplayListBoxGroupBox.Controls.Add(this.startDatePicker);
            this.warrantsDisplayListBoxGroupBox.Controls.Add(this.warrantsDisplayListBox);
            this.warrantsDisplayListBoxGroupBox.Location = new System.Drawing.Point(7, 1);
            this.warrantsDisplayListBoxGroupBox.Name = "warrantsDisplayListBoxGroupBox";
            this.warrantsDisplayListBoxGroupBox.Size = new System.Drawing.Size(233, 491);
            this.warrantsDisplayListBoxGroupBox.TabIndex = 0;
            this.warrantsDisplayListBoxGroupBox.TabStop = false;
            // 
            // viewOneCycleFormButton
            // 
            this.viewOneCycleFormButton.Location = new System.Drawing.Point(10, 460);
            this.viewOneCycleFormButton.Name = "viewOneCycleFormButton";
            this.viewOneCycleFormButton.Size = new System.Drawing.Size(115, 24);
            this.viewOneCycleFormButton.TabIndex = 0;
            this.viewOneCycleFormButton.Text = "显示单个周期";
            this.viewOneCycleFormButton.UseVisualStyleBackColor = true;
            this.viewOneCycleFormButton.Click += new System.EventHandler(this.viewOneCycleFormButton_Click);
            // 
            // targetCustomerPicker
            // 
            this.targetCustomerPicker.customerList = null;
            this.targetCustomerPicker.Location = new System.Drawing.Point(12, 342);
            this.targetCustomerPicker.Name = "targetCustomerPicker";
            this.targetCustomerPicker.Size = new System.Drawing.Size(114, 27);
            this.targetCustomerPicker.TabIndex = 1;
            // 
            // typeCheckBoxGift
            // 
            this.typeCheckBoxGift.AutoSize = true;
            this.typeCheckBoxGift.Location = new System.Drawing.Point(154, 434);
            this.typeCheckBoxGift.Name = "typeCheckBoxGift";
            this.typeCheckBoxGift.Size = new System.Drawing.Size(50, 17);
            this.typeCheckBoxGift.TabIndex = 7;
            this.typeCheckBoxGift.Text = "折让";
            this.typeCheckBoxGift.UseVisualStyleBackColor = true;
            // 
            // typeCheckBoxService
            // 
            this.typeCheckBoxService.AutoSize = true;
            this.typeCheckBoxService.Location = new System.Drawing.Point(154, 388);
            this.typeCheckBoxService.Name = "typeCheckBoxService";
            this.typeCheckBoxService.Size = new System.Drawing.Size(50, 17);
            this.typeCheckBoxService.TabIndex = 7;
            this.typeCheckBoxService.Text = "三包";
            this.typeCheckBoxService.UseVisualStyleBackColor = true;
            // 
            // typeCheckBoxWithoutReceipt
            // 
            this.typeCheckBoxWithoutReceipt.AutoSize = true;
            this.typeCheckBoxWithoutReceipt.Location = new System.Drawing.Point(154, 365);
            this.typeCheckBoxWithoutReceipt.Name = "typeCheckBoxWithoutReceipt";
            this.typeCheckBoxWithoutReceipt.Size = new System.Drawing.Size(38, 17);
            this.typeCheckBoxWithoutReceipt.TabIndex = 5;
            this.typeCheckBoxWithoutReceipt.Text = "无";
            this.typeCheckBoxWithoutReceipt.UseVisualStyleBackColor = true;
            // 
            // typeCheckBoxWithReceipt
            // 
            this.typeCheckBoxWithReceipt.AutoSize = true;
            this.typeCheckBoxWithReceipt.Location = new System.Drawing.Point(154, 342);
            this.typeCheckBoxWithReceipt.Name = "typeCheckBoxWithReceipt";
            this.typeCheckBoxWithReceipt.Size = new System.Drawing.Size(38, 17);
            this.typeCheckBoxWithReceipt.TabIndex = 4;
            this.typeCheckBoxWithReceipt.Text = "有";
            this.typeCheckBoxWithReceipt.UseVisualStyleBackColor = true;
            // 
            // reloadWarrantListButton
            // 
            this.reloadWarrantListButton.Location = new System.Drawing.Point(10, 434);
            this.reloadWarrantListButton.Name = "reloadWarrantListButton";
            this.reloadWarrantListButton.Size = new System.Drawing.Size(115, 24);
            this.reloadWarrantListButton.TabIndex = 8;
            this.reloadWarrantListButton.Text = "加载列表";
            this.reloadWarrantListButton.UseVisualStyleBackColor = true;
            this.reloadWarrantListButton.Click += new System.EventHandler(this.reloadWarrantListButton_Click);
            // 
            // stopDatePicker
            // 
            this.stopDatePicker.Location = new System.Drawing.Point(12, 405);
            this.stopDatePicker.Name = "stopDatePicker";
            this.stopDatePicker.Size = new System.Drawing.Size(115, 20);
            this.stopDatePicker.TabIndex = 3;
            // 
            // startDatePicker
            // 
            this.startDatePicker.Location = new System.Drawing.Point(11, 376);
            this.startDatePicker.Name = "startDatePicker";
            this.startDatePicker.Size = new System.Drawing.Size(115, 20);
            this.startDatePicker.TabIndex = 2;
            // 
            // typeCheckBoxAdjustOut
            // 
            this.typeCheckBoxAdjustOut.AutoSize = true;
            this.typeCheckBoxAdjustOut.Location = new System.Drawing.Point(154, 411);
            this.typeCheckBoxAdjustOut.Name = "typeCheckBoxAdjustOut";
            this.typeCheckBoxAdjustOut.Size = new System.Drawing.Size(50, 17);
            this.typeCheckBoxAdjustOut.TabIndex = 7;
            this.typeCheckBoxAdjustOut.Text = "调整";
            this.typeCheckBoxAdjustOut.UseVisualStyleBackColor = true;
            // 
            // WarrantForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 501);
            this.Controls.Add(this.warrantsDisplayListBoxGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "WarrantForm";
            this.Text = "WarrantForm";
            this.warrantsDisplayListBoxGroupBox.ResumeLayout(false);
            this.warrantsDisplayListBoxGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.ListBox warrantsDisplayListBox;
        private System.Windows.Forms.GroupBox warrantsDisplayListBoxGroupBox;
        protected System.Windows.Forms.DateTimePicker stopDatePicker;
        protected System.Windows.Forms.DateTimePicker startDatePicker;
        protected System.Windows.Forms.Button reloadWarrantListButton;
        protected System.Windows.Forms.CheckBox typeCheckBoxService;
        protected System.Windows.Forms.CheckBox typeCheckBoxWithoutReceipt;
        protected System.Windows.Forms.CheckBox typeCheckBoxWithReceipt;
        protected CustomerManage.Stuff.CustomerPicker targetCustomerPicker;
        private System.Windows.Forms.Button viewOneCycleFormButton;
        protected System.Windows.Forms.CheckBox typeCheckBoxGift;
        protected System.Windows.Forms.CheckBox typeCheckBoxAdjustOut;
    }
}