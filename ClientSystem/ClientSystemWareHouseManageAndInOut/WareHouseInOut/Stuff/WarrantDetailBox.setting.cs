﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClassObjectToNum;
using ClassObjectToNum.DateTime;
using SharedObject.WareHouseInOut.Stuff;
using ClassObjectToNum.Money;

namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class WarrantDetailBox
    {
        private void setVisible()
        {
            if (_warrant != null)
                setVisible(_warrant.state);
            else
                setVisible(null);
        }
        private void setVisible(AbstractWarrant.WarrantState? state)
        {
            bool priceInV = false, priceAdjustV = false, numReturnV = false, amountPermitV = false, amountPaidV = false, priceAllV = false;
            if (state != null)
            {
                priceAllV = true;
                if (_warrant is InWarrant)
                {
                    amountPermitV = false;
                }
                else if (_warrant is OutWarrant)
                {
                    if (((OutWarrant)_warrant).amountPermit != null)
                        amountPermitV = true;
                    else
                        amountPermitV = false;
                    if (((OutWarrant)_warrant).amountPaid != null)
                        amountPaidV = true;
                    else
                        amountPaidV = false;
                    priceAdjustV = true;
                    switch (state)
                    {
                        case AbstractWarrant.WarrantState.Completed:
                        case AbstractWarrant.WarrantState.Submitted:
                        case AbstractWarrant.WarrantState.Printed:
                        case AbstractWarrant.WarrantState.ForceCompleted:
                        case AbstractWarrant.WarrantState.CompletedAndPaid:
                            priceInV = true;
                            numReturnV = true;
                            break;
                        case AbstractWarrant.WarrantState.TakenOut:
                        case AbstractWarrant.WarrantState.Returned:
                            numReturnV = true;
                            break;
                        case AbstractWarrant.WarrantState.SubmittedCustomerPage:
                        case AbstractWarrant.WarrantState.PrintedCustomerPage:
                        case AbstractWarrant.WarrantState.Abnormal:
                        case AbstractWarrant.WarrantState.Normal:
                            break;
                    }
                }
                if (!_visiblePriceIn)
                {
                    priceInV = false;
                }
            }
            itemGridView.Columns["PriceIn"].Visible = priceInV;
            itemGridView.Columns["PriceInAll"].Visible = priceInV;
            totalAmountCostLabel.Visible = priceInV;
            amountPermitLabel.Visible = amountPermitV;
            amountPaidLabel.Visible = amountPaidV;
            totalAmountLabel.Visible = priceAllV;
            itemGridView.Columns["PriceAdjust"].Visible = priceAdjustV;
            itemGridView.Columns["numReturn"].Visible = numReturnV;
        }

        private void setAccessiable()
        {
            bool numR = true;
            bool numReturnR = true;
            bool priceR = true;
            bool priceAdjustR = true;
            bool remarkR = true;

            bool targetE = false;
            bool attendantE = false;
            bool typeButtonInE = false;
            bool typeButtonOutE = false;
            bool commentE = false;

            //we disable price adjust function
            priceAdjustR = false;

            if (_warrant != null && (!_readOnly))
            {
                if (_warrant is InWarrant)
                {
                    switch (_warrant.state)
                    {
                        case AbstractWarrant.WarrantState.Normal:
                            numR = false;
                            priceR = false;
                            remarkR = false;
                            targetE = true;
                            attendantE = false;
                            commentE = true;
                            typeButtonInE = true;
                            break;
                        case AbstractWarrant.WarrantState.Printed:
                        case AbstractWarrant.WarrantState.Submitted:
                        case AbstractWarrant.WarrantState.Completed:
                        case AbstractWarrant.WarrantState.Abnormal:
                            break;
                    }
                }
                else if (_warrant is OutWarrant)
                {
                    switch (_warrant.state)
                    {
                        case AbstractWarrant.WarrantState.Abnormal:
                        case AbstractWarrant.WarrantState.Completed:
                        case AbstractWarrant.WarrantState.Printed:
                        case AbstractWarrant.WarrantState.Submitted:
                        case AbstractWarrant.WarrantState.Cancelled:
                        case AbstractWarrant.WarrantState.PrintedCustomerPage:
                        case AbstractWarrant.WarrantState.SubmittedCustomerPage:
                            break;
                        case AbstractWarrant.WarrantState.TakenOut:
                        case AbstractWarrant.WarrantState.Returned:
                            numReturnR = false;
                            attendantE = true;
                            break;
                        case AbstractWarrant.WarrantState.Normal:
                            priceAdjustR = false;
                            numR = false;
                            remarkR = false;
                            targetE = true;
                            attendantE = true;
                            typeButtonOutE = true;
                            commentE = true;
                            break;
                    }
                }
            }
            itemGridView.Columns["Price"].ReadOnly = priceR;
            itemGridView.Columns["PriceAdjust"].ReadOnly = priceAdjustR;
            itemGridView.Columns["num"].ReadOnly = numR;
            itemGridView.Columns["numReturn"].ReadOnly = numReturnR;
            itemGridView.Columns["Remark"].ReadOnly = remarkR;

            typeRadioButtonWithoutReceipt.Enabled = typeButtonOutE;
            typeRadioButtonWithReceipt.Enabled = typeButtonOutE;
            typeRadioButtonService.Enabled = typeButtonOutE;
            typeRadioButtonAdjustOut.Enabled = typeButtonOutE;
            typeRadioButtonGift.Enabled = typeButtonOutE;

            typeRadioButtonIn.Enabled = typeButtonInE;
            typeRadioButtonAdjustIn.Enabled = typeButtonInE;

            targetPicker.Enabled = targetE;
            foreach (AttendantPicker picker in this.attendantPicker)
                picker.Enabled = attendantE;
            CommentTextBox.Enabled = commentE;
        }

        private void itemGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (_warrant is InWarrant)
            {
                int columnIndex = itemGridView.CurrentCell.ColumnIndex;
                string columnName = itemGridView.Columns[columnIndex].Name;
                int rowIndex;
                rowIndex = itemGridView.CurrentCell.RowIndex;
                DataGridViewRow row = itemGridView.Rows[rowIndex];
                bool t1, t2;
                int price, num;
                InWarrant inWarrant = (InWarrant)_warrant;
                switch (columnName)
                {
                    case "Price":
                        t1 = ClassMoney.TryMoneyStringToNum(row.Cells[columnIndex].Value.ToString(), out price);
                        if (t1 && price >= 0)
                            inWarrant.items[rowIndex].price = price;
                        else
                        {
                            row.Cells[columnIndex].Value = 0;
                            MessageBox.Show("数据格式不合法");
                        }
                        break;
                    case "num":
                        string num_column = row.Cells[columnIndex].Value.ToString();
                        int unit_start = num_column.IndexOf("(");
                        if (unit_start >= 0)
                            num_column = num_column.Substring(0, unit_start);
                        t2 = Int32.TryParse(num_column, out num);
                        if (t2 && num >= 0)
                            inWarrant.items[rowIndex].num = num;
                        else
                        {
                            row.Cells[columnIndex].Value = 0;
                            MessageBox.Show("数据格式不合法");
                        }
                        break;
                    case "Remark":
                        inWarrant.items[rowIndex].remark = row.Cells[columnIndex].Value.ToString();
                        break;
                }
                if (columnName == "Price" || columnName == "num")
                {
                    t1 = ClassMoney.TryMoneyStringToNum(row.Cells["Price"].Value.ToString(), out price);
                    t2 = Int32.TryParse(row.Cells["num"].Value.ToString(), out num);
                    if (t1 && t2)
                        row.Cells["PriceAll"].Value = ClassMoney.MoneyNumToDisplayString(price * num);
                }
            }
            else if (_warrant is OutWarrant)
            {
                int columnIndex = itemGridView.CurrentCell.ColumnIndex;
                string columnName = itemGridView.Columns[columnIndex].Name;
                int rowIndex;
                rowIndex = itemGridView.CurrentCell.RowIndex;
                DataGridViewRow row = itemGridView.Rows[rowIndex];
                bool t1, t2, t3, t4;
                object obj2, obj3, obj4;
                int price, num, priceAdjust, numReturn;
                OutWarrant outWarrant = (OutWarrant)_warrant;
                switch (columnName)
                {
                    case "PriceAdjust":
                        obj3 = row.Cells[columnIndex].Value;
                        t3 = ClassMoney.TryMoneyStringToNum(obj3.ToString(), out priceAdjust);
                        if (t3 && priceAdjust >= 0)
                            outWarrant.items[rowIndex].priceAdjustMannual = priceAdjust;
                        else
                        {
                            row.Cells[columnIndex].Value = 0;
                            MessageBox.Show("数据格式不合法");
                        }
                        break;
                    case "num":
                        string num_column = row.Cells[columnIndex].Value.ToString();
                        int unit_start = num_column.IndexOf("(");
                        if (unit_start >= 0)
                            num_column = num_column.Substring(0, unit_start);
                        t2 = Int32.TryParse(num_column, out num);
                        if (t2 && num >= 0)
                            outWarrant.items[rowIndex].num = num;
                        else
                        {
                            row.Cells[columnIndex].Value = 0;
                            MessageBox.Show("数据格式不合法");
                        }
                        break;
                    case "numReturn":
                        obj2 = row.Cells["numReturn"].Value;
                        t2 = Int32.TryParse(obj2.ToString(), out num);
                        obj4 = row.Cells[columnIndex].Value;
                        t4 = Int32.TryParse(obj4.ToString(), out numReturn);
                        if (t2 && t4 && numReturn >= 0 && numReturn <= num)
                            outWarrant.items[rowIndex].numReturn = numReturn;
                        else
                        {
                            row.Cells[columnIndex].Value = 0;
                            MessageBox.Show("数据格式不合法");
                        }
                        break;
                    case "Remark":
                        outWarrant.items[rowIndex].remark = row.Cells[columnIndex].Value.ToString();
                        break;
                }
                if (columnName == "PriceAdjust" || columnName == "num" || columnName == "numReturn")
                {
                    t1 = ClassMoney.TryMoneyStringToNum(row.Cells["Price"].Value.ToString(), out price);
                    t2 = Int32.TryParse(row.Cells["num"].Value.ToString(), out num);
                    t3 = ClassMoney.TryMoneyStringToNum(row.Cells["PriceAdjust"].Value.ToString(), out priceAdjust);
                    t4 = Int32.TryParse(row.Cells["numReturn"].Value.ToString(), out numReturn);
                    if (t1 && t2 && t3 & t4)
                    {
                        if (priceAdjust == 0)
                            priceAdjust = price;
                        row.Cells["PriceAll"].Value = ClassMoney.MoneyNumToDisplayString(priceAdjust * (num - numReturn));
                    }
                }
            }
        }
    }
}
