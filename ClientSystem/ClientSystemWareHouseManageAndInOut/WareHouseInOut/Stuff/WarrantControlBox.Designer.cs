﻿namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class WarrantControlBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addNewButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.editCancelButton = new System.Windows.Forms.Button();
            this.editOkButton = new System.Windows.Forms.Button();
            this.delItemButton = new System.Windows.Forms.Button();
            this.addItemButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // addNewButton
            // 
            this.addNewButton.Location = new System.Drawing.Point(17, 8);
            this.addNewButton.Name = "addNewButton";
            this.addNewButton.Size = new System.Drawing.Size(82, 23);
            this.addNewButton.TabIndex = 0;
            this.addNewButton.Text = "新建";
            this.addNewButton.UseVisualStyleBackColor = true;
            this.addNewButton.Click += new System.EventHandler(this.addNewButton_Click);
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(105, 8);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(82, 23);
            this.editButton.TabIndex = 1;
            this.editButton.Text = "编辑";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(193, 8);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(82, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "作废";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // editCancelButton
            // 
            this.editCancelButton.Location = new System.Drawing.Point(811, 8);
            this.editCancelButton.Name = "editCancelButton";
            this.editCancelButton.Size = new System.Drawing.Size(82, 23);
            this.editCancelButton.TabIndex = 15;
            this.editCancelButton.Text = "放弃";
            this.editCancelButton.UseVisualStyleBackColor = true;
            this.editCancelButton.Click += new System.EventHandler(this.editCancelButton_Click);
            // 
            // editOkButton
            // 
            this.editOkButton.Location = new System.Drawing.Point(723, 8);
            this.editOkButton.Name = "editOkButton";
            this.editOkButton.Size = new System.Drawing.Size(82, 23);
            this.editOkButton.TabIndex = 14;
            this.editOkButton.Text = "确定";
            this.editOkButton.UseVisualStyleBackColor = true;
            this.editOkButton.Click += new System.EventHandler(this.editOkButton_Click);
            // 
            // delItemButton
            // 
            this.delItemButton.Location = new System.Drawing.Point(635, 8);
            this.delItemButton.Name = "delItemButton";
            this.delItemButton.Size = new System.Drawing.Size(82, 23);
            this.delItemButton.TabIndex = 13;
            this.delItemButton.Text = "删件";
            this.delItemButton.UseVisualStyleBackColor = true;
            // 
            // addItemButton
            // 
            this.addItemButton.Location = new System.Drawing.Point(547, 8);
            this.addItemButton.Name = "addItemButton";
            this.addItemButton.Size = new System.Drawing.Size(82, 23);
            this.addItemButton.TabIndex = 12;
            this.addItemButton.Text = "加件";
            this.addItemButton.UseVisualStyleBackColor = true;
            this.addItemButton.Click += new System.EventHandler(this.addItemButton_Click);
            // 
            // WarrantControlBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.editCancelButton);
            this.Controls.Add(this.editOkButton);
            this.Controls.Add(this.delItemButton);
            this.Controls.Add(this.addItemButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.addNewButton);
            this.MaximumSize = new System.Drawing.Size(910, 453);
            this.MinimumSize = new System.Drawing.Size(910, 453);
            this.Name = "WarrantControlBox";
            this.Size = new System.Drawing.Size(910, 453);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Button addNewButton;
        protected System.Windows.Forms.Button editButton;
        protected System.Windows.Forms.Button cancelButton;
        protected System.Windows.Forms.Button delItemButton;
        protected System.Windows.Forms.Button addItemButton;
        protected System.Windows.Forms.Button editCancelButton;
        protected System.Windows.Forms.Button editOkButton;
    }
}
