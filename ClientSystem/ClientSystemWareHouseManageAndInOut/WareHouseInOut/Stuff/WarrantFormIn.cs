﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.WareHouseManage.Transmission;

namespace ClientSystem.WareHouseInOut.Stuff
{
    public partial class WarrantFormIn : WarrantForm
    {
        protected WarrantControlBoxIn warrantControlBox;

        public WarrantFormIn()
            : base ()
        {
            warrantControlBox = new WarrantControlBoxIn(this.mainTransmission);
            warrantControlBox.Location = new Point(245, 10);
            warrantControlBox.allowUserToAddNew = true;
            this.Controls.Add(warrantControlBox);
            warrantControlBox.updateWarrantDisplayList += new WarrantControlBoxIn.UpdateWarrantDisplayListEventHandler(updateWarrantDisplayList);
            this.Text = "入库管理";
            this.warrantControlBox.DoubleClick += new WarrantDetailBox.DDoubleClick(warrantControlBox_DoubleClick);
        }

        protected override void WarrantForm_Load(object sender, EventArgs e)
        {
            CustomerManage.Connector.CustomerConnector connector = new CustomerManage.Connector.CustomerConnector(Instruction.customerInSourceList, mainTransmission, customerConnectors);
            connector.ECustomerList += ECustomersList;
            connector.getWarrantCustomersList();
            getInWarrantList();
        }

        protected override void reloadWarrantListButton_Click(object sender, EventArgs e)
        {
            getInWarrantList();
        }

        private void getInWarrantList()
        {
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.inWarrantList, mainTransmission, warrantConnectors);
            warrantConnector.EWarrantList += EWarrantList;
            int target = 0;
            SharedObject.CustomerManage.Stuff.DefaultCustomer targetCustomer = targetCustomerPicker.getChosenCustomer();
            if (targetCustomer != null)
                target = targetCustomer.id;
            warrantConnector.getWarrantList(WarrantType.In, startDatePicker.Value, stopDatePicker.Value, target);
        }

        protected override void warrantsDisplayListBox_Click(object sender, EventArgs e)
        {
            DefaultWarrant defaultWarrant = (DefaultWarrant)warrantsDisplayListBox.SelectedItem;
            if (defaultWarrant != null)
                warrantControlBox.getSpecialOneWarrant(defaultWarrant.ID);
        }
    }
}
