﻿namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class WarrantControlBoxOut
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.payCheckButton = new System.Windows.Forms.Button();
            this.printSecondReturnButton = new System.Windows.Forms.Button();
            this.forcePassButton = new System.Windows.Forms.Button();
            this.returnPayCheckButton = new System.Windows.Forms.Button();
            this.submitSecondReturnButton = new System.Windows.Forms.Button();
            this.secondReturnButton = new System.Windows.Forms.Button();
            this.printButton = new System.Windows.Forms.Button();
            this.submitButton = new System.Windows.Forms.Button();
            this.discountButton = new System.Windows.Forms.Button();
            this.takeOutButton = new System.Windows.Forms.Button();
            this.returnButton = new System.Windows.Forms.Button();
            this.submitCustomerPageButton = new System.Windows.Forms.Button();
            this.printCustomerPageButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // delItemButton
            // 
            this.delItemButton.Location = new System.Drawing.Point(809, 8);
            // 
            // addItemButton
            // 
            this.addItemButton.Location = new System.Drawing.Point(721, 8);
            // 
            // editCancelButton
            // 
            this.editCancelButton.Location = new System.Drawing.Point(809, 37);
            // 
            // editOkButton
            // 
            this.editOkButton.Location = new System.Drawing.Point(721, 37);
            // 
            // payCheckButton
            // 
            this.payCheckButton.Location = new System.Drawing.Point(17, 37);
            this.payCheckButton.Name = "payCheckButton";
            this.payCheckButton.Size = new System.Drawing.Size(82, 23);
            this.payCheckButton.TabIndex = 32;
            this.payCheckButton.Text = "付款";
            this.payCheckButton.UseVisualStyleBackColor = true;
            this.payCheckButton.Click += new System.EventHandler(this.payCheckButton_Click);
            // 
            // printSecondReturnButton
            // 
            this.printSecondReturnButton.Enabled = false;
            this.printSecondReturnButton.Location = new System.Drawing.Point(633, 8);
            this.printSecondReturnButton.Name = "printSecondReturnButton";
            this.printSecondReturnButton.Size = new System.Drawing.Size(82, 23);
            this.printSecondReturnButton.TabIndex = 42;
            this.printSecondReturnButton.Text = "打印退货单";
            this.printSecondReturnButton.UseVisualStyleBackColor = true;
            // 
            // forcePassButton
            // 
            this.forcePassButton.Location = new System.Drawing.Point(457, 8);
            this.forcePassButton.Name = "forcePassButton";
            this.forcePassButton.Size = new System.Drawing.Size(82, 23);
            this.forcePassButton.TabIndex = 41;
            this.forcePassButton.Text = "强制出库";
            this.forcePassButton.UseVisualStyleBackColor = true;
            this.forcePassButton.Click += new System.EventHandler(this.forcePassButton_Click);
            // 
            // returnPayCheckButton
            // 
            this.returnPayCheckButton.Enabled = false;
            this.returnPayCheckButton.Location = new System.Drawing.Point(193, 37);
            this.returnPayCheckButton.Name = "returnPayCheckButton";
            this.returnPayCheckButton.Size = new System.Drawing.Size(82, 23);
            this.returnPayCheckButton.TabIndex = 43;
            this.returnPayCheckButton.Text = "退款";
            this.returnPayCheckButton.UseVisualStyleBackColor = true;
            this.returnPayCheckButton.Click += new System.EventHandler(this.returnPayCheckButton_Click);
            // 
            // submitSecondReturnButton
            // 
            this.submitSecondReturnButton.Enabled = false;
            this.submitSecondReturnButton.Location = new System.Drawing.Point(633, 37);
            this.submitSecondReturnButton.Name = "submitSecondReturnButton";
            this.submitSecondReturnButton.Size = new System.Drawing.Size(82, 23);
            this.submitSecondReturnButton.TabIndex = 45;
            this.submitSecondReturnButton.Text = "退货提交";
            this.submitSecondReturnButton.UseVisualStyleBackColor = true;
            // 
            // secondReturnButton
            // 
            this.secondReturnButton.Enabled = false;
            this.secondReturnButton.Location = new System.Drawing.Point(545, 37);
            this.secondReturnButton.Name = "secondReturnButton";
            this.secondReturnButton.Size = new System.Drawing.Size(82, 23);
            this.secondReturnButton.TabIndex = 44;
            this.secondReturnButton.Text = "出库后退货";
            this.secondReturnButton.UseVisualStyleBackColor = true;
            // 
            // printButton
            // 
            this.printButton.Location = new System.Drawing.Point(545, 8);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(82, 23);
            this.printButton.TabIndex = 40;
            this.printButton.Text = "打印出库单";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(457, 37);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(82, 23);
            this.submitButton.TabIndex = 39;
            this.submitButton.Text = "退货提交";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // discountButton
            // 
            this.discountButton.Location = new System.Drawing.Point(105, 37);
            this.discountButton.Name = "discountButton";
            this.discountButton.Size = new System.Drawing.Size(82, 23);
            this.discountButton.TabIndex = 36;
            this.discountButton.Text = "下浮";
            this.discountButton.UseVisualStyleBackColor = true;
            this.discountButton.Click += new System.EventHandler(this.discountButton_Click);
            // 
            // takeOutButton
            // 
            this.takeOutButton.Location = new System.Drawing.Point(281, 37);
            this.takeOutButton.Name = "takeOutButton";
            this.takeOutButton.Size = new System.Drawing.Size(82, 23);
            this.takeOutButton.TabIndex = 37;
            this.takeOutButton.Text = "出库";
            this.takeOutButton.UseVisualStyleBackColor = true;
            this.takeOutButton.Click += new System.EventHandler(this.takeOutButton_Click);
            // 
            // returnButton
            // 
            this.returnButton.Location = new System.Drawing.Point(369, 37);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(82, 23);
            this.returnButton.TabIndex = 38;
            this.returnButton.Text = "出库前退货";
            this.returnButton.UseVisualStyleBackColor = true;
            this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // submitCustomerPageButton
            // 
            this.submitCustomerPageButton.Location = new System.Drawing.Point(281, 8);
            this.submitCustomerPageButton.Name = "submitCustomerPageButton";
            this.submitCustomerPageButton.Size = new System.Drawing.Size(82, 23);
            this.submitCustomerPageButton.TabIndex = 33;
            this.submitCustomerPageButton.Text = "提交";
            this.submitCustomerPageButton.UseVisualStyleBackColor = true;
            this.submitCustomerPageButton.Click += new System.EventHandler(this.submitCustomerPageButton_Click);
            // 
            // printCustomerPageButton
            // 
            this.printCustomerPageButton.Location = new System.Drawing.Point(369, 8);
            this.printCustomerPageButton.Name = "printCustomerPageButton";
            this.printCustomerPageButton.Size = new System.Drawing.Size(82, 23);
            this.printCustomerPageButton.TabIndex = 34;
            this.printCustomerPageButton.Text = "打印销货单";
            this.printCustomerPageButton.UseVisualStyleBackColor = true;
            this.printCustomerPageButton.Click += new System.EventHandler(this.printCustomerPageButton_Click);
            // 
            // WarrantControlBoxOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.Controls.Add(this.printSecondReturnButton);
            this.Controls.Add(this.forcePassButton);
            this.Controls.Add(this.returnPayCheckButton);
            this.Controls.Add(this.submitSecondReturnButton);
            this.Controls.Add(this.secondReturnButton);
            this.Controls.Add(this.printButton);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.discountButton);
            this.Controls.Add(this.takeOutButton);
            this.Controls.Add(this.returnButton);
            this.Controls.Add(this.submitCustomerPageButton);
            this.Controls.Add(this.printCustomerPageButton);
            this.Controls.Add(this.payCheckButton);
            this.Name = "WarrantControlBoxOut";
            this.Controls.SetChildIndex(this.addNewButton, 0);
            this.Controls.SetChildIndex(this.editButton, 0);
            this.Controls.SetChildIndex(this.cancelButton, 0);
            this.Controls.SetChildIndex(this.addItemButton, 0);
            this.Controls.SetChildIndex(this.delItemButton, 0);
            this.Controls.SetChildIndex(this.editOkButton, 0);
            this.Controls.SetChildIndex(this.editCancelButton, 0);
            this.Controls.SetChildIndex(this.warrantDetailBox, 0);
            this.Controls.SetChildIndex(this.payCheckButton, 0);
            this.Controls.SetChildIndex(this.printCustomerPageButton, 0);
            this.Controls.SetChildIndex(this.submitCustomerPageButton, 0);
            this.Controls.SetChildIndex(this.returnButton, 0);
            this.Controls.SetChildIndex(this.takeOutButton, 0);
            this.Controls.SetChildIndex(this.discountButton, 0);
            this.Controls.SetChildIndex(this.submitButton, 0);
            this.Controls.SetChildIndex(this.printButton, 0);
            this.Controls.SetChildIndex(this.secondReturnButton, 0);
            this.Controls.SetChildIndex(this.submitSecondReturnButton, 0);
            this.Controls.SetChildIndex(this.returnPayCheckButton, 0);
            this.Controls.SetChildIndex(this.forcePassButton, 0);
            this.Controls.SetChildIndex(this.printSecondReturnButton, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button payCheckButton;
        private System.Windows.Forms.Button printSecondReturnButton;
        private System.Windows.Forms.Button forcePassButton;
        private System.Windows.Forms.Button returnPayCheckButton;
        private System.Windows.Forms.Button submitSecondReturnButton;
        private System.Windows.Forms.Button secondReturnButton;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Button discountButton;
        private System.Windows.Forms.Button takeOutButton;
        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.Button submitCustomerPageButton;
        private System.Windows.Forms.Button printCustomerPageButton;
    }
}
