﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ClientSystem.WareHouseInOut.Stuff
{
    public partial class WarrantControlBoxOut : WarrantControlBox
    {
        //the outWarrant displayed now
        protected OutWarrant warrantNow;

        //delegates and events
        protected Connector.WarrantConnector.DSpecialOneOutWarrant ESpecialOneOutWarrant;
        protected Connector.WarrantConnector.DPrint EPrint;
        protected Connector.WarrantConnector.DOutWarrantPrintCustomerPage EOutWarrantPrintCustomerPage;

        private delegate void DSpecialOneOutWarrant(OutWarrant outWarrant);
        public delegate void UpdateWarrantDisplayListEventHandler(AbstractWarrant abstractWarrant);
        public event UpdateWarrantDisplayListEventHandler updateWarrantDisplayList;

        private PayCheckInputForm payCheckInputForm;
        private ReturnPayCheckInputForm returnPayCheckInputForm;
        private DiscountInputForm discountInputForm;
        private bool ifRefreshWhenInputFormClosing = true;

        public WarrantControlBoxOut(Transmission.Transmission mainTransmission)
            : base(mainTransmission)
        {
            InitializeComponent();

            ESpecialOneOutWarrant = new Connector.WarrantConnector.DSpecialOneOutWarrant(outwarrant => this.Invoke(new DSpecialOneOutWarrant(responseForSpecialOneOutWarrant), new object[] { outwarrant }));
            EPrint = new Connector.WarrantConnector.DPrint(responseForPrint);
            EOutWarrantPrintCustomerPage = new Connector.WarrantConnector.DOutWarrantPrintCustomerPage(responseForOutWarrantPrintCustomerPage);

            this.warrantDetailBox.setStyle(WarrantType.Out);
            warrantNow = new OutWarrant();
            setAccessiable(null);
            this.Text = "出库管理";
        }

        protected override void Dispose(bool disposing)
        {
            if (discountInputForm != null)
                discountInputForm.Dispose();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        
        /// This method is used to show the WarrantList
        protected void responseForSpecialOneOutWarrant(OutWarrant outWarrant)
        {
            if (outWarrant != null && outWarrant.ID > 0)
            {
                warrantNow = outWarrant;
                editingState = EditingState.Normal;
                warrantDetailBox.warrant = warrantNow;
                warrantDetailBox.readOnly = true;
                setAccessiable(warrantNow);
                if (updateWarrantDisplayList != null)
                    updateWarrantDisplayList((AbstractWarrant)warrantNow);
            }
        }
        /// This method is used to show the WarrantList
        /// This method is called by mainTransmission
        protected void responseForPrint(OperatedState instrState)
        {
            if (instrState.state == OperatedState.OperatedStateEnum.success)
            {
                string name = "";
                foreach (DefaultAttendant attendant in warrantNow.getAttendants())
                    name = name + find_attendant_name(warrantDetailBox.warrantAttendantsList, attendant.id) + " ";
                new WarrantPrint(find_customer_name(warrantDetailBox.warrantCustomersList, warrantNow.target), name,
                    warrantNow.operationRecord[0].operationOperatorName).printOutWarrant(warrantNow);
            }
        }
        /// This method is used to show the WarrantList
        /// This method is called by mainTransmission
        protected void responseForOutWarrantPrintCustomerPage(OperatedState instrState)
        {
            if (instrState.state == OperatedState.OperatedStateEnum.success)
            {
                string name = "";
                foreach (DefaultAttendant attendant in warrantNow.getAttendants())
                    name = name + find_attendant_name(warrantDetailBox.warrantAttendantsList, attendant.id) + " ";
                new WarrantPrint(find_customer_name(warrantDetailBox.warrantCustomersList, warrantNow.target), name,
                    warrantNow.operationRecord[0].operationOperatorName).printOutCustomerPage1Warrant(warrantNow);
            }
        }

        /// <summary>
        /// setAccessiable is used to set which columns and which buttons can enabled and which ones shall be disabled according to the state
        /// </summary>
        protected void setAccessiable(OutWarrant warrant)
        {
            AbstractWarrant.WarrantState state;
            OutWarrant.OutWarrantPayState payState;
            if (warrant != null)
            {
                state = warrant.state;
                payState = warrant.payState;
            }
            else
            {
                state = AbstractWarrant.WarrantState.Abnormal;
                payState = OutWarrant.OutWarrantPayState.Abnormal;
            }

            bool addNewE = false, editE = false, cancelE = false, submitCustomerPageE = false, printCustomerPageE = false, forcePassE = false, printE = false;
            bool takeOutE = false, returnE = false, submitE = false, payCheckE = false, returnPayCheckE = false, discountE = false;
            bool addItemE = false, delItemE = false, editOkE = false, editCancelE = false;

            if (editingState == EditingState.Normal)
            {
                addNewE = _addNew;
                switch (state)
                {
                    case DefaultWarrant.WarrantState.Abnormal:
                    case DefaultWarrant.WarrantState.Normal:
                    case DefaultWarrant.WarrantState.Cancelled:
                    case DefaultWarrant.WarrantState.CompletedAndPaid:
                        break;
                    case DefaultWarrant.WarrantState.Completed:
                    case DefaultWarrant.WarrantState.ForceCompleted:
                        payCheckE = true;
                        returnPayCheckE = true;
                        break;
                    default:
                        payCheckE = true;
                        returnPayCheckE = true;
                        discountE = true;
                        break;
                }
                switch (state)
                {
                    case AbstractWarrant.WarrantState.Normal:
                        editE = true;
                        submitCustomerPageE = true;
                        break;
                    case AbstractWarrant.WarrantState.SubmittedCustomerPage:
                        printCustomerPageE = true;
                        break;
                    case AbstractWarrant.WarrantState.PrintedCustomerPage:
                        printCustomerPageE = true;
                        if (payState == OutWarrant.OutWarrantPayState.Paid || payState == OutWarrant.OutWarrantPayState.OverPaid || payState == OutWarrant.OutWarrantPayState.ForcePass)
                        {
                            takeOutE = true;
                        }
                        else
                        {
                            forcePassE = true;
                        }
                        break;
                    case AbstractWarrant.WarrantState.TakenOut:
                    case AbstractWarrant.WarrantState.Returned:
                        submitE = true;
                        returnE = true;
                        break;
                    case AbstractWarrant.WarrantState.Submitted:
                    case AbstractWarrant.WarrantState.Printed:
                        printE = true;
                        break;
                }
                if (warrant != null
                    && warrant.type != DefaultWarrant.SpecificWarrantType.withoutReceipt
                    && warrant.type != DefaultWarrant.SpecificWarrantType.withReceipt)
                {
                    discountE = false;
                    payCheckE = false;
                    returnPayCheckE = false;
                }
            }
            else if (editingState == EditingState.Editing)
            {
                addItemE = true;
                delItemE = true;
                editOkE = true;
                editCancelE = true;
            }
            else if (editingState == EditingState.Returning)
            {
                editOkE = true;
                editCancelE = true;
            }
            addNewButton.Enabled = addNewE;
            editButton.Enabled = editE;
            cancelButton.Enabled = cancelE;
            submitCustomerPageButton.Enabled = submitCustomerPageE;
            printCustomerPageButton.Enabled = printCustomerPageE;
            forcePassButton.Enabled = forcePassE;
            printButton.Enabled = printE;

            takeOutButton.Enabled = takeOutE;
            returnButton.Enabled = returnE;
            submitButton.Enabled = submitE;

            payCheckButton.Enabled = payCheckE;
            returnPayCheckButton.Enabled = returnPayCheckE;
            discountButton.Enabled = discountE;

            addItemButton.Enabled = addItemE;
            delItemButton.Enabled = delItemE;
            editOkButton.Enabled = editOkE;
            editCancelButton.Enabled = editCancelE;
        }


        public void getSpecialOneWarrant(int id)
        {
            if (id <= 0)
                return;
            if (discountInputForm != null)
                discountInputForm.Dispose();
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantSpecialOne, mainTransmission, warrantConnectors);
            warrantConnector.ESpecialOneOutWarrant += ESpecialOneOutWarrant;
            warrantConnector.EOperatedState += EOperateState;
            warrantConnector.getSpecialOneWarrant(WarrantType.Out, id);
        }

        protected override void warrantDraftChooseForm_Add(DefaultDraft draft)
        {
            foreach (OutWarrantItem owi in warrantNow.items)
            {
                if (owi.IDdraft == draft.id)
                {
                    owi.num++;
                    warrantDetailBox.refreshItem();
                    return;
                }
            }
            OutWarrantItem outWarrantItem = new OutWarrantItem();
            outWarrantItem.IDWarrant = warrantNow.ID;
            outWarrantItem.IDdraft = draft.id;
            outWarrantItem.name = draft.name;
            outWarrantItem.specification = draft.specification;
            outWarrantItem.hiddenSpecification = draft.hiddenSpecification;
            outWarrantItem.priceOutH = draft.priceOutH;
            outWarrantItem.priceOutL = draft.priceOutL;
            if (warrantNow.type == DefaultWarrant.SpecificWarrantType.withoutReceipt)
                outWarrantItem.price = outWarrantItem.priceOutL;
            else
                outWarrantItem.price = outWarrantItem.priceOutH;
            outWarrantItem.num = 1;
            warrantNow.items.Add(outWarrantItem);
            if (warrantNow.target != null)
                warrantDetailBox.refreshSpecialPrice();
            else
                warrantDetailBox.refreshItem();

        }

        protected override void editOkButton_Click(object sender, EventArgs e)
        {
            EditingState editingState = this.editingState;
            base.editOkButton_Click(sender, e);
            setAccessiable(warrantNow);
            Connector.WarrantConnector warrantConnector;
            if (warrantNow.ID > 0 && editingState == EditingState.Editing)
            {
                warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantEdit, mainTransmission, warrantConnectors);
                warrantConnector.EOperatedState += EOperateState;
                warrantConnector.ESpecialOneOutWarrant += ESpecialOneOutWarrant;
                warrantConnector.editOutWarrant(warrantNow);
            }
            else if (editingState == EditingState.Editing)
            {
                warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantAdd, mainTransmission, warrantConnectors);
                warrantConnector.EOperatedState += EOperateState;
                warrantConnector.ESpecialOneOutWarrant += ESpecialOneOutWarrant;
                warrantConnector.addOutWarrant(warrantNow);
            }
            else if (editingState == EditingState.Returning)
            {
                warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantReturn, mainTransmission, warrantConnectors);
                warrantConnector.EOperatedState += EOperateState;
                warrantConnector.EIdentityCheck += EIdentityCheck;
                warrantConnector.ESpecialOneOutWarrant += ESpecialOneOutWarrant;
                warrantConnector.returnWarrant(WarrantType.Out, warrantNow);
            }
        }

        protected override void editCancelButton_Click(object sender, EventArgs e)
        {
            base.editCancelButton_Click(sender, e);
            setAccessiable(null);
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantSpecialOne, mainTransmission, warrantConnectors);
            warrantConnector.ESpecialOneOutWarrant += ESpecialOneOutWarrant;
            warrantConnector.EOperatedState += EOperateState;
            if (warrantNow.ID > 0)
                warrantConnector.getSpecialOneWarrant(WarrantType.Out, warrantNow.ID);
        }

        protected override void addNewButton_Click(object sender, EventArgs e)
        {
            addEditing = true;
            warrantNow = new OutWarrant();
            editingState = EditingState.Editing;
            warrantDetailBox.warrant = warrantNow;
            warrantDetailBox.readOnly = false;
            setAccessiable(warrantNow);
        }

        protected override void editButton_Click(object sender, EventArgs e)
        {
            if (warrantNow != null)
            {
                addEditing = false;
                editingState = EditingState.Editing;
                //warrantDetailBox.warrant = outwarrantNow;
                warrantDetailBox.readOnly = false;
                setAccessiable(warrantNow);
            }
        }

        protected override void cancelButton_Click(object sender, EventArgs e)
        {
            editingState = EditingState.Normal;
            warrantDetailBox.readOnly = true;
            setAccessiable(warrantNow);
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantCancel, mainTransmission, warrantConnectors);
            warrantConnector.EOperatedState += EOperateState;
            warrantConnector.ESpecialOneOutWarrant += ESpecialOneOutWarrant;
            warrantConnector.cancelWarrant(WarrantType.Out, (AbstractWarrant)warrantNow);
        }

        protected void submitCustomerPageButton_Click(object sender, EventArgs e)
        {
            editingState = EditingState.Normal;
            warrantDetailBox.readOnly = true;
            setAccessiable(warrantNow);
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantSubmitCustomerPage, mainTransmission, warrantConnectors);
            warrantConnector.EOperatedState += EOperateState;
            warrantConnector.EIdentityCheck += EIdentityCheck;
            warrantConnector.ESpecialOneOutWarrant += ESpecialOneOutWarrant;
            warrantConnector.submitCustomerPageWarrant(WarrantType.Out, (AbstractWarrant)warrantNow);
        }

        protected void printCustomerPageButton_Click(object sender, EventArgs e)
        {
            editingState = EditingState.Normal;
            warrantDetailBox.readOnly = true;
            setAccessiable(warrantNow);
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantPrintCustomerPage, mainTransmission, warrantConnectors);
            warrantConnector.EOutWarrantPrintCustomerPage += EOutWarrantPrintCustomerPage;
            warrantConnector.EIdentityCheck += EIdentityCheck;
            warrantConnector.ESpecialOneOutWarrant += ESpecialOneOutWarrant;
            warrantConnector.printCustomerPageWarrant(WarrantType.Out, (AbstractWarrant)warrantNow);
        }

        protected void forcePassButton_Click(object sender, EventArgs e)
        {
            editingState = EditingState.Normal;
            warrantDetailBox.readOnly = true;
            setAccessiable(warrantNow);
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantForcePass, mainTransmission, warrantConnectors);
            warrantConnector.EOperatedState += EOperateState;
            warrantConnector.EIdentityCheck += EIdentityCheck;
            warrantConnector.ESpecialOneOutWarrant += ESpecialOneOutWarrant;
            warrantConnector.forcePass(WarrantType.Out, (AbstractWarrant)warrantNow);
        }

        protected void printButton_Click(object sender, EventArgs e)
        {
            editingState = EditingState.Normal;
            warrantDetailBox.readOnly = true;
            setAccessiable(warrantNow);
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantPrint, mainTransmission, warrantConnectors);
            warrantConnector.EPrint += EPrint;
            warrantConnector.EIdentityCheck += EIdentityCheck;
            warrantConnector.ESpecialOneOutWarrant += ESpecialOneOutWarrant;
            warrantConnector.PrintWarrant(WarrantType.Out, (AbstractWarrant)warrantNow);
        }

        protected void payCheckButton_Click(object sender, EventArgs e)
        {
            if (this.warrantNow == null)
            {
                MessageBox.Show("您还没有选择订单");
                return;
            }
            this.ifRefreshWhenInputFormClosing = false;
            if (this.payCheckInputForm != null)
                this.payCheckInputForm.Dispose();
            this.ifRefreshWhenInputFormClosing = true;
            this.payCheckInputForm = new PayCheckInputForm(this.mainTransmission);
            this.payCheckInputForm.init(this.warrantNow, this.warrantNow.amountTotal.Value, this.warrantNow.amountPermit, this.warrantNow.amountPaid);
            this.payCheckInputForm.FormClosing += new FormClosingEventHandler(InputForm_FormClosing);
            this.payCheckInputForm.Show();
        }

        private void returnPayCheckButton_Click(object sender, EventArgs e)
        {
            if (this.warrantNow == null)
            {
                MessageBox.Show("您还没有选择订单");
                return;
            }
            this.ifRefreshWhenInputFormClosing = false;
            if (this.returnPayCheckInputForm != null)
                this.returnPayCheckInputForm.Dispose();
            this.ifRefreshWhenInputFormClosing = true;
            this.returnPayCheckInputForm = new ReturnPayCheckInputForm(this.mainTransmission);
            this.returnPayCheckInputForm.init(this.warrantNow, this.warrantNow.amountTotal.Value, this.warrantNow.amountPermit, this.warrantNow.amountPaid);
            this.returnPayCheckInputForm.FormClosing += new FormClosingEventHandler(InputForm_FormClosing);
            this.returnPayCheckInputForm.Show();
        }

        protected void discountButton_Click(object sender, EventArgs e)
        {
            if (this.warrantNow == null)
            {
                MessageBox.Show("您还没有选择订单");
                return;
            }
            ifRefreshWhenInputFormClosing = false;
            if (discountInputForm != null)
                discountInputForm.Dispose();
            ifRefreshWhenInputFormClosing = true;
            discountInputForm = new DiscountInputForm(mainTransmission);
            discountInputForm.init(this.warrantNow, this.warrantNow.amountTotal.Value, this.warrantNow.amountPermit, this.warrantNow.amountPermitReason);
            discountInputForm.FormClosing += new FormClosingEventHandler(InputForm_FormClosing);
            discountInputForm.Show();
        }

        protected void takeOutButton_Click(object sender, EventArgs e)
        {
            editingState = EditingState.Normal;
            warrantDetailBox.readOnly = true;
            setAccessiable(warrantNow);
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantTakeOut, mainTransmission, warrantConnectors);
            warrantConnector.EOperatedState += EOperateState;
            warrantConnector.EIdentityCheck += EIdentityCheck;
            warrantConnector.ESpecialOneOutWarrant += ESpecialOneOutWarrant;
            warrantConnector.takeOut(WarrantType.Out, (AbstractWarrant)warrantNow);
        }

        protected void returnButton_Click(object sender, EventArgs e)
        {
            editingState = EditingState.Returning;
            warrantDetailBox.readOnly = false;
            setAccessiable(warrantNow);
        }

        protected void submitButton_Click(object sender, EventArgs e)
        {
            editingState = EditingState.Normal;
            warrantDetailBox.readOnly = true;
            setAccessiable(warrantNow);
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantSubmit, mainTransmission, warrantConnectors);
            warrantConnector.EOperatedState += EOperateState;
            warrantConnector.EIdentityCheck += EIdentityCheck;
            warrantConnector.ESpecialOneOutWarrant += ESpecialOneOutWarrant;
            warrantConnector.submitWarrant(WarrantType.Out, (AbstractWarrant)warrantNow);
        }

        private void InputForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ifRefreshWhenInputFormClosing && this.warrantNow != null)
                getSpecialOneWarrant(this.warrantNow.ID);
        }

        protected string find_attendant_name(List<DefaultAttendant> list, int? id)
        {
            foreach (DefaultAttendant attendant in list)
            {
                if (attendant.id == id)
                {
                    return attendant.name;
                }
            }
            return string.Empty;
        }
    }
}
