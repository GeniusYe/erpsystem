﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class ViewDetailForm : Form
    {
        protected DetailAccount detailAccount;
        protected List<DetailAccount.DefaultDetail> warrantList = new List<DetailAccount.DefaultDetail>();

        protected Transmission.Transmission mainTransmission;
        protected SharedObject.Normal.TransmissionCookie cookie = new SharedObject.Normal.TransmissionCookie();
        protected List<Transmission.Connector> warrantConnectors = new List<Transmission.Connector>();

        protected WarrantControlBoxIn warrantControlBoxIn;
        protected WarrantControlBoxOut warrantControlBoxOut;

        protected Connector.WarrantConnector.DViewDetail EViewDetail;
        private delegate void DViewDetail(DetailAccount detailAccount);

        private int draftID;

        public ViewDetailForm(Transmission.Transmission mainTransmission)
        {

            this.mainTransmission = Transmission.Transmission.mainTransmission;

            InitializeComponent();
            detailAccountDisplayGridView.Columns.Add("IDWarrant", "单号");
            detailAccountDisplayGridView.Columns.Add("Time", "时间");
            detailAccountDisplayGridView.Columns.Add("Name", "名称规格");
            detailAccountDisplayGridView.Columns.Add("price", "价格");
            detailAccountDisplayGridView.Columns.Add("num", "数量");
            detailAccountDisplayGridView.Columns.Add("priceInAll", "库值");
            detailAccountDisplayGridView.Columns.Add("numAll", "剩余数量");

            EViewDetail = new Connector.WarrantConnector.DViewDetail(detailAccount => this.Invoke(new DViewDetail(responseForViewDetail), new object[] { detailAccount }));
            if (mainTransmission != null)
                mainTransmission.addConnectors(SharedObject.Normal.SystemID.WareHouseInOut, warrantConnectors);
            else
                throw new Transmission.TransmissionIsNullException();

            warrantControlBoxIn = new WarrantControlBoxIn(this.mainTransmission);
            warrantControlBoxIn.Location = new Point(150, 20);
            warrantControlBoxIn.allowUserToAddNew = false;
            this.Controls.Add(warrantControlBoxIn);
            warrantControlBoxOut = new WarrantControlBoxOut(this.mainTransmission);
            warrantControlBoxOut.Location = new Point(150, 20);
            warrantControlBoxOut.allowUserToAddNew = false;
            this.Controls.Add(warrantControlBoxOut);
            this.FormClosing += new FormClosingEventHandler(WarrantForm_FormClosing);
            this.detailAccountDisplayGridView.Click += new System.EventHandler(this.detailAccountDisplayGridView_Click);
            this.detailAccountDisplayListBoxGroupBox.MouseEnter += new EventHandler(ListBox_MouseEnter);
            this.detailAccountDisplayListBoxGroupBox.MouseLeave += new EventHandler(ListBox_MouseLeave);
            this.detailAccountDisplayGridView.AllowUserToAddRows = false;

            DateTime startDate = DateTime.Now.AddMonths(-1);
            DateTime stopDate = DateTime.Now.AddDays(1);
            this.startDatePicker.Value = startDate;
            this.stopDatePicker.Value = stopDate;

        }
        
        private void ListBox_MouseEnter(object sender, EventArgs e)
        {
            this.detailAccountDisplayListBoxGroupBox.Width = 880;
            this.detailAccountDisplayGridView.Width = 740;
            warrantControlBoxIn.Left = 645;
            warrantControlBoxOut.Left = 645;
        }

        private void ListBox_MouseLeave(object sender, EventArgs e)
        {
            Point ptCursor = Cursor.Position;

            int x = ptCursor.X - this.Location.X - this.detailAccountDisplayListBoxGroupBox.Location.X;
            int y = ptCursor.Y - this.Location.Y - this.detailAccountDisplayListBoxGroupBox.Location.Y - SystemInformation.CaptionHeight;
            if (!detailAccountDisplayListBoxGroupBox.Bounds.Contains(x, y))
            {
                this.detailAccountDisplayListBoxGroupBox.Width = 135;
                this.detailAccountDisplayGridView.Width = 115;
                warrantControlBoxIn.Left = 155;
                warrantControlBoxOut.Left = 155;
            }
        }

        private void WarrantForm_FormClosing(object sender, FormClosingEventArgs args)
        {
            while (warrantConnectors.Count > 0)
                warrantConnectors[0].Dispose();
            mainTransmission.removeConnectors(SharedObject.Normal.SystemID.WareHouseInOut, warrantConnectors);
        }

        private void displayDetailAccount()
        {
            DataGridViewRow row;
            int i = 0;
            if (detailAccountDisplayGridView != null)
            {
                detailAccountDisplayGridView.Rows.Clear();
                int totalIn = 0, totalOut = 0;
                int totalAmountIn = 0, totalAmountOut = 0;
                foreach (DetailAccount.DefaultDetail defaultDetail in warrantList)
                {
                    i++;
                    row = new DataGridViewRow();
                    if (defaultDetail is DetailAccount.InDetail)
                    {
                        totalIn += defaultDetail.num;
                        totalAmountIn += defaultDetail.num * defaultDetail.price;
                        detailAccountDisplayGridView.Rows.Add(new object[] { defaultDetail.number, defaultDetail.dateTime, defaultDetail.name + " -> " + defaultDetail.specification, ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(defaultDetail.price), defaultDetail.num, ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(defaultDetail.currentPriceIn), defaultDetail.currentNumAll });
                    }
                    else if (defaultDetail is DetailAccount.OutDetail)
                    {
                        DetailAccount.OutDetail outDetail = (DetailAccount.OutDetail)defaultDetail;
                        totalOut += outDetail.num - outDetail.numReturn;
                        totalAmountOut += (outDetail.num - outDetail.numReturn) * defaultDetail.price;
                        detailAccountDisplayGridView.Rows.Add(new object[] { defaultDetail.number, defaultDetail.dateTime, defaultDetail.name + " -> " + defaultDetail.specification, ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(defaultDetail.price), "-" + (defaultDetail.num - ((DetailAccount.OutDetail)defaultDetail).numReturn), ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(defaultDetail.currentPriceIn), defaultDetail.currentNumAll });
                    }
                }
            }
        }

        /// This method is used to show the WarrantList
        private void responseForViewDetail(DetailAccount detailAccount)
        {
            this.detailAccount = detailAccount;
            int i = 0, j = 0;
            List<DetailAccount.InDetail> inDetails = detailAccount.inDetails;
            List<DetailAccount.OutDetail> outDetails = detailAccount.outDetails;
            warrantList = new List<DetailAccount.DefaultDetail>();
            while (i < inDetails.Count && j < outDetails.Count)
            {
                if (inDetails[i].dateTime <= outDetails[j].dateTime)
                {
                    warrantList.Add(inDetails[i]);
                    i++;
                }
                else
                {
                    warrantList.Add(outDetails[j]);
                    j++;
                }
            }
            while (i < inDetails.Count)
            {
                warrantList.Add(inDetails[i]);
                i++;
            }
            while (j < outDetails.Count)
            {
                warrantList.Add(outDetails[j]);
                j++;
            }
            displayDetailAccount();
        }

        protected void detailAccountDisplayGridView_Click(object sender, EventArgs e)
        {
            if (detailAccountDisplayGridView.SelectedRows.Count > 0)
            {
                DetailAccount.DefaultDetail defaultWarrant = warrantList[detailAccountDisplayGridView.SelectedRows[0].Index];
                if (defaultWarrant is DetailAccount.InDetail)
                {
                    warrantControlBoxOut.Visible = false;
                    warrantControlBoxIn.Visible = true;
                    warrantControlBoxIn.getSpecialOneWarrant(defaultWarrant.IDWarrant);
                }
                else if (defaultWarrant is DetailAccount.OutDetail)
                {
                    warrantControlBoxIn.Visible = false;
                    warrantControlBoxOut.Visible = true;
                    warrantControlBoxOut.getSpecialOneWarrant(defaultWarrant.IDWarrant);
                }
            }
        }

        private void reloadDetailButton_Click(object sender, EventArgs e)
        {
            getDetailAccount(this.draftID);
        }

        public void getDetailAccount(int id)
        {
            this.draftID = id;
            Connector.WarrantConnector warrantConnector;
            warrantConnector = new Connector.WarrantConnector(Instruction.warrantViewDetail, mainTransmission, warrantConnectors);
            warrantConnector.EViewDetail += EViewDetail;
            warrantConnector.getDetailAccount(id, this.startDatePicker.Value, this.stopDatePicker.Value);
        }
    }
}
