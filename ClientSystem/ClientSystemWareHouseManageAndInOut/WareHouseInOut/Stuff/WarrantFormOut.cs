﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ClientSystem.WareHouseInOut.Stuff
{
    public partial class WarrantFormOut : WarrantForm
    {
        protected WarrantControlBoxOut warrantControlBox;

        public WarrantFormOut()
            : base()
        {
            warrantControlBox = new WarrantControlBoxOut(this.mainTransmission);
            warrantControlBox.Location = new Point(245, 10);
            warrantControlBox.allowUserToAddNew = true; 
            this.Controls.Add(warrantControlBox);
            warrantControlBox.updateWarrantDisplayList += new WarrantControlBoxOut.UpdateWarrantDisplayListEventHandler(updateWarrantDisplayList);
            this.Text = "出库管理";
            typeCheckBoxWithReceipt.Visible = true;
            typeCheckBoxWithoutReceipt.Visible = true;
            typeCheckBoxService.Visible = true;
            typeCheckBoxGift.Visible = true;
            typeCheckBoxAdjustOut.Visible = true;
            typeCheckBoxWithReceipt.Checked = true;
            typeCheckBoxWithoutReceipt.Checked = true;
            typeCheckBoxAdjustOut.Checked = true;
            typeCheckBoxGift.Checked = true;
            typeCheckBoxService.Checked = true;
            typeCheckBoxWithReceipt.CheckedChanged += new EventHandler(typeCheckBox_CheckedChanged);
            typeCheckBoxWithoutReceipt.CheckedChanged += new EventHandler(typeCheckBox_CheckedChanged);
            typeCheckBoxAdjustOut.CheckedChanged += new EventHandler(typeCheckBox_CheckedChanged);
            typeCheckBoxService.CheckedChanged += new EventHandler(typeCheckBox_CheckedChanged);
            typeCheckBoxGift.CheckedChanged += new EventHandler(typeCheckBox_CheckedChanged);
            this.warrantControlBox.DoubleClick += new WarrantDetailBox.DDoubleClick(warrantControlBox_DoubleClick);
        }

        protected override void WarrantForm_Load(object sender, EventArgs e)
        {
            CustomerManage.Connector.CustomerConnector connector = new CustomerManage.Connector.CustomerConnector(Instruction.customerOutCustomerList, mainTransmission, customerConnectors);
            connector.ECustomerList += ECustomersList;
            connector.getWarrantCustomersList();
            getOutWarrantList();
        }

        protected override void reloadWarrantListButton_Click(object sender, EventArgs e) 
        {
            getOutWarrantList();
        }

        private void getOutWarrantList()
        {
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.outWarrantList, mainTransmission, warrantConnectors);
            warrantConnector.EWarrantList += EWarrantList;
            int target = 0;
            SharedObject.CustomerManage.Stuff.DefaultCustomer targetCustomer = targetCustomerPicker.getChosenCustomer();
            if (targetCustomer != null)
                target = targetCustomer.id;
            warrantConnector.getWarrantList(WarrantType.Out, startDatePicker.Value, stopDatePicker.Value, target);
        }

        protected override void warrantsDisplayListBox_Click(object sender, EventArgs e)
        {
            DefaultWarrant defaultWarrant = (DefaultWarrant)warrantsDisplayListBox.SelectedItem;
            if (defaultWarrant != null)
                warrantControlBox.getSpecialOneWarrant(defaultWarrant.ID);
        }

        private void typeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            responseForWarrantList(warrantList);
        }
    }
}
