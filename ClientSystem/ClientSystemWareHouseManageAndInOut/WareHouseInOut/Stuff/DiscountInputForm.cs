﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using ClassObjectToNum.Money;

namespace ClientSystem.WareHouseInOut.Stuff
{
    public partial class DiscountInputForm : Form
    {
        private OutWarrant warrant;
        private long amountTotal;
        private long? amountPermitNow;
        private string reason;

        //transmission
        private Transmission.Transmission mainTransmission;
        private SharedObject.Normal.TransmissionCookie cookie = new SharedObject.Normal.TransmissionCookie();
        private List<Transmission.Connector> warrantConnectors = new List<Transmission.Connector>();

        private delegate void DDiscountChange(DiscountItem discountItem);
        private delegate void DOperatedState(OperatedState instrState);
        private delegate void DIdentityCheck(Identity.IdentityCheckForm identityCheckForm);

        //delegates called by transmission
        private Connector.WarrantConnector.DDiscountChange EDiscountChange = null;
        private Connector.WarrantConnector.DOperatedState EOperateState;
        private Connector.WarrantConnector.DIdentityCheck EIdentityCheck;

        public DiscountInputForm(Transmission.Transmission mainTransmission)
        {
            InitializeComponent();

            this.mainTransmission = mainTransmission;

            this.mainTransmission.addConnectors(SharedObject.Normal.SystemID.WareHouseInOut, this.warrantConnectors);
            
            EDiscountChange = new Connector.WarrantConnector.DDiscountChange(discountItem => this.Invoke(new DDiscountChange(responseForDiscountChange), new object[] { discountItem }));
            EOperateState = new Connector.WarrantConnector.DOperatedState(state => this.Invoke(new DOperatedState(responseForOperatedState), new object[] { state }));
            EIdentityCheck = new Connector.WarrantConnector.DIdentityCheck(identityCheckForm => this.Invoke(new DIdentityCheck(responseForIdentityCheck), new object[] { identityCheckForm }));
            
        }

        public void init(OutWarrant warrant, long amountTotal, long? amountPermitNow, string reason)
        {
            this.warrant = warrant;
            this.amountTotal = amountTotal;
            this.amountPermitNow = amountPermitNow;
            this.reason = reason;

            refresh();
        }

        private void refresh()
        {
            this.totalAmountLabel.Text = "总金额：" + this.amountTotal.MoneyNumToDisplayString();
            if (amountPermitNow != null)
                this.amountPermitTextBox.Text = amountPermitNow.Value.MoneyNumToDisplayString();
            else
                this.amountPermitTextBox.Text = "";
            this.warrantIdLabel.Text = "单号：" + this.warrant.ID.ToString();
            this.reasonTextBox.Text = this.reason;
        }

        private void responseForDiscountChange(DiscountItem discountItem)
        {
            this.Close();
        }

        ///This method is used for response to server
        private void responseForOperatedState(OperatedState operatedState)
        {
            String str = Function.OperatedStateStringBuilder(operatedState);
            if (str.Length > 0)
                MessageBox.Show(str);
            //setAccessiable(null);
        }
        ///This method is used for response to server
        private void responseForIdentityCheck(Identity.IdentityCheckForm identityCheckForm)
        {
            identityCheckForm.Show();
        }

        private void discountOkButton_Click(object sender, EventArgs e)
        {
            int? amountPermit;
            try
            {
                if (this.amountPermitTextBox.Text == null || this.amountPermitTextBox.Text == "")
                    amountPermit = null;
                else
                    amountPermit = ClassMoney.MoneyStringToNum(this.amountPermitTextBox.Text);
            }
            catch (ClassMoney.NumberFormatNotRecognizableException)
            {
                MessageBox.Show("数字格式不正确");
                return;
            }
            if (amountPermit != null && amountPermit < 0)
            {
                MessageBox.Show("折让后金额不能为负值");
                return;
            }
            string confirm;
            if (amountPermit != null)
                confirm = "您要为用户折让" + ((int)(this.amountTotal - amountPermit)).MoneyNumToDisplayString() + "元，订单实际收款额为" + amountPermit.Value.MoneyNumToDisplayString() + "元，确定吗？";
            else
                confirm = "您要取消为此用户折让，确认吗？";
            if (MessageBox.Show(confirm, "折让金额确认", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                Connector.WarrantConnector connector = new Connector.WarrantConnector(SharedObject.Main.Transmission.Instruction.outWarrantDiscount, mainTransmission, warrantConnectors);
                connector.EDiscountChange += EDiscountChange;
                connector.EOperatedState += EOperateState;
                connector.EIdentityCheck += EIdentityCheck;
                connector.changeDiscount(this.warrant, amountPermit, this.reasonTextBox.Text);
            }
        }
    }
}
