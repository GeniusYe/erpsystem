﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.WareHouseManage.Stuff;
using ClientSystem.WareHouseManage.Stuff;
using SharedObject.Identity;

namespace ClientSystem.WareHouseInOut.Stuff
{
    public partial class WarrantDraftChooseForm : Form
    {
        private List<DraftDetailForm> detailForms;
        private Transmission.Transmission mainTransmission;

        private DraftChooseBox draftChooseBox;

        private BarCodeScanForm barCodeScanForm;

        /// <summary>
        /// AddEventHandler Add is called when user choose one draft to add
        /// </summary>
        /// <param name="draft"></param>
        public delegate void AddEventHandler(DefaultDraft draft);
        public event AddEventHandler Add;

        public WarrantDraftChooseForm(Transmission.Transmission mainTransmission)
        {
            InitializeComponent();
            this.mainTransmission = mainTransmission;
            detailForms = new List<DraftDetailForm>();

            draftChooseBox = new DraftChooseBox(mainTransmission);
            draftChooseBox.Scan += new DraftChooseBox.ScanResultEventHandler(draftChooseBox_Scan);
            barCodeScanForm = new BarCodeScanForm();

            this.draftChooseBox.Location = new System.Drawing.Point(12, 12);
            this.draftChooseBox.Name = "draftChooseBox";
            this.draftChooseBox.Size = new System.Drawing.Size(600, 295);
            this.draftChooseBox.TabIndex = 0;
            this.Controls.Add(draftChooseBox);
        }

        private void shutWindows()
        {
            foreach (DraftDetailForm f in detailForms)
            {
                f.Close();
                f.Dispose();
            }
        }

        private void shutWindowsButton_Click(object sender, EventArgs e)
        {
            shutWindows();
        }

        private void WarrantDraftChooseForm_FormClosing(object sender, EventArgs e)
        {
            shutWindows();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            DefaultDraft defaultDraft = draftChooseBox.getChosenDefaultDraft();
            if (defaultDraft == null)
            {
                MessageBox.Show("请选择配件");
                return;
            }
            if(Add != null)
                Add(defaultDraft);
        }

        private void scanIDButton_Click(object sender, EventArgs e)
        {
            if (barCodeScanForm != null)
                barCodeScanForm.Dispose();
            barCodeScanForm = new BarCodeScanForm();
            barCodeScanForm.Show();
            barCodeScanForm.FormClosed += new FormClosedEventHandler(barCodeScanForm_FormClosed);
        }

        private void barCodeScanForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            string scan_line = this.barCodeScanForm.cardInfoRead;
            int scan_result = 0;
            if (scan_line != "")
            {
                if (int.TryParse(scan_line, out scan_result))
                {
                    this.draftChooseBox.scanDraftByID(scan_result);
                }
                else
                {
                    MessageBox.Show("输入格式有误，请重新输入");
                }
            }
        }

        private void draftChooseBox_Scan(DefaultDraft draft)
        {
            if (Add != null)
                Add(draft);
            scanIDButton_Click(null, null);
        }
    }
}
