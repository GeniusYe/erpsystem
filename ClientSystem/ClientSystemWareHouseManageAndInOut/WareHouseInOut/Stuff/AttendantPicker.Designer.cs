﻿namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class AttendantPicker
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.AttendantComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // AttendantComboBox
            // 
            this.AttendantComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AttendantComboBox.FormattingEnabled = true;
            this.AttendantComboBox.Location = new System.Drawing.Point(0, 0);
            this.AttendantComboBox.Name = "AttendantComboBox";
            this.AttendantComboBox.Size = new System.Drawing.Size(130, 21);
            this.AttendantComboBox.TabIndex = 2;
            // 
            // AttendantPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.AttendantComboBox);
            this.Name = "AttendantPicker";
            this.Size = new System.Drawing.Size(130, 22);
            this.LostFocus += new System.EventHandler(this.AttendantPicker_LostFocus);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox AttendantComboBox;

    }
}
