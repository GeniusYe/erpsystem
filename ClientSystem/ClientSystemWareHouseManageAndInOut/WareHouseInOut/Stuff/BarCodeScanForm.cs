﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ClientSystem.WareHouseInOut.Stuff
{
    public partial class BarCodeScanForm : Form
    {
        public String cardInfoRead 
        {
            get
            {
                return this.barCodeInputBox.Text;
            }
        }

        public BarCodeScanForm()
        {
            InitializeComponent();

            this.barCodeInputBox.KeyDown += new KeyEventHandler(barCodeInputBox_KeyDown);
        }

        private void barCodeInputBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.Close();
            }
        }
    }
}
