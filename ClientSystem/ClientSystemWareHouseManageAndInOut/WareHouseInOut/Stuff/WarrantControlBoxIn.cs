﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ClientSystem.WareHouseInOut.Stuff
{
    public partial class WarrantControlBoxIn : WarrantControlBox
    {
        protected InWarrant warrantNow;

        //delegates and events
        protected Connector.WarrantConnector.DSpecialOneInWarrant ESpecialOneInWarrant;
        protected Connector.WarrantConnector.DPrint EPrint;

        private delegate void DSpecialOneInWarrant(InWarrant inWarrant);
        public delegate void UpdateWarrantDisplayListEventHandler(AbstractWarrant abstractWarrant);
        public event UpdateWarrantDisplayListEventHandler updateWarrantDisplayList;

        public WarrantControlBoxIn(Transmission.Transmission mainTransmission)
            : base(mainTransmission)
        {
            InitializeComponent();

            ESpecialOneInWarrant = new Connector.WarrantConnector.DSpecialOneInWarrant(inwarrant => this.Invoke(new DSpecialOneInWarrant(responseForSpecialOneInWarrant), new object[] { inwarrant }));
            EPrint = new Connector.WarrantConnector.DPrint(responseForPrint);

            this.warrantDetailBox.setStyle(WarrantType.In);
            warrantNow = null;
            setAccessiable(null);
            this.Text = "入库管理";
        }

        /// This method is used to show the WarrantList
        protected void responseForSpecialOneInWarrant(InWarrant inWarrant)
        {
            if (inWarrant != null && inWarrant.ID > 0)
            {
                warrantNow = inWarrant;
                editingState = EditingState.Normal;
                warrantDetailBox.warrant = warrantNow;
                warrantDetailBox.readOnly = true;
                setAccessiable(warrantNow);
                if (updateWarrantDisplayList != null)
                    updateWarrantDisplayList((AbstractWarrant)warrantNow);
            }
        }
        /// This method is used to print
        /// This method is called by mainTransmission
        protected void responseForPrint(OperatedState instrState)
        {
            if (instrState.state == OperatedState.OperatedStateEnum.success)
            {
                new WarrantPrint(find_customer_name(warrantDetailBox.warrantCustomersList, warrantNow.target), "", 
                    warrantNow.operationRecord[0].operationOperatorName).printInWarrant(warrantNow);
            }
        }

        /// <summary>
        /// setAccessiable is used to set which columns and which buttons can enabled and which ones shall be disabled according to the state
        /// </summary>
        protected void setAccessiable(InWarrant warrant)
        {
            AbstractWarrant.WarrantState state;
            if (warrant != null)
                state = warrant.state;
            else
                state = AbstractWarrant.WarrantState.Abnormal;

            bool addNewE = false, editE = false, cancelE = false;
            bool submitE = false, printE = false, addItemE = false, delItemE = false, editOkE = false, editCancelE = false;

            if (editingState == EditingState.Normal)
            {
                addNewE = _addNew;
                switch (state)
                {
                    case AbstractWarrant.WarrantState.Normal:
                        editE = true;
                        submitE = true;
                        break;
                    case AbstractWarrant.WarrantState.Submitted:
                    case AbstractWarrant.WarrantState.Printed:
                        printE = true;
                        break;
                }
            }
            else if (editingState == EditingState.Editing)
            {
                addItemE = true;
                delItemE = true;
                editOkE = true;
                editCancelE = true;
            }
            else if (editingState == EditingState.Returning)
            {
                editOkE = true;
                editCancelE = true;
            }
            addNewButton.Enabled = addNewE;
            editButton.Enabled = editE;
            cancelButton.Enabled = cancelE;
            submitButton.Enabled = submitE;
            printButton.Enabled = printE;
            addItemButton.Enabled = addItemE;
            delItemButton.Enabled = delItemE;
            editOkButton.Enabled = editOkE;
            editCancelButton.Enabled = editCancelE;
        }

        public void getSpecialOneWarrant(int id)
        {
            if (id <= 0)
                return;
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.inWarrantSpecialOne, mainTransmission, warrantConnectors);
            warrantConnector.ESpecialOneInWarrant += ESpecialOneInWarrant;
            warrantConnector.EOperatedState += EOperateState;
            warrantConnector.getSpecialOneWarrant(WarrantType.In, id);
        }

        protected override void warrantDraftChooseForm_Add(DefaultDraft draft)
        {
            foreach (InWarrantItem iwi in warrantNow.items)
            {
                if (iwi.IDdraft == draft.id)
                {
                    MessageBox.Show("对不起，不能在一张单据中添加两个相同配件");
                    return;
                }
            }
            InWarrantItem inWarrantItem = new InWarrantItem();
            inWarrantItem.IDWarrant = warrantNow.ID;
            inWarrantItem.IDdraft = draft.id;
            inWarrantItem.name = draft.name;
            inWarrantItem.specification = draft.specification;
            inWarrantItem.hiddenSpecification = draft.hiddenSpecification;
            warrantNow.items.Add(inWarrantItem);
            warrantDetailBox.refreshItem();
        }

        protected override void editOkButton_Click(object sender, EventArgs e)
        {
            base.editOkButton_Click(sender, e);
            setAccessiable(warrantNow);
            Connector.WarrantConnector warrantConnector;
            if (warrantNow.ID > 0)
            {
                warrantConnector = new Connector.WarrantConnector(Instruction.inWarrantEdit, mainTransmission, warrantConnectors);
                warrantConnector.EOperatedState += EOperateState;
                warrantConnector.ESpecialOneInWarrant += ESpecialOneInWarrant;
                warrantConnector.editInWarrant(warrantNow);
            }
            else
            {
                warrantConnector = new Connector.WarrantConnector(Instruction.inWarrantAdd, mainTransmission, warrantConnectors);
                warrantConnector.EOperatedState += EOperateState;
                warrantConnector.ESpecialOneInWarrant += ESpecialOneInWarrant;
                warrantConnector.addInWarrant(warrantNow);
            }
        }

        protected override void editCancelButton_Click(object sender, EventArgs e)
        {
            base.editCancelButton_Click(sender, e);
            setAccessiable(null);
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.inWarrantSpecialOne, mainTransmission, warrantConnectors);
            warrantConnector.ESpecialOneInWarrant += ESpecialOneInWarrant;
            warrantConnector.EOperatedState += EOperateState;
            if (warrantNow.ID > 0)
                warrantConnector.getSpecialOneWarrant(WarrantType.Out, warrantNow.ID);
        }

        protected override void addNewButton_Click(object sender, EventArgs e)
        {
            addEditing = true;
            warrantNow = new InWarrant();
            editingState = EditingState.Editing;
            warrantDetailBox.warrant = warrantNow;
            warrantDetailBox.readOnly = false;
            setAccessiable(warrantNow);
        }

        protected override void editButton_Click(object sender, EventArgs e)
        {
            if (warrantNow != null)
            {
                addEditing = false;
                editingState = EditingState.Editing;
                // warrantDetailBox.warrant = inwarrantNow;
                warrantDetailBox.readOnly = false;
                setAccessiable(warrantNow);
            }
        }

        protected override void cancelButton_Click(object sender, EventArgs e)
        {
            editingState = EditingState.Normal;
            warrantDetailBox.readOnly = false;
            setAccessiable(warrantNow);
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.inWarrantCancel, mainTransmission, warrantConnectors);
            warrantConnector.EOperatedState += EOperateState;
            warrantConnector.ESpecialOneInWarrant += ESpecialOneInWarrant;
            warrantConnector.cancelWarrant(WarrantType.In, (AbstractWarrant)warrantNow);
        }

        protected void printButton_Click(object sender, EventArgs e)
        {
            editingState = EditingState.Normal;
            warrantDetailBox.readOnly = false;
            setAccessiable(warrantNow);
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.inWarrantPrint, mainTransmission, warrantConnectors);
            warrantConnector.EPrint += EPrint;
            warrantConnector.EIdentityCheck += EIdentityCheck;
            warrantConnector.ESpecialOneInWarrant += ESpecialOneInWarrant;
            warrantConnector.PrintWarrant(WarrantType.In, (AbstractWarrant)warrantNow);
        }

        protected void submitButton_Click(object sender, EventArgs e)
        {
            editingState = EditingState.Normal;
            warrantDetailBox.readOnly = false;
            setAccessiable(warrantNow);
            Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(Instruction.inWarrantSubmit, mainTransmission, warrantConnectors);
            warrantConnector.EOperatedState += EOperateState;
            warrantConnector.EIdentityCheck += EIdentityCheck;
            warrantConnector.ESpecialOneInWarrant += ESpecialOneInWarrant;
            warrantConnector.submitWarrant(WarrantType.In, (AbstractWarrant)warrantNow);
        }
    }
}
