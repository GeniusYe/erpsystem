﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;
using SharedObject.WareHouseManage.Stuff;
using ClassObjectToNum.Money;

namespace ClientSystem.WareHouseInOut.Stuff
{
    public abstract partial class WarrantForm : Form
    {
        //transmission
        protected Transmission.Transmission mainTransmission;
        protected SharedObject.Normal.TransmissionCookie cookie = new SharedObject.Normal.TransmissionCookie();
        protected List<Transmission.Connector> warrantConnectors = new List<Transmission.Connector>();
        protected List<Transmission.Connector> customerConnectors = new List<Transmission.Connector>();

        //the draftDetailForm
        protected List<ClientSystem.WareHouseManage.Stuff.DraftDetailForm> draftDetailForms = new List<WareHouseManage.Stuff.DraftDetailForm>();

        //the inwarrants displayed now
        protected List<DefaultWarrant> warrantList = new List<DefaultWarrant>();

        //show warrantlist for thread safety
        private delegate void DWarrantList(List<DefaultWarrant> lists);
        private delegate void DCustomersList(List<SharedObject.CustomerManage.Stuff.DefaultCustomer> lists);

        //delegates called by transmission
        protected Connector.WarrantConnector.DWarrantList EWarrantList;
        protected CustomerManage.Connector.CustomerConnector.DCustomerList ECustomersList;

        public List<SharedObject.CustomerManage.Stuff.DefaultCustomer> warrantCustomersList
        {
            set
            {
                targetCustomerPicker.customerList = value;
            }
        }

        public WarrantForm()
        {
            InitializeComponent();

            this.mainTransmission = Transmission.Transmission.mainTransmission;
            if (mainTransmission != null)
            {
                mainTransmission.addConnectors(SharedObject.Normal.SystemID.WareHouseInOut, warrantConnectors);
                mainTransmission.addConnectors(SharedObject.Normal.SystemID.CustomerManage, customerConnectors);
            }
            else
                throw new Transmission.TransmissionIsNullException();

            EWarrantList = new Connector.WarrantConnector.DWarrantList(list => this.Invoke(new DWarrantList(responseForWarrantList), new object[] { list }));
            ECustomersList = new CustomerManage.Connector.CustomerConnector.DCustomerList(list => this.Invoke(new DCustomersList(mainTransmission_responseForCustomersList), new object[] { list }));

            this.Load += new EventHandler(WarrantForm_Load);
            this.FormClosing += new FormClosingEventHandler(WarrantForm_FormClosing);
            this.warrantsDisplayListBox.Click += new System.EventHandler(this.warrantsDisplayListBox_Click);

            typeCheckBoxWithReceipt.Visible = false;
            typeCheckBoxWithoutReceipt.Visible = false;
            typeCheckBoxService.Visible = false;
            typeCheckBoxGift.Visible = false;
            typeCheckBoxAdjustOut.Visible = false;

            DateTime startDate = DateTime.Now.AddMonths(-1);
            DateTime stopDate = DateTime.Now.AddDays(1);
            this.startDatePicker.Value = startDate;
            this.stopDatePicker.Value = stopDate;
        }

        protected void warrantControlBox_DoubleClick(int draftId)
        {
            ClientSystem.WareHouseManage.Stuff.DraftDetailForm draftDetailForm = new WareHouseManage.Stuff.DraftDetailForm(draftDetailForms);
            draftDetailForm.visibleNum = true;
            draftDetailForm.visiblePrice = true;
            draftDetailForm.changeDraft(draftId);
            draftDetailForm.Show();
        }

        protected abstract void WarrantForm_Load(object sender, EventArgs e);

        private void WarrantForm_FormClosing(object sender, FormClosingEventArgs args)
        {
            while (warrantConnectors.Count > 0)
                warrantConnectors[0].Dispose();
            mainTransmission.removeConnectors(SharedObject.Normal.SystemID.WareHouseInOut, warrantConnectors);
        }

        protected abstract void warrantsDisplayListBox_Click(object sender, EventArgs e);

        /// This method is used to show the WarrantList
        protected void responseForWarrantList(List<DefaultWarrant> lists)
        {
            if (lists != null)
            {
                warrantList = lists;
                bool withReceipt = typeCheckBoxWithReceipt.Checked;
                bool withoutReceipt = typeCheckBoxWithoutReceipt.Checked;
                bool service = typeCheckBoxService.Checked;
                bool gift = typeCheckBoxGift.Checked;
                bool adjustOut = typeCheckBoxAdjustOut.Checked;
                bool t = false;
                warrantsDisplayListBox.Items.Clear();
                foreach (DefaultWarrant warrant in lists)
                {
                    t = false;
                    if (warrant.type == DefaultWarrant.SpecificWarrantType.In ||
                            warrant.type == DefaultWarrant.SpecificWarrantType.AdjustIn)
                        t = true;
                    else if (warrant.type == DefaultWarrant.SpecificWarrantType.withReceipt && withReceipt)
                        t = true;
                    else if (warrant.type == DefaultWarrant.SpecificWarrantType.withoutReceipt && withoutReceipt)
                        t = true;
                    else if (warrant.type == DefaultWarrant.SpecificWarrantType.Service && service)
                        t = true;
                    else if (warrant.type == DefaultWarrant.SpecificWarrantType.Gift && gift)
                        t = true;
                    else if (warrant.type == DefaultWarrant.SpecificWarrantType.AdjustOut && adjustOut)
                        t = true;
                    if (t)
                        warrantsDisplayListBox.Items.Add(new OverriddenDefaultWarrant(warrant));
                }
            }
        }

        protected void updateWarrantDisplayList(AbstractWarrant abstractWarrant)
        {
            int i, k = warrantsDisplayListBox.Items.Count;
            DefaultWarrant warrant;
            for (i = 0; i < k; i++)
            {
                warrant = (DefaultWarrant)warrantsDisplayListBox.Items[i];
                if (warrant != null && warrant.ID == abstractWarrant.ID)
                {
                    warrantsDisplayListBox.Items[i] = new OverriddenDefaultWarrant(abstractWarrant);
                    break;
                }
            }
            if (i == k)
                warrantsDisplayListBox.Items.Add(new OverriddenDefaultWarrant(abstractWarrant));
        }

        /// This method is used to show the WarrantCustomersList
        private void mainTransmission_responseForCustomersList(List<SharedObject.CustomerManage.Stuff.DefaultCustomer> lists)
        {
            if (lists != null)
            {
                warrantCustomersList = lists;
            }
        }

        protected abstract void reloadWarrantListButton_Click(object sender, EventArgs e);

        class OverriddenDefaultWarrant : DefaultWarrant
        {
            public OverriddenDefaultWarrant(DefaultWarrant defaultWarrant)
            {
                this.ID = defaultWarrant.ID;
                this.number = defaultWarrant.number;
                this.payState = defaultWarrant.payState;
                this.state = defaultWarrant.state;
                this.type = defaultWarrant.type;
                this.amountCost = defaultWarrant.amountCost;
                this.amountIn = defaultWarrant.amountIn;
                this.amountTotal = defaultWarrant.amountTotal;
            }

            public override string ToString()
            {
                return this.number + " " + this.stateString + " " + this.payStateString;
            }
        }

        private void viewOneCycleFormButton_Click(object sender, EventArgs e)
        {
            OneCycleWarrantForm oneCycleWarrantForm = new OneCycleWarrantForm(mainTransmission);
            oneCycleWarrantForm.Show();
        }
    }
}
