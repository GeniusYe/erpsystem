﻿using ClassObjectToNum.DateTime;

namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class WarrantDetailBox
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.numberTextBox = new System.Windows.Forms.TextBox();
            this.numberLabel = new System.Windows.Forms.Label();
            this.itemGridView = new System.Windows.Forms.DataGridView();
            this.targetLabel = new System.Windows.Forms.Label();
            this.typeRadioButtonWithoutReceipt = new System.Windows.Forms.RadioButton();
            this.typeRadioButtonWithReceipt = new System.Windows.Forms.RadioButton();
            this.totalAmountLabel = new System.Windows.Forms.Label();
            this.totalAmountCostLabel = new System.Windows.Forms.Label();
            this.dateAndOperator = new System.Windows.Forms.DataGridView();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Operator = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CommentTextBox = new System.Windows.Forms.TextBox();
            this.typeRadioButtonService = new System.Windows.Forms.RadioButton();
            this.amountPermitLabel = new System.Windows.Forms.Label();
            this.amountPaidLabel = new System.Windows.Forms.Label();
            this.typeRadioButtonGift = new System.Windows.Forms.RadioButton();
            this.typeRadioButtonAdjustOut = new System.Windows.Forms.RadioButton();
            this.typeGroupBoxOut = new System.Windows.Forms.GroupBox();
            this.typeGroupBoxIn = new System.Windows.Forms.GroupBox();
            this.typeRadioButtonIn = new System.Windows.Forms.RadioButton();
            this.typeRadioButtonAdjustIn = new System.Windows.Forms.RadioButton();
            this.attendantLabel = new System.Windows.Forms.Label();
            this.targetPicker = new ClientSystem.CustomerManage.Stuff.CustomerPicker();
            this.attendantPicker.Add(new ClientSystem.WareHouseInOut.Stuff.AttendantPicker());
            this.attendantPicker.Add(new ClientSystem.WareHouseInOut.Stuff.AttendantPicker());
            this.attendantPicker.Add(new ClientSystem.WareHouseInOut.Stuff.AttendantPicker());
            this.attendantPicker.Add(new ClientSystem.WareHouseInOut.Stuff.AttendantPicker());
            this.attendantPicker.Add(new ClientSystem.WareHouseInOut.Stuff.AttendantPicker());
            ((System.ComponentModel.ISupportInitialize)(this.itemGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateAndOperator)).BeginInit();
            this.typeGroupBoxOut.SuspendLayout();
            this.typeGroupBoxIn.SuspendLayout();
            this.SuspendLayout();
            // 
            // numberTextBox
            // 
            this.numberTextBox.Location = new System.Drawing.Point(74, 7);
            this.numberTextBox.Name = "numberTextBox";
            this.numberTextBox.Size = new System.Drawing.Size(123, 21);
            this.numberTextBox.TabIndex = 1;
            // 
            // numberLabel
            // 
            this.numberLabel.AutoSize = true;
            this.numberLabel.Location = new System.Drawing.Point(20, 10);
            this.numberLabel.Name = "numberLabel";
            this.numberLabel.Size = new System.Drawing.Size(41, 12);
            this.numberLabel.TabIndex = 0;
            this.numberLabel.Text = "单号：";
            // 
            // itemGridView
            // 
            this.itemGridView.AllowUserToAddRows = false;
            this.itemGridView.AllowUserToDeleteRows = false;
            this.itemGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.itemGridView.Location = new System.Drawing.Point(0, 132);
            this.itemGridView.Name = "itemGridView";
            this.itemGridView.RowTemplate.Height = 23;
            this.itemGridView.Size = new System.Drawing.Size(869, 210);
            this.itemGridView.TabIndex = 9;
            this.itemGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.itemGridView_CellValueChanged);
            // 
            // targetLabel
            // 
            this.targetLabel.AutoSize = true;
            this.targetLabel.Location = new System.Drawing.Point(20, 40);
            this.targetLabel.Name = "targetLabel";
            this.targetLabel.Size = new System.Drawing.Size(29, 12);
            this.targetLabel.TabIndex = 2;
            this.targetLabel.Text = "目标";
            // 
            // typeRadioButtonWithoutReceipt
            // 
            this.typeRadioButtonWithoutReceipt.AutoSize = true;
            this.typeRadioButtonWithoutReceipt.Location = new System.Drawing.Point(12, 32);
            this.typeRadioButtonWithoutReceipt.Name = "typeRadioButtonWithoutReceipt";
            this.typeRadioButtonWithoutReceipt.Size = new System.Drawing.Size(35, 16);
            this.typeRadioButtonWithoutReceipt.TabIndex = 5;
            this.typeRadioButtonWithoutReceipt.TabStop = true;
            this.typeRadioButtonWithoutReceipt.Text = "无";
            this.typeRadioButtonWithoutReceipt.UseVisualStyleBackColor = true;
            this.typeRadioButtonWithoutReceipt.Click += new System.EventHandler(this.typeRadioButtonWithoutReceipt_Click);
            // 
            // typeRadioButtonWithReceipt
            // 
            this.typeRadioButtonWithReceipt.AutoSize = true;
            this.typeRadioButtonWithReceipt.Checked = true;
            this.typeRadioButtonWithReceipt.Location = new System.Drawing.Point(12, 15);
            this.typeRadioButtonWithReceipt.Name = "typeRadioButtonWithReceipt";
            this.typeRadioButtonWithReceipt.Size = new System.Drawing.Size(35, 16);
            this.typeRadioButtonWithReceipt.TabIndex = 4;
            this.typeRadioButtonWithReceipt.TabStop = true;
            this.typeRadioButtonWithReceipt.Text = "有";
            this.typeRadioButtonWithReceipt.UseVisualStyleBackColor = true;
            this.typeRadioButtonWithReceipt.Click += new System.EventHandler(this.typeRadioButtonWithReceipt_Click);
            // 
            // totalAmountLabel
            // 
            this.totalAmountLabel.Location = new System.Drawing.Point(694, 347);
            this.totalAmountLabel.Name = "totalAmountLabel";
            this.totalAmountLabel.Size = new System.Drawing.Size(160, 12);
            this.totalAmountLabel.TabIndex = 11;
            this.totalAmountLabel.Text = "totalAmountLabel";
            this.totalAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // totalAmountCostLabel
            // 
            this.totalAmountCostLabel.Location = new System.Drawing.Point(528, 347);
            this.totalAmountCostLabel.Name = "totalAmountCostLabel";
            this.totalAmountCostLabel.Size = new System.Drawing.Size(160, 12);
            this.totalAmountCostLabel.TabIndex = 10;
            this.totalAmountCostLabel.Text = "totalAmountCostLabel";
            this.totalAmountCostLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dateAndOperator
            // 
            this.dateAndOperator.AllowUserToAddRows = false;
            this.dateAndOperator.AllowUserToDeleteRows = false;
            this.dateAndOperator.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dateAndOperator.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Title,
            this.Operator,
            this.Date,
            this.Remark});
            this.dateAndOperator.Location = new System.Drawing.Point(467, 35);
            this.dateAndOperator.Name = "dateAndOperator";
            this.dateAndOperator.ReadOnly = true;
            this.dateAndOperator.RowTemplate.Height = 18;
            this.dateAndOperator.Size = new System.Drawing.Size(402, 91);
            this.dateAndOperator.TabIndex = 8;
            // 
            // Title
            // 
            this.Title.Frozen = true;
            this.Title.HeaderText = "项目";
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            this.Title.Width = 80;
            // 
            // Operator
            // 
            this.Operator.Frozen = true;
            this.Operator.HeaderText = "操作者";
            this.Operator.Name = "Operator";
            this.Operator.ReadOnly = true;
            this.Operator.Width = 80;
            // 
            // Date
            // 
            this.Date.Frozen = true;
            this.Date.HeaderText = "日期";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            this.Date.Width = 120;
            // 
            // Remark
            // 
            this.Remark.Frozen = true;
            this.Remark.HeaderText = "其它说明";
            this.Remark.Name = "Remark";
            this.Remark.ReadOnly = true;
            // 
            // CommentTextBox
            // 
            this.CommentTextBox.Location = new System.Drawing.Point(221, 37);
            this.CommentTextBox.Multiline = true;
            this.CommentTextBox.Name = "CommentTextBox";
            this.CommentTextBox.Size = new System.Drawing.Size(240, 87);
            this.CommentTextBox.TabIndex = 7;
            this.CommentTextBox.TextChanged += new System.EventHandler(this.CommentTextBox_TextChanged);
            // 
            // typeRadioButtonService
            // 
            this.typeRadioButtonService.AutoSize = true;
            this.typeRadioButtonService.Location = new System.Drawing.Point(53, 32);
            this.typeRadioButtonService.Name = "typeRadioButtonService";
            this.typeRadioButtonService.Size = new System.Drawing.Size(47, 16);
            this.typeRadioButtonService.TabIndex = 6;
            this.typeRadioButtonService.TabStop = true;
            this.typeRadioButtonService.Text = "三包";
            this.typeRadioButtonService.UseVisualStyleBackColor = true;
            this.typeRadioButtonService.Click += new System.EventHandler(this.typeRadioButtonService_Click);
            // 
            // amountPermitLabel
            // 
            this.amountPermitLabel.Location = new System.Drawing.Point(196, 347);
            this.amountPermitLabel.Name = "amountPermitLabel";
            this.amountPermitLabel.Size = new System.Drawing.Size(160, 12);
            this.amountPermitLabel.TabIndex = 12;
            this.amountPermitLabel.Text = "amountPermitLabel";
            this.amountPermitLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // amountPaidLabel
            // 
            this.amountPaidLabel.Location = new System.Drawing.Point(362, 347);
            this.amountPaidLabel.Name = "amountPaidLabel";
            this.amountPaidLabel.Size = new System.Drawing.Size(160, 12);
            this.amountPaidLabel.TabIndex = 12;
            this.amountPaidLabel.Text = "amountPaidLabel";
            this.amountPaidLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // typeRadioButtonGift
            // 
            this.typeRadioButtonGift.AutoSize = true;
            this.typeRadioButtonGift.Enabled = false;
            this.typeRadioButtonGift.Location = new System.Drawing.Point(105, 15);
            this.typeRadioButtonGift.Name = "typeRadioButtonGift";
            this.typeRadioButtonGift.Size = new System.Drawing.Size(47, 16);
            this.typeRadioButtonGift.TabIndex = 6;
            this.typeRadioButtonGift.TabStop = true;
            this.typeRadioButtonGift.Text = "A";
            this.typeRadioButtonGift.UseVisualStyleBackColor = true;
            this.typeRadioButtonGift.Visible = true;
            this.typeRadioButtonGift.Click += new System.EventHandler(this.typeRadioButtonGift_Click);
            // 
            // typeRadioButtonAdjustOut
            // 
            this.typeRadioButtonAdjustOut.AutoSize = true;
            this.typeRadioButtonAdjustOut.Enabled = false;
            this.typeRadioButtonAdjustOut.Location = new System.Drawing.Point(53, 15);
            this.typeRadioButtonAdjustOut.Name = "typeRadioButtonAdjustOut";
            this.typeRadioButtonAdjustOut.Size = new System.Drawing.Size(47, 16);
            this.typeRadioButtonAdjustOut.TabIndex = 6;
            this.typeRadioButtonAdjustOut.TabStop = true;
            this.typeRadioButtonAdjustOut.Text = "调整";
            this.typeRadioButtonAdjustOut.UseVisualStyleBackColor = true;
            this.typeRadioButtonAdjustOut.Click += new System.EventHandler(this.typeRadioButtonAdjustOut_Click);
            // 
            // typeGroupBoxOut
            // 
            this.typeGroupBoxOut.Controls.Add(this.typeRadioButtonAdjustOut);
            this.typeGroupBoxOut.Controls.Add(this.typeRadioButtonService);
            this.typeGroupBoxOut.Controls.Add(this.typeRadioButtonWithoutReceipt);
            this.typeGroupBoxOut.Controls.Add(this.typeRadioButtonWithReceipt);
            this.typeGroupBoxOut.Controls.Add(this.typeRadioButtonGift);
            this.typeGroupBoxOut.Location = new System.Drawing.Point(13, 64);
            this.typeGroupBoxOut.Name = "typeGroupBoxOut";
            this.typeGroupBoxOut.Size = new System.Drawing.Size(162, 55);
            this.typeGroupBoxOut.TabIndex = 13;
            this.typeGroupBoxOut.TabStop = false;
            // 
            // typeGroupBoxIn
            // 
            this.typeGroupBoxIn.Controls.Add(this.typeRadioButtonIn);
            this.typeGroupBoxIn.Controls.Add(this.typeRadioButtonAdjustIn);
            this.typeGroupBoxIn.Location = new System.Drawing.Point(18, 67);
            this.typeGroupBoxIn.Name = "typeGroupBoxIn";
            this.typeGroupBoxIn.Size = new System.Drawing.Size(152, 47);
            this.typeGroupBoxIn.TabIndex = 14;
            this.typeGroupBoxIn.TabStop = false;
            // 
            // typeRadioButtonIn
            // 
            this.typeRadioButtonIn.AutoSize = true;
            this.typeRadioButtonIn.Checked = true;
            this.typeRadioButtonIn.Enabled = false;
            this.typeRadioButtonIn.Location = new System.Drawing.Point(30, 18);
            this.typeRadioButtonIn.Name = "typeRadioButtonIn";
            this.typeRadioButtonIn.Size = new System.Drawing.Size(47, 16);
            this.typeRadioButtonIn.TabIndex = 7;
            this.typeRadioButtonIn.TabStop = true;
            this.typeRadioButtonIn.Text = "普通";
            this.typeRadioButtonIn.UseVisualStyleBackColor = true;
            this.typeRadioButtonIn.Click += new System.EventHandler(this.typeRadioButtonIn_Click);
            // 
            // typeRadioButtonAdjustIn
            // 
            this.typeRadioButtonAdjustIn.AutoSize = true;
            this.typeRadioButtonAdjustIn.Enabled = false;
            this.typeRadioButtonAdjustIn.Location = new System.Drawing.Point(83, 18);
            this.typeRadioButtonAdjustIn.Name = "typeRadioButtonAdjustIn";
            this.typeRadioButtonAdjustIn.Size = new System.Drawing.Size(47, 16);
            this.typeRadioButtonAdjustIn.TabIndex = 8;
            this.typeRadioButtonAdjustIn.Text = "调整";
            this.typeRadioButtonAdjustIn.UseVisualStyleBackColor = true;
            this.typeRadioButtonAdjustIn.Click += new System.EventHandler(this.typeRadioButtonAdjustIn_Click);
            // 
            // attendantLabel
            // 
            this.attendantLabel.AutoSize = true;
            this.attendantLabel.Location = new System.Drawing.Point(219, 10);
            this.attendantLabel.Name = "attendantLabel";
            this.attendantLabel.Size = new System.Drawing.Size(53, 12);
            this.attendantLabel.TabIndex = 2;
            this.attendantLabel.Text = "服务人员";
            // 
            // targetPicker
            // 
            this.targetPicker.customerList = null;
            this.targetPicker.Location = new System.Drawing.Point(74, 37);
            this.targetPicker.Name = "targetPicker";
            this.targetPicker.Size = new System.Drawing.Size(123, 21);
            this.targetPicker.TabIndex = 3;
            // 
            // attendantPicker[0]
            // 
            this.attendantPicker[0].attendantList = null;
            this.attendantPicker[0].Location = new System.Drawing.Point(299, 7);
            this.attendantPicker[0].Name = "attendantPicker[0]";
            this.attendantPicker[0].Size = new System.Drawing.Size(101, 24);
            this.attendantPicker[0].TabIndex = 3;
            // 
            // attendantPicker[1]
            // 
            this.attendantPicker[1].attendantList = null;
            this.attendantPicker[1].Location = new System.Drawing.Point(406, 7);
            this.attendantPicker[1].Name = "attendantPicker[1]";
            this.attendantPicker[1].Size = new System.Drawing.Size(101, 22);
            this.attendantPicker[1].TabIndex = 3;
            // 
            // attendantPicker[2]
            // 
            this.attendantPicker[2].attendantList = null;
            this.attendantPicker[2].Location = new System.Drawing.Point(513, 7);
            this.attendantPicker[2].Name = "attendantPicker[2]";
            this.attendantPicker[2].Size = new System.Drawing.Size(101, 20);
            this.attendantPicker[2].TabIndex = 3;
            // 
            // attendantPicker[3]
            // 
            this.attendantPicker[3].attendantList = null;
            this.attendantPicker[3].Location = new System.Drawing.Point(620, 7);
            this.attendantPicker[3].Name = "attendantPicker[3]";
            this.attendantPicker[3].Size = new System.Drawing.Size(101, 22);
            this.attendantPicker[3].TabIndex = 3;
            // 
            // attendantPicker[4]
            // 
            this.attendantPicker[4].attendantList = null;
            this.attendantPicker[4].Location = new System.Drawing.Point(727, 7);
            this.attendantPicker[4].Name = "attendantPicker[4]";
            this.attendantPicker[4].Size = new System.Drawing.Size(101, 24);
            this.attendantPicker[4].TabIndex = 3;
            // 
            // WarrantDetailBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.typeGroupBoxIn);
            this.Controls.Add(this.typeGroupBoxOut);
            this.Controls.Add(this.attendantPicker[4]);
            this.Controls.Add(this.attendantPicker[3]);
            this.Controls.Add(this.attendantPicker[2]);
            this.Controls.Add(this.attendantPicker[1]);
            this.Controls.Add(this.attendantPicker[0]);
            this.Controls.Add(this.targetPicker);
            this.Controls.Add(this.CommentTextBox);
            this.Controls.Add(this.dateAndOperator);
            this.Controls.Add(this.amountPaidLabel);
            this.Controls.Add(this.amountPermitLabel);
            this.Controls.Add(this.totalAmountCostLabel);
            this.Controls.Add(this.totalAmountLabel);
            this.Controls.Add(this.attendantLabel);
            this.Controls.Add(this.targetLabel);
            this.Controls.Add(this.itemGridView);
            this.Controls.Add(this.numberLabel);
            this.Controls.Add(this.numberTextBox);
            this.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "WarrantDetailBox";
            this.Size = new System.Drawing.Size(872, 364);
            ((System.ComponentModel.ISupportInitialize)(this.itemGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateAndOperator)).EndInit();
            this.typeGroupBoxOut.ResumeLayout(false);
            this.typeGroupBoxOut.PerformLayout();
            this.typeGroupBoxIn.ResumeLayout(false);
            this.typeGroupBoxIn.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox numberTextBox;
        protected System.Windows.Forms.Label numberLabel;
        private System.Windows.Forms.DataGridView itemGridView;
        private System.Windows.Forms.Label targetLabel;
        private System.Windows.Forms.RadioButton typeRadioButtonWithoutReceipt;
        private System.Windows.Forms.RadioButton typeRadioButtonWithReceipt;
        private System.Windows.Forms.Label totalAmountLabel;
        private System.Windows.Forms.Label totalAmountCostLabel;
        private System.Windows.Forms.DataGridView dateAndOperator;
        private System.Windows.Forms.TextBox CommentTextBox;
        private System.Windows.Forms.RadioButton typeRadioButtonService;
        private CustomerManage.Stuff.CustomerPicker targetPicker;
        private System.Windows.Forms.Label amountPermitLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Operator;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remark;
        private System.Windows.Forms.Label amountPaidLabel;
        private System.Windows.Forms.RadioButton typeRadioButtonGift;
        private System.Windows.Forms.RadioButton typeRadioButtonAdjustOut;
        private System.Windows.Forms.GroupBox typeGroupBoxOut;
        private System.Windows.Forms.GroupBox typeGroupBoxIn;
        private System.Windows.Forms.RadioButton typeRadioButtonIn;
        private System.Windows.Forms.RadioButton typeRadioButtonAdjustIn;
        private System.Windows.Forms.Label attendantLabel;
    }
}
