﻿namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class WarrantControlBoxIn
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.printButton = new System.Windows.Forms.Button();
            this.submitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // printButton
            // 
            this.printButton.Location = new System.Drawing.Point(369, 8);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(82, 23);
            this.printButton.TabIndex = 42;
            this.printButton.Text = "打印入库单";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(281, 8);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(82, 23);
            this.submitButton.TabIndex = 41;
            this.submitButton.Text = "提交";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // WarrantControlBoxIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.Controls.Add(this.printButton);
            this.Controls.Add(this.submitButton);
            this.Name = "WarrantControlBoxIn";
            this.Controls.SetChildIndex(this.warrantDetailBox, 0);
            this.Controls.SetChildIndex(this.addNewButton, 0);
            this.Controls.SetChildIndex(this.editButton, 0);
            this.Controls.SetChildIndex(this.cancelButton, 0);
            this.Controls.SetChildIndex(this.addItemButton, 0);
            this.Controls.SetChildIndex(this.delItemButton, 0);
            this.Controls.SetChildIndex(this.editOkButton, 0);
            this.Controls.SetChildIndex(this.editCancelButton, 0);
            this.Controls.SetChildIndex(this.submitButton, 0);
            this.Controls.SetChildIndex(this.printButton, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button submitButton;
    }
}
