﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;
using ClassObjectToNum.Money;

namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class OneCycleWarrantForm : Form
    {
        private Transmission.Transmission mainTransmission;
        private SharedObject.Normal.TransmissionCookie cookie = new SharedObject.Normal.TransmissionCookie();
        private List<Transmission.Connector> warrantConnectors = new List<Transmission.Connector>();
        //used for mail sending
        private ViewOneCycle cycleData;

        private WarrantControlBoxIn warrantControlBoxIn;
        private WarrantControlBoxOut warrantControlBoxOut;

        private Connector.WarrantConnector.DViewOneCycle EViewOneCycle;
        private delegate void DViewOneCycle(ViewOneCycle viewOneCycle);

        public OneCycleWarrantForm(Transmission.Transmission mainTransmission)
        {

            this.mainTransmission = Transmission.Transmission.mainTransmission;

            InitializeComponent();

            if (mainTransmission != null)
                mainTransmission.addConnectors(SharedObject.Normal.SystemID.WareHouseInOut, warrantConnectors);
            else
                throw new Transmission.TransmissionIsNullException();

            EViewOneCycle = new Connector.WarrantConnector.DViewOneCycle(viewOneCycle => this.Invoke(new DViewOneCycle(responseForViewOneCycle), new object[] { viewOneCycle }));

            inWarrantGridView.Click += new EventHandler(inWarrantGridView_Click);
            outWarrantGridView.Click += new EventHandler(outWarrantGridView_Click);
            
            warrantControlBoxIn = new WarrantControlBoxIn(this.mainTransmission);
            warrantControlBoxIn.Location = new Point(0, 0);
            warrantControlBoxIn.allowUserToAddNew = false;
            this.warrantTabPage.Controls.Add(warrantControlBoxIn);
            warrantControlBoxOut = new WarrantControlBoxOut(this.mainTransmission);
            warrantControlBoxOut.Location = new Point(0, 0);
            warrantControlBoxOut.allowUserToAddNew = false;
            this.warrantTabPage.Controls.Add(warrantControlBoxOut);

            this.FormClosing += new FormClosingEventHandler(WarrantForm_FormClosing);
            this.dateTimePicker.Value = DateTime.Now;
        }

        private void outWarrantGridView_Click(object sender, EventArgs e)
        {
            if (outWarrantGridView.SelectedRows.Count <= 0)
                return;
            DefaultWarrant warrant = ((DataGridViewRowWithWarrant)outWarrantGridView.SelectedRows[0]).defaultWarrant;
            if (warrant != null)
            {
                warrantControlBoxOut.Visible = true;
                warrantControlBoxIn.Visible = false;
                warrantControlBoxOut.getSpecialOneWarrant(warrant.ID);
            }
        }

        private void inWarrantGridView_Click(object sender, EventArgs e)
        {
            if (inWarrantGridView.SelectedRows.Count <= 0)
                return;
            DefaultWarrant warrant = ((DataGridViewRowWithWarrant)inWarrantGridView.SelectedRows[0]).defaultWarrant;
            if (warrant != null)
            {
                warrantControlBoxOut.Visible = false;
                warrantControlBoxIn.Visible = true;
                warrantControlBoxIn.getSpecialOneWarrant(warrant.ID);
            }
        }

        private void WarrantForm_FormClosing(object sender, FormClosingEventArgs args)
        {
            while (warrantConnectors.Count > 0)
                warrantConnectors[0].Dispose();
            mainTransmission.removeConnectors(SharedObject.Normal.SystemID.WareHouseInOut, warrantConnectors);
        }

        /// This method is used to show the WarrantList
        /// This method is called by warrantTransmission
        private void responseForViewOneCycle(ViewOneCycle viewOneCycle)
        {
            this.cycleData = viewOneCycle;
            int amountIn = 0, amountAdjustIn = 0,
                amountSell = 0, amountSellTotal = 0, amountPaid = 0, amountCost = 0,
                amountService = 0, amountServiceCost = 0,
                amountGift = 0, amountGiftCost = 0,
                amountAdjustOut = 0, amountServiceFee = 0;
            int tempAmount = 0, tempAmount2 = 0;
            inWarrantGridView.Rows.Clear();
            outWarrantGridView.Rows.Clear();
            foreach (DefaultWarrant warrant in viewOneCycle.inWarrantList)
            {
                inWarrantGridView.Rows.Add(new DataGridViewRowWithWarrant(warrant, WarrantType.In));
                switch (warrant.type)
                {
                    case DefaultWarrant.SpecificWarrantType.In:
                        amountIn += warrant.amountIn.Value;
                        break;
                    case DefaultWarrant.SpecificWarrantType.AdjustIn:
                        amountAdjustIn += warrant.amountIn.Value;
                        break;
                }
            }
            foreach (DefaultWarrant warrant in viewOneCycle.outWarrantList)
            {
                outWarrantGridView.Rows.Add(new DataGridViewRowWithWarrant(warrant, WarrantType.Out));
                if (warrant.amountPermit != null)
                    tempAmount = warrant.amountPermit.Value;
                else
                    tempAmount = warrant.amountTotal.Value;
                if (warrant.amountPaid != null)
                    tempAmount2 = warrant.amountPaid.Value;
                else
                    tempAmount2 = 0;
                int tempAmountCost = warrant.amountCost == null ? 0 : warrant.amountCost.Value;
                switch (warrant.type)
                {
                    case DefaultWarrant.SpecificWarrantType.Service:
                        amountService += tempAmount;
                        amountServiceCost += tempAmountCost;
                        break;
                    case DefaultWarrant.SpecificWarrantType.withoutReceipt:
                    case DefaultWarrant.SpecificWarrantType.withReceipt:
                        amountSell += tempAmount;
                        amountSellTotal += warrant.amountTotal.Value;
                        amountCost += tempAmountCost;
                        amountPaid += tempAmount2;
                        break;
                    case DefaultWarrant.SpecificWarrantType.Gift:
                        amountGift += tempAmount;
                        amountGiftCost += tempAmountCost;
                        break;
                    case DefaultWarrant.SpecificWarrantType.AdjustOut:
                        amountAdjustOut += tempAmountCost;
                        break;
                }
                if (warrant.amountServiceFee != null)
                {
                    amountServiceFee += warrant.amountServiceFee.Value;
                }
            }

            amountInTextBox.Text = amountIn.MoneyNumToDisplayString();
            amountAdjustInTextBox.Text = amountAdjustIn.MoneyNumToDisplayString();

            amountSellTextBox.Text = amountSell.MoneyNumToDisplayString();
            amountSellTotalTextBox.Text = amountSellTotal.MoneyNumToDisplayString();
            amountPaidTextBox.Text = amountPaid.MoneyNumToDisplayString();
            amountCostTextBox.Text = amountCost.MoneyNumToDisplayString();

            amountServiceTextBox.Text = amountService.MoneyNumToDisplayString();
            amountServiceCostTextBox.Text = amountServiceCost.MoneyNumToDisplayString();
            amountServiceFeeTextBox.Text = amountServiceFee.MoneyNumToDisplayString();

            amountGiftCostTextBox.Text = amountGiftCost.MoneyNumToDisplayString();

            amountAdjustOutTextBox.Text = amountAdjustOut.MoneyNumToDisplayString();
        }

        class DataGridViewRowWithWarrant : DataGridViewRow
        {
            public DefaultWarrant defaultWarrant { get; private set; }

            public DataGridViewRowWithWarrant(DefaultWarrant defaultWarrant, WarrantType warrantType)
                : base()
            {
                this.defaultWarrant = defaultWarrant;
                switch (warrantType)
                {
                    case WarrantType.In:
                        {
                            ViewOnceCycleInWarrant w = (ViewOnceCycleInWarrant)this.defaultWarrant;
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.number });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.submitDate });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.target_name });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.specificWarrantTypeString });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.amountIn.Value.MoneyNumToDisplayString() });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.stateString });
                        }
                        break;
                    case WarrantType.Out:
                        {
                            ViewOnceCycleOutWarrant w = (ViewOnceCycleOutWarrant)this.defaultWarrant;
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.number });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.submitCustomerPageDate });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.submitDate });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.specificWarrantTypeString });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.target_name });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.attendant_names });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.amountCost.Value.MoneyNumToDisplayString() });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.amountTotal.Value.MoneyNumToDisplayString() });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.amountServiceFee == null ? "" : w.amountServiceFee.Value.MoneyNumToDisplayString() });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.amountPermit == null ? "" : w.amountPermit.Value.MoneyNumToDisplayString() });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.amountPaid == null ? "" : w.amountPaid.Value.MoneyNumToDisplayString() });
                            this.Cells.Add(new DataGridViewTextBoxCell() { Value = w.stateString + " " + w.payStateString });
                        }
                        break;
                }
            }
        }

        private void reloadWarrantButton_Click(object sender, EventArgs e)
        {
            getViewAccount();
        }

        private void sendResultButton_Click(object sender, EventArgs e)
        {
            ViewOneCycle viewOneCycle = this.cycleData;
            int amountIn = 0, amountAdjustIn = 0,
                amountSell = 0, amountSellTotal = 0, amountPaid = 0, amountCost = 0,
                amountService = 0, amountServiceCost = 0,
                amountGift = 0, amountGiftCost = 0,
                amountAdjustOut = 0, amountServiceFee = 0;
            int tempAmount = 0, tempAmount2 = 0;
            StringBuilder body = new StringBuilder();
            StringBuilder body2 = new StringBuilder();

            body.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta charset=\"utf-8\" /><link href=\"https://www.youlinjixie.com/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" /><link href=\"https://www.youlinjixie.com/css/draft_monthly_report.css\" rel=\"stylesheet\" type=\"text/css\" /></head>");

            DateTime date = viewOneCycle.date;
            DateTime now = DateTime.Now;
            DateTime end_date = new DateTime(date.Year, date.Month, 1, 0, 0, 0).AddMonths(1);
            String subject = "" + Function.company_name + "配件" + date.Year + "年" + date.Month + "月统计信息";
            body.Append("<html><body><h1>" + subject + "</h1>");

            if (now < end_date)
            {
                MessageBox.Show("当周期还未结束，此时发送的报告中会强调些行为，请您注意");
                body.Append("<h4>注意：这份报告发送时，本周期还未结束，也就意味着，本周期结束后本报告可能发生改变，建议您视此报告为一个参考报告</h4>");
            }

            body2.Append("<table id=\"inWarrant\" class=\"table\" style=\"width: 100%\"><caption>入库单</caption><thead><tr>");
            body2.Append("<th width=\"10%\">单号</th>");
            body2.Append("<th width=\"30%\">提交时间</th>");
            body2.Append("<th width=\"20%\">客户</th>");
            body2.Append("<th width=\"30%\">金额</th>");
            body2.Append("<th width=\"10%\">状态</th>");
            body2.Append("</tr></thead><tbody>");
            foreach (ViewOnceCycleInWarrant warrant in viewOneCycle.inWarrantList)
            {
                body2.Append("<tr>");
                body2.Append("<td>" + warrant.number + "</td>");
                body2.Append("<td>" + warrant.submitDate + "</td>");
                body2.Append("<td>" + warrant.target_name + "</td>");
                body2.Append("<td style=\"text-align: right;\">" + warrant.amountIn.Value.MoneyNumToDisplayString() + "</td>");
                body2.Append("<td>" + warrant.stateString + "</td>");
                body2.Append("</tr>");
                switch (warrant.type)
                {
                    case DefaultWarrant.SpecificWarrantType.In:
                        amountIn += warrant.amountIn.Value;
                        break;
                    case DefaultWarrant.SpecificWarrantType.AdjustIn:
                        amountAdjustIn += warrant.amountIn.Value;
                        break;
                }
            }
            body2.Append("</tbody></table>");
            body2.Append("<table id=\"outWarrant\" class=\"table\" style=\"width: 100%; min-width: 1024px;\"><caption>出库单</caption><thead><tr>");
            body2.Append("<th width=\"5%\">单号</th>");
            body2.Append("<th width=\"11%\">销货时间</th>");
            body2.Append("<th width=\"11%\">提交时间</th>");
            body2.Append("<th width=\"5%\">类型</th>");
            body2.Append("<th width=\"13%\">客户</th>");
            body2.Append("<th width=\"13%\">服务人员</th>");
            body2.Append("<th width=\"9%\">成本</th>");
            body2.Append("<th width=\"9%\">销售金额</th>");
            body2.Append("<th width=\"9%\">服务费金额</th>");
            body2.Append("<th width=\"9%\">折让后金额</th>");
            body2.Append("<th width=\"9%\">已付款金额</th>");
            body2.Append("</tr></thead><tbody>");
            foreach (ViewOnceCycleOutWarrant warrant in viewOneCycle.outWarrantList)
            {
                body2.Append("<tr>");
                body2.Append("<td>" + warrant.number + "</td>");
                body2.Append("<td>" + warrant.submitCustomerPageDate + "</td>");
                body2.Append("<td>" + warrant.submitDate + "</td>");
                body2.Append("<td>" + warrant.specificWarrantTypeString + "</td>");
                body2.Append("<td>" + warrant.target_name + "</td>");
                body2.Append("<td>" + warrant.attendant_names + "</td>");
                body2.Append("<td style=\"text-align: right;\">" + warrant.amountCost.Value.MoneyNumToDisplayString() + "</td>");
                body2.Append("<td style=\"text-align: right;\">" + warrant.amountTotal.Value.MoneyNumToDisplayString() + "</td>");
                body2.Append("<td style=\"text-align: right;\">" + (
                        warrant.amountServiceFee == null
                        ? ""
                        : warrant.amountServiceFee.Value.MoneyNumToDisplayString()
                    ) + "</td>");
                body2.Append("<td style=\"text-align: right;\">" + (
                        warrant.amountPermit == null
                        ? ""
                        : warrant.amountPermit.Value.MoneyNumToDisplayString()
                    ) + "</td>");
                body2.Append("<td style=\"text-align: right;\">" + (
                        warrant.amountPaid == null
                        ? ""
                        : warrant.amountPaid.Value.MoneyNumToDisplayString()
                    ) + "</td>");
                body2.Append("</tr>");

                if (warrant.amountPermit != null)
                    tempAmount = warrant.amountPermit.Value;
                else
                    tempAmount = warrant.amountTotal.Value;
                if (warrant.amountPaid != null)
                    tempAmount2 = warrant.amountPaid.Value;
                else
                    tempAmount2 = 0;
                int tempAmountCost = warrant.amountCost == null ? 0 : warrant.amountCost.Value;
                switch (warrant.type)
                {
                    case DefaultWarrant.SpecificWarrantType.Service:
                        amountService += tempAmount;
                        amountServiceCost += tempAmountCost;
                        break;
                    case DefaultWarrant.SpecificWarrantType.withoutReceipt:
                    case DefaultWarrant.SpecificWarrantType.withReceipt:
                        amountSell += tempAmount;
                        amountSellTotal += warrant.amountTotal.Value;
                        amountCost += tempAmountCost;
                        amountPaid += tempAmount2;
                        break;
                    case DefaultWarrant.SpecificWarrantType.Gift:
                        amountGift += tempAmount;
                        amountGiftCost += tempAmountCost;
                        break;
                    case DefaultWarrant.SpecificWarrantType.AdjustOut:
                        amountAdjustOut += tempAmountCost;
                        break;
                }
                if (warrant.amountServiceFee != null)
                {
                    amountServiceFee += warrant.amountServiceFee.Value;
                }
            }
            body2.Append("</tbody></table>");

            body.Append("<table id=\"statistic\" class=\"table\" style=\"width: 100%\"><caption>统计</caption><thead><tr><th width=\"50%\">项目</th><th width=\"50%\">值</th></thead><tbody>");
            body.Append("<tr><td>普通入库总额</td><td>" + amountIn.MoneyNumToDisplayString() + "</td></tr>");
            body.Append("<tr><td>调整入库总额</td><td>" + amountAdjustIn.MoneyNumToDisplayString() + "</td></tr>");
            body.Append("<tr><td>销货总额</td><td>" + amountSell.MoneyNumToDisplayString() + "</td></tr>");
            body.Append("<tr><td>销货总额（不考虑折让）</td><td>" + amountSellTotal.MoneyNumToDisplayString() + "</td></tr>");
            body.Append("<tr><td>销货成本</td><td>" + amountCost.MoneyNumToDisplayString() + "</td></tr>");
            body.Append("<tr><td>销货收款总额</td><td>" + amountPaid.MoneyNumToDisplayString() + "</td></tr>");
            body.Append("<tr><td>三包总额</td><td>" + amountService.MoneyNumToDisplayString() + "</td></tr>");
            body.Append("<tr><td>三包成本</td><td>" + amountServiceCost.MoneyNumToDisplayString() + "</td></tr>");
            body.Append("<tr><td>折让总额</td><td>" + amountGift.MoneyNumToDisplayString() + "</td></tr>");
            body.Append("<tr><td>折让成本</td><td>" + amountGiftCost.MoneyNumToDisplayString() + "</td></tr>");
            body.Append("<tr><td>出库调整总额（成本）</td><td>" + amountAdjustOut.MoneyNumToDisplayString() + "</td></tr>");
            body.Append("<tr><td>服务费</td><td>" + amountServiceFee.MoneyNumToDisplayString() + "</td></tr>");
            body.Append("</tbody></table>");

            if (Function.sendMail(
                new String[] {
                    "cwb@youlinjixie.com",
                    "admin@youlinjixie.com",
                    "shfwb@youlinjixie.com",
                    "geniusye@geniusye.com",
                },
                subject,
                body.ToString() + body2.ToString(),
                true
            ))
            {
                MessageBox.Show("发送成功");
            }
            else
                MessageBox.Show("发送失败");
        }

        public void getViewAccount()
        {
            Connector.WarrantConnector warrantConnector;
            warrantConnector = new Connector.WarrantConnector(Instruction.warrantViewOneCycle, mainTransmission, warrantConnectors);
            warrantConnector.EViewOneCycle += EViewOneCycle;
            warrantConnector.getOneCycleWarrant(this.dateTimePicker.Value);
        }
    }
}
