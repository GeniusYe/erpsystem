﻿namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class DiscountInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.totalAmountLabel = new System.Windows.Forms.Label();
            this.amountPermitTextBox = new System.Windows.Forms.TextBox();
            this.discountOkButton = new System.Windows.Forms.Button();
            this.warrantIdLabel = new System.Windows.Forms.Label();
            this.LabelAmountPermit = new System.Windows.Forms.Label();
            this.LabelReason = new System.Windows.Forms.Label();
            this.reasonTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // totalAmountLabel
            // 
            this.totalAmountLabel.Location = new System.Drawing.Point(61, 28);
            this.totalAmountLabel.Name = "totalAmountLabel";
            this.totalAmountLabel.Size = new System.Drawing.Size(200, 12);
            this.totalAmountLabel.TabIndex = 1;
            this.totalAmountLabel.Text = "totalAmountLabel";
            this.totalAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // amountPermitTextBox
            // 
            this.amountPermitTextBox.Location = new System.Drawing.Point(90, 53);
            this.amountPermitTextBox.Name = "amountPermitTextBox";
            this.amountPermitTextBox.Size = new System.Drawing.Size(193, 21);
            this.amountPermitTextBox.TabIndex = 3;
            // 
            // discountOkButton
            // 
            this.discountOkButton.Location = new System.Drawing.Point(100, 126);
            this.discountOkButton.Name = "discountOkButton";
            this.discountOkButton.Size = new System.Drawing.Size(117, 21);
            this.discountOkButton.TabIndex = 6;
            this.discountOkButton.Text = "立即折让";
            this.discountOkButton.UseVisualStyleBackColor = true;
            this.discountOkButton.Click += new System.EventHandler(this.discountOkButton_Click);
            // 
            // warrantIdLabel
            // 
            this.warrantIdLabel.Location = new System.Drawing.Point(61, 9);
            this.warrantIdLabel.Name = "warrantIdLabel";
            this.warrantIdLabel.Size = new System.Drawing.Size(200, 12);
            this.warrantIdLabel.TabIndex = 0;
            this.warrantIdLabel.Text = "单号：";
            this.warrantIdLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelAmountPermit
            // 
            this.LabelAmountPermit.AutoSize = true;
            this.LabelAmountPermit.Location = new System.Drawing.Point(19, 57);
            this.LabelAmountPermit.Name = "LabelAmountPermit";
            this.LabelAmountPermit.Size = new System.Drawing.Size(65, 12);
            this.LabelAmountPermit.TabIndex = 2;
            this.LabelAmountPermit.Text = "下浮后金额";
            // 
            // LabelReason
            // 
            this.LabelReason.AutoSize = true;
            this.LabelReason.Location = new System.Drawing.Point(19, 90);
            this.LabelReason.Name = "LabelReason";
            this.LabelReason.Size = new System.Drawing.Size(53, 12);
            this.LabelReason.TabIndex = 4;
            this.LabelReason.Text = "下浮理由";
            // 
            // reasonTextBox
            // 
            this.reasonTextBox.Location = new System.Drawing.Point(90, 87);
            this.reasonTextBox.Name = "reasonTextBox";
            this.reasonTextBox.Size = new System.Drawing.Size(193, 21);
            this.reasonTextBox.TabIndex = 5;
            // 
            // DiscountInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 161);
            this.Controls.Add(this.discountOkButton);
            this.Controls.Add(this.reasonTextBox);
            this.Controls.Add(this.amountPermitTextBox);
            this.Controls.Add(this.LabelReason);
            this.Controls.Add(this.LabelAmountPermit);
            this.Controls.Add(this.warrantIdLabel);
            this.Controls.Add(this.totalAmountLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "DiscountInputForm";
            this.Text = "折扣窗体";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label totalAmountLabel;
        private System.Windows.Forms.TextBox amountPermitTextBox;
        private System.Windows.Forms.Button discountOkButton;
        private System.Windows.Forms.Label warrantIdLabel;
        private System.Windows.Forms.Label LabelAmountPermit;
        private System.Windows.Forms.Label LabelReason;
        private System.Windows.Forms.TextBox reasonTextBox;
    }
}