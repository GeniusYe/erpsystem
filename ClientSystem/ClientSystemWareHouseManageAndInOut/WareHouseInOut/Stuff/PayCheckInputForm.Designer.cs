﻿namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class PayCheckInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.totalAmountLabel = new System.Windows.Forms.Label();
            this.payCheckOkButton = new System.Windows.Forms.Button();
            this.warrantIdLabel = new System.Windows.Forms.Label();
            this.LabelActuallyPaid = new System.Windows.Forms.Label();
            this.actuallyPaidTextBox = new System.Windows.Forms.TextBox();
            this.amountPaidLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // totalAmountLabel
            // 
            this.totalAmountLabel.Location = new System.Drawing.Point(61, 32);
            this.totalAmountLabel.Name = "totalAmountLabel";
            this.totalAmountLabel.Size = new System.Drawing.Size(200, 12);
            this.totalAmountLabel.TabIndex = 1;
            this.totalAmountLabel.Text = "totalAmountLabel";
            this.totalAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // payCheckOkButton
            // 
            this.payCheckOkButton.Location = new System.Drawing.Point(100, 114);
            this.payCheckOkButton.Name = "payCheckOkButton";
            this.payCheckOkButton.Size = new System.Drawing.Size(117, 21);
            this.payCheckOkButton.TabIndex = 6;
            this.payCheckOkButton.Text = "确定";
            this.payCheckOkButton.UseVisualStyleBackColor = true;
            this.payCheckOkButton.Click += new System.EventHandler(this.payCheckOkButton_Click);
            // 
            // warrantIdLabel
            // 
            this.warrantIdLabel.Location = new System.Drawing.Point(61, 9);
            this.warrantIdLabel.Name = "warrantIdLabel";
            this.warrantIdLabel.Size = new System.Drawing.Size(200, 12);
            this.warrantIdLabel.TabIndex = 0;
            this.warrantIdLabel.Text = "单号：";
            this.warrantIdLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelActuallyPaid
            // 
            this.LabelActuallyPaid.AutoSize = true;
            this.LabelActuallyPaid.Location = new System.Drawing.Point(19, 90);
            this.LabelActuallyPaid.Name = "LabelActuallyPaid";
            this.LabelActuallyPaid.Size = new System.Drawing.Size(53, 12);
            this.LabelActuallyPaid.TabIndex = 4;
            this.LabelActuallyPaid.Text = "实付金额";
            // 
            // actuallyPaidTextBox
            // 
            this.actuallyPaidTextBox.Location = new System.Drawing.Point(90, 87);
            this.actuallyPaidTextBox.Name = "actuallyPaidTextBox";
            this.actuallyPaidTextBox.Size = new System.Drawing.Size(193, 21);
            this.actuallyPaidTextBox.TabIndex = 5;
            // 
            // amountPaidLabel
            // 
            this.amountPaidLabel.Location = new System.Drawing.Point(61, 57);
            this.amountPaidLabel.Name = "amountPaidLabel";
            this.amountPaidLabel.Size = new System.Drawing.Size(200, 12);
            this.amountPaidLabel.TabIndex = 1;
            this.amountPaidLabel.Text = "amountPaidLabel";
            this.amountPaidLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PayCheckInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 146);
            this.Controls.Add(this.payCheckOkButton);
            this.Controls.Add(this.actuallyPaidTextBox);
            this.Controls.Add(this.LabelActuallyPaid);
            this.Controls.Add(this.warrantIdLabel);
            this.Controls.Add(this.amountPaidLabel);
            this.Controls.Add(this.totalAmountLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "PayCheckInputForm";
            this.Text = "订单支付";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label totalAmountLabel;
        private System.Windows.Forms.Button payCheckOkButton;
        private System.Windows.Forms.Label warrantIdLabel;
        private System.Windows.Forms.Label LabelActuallyPaid;
        private System.Windows.Forms.TextBox actuallyPaidTextBox;
        private System.Windows.Forms.Label amountPaidLabel;
    }
}