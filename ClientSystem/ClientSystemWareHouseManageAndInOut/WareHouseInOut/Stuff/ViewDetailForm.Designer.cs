﻿namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class ViewDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.detailAccountDisplayListBoxGroupBox = new System.Windows.Forms.GroupBox();
            this.reloadDetailButton = new System.Windows.Forms.Button();
            this.stopDatePicker = new System.Windows.Forms.DateTimePicker();
            this.startDatePicker = new System.Windows.Forms.DateTimePicker();
            this.detailAccountDisplayGridView = new System.Windows.Forms.DataGridView();
            this.detailAccountDisplayListBoxGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detailAccountDisplayGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // detailAccountDisplayListBoxGroupBox
            // 
            this.detailAccountDisplayListBoxGroupBox.Controls.Add(this.reloadDetailButton);
            this.detailAccountDisplayListBoxGroupBox.Controls.Add(this.stopDatePicker);
            this.detailAccountDisplayListBoxGroupBox.Controls.Add(this.startDatePicker);
            this.detailAccountDisplayListBoxGroupBox.Controls.Add(this.detailAccountDisplayGridView);
            this.detailAccountDisplayListBoxGroupBox.Location = new System.Drawing.Point(12, 12);
            this.detailAccountDisplayListBoxGroupBox.Name = "detailAccountDisplayListBoxGroupBox";
            this.detailAccountDisplayListBoxGroupBox.Size = new System.Drawing.Size(880, 431);
            this.detailAccountDisplayListBoxGroupBox.TabIndex = 0;
            this.detailAccountDisplayListBoxGroupBox.TabStop = false;
            // 
            // reloadDetailButton
            // 
            this.reloadDetailButton.Location = new System.Drawing.Point(757, 133);
            this.reloadDetailButton.Name = "reloadDetailButton";
            this.reloadDetailButton.Size = new System.Drawing.Size(117, 24);
            this.reloadDetailButton.TabIndex = 3;
            this.reloadDetailButton.Text = "重新加载";
            this.reloadDetailButton.UseVisualStyleBackColor = true;
            this.reloadDetailButton.Click += new System.EventHandler(this.reloadDetailButton_Click);
            // 
            // stopDatePicker
            // 
            this.stopDatePicker.Location = new System.Drawing.Point(757, 83);
            this.stopDatePicker.Name = "stopDatePicker";
            this.stopDatePicker.Size = new System.Drawing.Size(118, 21);
            this.stopDatePicker.TabIndex = 2;
            // 
            // startDatePicker
            // 
            this.startDatePicker.Location = new System.Drawing.Point(757, 38);
            this.startDatePicker.Name = "startDatePicker";
            this.startDatePicker.Size = new System.Drawing.Size(118, 21);
            this.startDatePicker.TabIndex = 1;
            // 
            // detailAccountDisplayGridView
            // 
            this.detailAccountDisplayGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detailAccountDisplayGridView.Location = new System.Drawing.Point(12, 20);
            this.detailAccountDisplayGridView.Name = "detailAccountDisplayGridView";
            this.detailAccountDisplayGridView.RowTemplate.Height = 23;
            this.detailAccountDisplayGridView.Size = new System.Drawing.Size(739, 405);
            this.detailAccountDisplayGridView.TabIndex = 0;
            // 
            // ViewDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1074, 455);
            this.Controls.Add(this.detailAccountDisplayListBoxGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ViewDetailForm";
            this.Text = "明细查询";
            this.detailAccountDisplayListBoxGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detailAccountDisplayGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox detailAccountDisplayListBoxGroupBox;
        private System.Windows.Forms.DataGridView detailAccountDisplayGridView;
        private System.Windows.Forms.Button reloadDetailButton;
        private System.Windows.Forms.DateTimePicker stopDatePicker;
        private System.Windows.Forms.DateTimePicker startDatePicker;
    }
}