﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClassObjectToNum;
using ClassObjectToNum.DateTime;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using ClassObjectToNum.Money;

namespace ClientSystem.WareHouseInOut.Stuff
{
    public partial class WarrantDetailBox : UserControl
    {

        private List<ClientSystem.WareHouseInOut.Stuff.AttendantPicker> attendantPicker = new List<AttendantPicker>();

        //transmission
        private Transmission.Transmission mainTransmission;
        private List<Transmission.Connector> warrantConnectors = new List<Transmission.Connector>();
        public List<Transmission.Connector> customerConnectors { get; set; }

        private AbstractWarrant _warrant;
        private List<SharedObject.CustomerManage.Stuff.DefaultCustomer> _warrantCustomersList;
        private List<SharedObject.WareHouseInOut.Stuff.DefaultAttendant> _warrantAttendantsList;
        private bool _readOnly, _visiblePriceIn;

        public delegate void DCustomersList(List<SharedObject.CustomerManage.Stuff.DefaultCustomer> lists);
        public delegate void DAttendantsList(List<SharedObject.WareHouseInOut.Stuff.DefaultAttendant> lists);
        public delegate void DSpecialPrice(List<SpecialPriceSC.DraftWithSpecialPrice> specialPrice);
        public delegate void DDoubleClick(int draftId);

        protected CustomerManage.Connector.CustomerConnector.DCustomerList ECustomersList;
        protected Connector.WarrantConnector.DAttendantList EAttendantList;
        protected Connector.WarrantConnector.DOutWarrantSpecialPrice EOutWarrantSpecialPrice;
        public new event DDoubleClick DoubleClick;

        CustomerManage.Connector.CustomerConnector customerConnector;
        WareHouseInOut.Connector.WarrantConnector warrantConnector;

        public AbstractWarrant warrant
        {
            get 
            { 
                if (_warrant != null && CommentTextBox != null)
                    _warrant.comment = CommentTextBox.Text.Trim();
                return _warrant;
            }
            set
            {
                _warrant = value;
                if (_warrant == null)
                    return;
                displayWarrant();
                if (_warrant is InWarrant)
                {
                    customerConnector = new CustomerManage.Connector.CustomerConnector(Instruction.customerInSourceList, mainTransmission, customerConnectors);
                    customerConnector.ECustomerList += ECustomersList;
                    customerConnector.getWarrantCustomersList();
                }
                else if (_warrant is OutWarrant)
                {
                    customerConnector = new CustomerManage.Connector.CustomerConnector(Instruction.customerOutCustomerList, mainTransmission, customerConnectors);
                    customerConnector.ECustomerList += ECustomersList;
                    customerConnector.getWarrantCustomersList();
                    warrantConnector = new Connector.WarrantConnector(Instruction.warrantAttendantsList, mainTransmission, warrantConnectors);
                    warrantConnector.EAttendantList += EAttendantList;
                    warrantConnector.getAttendantsList();
                }
            }
        }
        
        /// <summary>
        /// readOnly mark whether user can edit warrant
        /// readOnly can set the enabled when readOnly is set
        /// </summary>
        public bool readOnly
        {
            get
            {
                return _readOnly;
            }
            set
            {
                _readOnly = value;
                setAccessiable();
            }
        }

        /// <summary>
        /// visiblePriceIn mark whether user can see the priceIn
        /// visiblePriceIn can set the visible when visiblePriceIn is set
        /// </summary>
        public bool visiblePriceIn
        {
            get
            {
                return _visiblePriceIn;
            }
            set
            {
                _visiblePriceIn = value;
                setVisible();
            }
        }

        public WarrantDetailBox(Transmission.Transmission mainTransmission)
        {
            InitializeComponent();
            customerConnectors = new List<Transmission.Connector>();
            this.mainTransmission = mainTransmission;

            numberTextBox.ReadOnly = true;
            initGridView();

            ECustomersList = new CustomerManage.Connector.CustomerConnector.DCustomerList(list => this.Invoke(new DCustomersList(mainTransmission_responseForCustomersList), new object[] { list }));
            EOutWarrantSpecialPrice = new Connector.WarrantConnector.DOutWarrantSpecialPrice(price => this.Invoke(new DSpecialPrice(mainTransmission_responseForSpecialPrice), new object[] { price }));
            EAttendantList = new Connector.WarrantConnector.DAttendantList(list => this.Invoke(new DAttendantsList(mainTransmission_responseForAttendantsList), new object[] { list }));

            if (mainTransmission != null)
            {
                mainTransmission.addConnectors(SharedObject.Normal.SystemID.WareHouseInOut, warrantConnectors);
                mainTransmission.addConnectors(SharedObject.Normal.SystemID.CustomerManage, customerConnectors);
            }
            else
                throw new Transmission.TransmissionIsNullException();

            this.itemGridView.DoubleClick += new EventHandler(itemGridView_DoubleClick);
            this.targetPicker.ECustomerChosen += new CustomerManage.Stuff.CustomerPicker.DCustomerChosen(targetPicker_ECustomerChosen);
            foreach (AttendantPicker picker in this.attendantPicker)
                picker.EAttendantChosen += new AttendantPicker.DAttendantChosen(AttendantPicker_EAttendantChosen);
            this.Disposed += new EventHandler(WarrantDetailBox_Disposed);
        }

        private void itemGridView_DoubleClick(object sender, EventArgs e)
        {
            if (itemGridView.SelectedColumns.Count > 0)
                return;
            int index;
            if (itemGridView.SelectedRows.Count > 0)
                index = itemGridView.SelectedRows[0].Index;
            else
                return;
            int draftId;
            if (_warrant is InWarrant)
            {
                draftId = ((InWarrant)_warrant).items[index].IDdraft;
            }
            else if (_warrant is OutWarrant)
            {
                draftId = ((OutWarrant)_warrant).items[index].IDdraft;
            }
            else
                return;
            if (DoubleClick != null)
                DoubleClick(draftId);
        }

        private void initGridView()
        {
            itemGridView.Columns.Add("Number", "序号");               //0
            itemGridView.Columns.Add("Name", "品名");                 //1
            itemGridView.Columns.Add("Specification", "规格");        //2
            itemGridView.Columns.Add("num", "数量");                  //3
            itemGridView.Columns.Add("numReturn", "退回数量");        //4
            itemGridView.Columns.Add("PriceIn", "成本价");            //5
            itemGridView.Columns.Add("Price", "单价");                //6
            itemGridView.Columns.Add("PriceAdjust", "调整单价");      //7
            itemGridView.Columns.Add("PriceInAll", "成本金额");       //8
            itemGridView.Columns.Add("PriceAll", "金额");             //9
            itemGridView.Columns.Add("Remark", "备注");               //10
            itemGridView.Columns.Add("HiddenSpecification", "说明");  //11
            itemGridView.Columns["PriceIn"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            itemGridView.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            itemGridView.Columns["PriceAdjust"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            itemGridView.Columns["num"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            itemGridView.Columns["numReturn"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            itemGridView.Columns["PriceInAll"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            itemGridView.Columns["PriceAll"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            for (int i = 0; i < 12; i++)
            {
                itemGridView.Columns[i].ReadOnly = true;
                itemGridView.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            readOnly = true;
            //itemGridView.MultiSelect = false;
            itemGridView.AllowUserToAddRows = false;
            itemGridView.AllowUserToDeleteRows = false;
        }

        private void WarrantDetailBox_Disposed(object sender, EventArgs e)
        {
            while (warrantConnectors.Count > 0)
                warrantConnectors[0].Dispose();
            while (customerConnectors.Count > 0)
                customerConnectors[0].Dispose();
            mainTransmission.removeConnectors(SharedObject.Normal.SystemID.WareHouseInOut, warrantConnectors);
            mainTransmission.removeConnectors(SharedObject.Normal.SystemID.CustomerManage, customerConnectors);
        }
    }
}
