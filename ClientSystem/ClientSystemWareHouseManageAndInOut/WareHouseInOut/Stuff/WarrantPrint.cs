﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SharedObject.WareHouseInOut.Stuff;
using Printer;
using System.Drawing.Printing;
using ClassObjectToNum.Money;
using System.Windows.Forms;

namespace ClientSystem.WareHouseInOut.Stuff
{
    class WarrantPrint
    {
        private Printer.Printer printer;
        private string customer_name, attendant_name, marker_name;

        private static readonly int scaleTitleLeft = 320;
        private static readonly int scaleTitleTop = 0;
        private static readonly int scaleTopInfoLeft = 10;
        private static readonly int scaleTopInfoRLeft = 600;
        private static readonly int scaleTopInfoMLeft = 400;
        private static readonly int scaleTopInfoTop = 40;
        private static readonly int TopInfoHeight = 20;
        
        int ItemsNum = 0;
        int[] scaleDetailLeft;
        int[] scaleDetailStringLeft;
        string[] HeaderString;
        string stringDate, stringPrintTimes, strWarrantID, stringComment, bottom1, bottom2, title;
        int? targetID;
        private static readonly int scaleHeaderTop = 80;
        private static readonly int HeaderHeight = 28;
        private static readonly int DetailHeight = 28;
        private static readonly int scaleDetailStringTop = 8;
        private static readonly int scaleBottomInfoLeft = 10;
        //private static readonly int scaleAmountTop = 380;
        //private static readonly int scaleAmountLeft = 20;
        private static readonly int scaleBottomInfoTop = 430;
        private static readonly int BottomInfoHeight = 20;
        Margins margin;

        Font TitleFont = new Font("华文行楷", 15, FontStyle.Regular);
        Font topInfoFont = new Font("宋体", 10, FontStyle.Regular);
        Font HeaderFont = new Font("宋体", 13, FontStyle.Regular);
        Font DetailFont = new Font("宋体", 10, FontStyle.Regular);
        Font bottomFont = new Font("宋体", 10, FontStyle.Regular);

        public WarrantPrint(string customer_name, string attendant_name, string marker_name)
        {
            this.customer_name = customer_name;
            this.attendant_name = attendant_name;
            this.marker_name = marker_name;
        }

        private Tuple<int[], int[]> updateGridWithOffset(int[] ori)
        {
            int totalOffsetLeft = 0;
            int[] offsets = new int[ori.Length];
            int[] stringOffsets = new int[ori.Length];
            for (int i = 0; i < ori.Length; i++) {
                offsets[i] = ori[i] + totalOffsetLeft;
                stringOffsets[i] = offsets[i] + 1;
            }
            return Tuple.Create(offsets, stringOffsets);
        }

        private bool printSpecification()
        {
            DialogResult dialogResult = MessageBox.Show("打印规格？", "打印选项", MessageBoxButtons.YesNo);
            return dialogResult == DialogResult.Yes;
        }

        private bool printCost()
        {
            DialogResult dialogResult = MessageBox.Show("打印成本？", "打印选项", MessageBoxButtons.YesNo);
            return dialogResult == DialogResult.Yes;
        }

        private bool printValue()
        {
            DialogResult dialogResult = MessageBox.Show("打印售价？", "打印选项", MessageBoxButtons.YesNo);
            return dialogResult == DialogResult.Yes;
        }

        public void printInWarrant(InWarrant inWarrant)
        {
            printer = new Printer.Printer();
            int[] oriScale = new int[] { 0, 40, 300, 375, 445, 520, 555, 760 };
            var result = updateGridWithOffset(oriScale);
            scaleDetailLeft = result.Item1;
            scaleDetailStringLeft = result.Item2;
            HeaderString = new string[] {
                "序号",
                "品名及规格",
                "数量",
                "单价",
                "金额",
                "库位",
                "备注",
            };
            margin = new Margins();
            margin.Left = 40;
            margin.Top = 0;
            printer.Margins = margin;

            switch (inWarrant.type)
            {
                case DefaultWarrant.SpecificWarrantType.In:
                    title = "    入库清单    ";
                    break;
                case DefaultWarrant.SpecificWarrantType.AdjustIn:
                    title = " 仓库调整入库单 ";
                    break;
            }

            DateTime date;
            WarrantOperationRecord submitRecord = inWarrant.submitRecord;
            if (submitRecord != null)
            {
                date = submitRecord.operatedTime;
                stringDate = date.ToLongDateString() + date.ToLongTimeString();
            }
            else
                stringDate = "";
            stringPrintTimes = "第" + (inWarrant.printTimes + 1) + "次打印";
            strWarrantID = inWarrant.number;
            bottom1 = "保管：                               入库经手人：                              ";
            bottom2 = "第一联：财务联。第二联：仓库联。";
            int top;
            int amountTemp;
            top = scaleHeaderTop + HeaderHeight + scaleDetailStringTop;
            ItemsNum = inWarrant.items.Count;
            int i = 0;
            foreach (InWarrantItem row in inWarrant.items)
            {
                PrintDocumentItem item;
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[0], top, ++i });
                printer.addDocumentItem(item);
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[1], top, row.name + ">" + row.specification });
                printer.addDocumentItem(item);
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[2], top, row.num + "(" + row.unit + ")" });
                printer.addDocumentItem(item);
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[3], top, ClassMoney.MoneyNumToDisplayString(row.price) });
                printer.addDocumentItem(item);
                amountTemp = row.num * row.price;
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[4], top, ClassMoney.MoneyNumToDisplayString(amountTemp) });
                printer.addDocumentItem(item);
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[5], top, row.IDdraft });
                printer.addDocumentItem(item);
                printer.addDocumentItem(new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[6], top, row.remark }));
                top += DetailHeight;
            }
            printer.addDocumentItem(new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[4], top, ClassMoney.MoneyNumToDisplayString(inWarrant.amountIn == null ? 0 : inWarrant.amountIn.Value) }));
            //item = new PrintDocumentItem(ItemType.String, new Object[] { scaleAmountLeft, scaleAmountTop, "总额：" + ClassMoney.MoneyNumToDisplayLString(amountTotal) });
            //printer.addDocumentItem(item);
            targetID = inWarrant.target;
            stringComment = inWarrant.comment;
            printShell(DefaultWarrant.SpecificWarrantType.In, new string[1][] { HeaderString }, new int[1][] { scaleDetailLeft });
            print(true);
        }

        public void printOutCustomerPage1Warrant(OutWarrant outWarrant)
        {
            bool printSpecification = this.printSpecification();

            printer = new Printer.Printer();
            int[] oriScale = new int[] { 0, 40, 340, 415, 475, 560, 595, 760 };
            var result = updateGridWithOffset(oriScale);
            scaleDetailLeft = result.Item1;
            scaleDetailStringLeft = result.Item2;
            HeaderString = new string[] {
                "序号",
                "品名及规格",
                "数量",
                "单价",
                "金额",
                "库位",
                "备注",
            };
            margin = new Margins();
            margin.Left = 40;
            margin.Top = 0;
            printer.Margins = margin;

            switch (outWarrant.type)
            {
                case DefaultWarrant.SpecificWarrantType.withoutReceipt:
                case DefaultWarrant.SpecificWarrantType.withReceipt:
                    title = " 配件及服务清单 ";
                    break;
                case DefaultWarrant.SpecificWarrantType.Service:
                    title = "三包更换配件清单";
                    break;
                case DefaultWarrant.SpecificWarrantType.Gift:
                    title = " 配件及服务清单A ";
                    break;
                case DefaultWarrant.SpecificWarrantType.AdjustOut:
                    title = "  仓库调整清单  ";
                    break;
            }
            DateTime date;
            WarrantOperationRecord submitCustomerPageRecord = outWarrant.submitCustomerPageRecord;
            if (submitCustomerPageRecord != null)
            {
                date = submitCustomerPageRecord.operatedTime;
                stringDate = date.ToLongDateString() + date.ToLongTimeString();
            }
            else
                stringDate = "";
            stringPrintTimes = "第" + (outWarrant.printCustomerPageTimes + 1) + "次打印";
            strWarrantID = outWarrant.number;
            bottom1 = "制单人：                       保管：                       经办人：                       ";
            bottom2 = "第一联：存根  第二联：会计  第三联：统计  第四联：客户签字联  第五联：客户联。";
            int top;
            int price;
            int amountTemp;
            int amountTotal = 0;
            top = scaleHeaderTop + HeaderHeight + scaleDetailStringTop;
            int? priceAdjust;
            ItemsNum = outWarrant.items.Count;
            int i = 0;
            foreach (OutWarrantItem row in outWarrant.items)
            {
                PrintDocumentItem item;
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[0], top, ++i });
                printer.addDocumentItem(item);
                item = new PrintDocumentItem(ItemType.String, new Object[] {
                    scaleDetailStringLeft[1],
                    top,
                    row.name + (printSpecification ? ">" + row.specification : "")
                });
                printer.addDocumentItem(item);
                priceAdjust = row.priceAdjustMannual;
                if (priceAdjust != null && priceAdjust != 0)
                    price = priceAdjust.Value;
                else
                    price = row.price;
                if (outWarrant.type == DefaultWarrant.SpecificWarrantType.AdjustOut)
                {
                    item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[2], top, row.num + "(" + row.unit + ")" });
                    printer.addDocumentItem(item);
                    item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[3], top, "--" /* ClassMoney.MoneyNumToDisplayString(price) */ });
                    printer.addDocumentItem(item);
                    //amountTemp = row.num * price;
                    //amountTotal += amountTemp;
                    item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[4], top, "--" /* ClassMoney.MoneyNumToDisplayString(amountTemp) */ });
                    printer.addDocumentItem(item);
                }
                else
                {
                    item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[2], top, row.num + "(" + row.unit + ")" });
                    printer.addDocumentItem(item);
                    item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[3], top, ClassMoney.MoneyNumToDisplayString(price) });
                    printer.addDocumentItem(item);
                    amountTemp = row.num * price;
                    amountTotal += amountTemp;
                    item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[4], top, ClassMoney.MoneyNumToDisplayString(amountTemp) });
                    printer.addDocumentItem(item);
                }
                printer.addDocumentItem(new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[5], top, row.IDdraft }));
                printer.addDocumentItem(new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[6], top, row.remark }));
                top += DetailHeight;
            }

            if (outWarrant.amountPermit != null)
            {
                PrintDocumentItem item;
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[1], top, "折让" });
                printer.addDocumentItem(item);
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[4], top, "-" + ClassMoney.MoneyNumToDisplayString(outWarrant.amountTotal.Value - outWarrant.amountPermit.Value) });
                printer.addDocumentItem(item);
                top += DetailHeight;
                ItemsNum++;
            }
            
            if (outWarrant.type == DefaultWarrant.SpecificWarrantType.AdjustOut)
            {
                printer.addDocumentItem(new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[4], top, "--" /* ClassMoney.MoneyNumToDisplayString(amountTotal) */ }));
            }
            else
            {
                PrintDocumentItem item;
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[4], top, ClassMoney.MoneyNumToDisplayString(amountTotal) });
                printer.addDocumentItem(item);
                string amountTotalStr = ClassMoney.MoneyNumToDisplayString(outWarrant.amountTotal.Value);
                string amountPermitStr = "";
                if (outWarrant.amountPermit != null)
                    amountPermitStr = ClassMoney.MoneyNumToDisplayString(outWarrant.amountPermit.Value);

                if (amountTotal != outWarrant.amountTotal.Value)
                {
                    // this should not happen, the amount total stored in DB should be consistent with sum of all items
                    Console.WriteLine("Amount total discrepency. amount total not equal to DB amountTotal. ID: ", outWarrant.ID);
                }

                item = new PrintDocumentItem(ItemType.String, new Object[] {
                    scaleDetailStringLeft[2],
                    top + 1.5 * DetailHeight, 
                    "订单金额：" + amountTotalStr,
                });
                printer.addDocumentItem(item);
                if (outWarrant.amountPermit != null)
                {
                    amountPermitStr = ClassMoney.MoneyNumToDisplayString(outWarrant.amountPermit.Value);
                    item = new PrintDocumentItem(ItemType.String, new Object[] { 
                    scaleDetailStringLeft[2] + 20, top + 2.5 * DetailHeight, "折让后订单金额：" + amountPermitStr });
                    printer.addDocumentItem(item);
                }
            }
            targetID = outWarrant.target;
            stringComment = outWarrant.comment;
            printShell(outWarrant.type, new string[1][] { HeaderString }, new int[1][] { scaleDetailLeft });
            print(true);
        }

        public void printOutWarrant(OutWarrant outWarrant)
        {
            bool printCost = this.printCost();
            bool printValue = false;
            if (outWarrant.type != DefaultWarrant.SpecificWarrantType.AdjustOut)
                printValue = this.printValue();
            printer = new Printer.Printer();
            int[] oriScale = new int[10] { 0, 40, 275, 325, 375, 455, 535, 615, 695, 760 };
            var result = updateGridWithOffset(oriScale);
            scaleDetailLeft = result.Item1;
            scaleDetailStringLeft = result.Item2;

            HeaderString = new string[9] {
                "",
                "名称和规格",
                "数量",
                "实数量",
                "成本价",
                "单价",
                "成本金额",
                "金额",
                "备注",
            };
            margin = new Margins();
            margin.Left = 40;
            margin.Top = 0;
            printer.Margins = margin;

            switch (outWarrant.type)
            {
                case DefaultWarrant.SpecificWarrantType.withoutReceipt:
                case DefaultWarrant.SpecificWarrantType.withReceipt:
                    title = "   出库清单    ";
                    break;
                case DefaultWarrant.SpecificWarrantType.Service:
                    title = "  三包出库清单  ";
                    break;
                case DefaultWarrant.SpecificWarrantType.Gift:
                    title = "   出库清单A";
                    break;
                case DefaultWarrant.SpecificWarrantType.AdjustOut:
                    title = " 仓库调整出库清单";
                    break;
            }

            DateTime date;
            WarrantOperationRecord submitRecord = outWarrant.submitRecord;
            if (submitRecord != null)
            {
                date = submitRecord.operatedTime;
                stringDate = date.ToLongDateString() + date.ToLongTimeString();
            }
            else
                stringDate = "";
            stringPrintTimes = "第" + (outWarrant.printTimes + 1) + "次打印";
            strWarrantID = outWarrant.number;
            bottom1 = "负责人：            制单人：            收款人：            保管：            经办人（" + attendant_name + "）：";
            bottom2 = "第一联：仓库联。第二联：记账联。第三联：存根联";
            int top;
            int amountTemp;
            top = scaleHeaderTop + HeaderHeight + scaleDetailStringTop;   
            int numOri, num, price;
            int? priceAdjust;
            ItemsNum = outWarrant.items.Count;
            int i = 0;
            foreach (OutWarrantItem row in outWarrant.items)
            {
                PrintDocumentItem item;
                priceAdjust = row.priceAdjustMannual;
                if (priceAdjust != null && priceAdjust != 0)
                    price = priceAdjust.Value;
                else
                    price = row.price;
                numOri = row.num;
                num = row.num - row.numReturn;
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[0], top, ++i });
                printer.addDocumentItem(item);
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[1], top, row.name + ">" + row.specification });
                printer.addDocumentItem(item);
                printer.addDocumentItem(new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[8], top, row.IDdraft + " " + row.remark }));
                if (outWarrant.type == DefaultWarrant.SpecificWarrantType.AdjustOut)
                {
                    item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[2], top, row.num + "(" + row.unit + ")" });
                    printer.addDocumentItem(item);
                    item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[3], top, num });
                    printer.addDocumentItem(item);
                    item = new PrintDocumentItem(
                        ItemType.String,
                        new Object[] {
                            scaleDetailStringLeft[4],
                            top,
                            (printCost ? ClassMoney.MoneyNumToDisplayString(row.priceCost) : "--"),
                        }
                    );
                    printer.addDocumentItem(item);
                    item = new PrintDocumentItem(
                        ItemType.String,
                        new Object[] {
                            scaleDetailStringLeft[5],
                            top,
                            "--", // Sale price is not relevant for adjustment type
                        }
                    );
                    printer.addDocumentItem(item);
                    amountTemp = row.priceCostValue;
                    item = new PrintDocumentItem(
                        ItemType.String,
                        new Object[] {
                            scaleDetailStringLeft[6],
                            top,
                            (printCost ? ClassMoney.MoneyNumToDisplayString(amountTemp) : "--"),
                        }
                    );
                    printer.addDocumentItem(item);
                    item = new PrintDocumentItem(
                        ItemType.String,
                        new Object[] {
                            scaleDetailStringLeft[7],
                            top,
                            "--", // Sale price is not relevant for adjustment type
                        }
                    );
                    printer.addDocumentItem(item);
                }
                else
                {
                    item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[2], top, row.num });
                    printer.addDocumentItem(item);
                    item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[3], top, num });
                    printer.addDocumentItem(item);
                    item = new PrintDocumentItem(
                        ItemType.String,
                        new Object[] {
                            scaleDetailStringLeft[4],
                            top,
                            (printCost ? ClassMoney.MoneyNumToDisplayString(row.priceCost) : "--"),
                        }
                    );
                    printer.addDocumentItem(item);
                    item = new PrintDocumentItem(
                        ItemType.String,
                        new Object[] {
                            scaleDetailStringLeft[5],
                            top,
                            (printValue ? ClassMoney.MoneyNumToDisplayString(price) : "--"),
                        }
                    );
                    printer.addDocumentItem(item);
                    item = new PrintDocumentItem(
                        ItemType.String,
                        new Object[] {
                            scaleDetailStringLeft[6],
                            top,
                            (printCost ? ClassMoney.MoneyNumToDisplayString(row.priceCostValue) : "--"),
                        }
                    );
                    printer.addDocumentItem(item);
                    amountTemp = num * price;
                    item = new PrintDocumentItem(
                        ItemType.String,
                        new Object[] {
                            scaleDetailStringLeft[7],
                            top,
                            (printValue ? ClassMoney.MoneyNumToDisplayString(amountTemp) : "--")
                        }
                    );
                    printer.addDocumentItem(item);
                }
                top += DetailHeight;
            }

            if (outWarrant.amountPermit != null)
            {
                PrintDocumentItem item;
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[1], top, "折让" });
                printer.addDocumentItem(item);
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleDetailStringLeft[6], top, "-" + ClassMoney.MoneyNumToDisplayString(outWarrant.amountTotal.Value - outWarrant.amountPermit.Value) });
                printer.addDocumentItem(item);
                top += DetailHeight;
                ItemsNum++;
            }

            // some type of receipt 
            bool costType = true;
            bool valueType = true;
            if (outWarrant.type == DefaultWarrant.SpecificWarrantType.AdjustOut)
            {
                valueType = false;
            }

            if (costType && printCost)
            {
                PrintDocumentItem item;
                string amountCostStr = ClassMoney.MoneyNumToDisplayString(outWarrant.amountCost.Value);
                item = new PrintDocumentItem(ItemType.String, new Object[] {
                    scaleDetailStringLeft[3] + 20,
                    top + 0.5 * DetailHeight,
                    "成本：" + amountCostStr,
                });
                printer.addDocumentItem(item);
            }
            if (valueType && printValue)
            {
                PrintDocumentItem item;
                string amountTotalStr = ClassMoney.MoneyNumToDisplayString(outWarrant.amountTotal.Value);
                string amountPermitStr = "";
                item = new PrintDocumentItem(ItemType.String, new Object[] { 
                    scaleDetailStringLeft[5] + 20,
                    top + 0.5 * DetailHeight,
                    "销售额:" + amountTotalStr,
                });
                printer.addDocumentItem(item);
                if (outWarrant.amountPermit != null)
                {
                    amountPermitStr = ClassMoney.MoneyNumToDisplayString(outWarrant.amountPermit.Value);
                    item = new PrintDocumentItem(ItemType.String, new Object[] { 
                    scaleDetailStringLeft[4] + 20, top + 1.5 * DetailHeight, "下浮后订单金额：" + amountPermitStr });
                    printer.addDocumentItem(item);
                }
            }
            targetID = outWarrant.target;
            stringComment = outWarrant.comment;
            printShell(outWarrant.type, new string[][] { HeaderString }, new int[][] { scaleDetailLeft });
            print(true);
        }

        private void printShell(DefaultWarrant.SpecificWarrantType type, string[][] headers, int[][] scales)
        {
            Printer.PrintDocumentItem item;
            int top;
            item = new PrintDocumentItem(ItemType.String, TitleFont, null, null, new Object[] { scaleTitleLeft, scaleTitleTop, title });
            printer.addDocumentItem(item);
            top = scaleTopInfoTop;
            item = new PrintDocumentItem(ItemType.String, new Object[] { scaleTopInfoLeft, top, stringDate + " " + stringPrintTimes });
            printer.addDocumentItem(item);
            item = new PrintDocumentItem(ItemType.String, new Object[] { scaleTopInfoRLeft, top, strWarrantID });
            printer.addDocumentItem(item);
            top += TopInfoHeight;
            if (targetID > 0)
            {
                string target_name_representation;
                if (type == DefaultWarrant.SpecificWarrantType.In)
                {
                    target_name_representation = "供货商";
                }
                else
                {
                    target_name_representation = "用户姓名";
                }
                item = new PrintDocumentItem(
                    ItemType.String,
                    new Object[] {
                        scaleTopInfoLeft,
                        top,
                        target_name_representation + "：" + customer_name,
                    }
                );
                printer.addDocumentItem(item);
            }
            if (stringComment != null)
            {
                item = new PrintDocumentItem(ItemType.String, new Object[] { scaleTopInfoMLeft, top, stringComment });
                printer.addDocumentItem(item);
            }
            top = scaleHeaderTop;

            // headers
            {
                int headerTop = top;
                // print header strings
                for (int j = 0; j < headers.Length; j++)
                {
                    int[] scale = scales[j];
                    for (int k = 0; k < headers[j].Length; k++)
                    {
                        printer.addDocumentItem(new PrintDocumentItem(
                            ItemType.String,
                            new Object[] {
                                    scale[k] + 1,
                                    headerTop + scaleDetailStringTop,
                                    headers[j][k],
                            }
                        ));
                    }
                    headerTop += HeaderHeight;
                }

                // header frames
                for (int j = 0; j < headers.Length; j++)
                {
                    int[] scale = scales[j];
                    int leftmost = scale[0];
                    int rightmost = scale[scale.Length - 1];
                    // top line
                    printer.addDocumentItem(new PrintDocumentItem(
                        ItemType.Line,
                        new Object[] {
                            leftmost,
                            top,
                            rightmost,
                            top,
                        }
                    ));
                    // bottom line
                    printer.addDocumentItem(new PrintDocumentItem(
                        ItemType.Line,
                        new Object[] {
                            leftmost,
                            top + HeaderHeight,
                            rightmost,
                            top + HeaderHeight,
                        }
                    ));
                    // vertical line
                    for (int k = 0; k < scale.Length; k++)
                    {
                        printer.addDocumentItem(new PrintDocumentItem(
                            ItemType.Line,
                            new Object[] {
                                scale[k],
                                top,
                                scale[k],
                                top + HeaderHeight,
                            }
                        ));
                    }
                    top += HeaderHeight;
                }
            }

            // main frames
            for (int i = 0; i < ItemsNum; i++)
            {
                // print all frames
                for (int j = 0; j < scales.Length; j++)
                {
                    int[] scale = scales[j];
                    int leftmost = scale[0];
                    int rightmost = scale[scale.Length - 1];
                    // bottom line
                    printer.addDocumentItem(new PrintDocumentItem(
                        ItemType.Line,
                        new Object[] {
                            leftmost,
                            top + DetailHeight,
                            rightmost,
                            top + DetailHeight,
                        }
                    ));
                    // vertical line
                    for (int k = 0; k < scale.Length; k++)
                    {
                        printer.addDocumentItem(new PrintDocumentItem(
                            ItemType.Line,
                            new Object[] {
                            scale[k],
                            top,
                            scale[k],
                            top + DetailHeight,
                            }
                        ));
                    }
                    top += DetailHeight;
                }
            }
            
            top = scaleBottomInfoTop;
            item = new PrintDocumentItem(ItemType.String, new Object[] { scaleBottomInfoLeft, top, bottom1 });
            printer.addDocumentItem(item);
            top += BottomInfoHeight;
            item = new PrintDocumentItem(ItemType.String, new Object[] { scaleBottomInfoLeft, top, bottom2 });
            printer.addDocumentItem(item);
            top += BottomInfoHeight;
        }

        public void print(bool ifShowDialog)
        {
            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(_print));
            t.Start(ifShowDialog);
        }

        private void _print(Object ifShowDialog)
        {
            printer.print((bool)ifShowDialog);
        }
    }
}
