﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.WareHouseInOut.Stuff;

namespace ClientSystem.WareHouseInOut.Stuff
{
    public partial class AttendantPicker : UserControl
    {
        private List<DefaultAttendant> _attendantList;
        private DefaultAttendant chosenAttendant;

        public delegate void DAttendantChosen(DefaultAttendant attendant);
        public event DAttendantChosen EAttendantChosen;
        private bool ifTrigerAttendantChosen = true;

        public List<DefaultAttendant> attendantList
        {
            get
            {
                return _attendantList;
            }
            set
            {
                this._attendantList = value;
                displayAttendantList();
            }
        }

        public AttendantPicker()
        {
            InitializeComponent();
            this.AttendantComboBox.SelectedIndexChanged += new EventHandler(AttendantComboBox_SelectedIndexChanged);
            this.AttendantComboBox.TextChanged += new EventHandler(this.AttendantComboBox_TextChanged);
        }

        private void AttendantComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DefaultAttendant defaultAttendant = (DefaultAttendant)AttendantComboBox.SelectedItem;
            if (defaultAttendant != null)
            {
                chosenAttendant = defaultAttendant;
                if (ifTrigerAttendantChosen && EAttendantChosen != null)
                    EAttendantChosen(chosenAttendant);
            }
        }

        private void AttendantComboBox_TextChanged(object sender, EventArgs e)
        {
            DefaultAttendant defaultAttendant = (DefaultAttendant)AttendantComboBox.SelectedItem;
            if (defaultAttendant == null)
            {
                chosenAttendant = null;
                AttendantComboBox.Items.Clear();
                var list = from Attendant in _attendantList where Attendant.name.Contains(AttendantComboBox.Text) select Attendant;
                foreach (DefaultAttendant Attendant in list)
                    AttendantComboBox.Items.Add(Attendant);
                if (AttendantComboBox.Items.Count == 1 && ((DefaultAttendant)AttendantComboBox.Items[0]).name == AttendantComboBox.Text)
                    chosenAttendant = (DefaultAttendant)AttendantComboBox.Items[0];
                if (ifTrigerAttendantChosen && EAttendantChosen != null)
                    EAttendantChosen(chosenAttendant);
                AttendantComboBox.Select(AttendantComboBox.Text.Length, 0);
            }
        }

        private void displayAttendantList()
        {
            if (attendantList != null)
            {
                AttendantComboBox.Items.Clear();
                foreach (DefaultAttendant Attendant in attendantList)
                    AttendantComboBox.Items.Add(Attendant);
            }
        }

        public DefaultAttendant getChosenAttendant()
        {
            return chosenAttendant;
        }

        public void setTarget(int? id)
        {
            if (id == null)
                AttendantComboBox.SelectedIndex = -1;
            if (attendantList != null)
            {
                ifTrigerAttendantChosen = false;
                int i;
                for (i = 0; i < attendantList.Count; i++)
                    if (attendantList[i].id == id)
                    {
                        AttendantComboBox.SelectedIndex = i;
                        break;
                    }
                if (i == attendantList.Count)
                    AttendantComboBox.Text = "";
                ifTrigerAttendantChosen = true;
            }
        }

        private void AttendantPicker_LostFocus(object sender, System.EventArgs e)
        {
            if (chosenAttendant == null)
                AttendantComboBox.Text = "";
            if (EAttendantChosen != null)
                EAttendantChosen(chosenAttendant);
        }
    }
}
