﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClassObjectToNum;
using ClassObjectToNum.DateTime;
using SharedObject.WareHouseInOut.Stuff;
using ClassObjectToNum.Money;

namespace ClientSystem.WareHouseInOut.Stuff
{
    partial class WarrantDetailBox
    {
        public List<SharedObject.CustomerManage.Stuff.DefaultCustomer> warrantCustomersList
        {
            get
            {
                return _warrantCustomersList;
            }
            set
            {
                this._warrantCustomersList = value;
                targetPicker.customerList = value;
                targetPicker.setTarget(warrant.target);
            }
        }
        public List<SharedObject.WareHouseInOut.Stuff.DefaultAttendant> warrantAttendantsList
        {
            get
            {
                return _warrantAttendantsList;
            }
            set
            {
                if (warrant is OutWarrant)
                {
                    this._warrantAttendantsList = value;
                    foreach (AttendantPicker picker in attendantPicker)
                        picker.attendantList = value;
                    OutWarrant outW = (OutWarrant)warrant;
                    List<DefaultAttendant> defaultAttendants = outW.getAttendants();
                    int i;
                    for (i = 0; i < defaultAttendants.Count && i < attendantPicker.Count; i++)
                        attendantPicker[i].setTarget(defaultAttendants[i].id);
                    for (; i < attendantPicker.Count; i++)
                        attendantPicker[i].setTarget(null);
                }
            }
        }

        /// This method is used to show the WarrantCustomersList
        private void mainTransmission_responseForCustomersList(List<SharedObject.CustomerManage.Stuff.DefaultCustomer> list)
        {
            warrantCustomersList = list;
        }
        /// This method is used to show the WarrantCustomersList
        private void mainTransmission_responseForAttendantsList(List<SharedObject.WareHouseInOut.Stuff.DefaultAttendant> list)
        {
            warrantAttendantsList = list;
        }
        /// This method is used to change the adjust price
        /// This method is called by warrantTransmission
        private void mainTransmission_responseForSpecialPrice(List<SpecialPriceSC.DraftWithSpecialPrice> specialPrice)
        {
            if (specialPrice != null)
                this.Invoke(new DSpecialPrice(mainTransmission_responseForSpecialPrice_Invoke), new object[] { specialPrice });
        }
        /// This method is used to change the adjust price
        private void mainTransmission_responseForSpecialPrice_Invoke(List<SpecialPriceSC.DraftWithSpecialPrice> specialPrice)
        {
            if (specialPrice != null)
            {
                if (_warrant is OutWarrant && _warrant.state == 0)
                {
                    OutWarrant outWarrant = (OutWarrant)_warrant;
                    foreach (OutWarrantItem item in outWarrant.items)
                    {
                        item.priceAdjustAuto = 0;
                        item.priceAdjustMannual = 0;
                        foreach (SpecialPriceSC.DraftWithSpecialPrice i in specialPrice)
                        {
                            if (item.IDdraft == i.draftID)
                            {
                                item.priceAdjustAuto = i.priceOutH == null ? 0 : i.priceOutH.Value;
                                item.priceAdjustMannual = i.priceOutH;
                                break;
                            }
                        }
                    }
                    refreshItem();
                }
            }
        }

        /// <summary>
        /// Method refreshSpecialPrice is used to mannual refresh the specialPrice
        /// </summary>
        public void refreshSpecialPrice()
        {
            if (_warrant != null)
            {
                SharedObject.CustomerManage.Stuff.DefaultCustomer customer = targetPicker.getChosenCustomer();
                if (customer != null)
                    _warrant.target = customer.id;
            }
            if (_warrant is OutWarrant && ((OutWarrant)_warrant).items.Count > 0 && _warrant.state == DefaultWarrant.WarrantState.Normal)
            {
                Connector.WarrantConnector warrantConnector = new Connector.WarrantConnector(SharedObject.Main.Transmission.Instruction.outWarrantSpecialPrice, mainTransmission, warrantConnectors);
                warrantConnector.EOutWarrantSpecialPrice += EOutWarrantSpecialPrice;
                warrantConnector.getOutWarrantSpecialPrice((OutWarrant)_warrant);
            }
        }

        private void targetPicker_ECustomerChosen(SharedObject.CustomerManage.Stuff.DefaultCustomer customer)
        {
            if (_warrant != null)
                if (customer != null)
                    _warrant.target = customer.id;
                else
                    _warrant.target = null;
            refreshSpecialPrice();
        }

        private void AttendantPicker_EAttendantChosen(DefaultAttendant defaultAttendant)
        {
            if (_warrant != null && _warrant is OutWarrant)
            {
                OutWarrant outW = (OutWarrant)_warrant;
                outW.clearAttendants();
                DefaultAttendant attendant;
                foreach (AttendantPicker picker in attendantPicker)
                {
                    attendant = picker.getChosenAttendant();
                    if (attendant != null) 
                        outW.addAttendants(attendant);
                }
            }
        }
    }
}
