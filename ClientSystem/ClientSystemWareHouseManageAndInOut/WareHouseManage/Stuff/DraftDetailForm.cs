﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClientSystem.Normal;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ClientSystem.WareHouseManage.Stuff
{
    public partial class DraftDetailForm : Form
    {
        //transmission
        private Transmission.Transmission mainTransmission;
        private List<Transmission.Connector> draftConnectors = new List<Transmission.Connector>();

        //identities
        private Identity.IdentityCheck identityCheck = new Identity.IdentityCheck();

        //show the DraftDetailForm for thread safety
        private delegate void DSpecialOneDraft(Draft draft);
        private delegate void DOperatedState(OperatedState operatedState);
        private delegate void DIdentityCheck(Identity.IdentityCheckForm identityCheckForm);

        //delegates called by transmission
        private Connector.DraftConnector.DSpecialOneDraft ESpecialOneDraft;
        private Connector.DraftConnector.DOperatedState EOperatedState;
        private Connector.DraftConnector.DIdentityCheck EIdentityCheck;

        private Draft _draft;
        public Draft draft
        {
            get
            {
                _draft.specification = specificationTextBox.Text;
                return _draft;
            }
            set
            {
                this._draft = value;
                displayDetail(this._draft);
            }
        }
        private TextBoxWithPrefix idTextBoxWithID;
        private bool _visibleNum, _visiblePrice, _visibleSubmit, _readonlyNum, _readonlyPrice, _readonlyAll;

        //delegates and events
        public delegate void SubmitEventHandler(Draft draft);         //when release submitButton
        public delegate void OkEventHandler(Draft draft);             //when release okButton
        public delegate void CancelEventHandler(Draft draft);         //when release cancelButton
        public event CancelEventHandler Cancel;
        public event SubmitEventHandler Submit;
        public event OkEventHandler Ok;

        private List<DraftDetailForm> detailForms = new List<DraftDetailForm>();

        private String oriName, oriSpe, oriHiddenSpe;
        private int oriPriceOutH, oriPriceOutL;

        /// <summary>
        /// readonlyNum mark whether user can edit the num of the draft
        /// readonlyNum can set the enabled when readonlyNum is set
        /// </summary>
        public bool readonlyNum
        {
            get { return _readonlyNum; }
            set
            {
                _readonlyNum = value;
                numAllTextBox.ReadOnly = _readonlyNum;
                numAccessiableTextBox.ReadOnly = _readonlyNum;
            }
        }
        /// <summary>
        /// readonlyPrice mark whether user can edit the price of the draft
        /// readonlyPrice can set the enabled when readonlyPrice is set
        /// </summary>
        public bool readonlyPrice
        {
            get { return _readonlyPrice; }
            set
            {
                _readonlyPrice = value;
                priceInTextBox.ReadOnly = _readonlyPrice;
                priceOutHTextBox.ReadOnly = _readonlyPrice;
                priceOutLTextBox.ReadOnly = _readonlyPrice;
            }
        }
        /// <summary>
        /// readonlyAll mark whether user can edit the draft
        /// readonlyAll can set the enabled when readonlyAll is set
        /// Warning : readonlyAll will also set the readonlyNum and readonlyPrice to suit readonlyAll
        /// </summary>
        public bool readonlyAll
        {
            get { return _readonlyAll; }
            set
            {
                _readonlyAll = value;
                readonlyNum = _readonlyAll;
                readonlyPrice = _readonlyAll;
                belongsToTextBox.ReadOnly = _readonlyAll;
                nameTextBox.ReadOnly = _readonlyAll;
                specificationTextBox.ReadOnly = _readonlyAll;
                hiddenSpecificationTextBox.ReadOnly = _readonlyAll;
                locationTextBox.ReadOnly = _readonlyAll;
                unitTextBox.ReadOnly = _readonlyAll;
            }
        }
        /// <summary>
        /// visibleNum mark whether user can see the num of the draft
        /// visibleNum can set the visible when visibleNum is set
        /// </summary>
        public bool visibleNum 
        {
            get { return _visibleNum; }
            set
            {
                _visibleNum = value;
                adjustVisibility();
            }
        }
        /// <summary>
        /// visiblePrice mark whether user can see the price of the draft
        /// visiblePrice can set the visible when visiblePrice is set
        /// </summary>
        public bool visiblePrice
        {
            get { return _visiblePrice; }
            set
            {
                _visiblePrice = value;
                adjustVisibility();
            }
        }
        /// <summary>
        /// visibleSubmit mark whether user can see the submitButton
        /// visibleSubmit can set the visible when visibleSubmit is set
        /// </summary>
        public bool visibleSubmit
        {
            get { return _visibleSubmit; }
            set
            {
                _visibleSubmit = value;
                this.submitButton.Visible = _visibleSubmit;
            }
        }

        /// <summary>
        /// adjustVisibility is to mannual adjust the visiblity
        /// </summary>
        public void adjustVisibility()
        {
            numGroupBox.Visible = _visibleNum;
            priceGroupBox.Visible = _visiblePrice;
            if (_visiblePrice || _visibleNum)
                this.Height = 430;
            else
                this.Height = 335;
        }

        public DraftDetailForm(List<DraftDetailForm> detailForms)
        {
            InitializeComponent();

            this.detailForms = detailForms;
            this.detailForms.Add(this);
            this.mainTransmission = Transmission.Transmission.mainTransmission;
            if (mainTransmission != null)
                mainTransmission.addConnectors(SharedObject.Normal.SystemID.WareHouseManage, draftConnectors);
            else
                throw new Transmission.TransmissionIsNullException();

            ESpecialOneDraft = new Connector.DraftConnector.DSpecialOneDraft(draft => this.Invoke(new DSpecialOneDraft(responseForAskForSpecialOneDraft), new object[] { draft }));
            EOperatedState = new Connector.DraftConnector.DOperatedState(state => this.Invoke(new DOperatedState(responseForOperatedState), new object[] { state }));
            EIdentityCheck = new Connector.DraftConnector.DIdentityCheck(identityCheckForm => this.Invoke(new DIdentityCheck(responseForIdentityCheck), new object[] { identityCheckForm }));

            this.FormClosing += new FormClosingEventHandler(DraftDetailForm_FormClosing);
            
            idTextBoxWithID = new TextBoxWithPrefix("Draft");
            this.idTextBoxWithID.Location = new System.Drawing.Point(56, 18);
            this.idTextBoxWithID.Name = "idTextBoxWithID";
            this.idTextBoxWithID.Size = new System.Drawing.Size(115, 23);
            this.idTextBoxWithID.TabIndex = 2;
            this.standardGroupBox.Controls.Add(idTextBoxWithID);
            this.numAllTextBox.ReadOnly = true;
            this.numAccessiableTextBox.ReadOnly = true;
            this.priceInTextBox.ReadOnly = true;
            this.belongsToTextBox.ReadOnly = true;
            this.unitTextBox.ReadOnly = true;
            visibleNum = false;
            visiblePrice = false;
            visibleSubmit = false;
        }

        private void  DraftDetailForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            while (draftConnectors.Count > 0)
                draftConnectors[0].Dispose();
            mainTransmission.removeConnectors(SharedObject.Normal.SystemID.WareHouseManage, draftConnectors);
        }

        public void changeDraft(int draftId)
        {
            Connector.DraftConnector draftConnector = new Connector.DraftConnector(Instruction.draftSpecialOne, mainTransmission, draftConnectors);
            draftConnector.ESpecialOneDraft += ESpecialOneDraft;
            draftConnector.getSpecialOneDraft(draftId);
        }

        public void newDraft(int belongsTo)
        {
            Draft draft = new Draft();
            draft.belongsTo = belongsTo;
            this.draft = draft;
        }

        /// This method is used to show a special draft
        private void responseForAskForSpecialOneDraft(Draft draft)
        {
            if (draft != null)
            {
                this.draft = draft;         //automantic refresh
            }
        }

        ///This method is used for response to server
        private void responseForOperatedState(OperatedState operatedState)
        {
            String str = Function.OperatedStateStringBuilder(operatedState);
            if (str.Length > 0)
                MessageBox.Show(str);
        }

        ///This method is used for response to server
        private void responseForIdentityCheck(Identity.IdentityCheckForm identityCheckForm)
        {
            identityCheckForm.Show();
        }

        private void displayDetail(Draft draft)
        {
            this.idTextBoxWithID.Text = draft.id.ToString();
            if (draft.belongsToString != null)
                this.belongsToTextBox.Text = draft.belongsToString.ToString();
            else
                this.belongsToTextBox.Text = "";
            this.oriName = draft.name;
            this.nameTextBox.Text = this.oriName;
            this.oriSpe = draft.specification;
            this.specificationTextBox.Text = this.oriSpe;
            this.oriHiddenSpe = draft.hiddenSpecification;
            this.hiddenSpecificationTextBox.Text = this.oriHiddenSpe;
            this.locationTextBox.Text = draft.location;
            this.unitTextBox.Text = draft.unit;
            this.numAllTextBox.Text = draft.numAll.ToString();
            this.numAccessiableTextBox.Text = draft.numAccessiable.ToString();
            this.numWarningTextBox.Text = draft.numWarning.ToString();
            this.priceInTextBox.Text = ClassObjectToNum.Money.ClassMoney.MoneyNumToString(draft.priceIn);
            this.oriPriceOutH = draft.priceOutH;
            this.priceOutHTextBox.Text = ClassObjectToNum.Money.ClassMoney.MoneyNumToString(this.oriPriceOutH);
            this.oriPriceOutL = draft.priceOutL;
            this.priceOutLTextBox.Text = ClassObjectToNum.Money.ClassMoney.MoneyNumToString(this.oriPriceOutL);
            this.serviceFeeCheckBox.Checked = draft.is_servicefee;
            this.unitTextBox.ReadOnly = (draft.id != 0);
            this.visibleSubmit = (draft.state == 0);
            foreach (DraftOperationRecord record in draft.operationRecord)
            {
                string mark;
                switch (record.operation)
                {
                    case DraftOperationEnum.create:
                        mark = "创建";
                        break;
                    case DraftOperationEnum.edit:
                        mark = "修改";
                        break;
                    case DraftOperationEnum.submit:
                        mark = "提交";
                        break;
                    default:
                        mark = "";
                        break;
                }
                dateAndOperator.Rows.Add(new object[] { mark, record.operationOperatorName, record.operatedTime.ToString() });
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (nameTextBox.Text == "" ||
                specificationTextBox.Text == "" ||
                unitTextBox.Text == "")
            {
                MessageBox.Show("名称，规格和单位都不得为空");
                return;
            }
            Draft draftNew = new Draft();
            draftNew.id = draft.id;
            draftNew.belongsTo = draft.belongsTo;
            draftNew.state = draft.state;
            draftNew.cookie = draft.cookie;
            if (this.oriHiddenSpe != hiddenSpecificationTextBox.Text.Trim())
                draftNew.hiddenSpecification = hiddenSpecificationTextBox.Text.Trim();
            if (this.oriSpe != specificationTextBox.Text.Trim())
                draftNew.specification = specificationTextBox.Text.Trim();
            draftNew.is_servicefee = serviceFeeCheckBox.Checked;
            draftNew.location = locationTextBox.Text.Trim();
            draftNew.unit = unitTextBox.Text.Trim();
            if (this.oriName != nameTextBox.Text.Trim())
                draftNew.name = nameTextBox.Text.Trim();
            {
                int priceOutH = ClassObjectToNum.Money.ClassMoney.MoneyStringToNum(priceOutHTextBox.Text);
                int priceOutL = ClassObjectToNum.Money.ClassMoney.MoneyStringToNum(priceOutLTextBox.Text);
                if (this.oriPriceOutH != priceOutH)
                {
                    draftNew.priceOutH = priceOutH;
                }
                if (this.oriPriceOutL != priceOutL)
                {
                    draftNew.priceOutL = priceOutL;
                }
            }
            draftNew.numWarning = Int32.Parse(numWarningTextBox.Text);

            Connector.DraftConnector draftConnector;
            if (draftNew.id == 0)
                draftConnector = new Connector.DraftConnector(Instruction.draftAdd, mainTransmission, draftConnectors);
            else
                draftConnector = new Connector.DraftConnector(Instruction.draftEdit, mainTransmission, draftConnectors);
            draftConnector.EOperatedState += EOperatedState;
            draftConnector.EIdentityCheck += EIdentityCheck;
            draftConnector.ESpecialOneDraft += ESpecialOneDraft;
            draftConnector.addEditDraft(draftNew);

            if (Ok != null)
                Ok(draftNew);
        }

        private void DraftDetailForm_FormClosing(object sender, EventArgs e)
        {
            this.detailForms.Remove(this);
            if (Cancel != null)
                Cancel(draft);
            this.Dispose();
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            DefaultDraft defaultDraft = draft;
            if (defaultDraft != null)
            {
                if (defaultDraft.state == 0)
                {
                    Connector.DraftConnector draftConnector = new Connector.DraftConnector(Instruction.draftSubmit, mainTransmission, draftConnectors);
                    draftConnector.EOperatedState += EOperatedState;
                    draftConnector.submitDraft(defaultDraft);
                }
                else
                    MessageBox.Show("配件已经被审核");
            }
            if (Submit != null)
                Submit(draft);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
