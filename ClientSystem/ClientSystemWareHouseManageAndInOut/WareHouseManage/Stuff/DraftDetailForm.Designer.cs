﻿namespace ClientSystem.WareHouseManage.Stuff
{
    partial class DraftDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.idLabel = new System.Windows.Forms.Label();
            this.belongsToLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.specificationLabel = new System.Windows.Forms.Label();
            this.specificationTextBox = new System.Windows.Forms.TextBox();
            this.hiddenSpecificationTextBox = new System.Windows.Forms.TextBox();
            this.hiddenSpecificationLabel = new System.Windows.Forms.Label();
            this.belongsToTextBox = new System.Windows.Forms.TextBox();
            this.standardGroupBox = new System.Windows.Forms.GroupBox();
            this.serviceFeeCheckBox = new System.Windows.Forms.CheckBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.unitTextBox = new System.Windows.Forms.TextBox();
            this.locationTextBox = new System.Windows.Forms.TextBox();
            this.locationLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numGroupBox = new System.Windows.Forms.GroupBox();
            this.numWarningLabel = new System.Windows.Forms.Label();
            this.numAccessiableLabel = new System.Windows.Forms.Label();
            this.numAllLabel = new System.Windows.Forms.Label();
            this.numWarningTextBox = new System.Windows.Forms.TextBox();
            this.numAccessiableTextBox = new System.Windows.Forms.TextBox();
            this.numAllTextBox = new System.Windows.Forms.TextBox();
            this.priceGroupBox = new System.Windows.Forms.GroupBox();
            this.priceOutLLabel = new System.Windows.Forms.Label();
            this.priceOutHLabel = new System.Windows.Forms.Label();
            this.priceInLabel = new System.Windows.Forms.Label();
            this.priceInTextBox = new System.Windows.Forms.TextBox();
            this.priceOutLTextBox = new System.Windows.Forms.TextBox();
            this.priceOutHTextBox = new System.Windows.Forms.TextBox();
            this.dateAndOperator = new System.Windows.Forms.DataGridView();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Operator = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.standardGroupBox.SuspendLayout();
            this.numGroupBox.SuspendLayout();
            this.priceGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateAndOperator)).BeginInit();
            this.SuspendLayout();
            // 
            // idLabel
            // 
            this.idLabel.AutoSize = true;
            this.idLabel.Location = new System.Drawing.Point(15, 22);
            this.idLabel.Name = "idLabel";
            this.idLabel.Size = new System.Drawing.Size(35, 14);
            this.idLabel.TabIndex = 0;
            this.idLabel.Text = "编号";
            // 
            // belongsToLabel
            // 
            this.belongsToLabel.AutoSize = true;
            this.belongsToLabel.Location = new System.Drawing.Point(176, 22);
            this.belongsToLabel.Name = "belongsToLabel";
            this.belongsToLabel.Size = new System.Drawing.Size(35, 14);
            this.belongsToLabel.TabIndex = 1;
            this.belongsToLabel.Text = "隶属";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(15, 51);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(35, 14);
            this.nameLabel.TabIndex = 3;
            this.nameLabel.Text = "名称";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(56, 48);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(401, 23);
            this.nameTextBox.TabIndex = 4;
            // 
            // specificationLabel
            // 
            this.specificationLabel.AutoSize = true;
            this.specificationLabel.Location = new System.Drawing.Point(15, 80);
            this.specificationLabel.Name = "specificationLabel";
            this.specificationLabel.Size = new System.Drawing.Size(35, 14);
            this.specificationLabel.TabIndex = 7;
            this.specificationLabel.Text = "规格";
            // 
            // specificationTextBox
            // 
            this.specificationTextBox.Location = new System.Drawing.Point(56, 77);
            this.specificationTextBox.Name = "specificationTextBox";
            this.specificationTextBox.Size = new System.Drawing.Size(511, 23);
            this.specificationTextBox.TabIndex = 8;
            // 
            // hiddenSpecificationTextBox
            // 
            this.hiddenSpecificationTextBox.Location = new System.Drawing.Point(98, 109);
            this.hiddenSpecificationTextBox.Name = "hiddenSpecificationTextBox";
            this.hiddenSpecificationTextBox.Size = new System.Drawing.Size(469, 23);
            this.hiddenSpecificationTextBox.TabIndex = 10;
            // 
            // hiddenSpecificationLabel
            // 
            this.hiddenSpecificationLabel.AutoSize = true;
            this.hiddenSpecificationLabel.Location = new System.Drawing.Point(15, 112);
            this.hiddenSpecificationLabel.Name = "hiddenSpecificationLabel";
            this.hiddenSpecificationLabel.Size = new System.Drawing.Size(77, 14);
            this.hiddenSpecificationLabel.TabIndex = 9;
            this.hiddenSpecificationLabel.Text = "非打印规格";
            // 
            // belongsToTextBox
            // 
            this.belongsToTextBox.Location = new System.Drawing.Point(216, 19);
            this.belongsToTextBox.Name = "belongsToTextBox";
            this.belongsToTextBox.Size = new System.Drawing.Size(361, 23);
            this.belongsToTextBox.TabIndex = 2;
            // 
            // standardGroupBox
            // 
            this.standardGroupBox.Controls.Add(this.serviceFeeCheckBox);
            this.standardGroupBox.Controls.Add(this.submitButton);
            this.standardGroupBox.Controls.Add(this.hiddenSpecificationTextBox);
            this.standardGroupBox.Controls.Add(this.cancelButton);
            this.standardGroupBox.Controls.Add(this.specificationTextBox);
            this.standardGroupBox.Controls.Add(this.okButton);
            this.standardGroupBox.Controls.Add(this.belongsToTextBox);
            this.standardGroupBox.Controls.Add(this.unitTextBox);
            this.standardGroupBox.Controls.Add(this.locationTextBox);
            this.standardGroupBox.Controls.Add(this.nameTextBox);
            this.standardGroupBox.Controls.Add(this.hiddenSpecificationLabel);
            this.standardGroupBox.Controls.Add(this.locationLabel);
            this.standardGroupBox.Controls.Add(this.specificationLabel);
            this.standardGroupBox.Controls.Add(this.nameLabel);
            this.standardGroupBox.Controls.Add(this.label1);
            this.standardGroupBox.Controls.Add(this.belongsToLabel);
            this.standardGroupBox.Controls.Add(this.idLabel);
            this.standardGroupBox.Location = new System.Drawing.Point(12, 12);
            this.standardGroupBox.Name = "standardGroupBox";
            this.standardGroupBox.Size = new System.Drawing.Size(583, 198);
            this.standardGroupBox.TabIndex = 0;
            this.standardGroupBox.TabStop = false;
            // 
            // serviceFeeCheckBox
            // 
            this.serviceFeeCheckBox.AutoSize = true;
            this.serviceFeeCheckBox.Location = new System.Drawing.Point(485, 51);
            this.serviceFeeCheckBox.Name = "serviceFeeCheckBox";
            this.serviceFeeCheckBox.Size = new System.Drawing.Size(82, 18);
            this.serviceFeeCheckBox.TabIndex = 16;
            this.serviceFeeCheckBox.Text = "维修费？";
            this.serviceFeeCheckBox.UseVisualStyleBackColor = true;
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(412, 167);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(92, 21);
            this.submitButton.TabIndex = 15;
            this.submitButton.Text = "审核通过";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(56, 167);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(92, 21);
            this.cancelButton.TabIndex = 13;
            this.cancelButton.Text = "关闭";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(242, 167);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(92, 21);
            this.okButton.TabIndex = 14;
            this.okButton.Text = "确定";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // unitTextBox
            // 
            this.unitTextBox.Location = new System.Drawing.Point(412, 138);
            this.unitTextBox.Name = "unitTextBox";
            this.unitTextBox.Size = new System.Drawing.Size(155, 23);
            this.unitTextBox.TabIndex = 12;
            // 
            // locationTextBox
            // 
            this.locationTextBox.Location = new System.Drawing.Point(56, 138);
            this.locationTextBox.Name = "locationTextBox";
            this.locationTextBox.Size = new System.Drawing.Size(309, 23);
            this.locationTextBox.TabIndex = 12;
            // 
            // locationLabel
            // 
            this.locationLabel.AutoSize = true;
            this.locationLabel.Location = new System.Drawing.Point(15, 141);
            this.locationLabel.Name = "locationLabel";
            this.locationLabel.Size = new System.Drawing.Size(35, 14);
            this.locationLabel.TabIndex = 11;
            this.locationLabel.Text = "位置";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(371, 141);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "单位";
            // 
            // numGroupBox
            // 
            this.numGroupBox.Controls.Add(this.numWarningLabel);
            this.numGroupBox.Controls.Add(this.numAccessiableLabel);
            this.numGroupBox.Controls.Add(this.numAllLabel);
            this.numGroupBox.Controls.Add(this.numWarningTextBox);
            this.numGroupBox.Controls.Add(this.numAccessiableTextBox);
            this.numGroupBox.Controls.Add(this.numAllTextBox);
            this.numGroupBox.Location = new System.Drawing.Point(12, 291);
            this.numGroupBox.Name = "numGroupBox";
            this.numGroupBox.Size = new System.Drawing.Size(281, 106);
            this.numGroupBox.TabIndex = 2;
            this.numGroupBox.TabStop = false;
            // 
            // numWarningLabel
            // 
            this.numWarningLabel.AutoSize = true;
            this.numWarningLabel.Location = new System.Drawing.Point(13, 78);
            this.numWarningLabel.Name = "numWarningLabel";
            this.numWarningLabel.Size = new System.Drawing.Size(63, 14);
            this.numWarningLabel.TabIndex = 4;
            this.numWarningLabel.Text = "警戒余额";
            // 
            // numAccessiableLabel
            // 
            this.numAccessiableLabel.AutoSize = true;
            this.numAccessiableLabel.Location = new System.Drawing.Point(13, 49);
            this.numAccessiableLabel.Name = "numAccessiableLabel";
            this.numAccessiableLabel.Size = new System.Drawing.Size(63, 14);
            this.numAccessiableLabel.TabIndex = 2;
            this.numAccessiableLabel.Text = "可用余额";
            // 
            // numAllLabel
            // 
            this.numAllLabel.AutoSize = true;
            this.numAllLabel.Location = new System.Drawing.Point(13, 20);
            this.numAllLabel.Name = "numAllLabel";
            this.numAllLabel.Size = new System.Drawing.Size(35, 14);
            this.numAllLabel.TabIndex = 0;
            this.numAllLabel.Text = "余额";
            // 
            // numWarningTextBox
            // 
            this.numWarningTextBox.Location = new System.Drawing.Point(86, 75);
            this.numWarningTextBox.Name = "numWarningTextBox";
            this.numWarningTextBox.Size = new System.Drawing.Size(134, 23);
            this.numWarningTextBox.TabIndex = 5;
            // 
            // numAccessiableTextBox
            // 
            this.numAccessiableTextBox.Location = new System.Drawing.Point(86, 46);
            this.numAccessiableTextBox.Name = "numAccessiableTextBox";
            this.numAccessiableTextBox.Size = new System.Drawing.Size(134, 23);
            this.numAccessiableTextBox.TabIndex = 3;
            // 
            // numAllTextBox
            // 
            this.numAllTextBox.Location = new System.Drawing.Point(86, 17);
            this.numAllTextBox.Name = "numAllTextBox";
            this.numAllTextBox.Size = new System.Drawing.Size(134, 23);
            this.numAllTextBox.TabIndex = 1;
            // 
            // priceGroupBox
            // 
            this.priceGroupBox.Controls.Add(this.priceOutLLabel);
            this.priceGroupBox.Controls.Add(this.priceOutHLabel);
            this.priceGroupBox.Controls.Add(this.priceInLabel);
            this.priceGroupBox.Controls.Add(this.priceInTextBox);
            this.priceGroupBox.Controls.Add(this.priceOutLTextBox);
            this.priceGroupBox.Controls.Add(this.priceOutHTextBox);
            this.priceGroupBox.Location = new System.Drawing.Point(299, 291);
            this.priceGroupBox.Name = "priceGroupBox";
            this.priceGroupBox.Size = new System.Drawing.Size(296, 106);
            this.priceGroupBox.TabIndex = 3;
            this.priceGroupBox.TabStop = false;
            // 
            // priceOutLLabel
            // 
            this.priceOutLLabel.AutoSize = true;
            this.priceOutLLabel.Location = new System.Drawing.Point(12, 78);
            this.priceOutLLabel.Name = "priceOutLLabel";
            this.priceOutLLabel.Size = new System.Drawing.Size(35, 14);
            this.priceOutLLabel.TabIndex = 4;
            this.priceOutLLabel.Text = "价格";
            // 
            // priceOutHLabel
            // 
            this.priceOutHLabel.AutoSize = true;
            this.priceOutHLabel.Location = new System.Drawing.Point(12, 49);
            this.priceOutHLabel.Name = "priceOutHLabel";
            this.priceOutHLabel.Size = new System.Drawing.Size(35, 14);
            this.priceOutHLabel.TabIndex = 2;
            this.priceOutHLabel.Text = "价格";
            // 
            // priceInLabel
            // 
            this.priceInLabel.AutoSize = true;
            this.priceInLabel.Location = new System.Drawing.Point(12, 20);
            this.priceInLabel.Name = "priceInLabel";
            this.priceInLabel.Size = new System.Drawing.Size(35, 14);
            this.priceInLabel.TabIndex = 0;
            this.priceInLabel.Text = "库值";
            // 
            // priceInTextBox
            // 
            this.priceInTextBox.Location = new System.Drawing.Point(85, 17);
            this.priceInTextBox.Name = "priceInTextBox";
            this.priceInTextBox.Size = new System.Drawing.Size(134, 23);
            this.priceInTextBox.TabIndex = 1;
            // 
            // priceOutLTextBox
            // 
            this.priceOutLTextBox.Location = new System.Drawing.Point(85, 75);
            this.priceOutLTextBox.Name = "priceOutLTextBox";
            this.priceOutLTextBox.Size = new System.Drawing.Size(134, 23);
            this.priceOutLTextBox.TabIndex = 5;
            // 
            // priceOutHTextBox
            // 
            this.priceOutHTextBox.Location = new System.Drawing.Point(85, 46);
            this.priceOutHTextBox.Name = "priceOutHTextBox";
            this.priceOutHTextBox.Size = new System.Drawing.Size(134, 23);
            this.priceOutHTextBox.TabIndex = 3;
            // 
            // dateAndOperator
            // 
            this.dateAndOperator.AllowUserToAddRows = false;
            this.dateAndOperator.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dateAndOperator.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Title,
            this.Operator,
            this.Date});
            this.dateAndOperator.Location = new System.Drawing.Point(12, 222);
            this.dateAndOperator.Name = "dateAndOperator";
            this.dateAndOperator.ReadOnly = true;
            this.dateAndOperator.RowTemplate.Height = 18;
            this.dateAndOperator.Size = new System.Drawing.Size(583, 63);
            this.dateAndOperator.TabIndex = 1;
            // 
            // Title
            // 
            this.Title.Frozen = true;
            this.Title.HeaderText = "项目";
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            this.Title.Width = 80;
            // 
            // Operator
            // 
            this.Operator.Frozen = true;
            this.Operator.HeaderText = "操作者";
            this.Operator.Name = "Operator";
            this.Operator.ReadOnly = true;
            this.Operator.Width = 80;
            // 
            // Date
            // 
            this.Date.Frozen = true;
            this.Date.HeaderText = "日期";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            this.Date.Width = 170;
            // 
            // DraftDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 403);
            this.Controls.Add(this.dateAndOperator);
            this.Controls.Add(this.priceGroupBox);
            this.Controls.Add(this.numGroupBox);
            this.Controls.Add(this.standardGroupBox);
            this.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "DraftDetailForm";
            this.Text = "配件明细";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DraftDetailForm_FormClosing);
            this.standardGroupBox.ResumeLayout(false);
            this.standardGroupBox.PerformLayout();
            this.numGroupBox.ResumeLayout(false);
            this.numGroupBox.PerformLayout();
            this.priceGroupBox.ResumeLayout(false);
            this.priceGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateAndOperator)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label idLabel;
        private System.Windows.Forms.Label belongsToLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label specificationLabel;
        private System.Windows.Forms.TextBox specificationTextBox;
        private System.Windows.Forms.TextBox hiddenSpecificationTextBox;
        private System.Windows.Forms.Label hiddenSpecificationLabel;
        private System.Windows.Forms.TextBox belongsToTextBox;
        private System.Windows.Forms.GroupBox standardGroupBox;
        private System.Windows.Forms.TextBox locationTextBox;
        private System.Windows.Forms.Label locationLabel;
        private System.Windows.Forms.GroupBox numGroupBox;
        private System.Windows.Forms.Label numAccessiableLabel;
        private System.Windows.Forms.Label numAllLabel;
        private System.Windows.Forms.TextBox numAccessiableTextBox;
        private System.Windows.Forms.TextBox numAllTextBox;
        private System.Windows.Forms.GroupBox priceGroupBox;
        private System.Windows.Forms.Label priceOutLLabel;
        private System.Windows.Forms.Label priceOutHLabel;
        private System.Windows.Forms.Label priceInLabel;
        private System.Windows.Forms.TextBox priceInTextBox;
        private System.Windows.Forms.TextBox priceOutLTextBox;
        private System.Windows.Forms.TextBox priceOutHTextBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Label numWarningLabel;
        private System.Windows.Forms.TextBox numWarningTextBox;
        private System.Windows.Forms.DataGridView dateAndOperator;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Operator;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.CheckBox serviceFeeCheckBox;
        private System.Windows.Forms.TextBox unitTextBox;
        private System.Windows.Forms.Label label1;
    }
}