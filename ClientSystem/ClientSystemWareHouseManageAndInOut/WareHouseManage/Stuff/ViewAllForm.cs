﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ClientSystem.WareHouseManage.Stuff
{
    public partial class ViewAllForm : Form
    {
        //transmission
        private Transmission.Transmission mainTransmission;
        private List<Transmission.Connector> draftConnectors = new List<Transmission.Connector>();

        protected delegate void DViewAllDraft(List<ViewAllDraft> viewAllDraftList);
        protected delegate void DIdentityCheck(Identity.IdentityCheckForm identityCheckForm);
        protected delegate void DOperatedState(OperatedState operatedState);

        protected Connector.DraftConnector.DViewAllDraft EViewAllDraft;
        protected Connector.DraftConnector.DIdentityCheck EIdentityCheck;
        protected Connector.DraftConnector.DOperatedState EOperatedState;

        public List<ViewAllDraft> viewAllDraftList { get; set; }
        public int rangeType { get; set; }
        public int range { get; set; }

        public ViewAllForm(Transmission.Transmission mainTransmission)
        {
            InitializeComponent();

            this.mainTransmission = mainTransmission;
            if (mainTransmission != null)
                mainTransmission.addConnectors(SharedObject.Normal.SystemID.WareHouseManage, draftConnectors);
            else
                throw new Transmission.TransmissionIsNullException();

            EViewAllDraft = new Connector.DraftConnector.DViewAllDraft(list => this.Invoke(new DViewAllDraft(responseForViewAllDraft), new object[] { list }));
            EIdentityCheck = new Connector.DraftConnector.DIdentityCheck(identityCheckForm => this.Invoke(new DIdentityCheck(responseForIdentityCheck), new object[] { identityCheckForm }));
            EOperatedState = new Connector.DraftConnector.DOperatedState(state => this.Invoke(new DOperatedState(responseForOperatedState), new object[] { state }));

            viewAllDraftGridView.Columns.Add("ID", "序号");
            viewAllDraftGridView.Columns.Add("Name", "名称");
            viewAllDraftGridView.Columns.Add("specification", "规格");
            viewAllDraftGridView.Columns.Add("hiddenSpecification", "非打印规格");
            viewAllDraftGridView.Columns.Add("priceIn", "库值");
            viewAllDraftGridView.Columns.Add("numAll", "余量");
            viewAllDraftGridView.Columns.Add("priceOutH", "售价A");
            viewAllDraftGridView.Columns.Add("priceOutL", "售价");
            viewAllDraftGridView.Columns[1].Width = 200;
            amountAllLabel.Text = "总值：";
            amountAllNumericLabel.Text = "";
            amountAllChineseLabel.Text = "";

            dateTimePicker.Value = DateTime.Now.AddMonths(-1);

            this.Load += new EventHandler(ViewAllForm_Load);
            this.FormClosing += new FormClosingEventHandler(ViewAllForm_FormClosing);

            ToolTip toolTip = new ToolTip();

            // Set up the ToolTip text for the Button and Checkbox.
            toolTip.SetToolTip(this.amountAllNumericLabel, "单击复制");
            toolTip.SetToolTip(this.amountAllChineseLabel, "单击复制");
        }

        private void ViewAllForm_Load(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("要立即当前库存信息吗？按是立即查询。如果您想查询历史记录，请按否，选择日期后再查询", "", MessageBoxButtons.YesNo);
            if (result == System.Windows.Forms.DialogResult.Yes)
                refreshNowButton_Click(sender, e);
        }

        private void ViewAllForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            while (draftConnectors.Count > 0)
                draftConnectors[0].Dispose();
            mainTransmission.removeConnectors(SharedObject.Normal.SystemID.WareHouseManage, draftConnectors);
        }

        public void getViewAllDraftList(DateTime date)
        {
            Connector.DraftConnector draftConnector;
            draftConnector = new Connector.DraftConnector(Instruction.draftViewAll, mainTransmission, draftConnectors);
            draftConnector.EViewAllDraft += EViewAllDraft;
            draftConnector.EIdentityCheck += EIdentityCheck;
            draftConnector.EOperatedState += EOperatedState;
            draftConnector.getViewDraftList(rangeType, range, date);
        }

        ///This method is used for response to server
        private void responseForOperatedState(OperatedState operatedState)
        {
            String str = Function.OperatedStateStringBuilder(operatedState);
            if (str.Length > 0)
                MessageBox.Show(str);
        }

        ///This method is used for response to server
        private void responseForViewAllDraft(List<ViewAllDraft> viewAllDraftList)
        {
            this.viewAllDraftList = viewAllDraftList;
            displayAllDraft();
        }

        public void displayAllDraft()
        {
            DataGridViewRow row;
            int i = 0;
            long amountAll = 0;
            if (viewAllDraftList != null)
            {
                viewAllDraftGridView.Rows.Clear();
                foreach (ViewAllDraft draft in viewAllDraftList)
                {
                    i++;
                    row = new DataGridViewRow();
                    viewAllDraftGridView.Rows.Add(
                        new object[] {
                            draft.id,
                            draft.name,
                            draft.specification,
                            draft.hiddenSpecification,
                            ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(draft.priceIn),
                            draft.numAll + "(" + draft.unit + ")",
                            ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(draft.priceOutH),
                            ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(draft.priceOutL),
                        }
                    );
                    amountAll += draft.value;
                }
            }
            amountAllNumericLabel.Text = ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(amountAll);
            amountAllChineseLabel.Text = ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayLString(amountAll);
        }

        ///This method is used for response to server
        private void responseForIdentityCheck(Identity.IdentityCheckForm identityCheckForm)
        {
            identityCheckForm.Show();
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            getViewAllDraftList(dateTimePicker.Value);
        }

        private void refreshNowButton_Click(object sender, EventArgs e)
        {
            getViewAllDraftList(default(DateTime));
        }

        private void amountAllNumericLabel_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(amountAllNumericLabel.Text);
        }

        private void amountAllChineseLabel_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(amountAllChineseLabel.Text);
        }
    }
}
