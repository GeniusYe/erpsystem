﻿namespace ClientSystem.WareHouseManage.Stuff
{
    partial class DraftPrintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.detailButton = new System.Windows.Forms.Button();
            this.submitButton = new System.Windows.Forms.Button();
            this.shutWindowsButton = new System.Windows.Forms.Button();
            this.viewAllButton = new System.Windows.Forms.Button();
            this.ViewAllButtonAll = new System.Windows.Forms.Button();
            this.delButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(817, 12);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 35);
            this.addButton.TabIndex = 0;
            this.addButton.Text = "添加";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(817, 53);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(75, 35);
            this.editButton.TabIndex = 1;
            this.editButton.Text = "编辑";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // detailButton
            // 
            this.detailButton.Location = new System.Drawing.Point(817, 214);
            this.detailButton.Name = "detailButton";
            this.detailButton.Size = new System.Drawing.Size(75, 35);
            this.detailButton.TabIndex = 5;
            this.detailButton.Text = "明细";
            this.detailButton.UseVisualStyleBackColor = true;
            this.detailButton.Click += new System.EventHandler(this.detailButton_Click);
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(817, 93);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 35);
            this.submitButton.TabIndex = 2;
            this.submitButton.Text = "审核";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // shutWindowsButton
            // 
            this.shutWindowsButton.Location = new System.Drawing.Point(817, 173);
            this.shutWindowsButton.Name = "shutWindowsButton";
            this.shutWindowsButton.Size = new System.Drawing.Size(75, 35);
            this.shutWindowsButton.TabIndex = 4;
            this.shutWindowsButton.Text = "关闭子窗口";
            this.shutWindowsButton.UseVisualStyleBackColor = true;
            this.shutWindowsButton.Click += new System.EventHandler(this.shutWindowsButton_Click);
            // 
            // viewAllButton
            // 
            this.viewAllButton.Location = new System.Drawing.Point(819, 265);
            this.viewAllButton.Name = "viewAllButton";
            this.viewAllButton.Size = new System.Drawing.Size(73, 45);
            this.viewAllButton.TabIndex = 6;
            this.viewAllButton.Text = "已选项库存";
            this.viewAllButton.UseVisualStyleBackColor = true;
            this.viewAllButton.Click += new System.EventHandler(this.viewAllButton_Click);
            // 
            // ViewAllButtonAll
            // 
            this.ViewAllButtonAll.Location = new System.Drawing.Point(819, 316);
            this.ViewAllButtonAll.Name = "ViewAllButtonAll";
            this.ViewAllButtonAll.Size = new System.Drawing.Size(73, 45);
            this.ViewAllButtonAll.TabIndex = 7;
            this.ViewAllButtonAll.Text = "全部库存";
            this.ViewAllButtonAll.UseVisualStyleBackColor = true;
            this.ViewAllButtonAll.Click += new System.EventHandler(this.ViewAllButtonAll_Click);
            // 
            // delButton
            // 
            this.delButton.Location = new System.Drawing.Point(817, 133);
            this.delButton.Name = "delButton";
            this.delButton.Size = new System.Drawing.Size(75, 35);
            this.delButton.TabIndex = 3;
            this.delButton.Text = "删除 ";
            this.delButton.UseVisualStyleBackColor = true;
            this.delButton.Click += new System.EventHandler(this.delButton_Click);
            // 
            // DraftPrintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 477);
            this.Controls.Add(this.delButton);
            this.Controls.Add(this.ViewAllButtonAll);
            this.Controls.Add(this.viewAllButton);
            this.Controls.Add(this.shutWindowsButton);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.detailButton);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.addButton);
            this.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "DraftPrintForm";
            this.Text = "配件信息";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button detailButton;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Button shutWindowsButton;
        private System.Windows.Forms.Button viewAllButton;
        private System.Windows.Forms.Button ViewAllButtonAll;
        private System.Windows.Forms.Button delButton;

    }
}