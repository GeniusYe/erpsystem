﻿namespace ClientSystem.WareHouseManage.Stuff
{
    partial class DraftChooseBox
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.containerListBox3 = new System.Windows.Forms.ListBox();
            this.containerListBox2 = new System.Windows.Forms.ListBox();
            this.containerListBox1 = new System.Windows.Forms.ListBox();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.draftListGridView = new System.Windows.Forms.DataGridView();
            this.DraftID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DraftName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Specification = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numAccessible = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.draftListGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // containerListBox3
            // 
            this.containerListBox3.FormattingEnabled = true;
            this.containerListBox3.Location = new System.Drawing.Point(186, 260);
            this.containerListBox3.Name = "containerListBox3";
            this.containerListBox3.Size = new System.Drawing.Size(180, 212);
            this.containerListBox3.TabIndex = 4;
            this.containerListBox3.SelectedIndexChanged += new System.EventHandler(this.containerListBox3_SelectedIndexChanged);
            // 
            // containerListBox2
            // 
            this.containerListBox2.FormattingEnabled = true;
            this.containerListBox2.Location = new System.Drawing.Point(186, 42);
            this.containerListBox2.Name = "containerListBox2";
            this.containerListBox2.Size = new System.Drawing.Size(180, 212);
            this.containerListBox2.TabIndex = 3;
            this.containerListBox2.SelectedIndexChanged += new System.EventHandler(this.containerListBox2_SelectedIndexChanged);
            // 
            // containerListBox1
            // 
            this.containerListBox1.FormattingEnabled = true;
            this.containerListBox1.Location = new System.Drawing.Point(0, 42);
            this.containerListBox1.Name = "containerListBox1";
            this.containerListBox1.Size = new System.Drawing.Size(180, 433);
            this.containerListBox1.TabIndex = 2;
            this.containerListBox1.SelectedIndexChanged += new System.EventHandler(this.containerListBox1_SelectedIndexChanged);
            // 
            // searchTextBox
            // 
            this.searchTextBox.Location = new System.Drawing.Point(0, 13);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(687, 20);
            this.searchTextBox.TabIndex = 0;
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(693, 13);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(95, 23);
            this.searchButton.TabIndex = 1;
            this.searchButton.Text = "搜索";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // draftListGridView
            // 
            this.draftListGridView.AllowUserToAddRows = false;
            this.draftListGridView.AllowUserToDeleteRows = false;
            this.draftListGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.draftListGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DraftID,
            this.DraftName,
            this.Specification,
            this.numAccessible,
            this.Price});
            this.draftListGridView.Location = new System.Drawing.Point(372, 42);
            this.draftListGridView.Name = "draftListGridView";
            this.draftListGridView.RowHeadersVisible = false;
            this.draftListGridView.RowTemplate.Height = 18;
            this.draftListGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.draftListGridView.Size = new System.Drawing.Size(416, 411);
            this.draftListGridView.TabIndex = 6;
            // 
            // DraftID
            // 
            this.DraftID.HeaderText = "库位";
            this.DraftID.Name = "DraftID";
            this.DraftID.ReadOnly = true;
            this.DraftID.Width = 40;
            // 
            // DraftName
            // 
            this.DraftName.HeaderText = "名称";
            this.DraftName.Name = "DraftName";
            this.DraftName.ReadOnly = true;
            // 
            // Specification
            // 
            this.Specification.HeaderText = "规格";
            this.Specification.Name = "Specification";
            this.Specification.ReadOnly = true;
            this.Specification.Width = 220;
            // 
            // numAccessible
            // 
            this.numAccessible.HeaderText = "余额";
            this.numAccessible.Name = "numAccessible";
            this.numAccessible.ReadOnly = true;
            this.numAccessible.Width = 40;
            // 
            // Price
            // 
            this.Price.HeaderText = "价格";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.Width = 40;
            // 
            // DraftChooseBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.draftListGridView);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.containerListBox3);
            this.Controls.Add(this.containerListBox2);
            this.Controls.Add(this.containerListBox1);
            this.MinimumSize = new System.Drawing.Size(800, 480);
            this.Name = "DraftChooseBox";
            this.Size = new System.Drawing.Size(800, 495);
            ((System.ComponentModel.ISupportInitialize)(this.draftListGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox containerListBox3;
        private System.Windows.Forms.ListBox containerListBox2;
        private System.Windows.Forms.ListBox containerListBox1;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.DataGridView draftListGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn DraftID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DraftName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Specification;
        private System.Windows.Forms.DataGridViewTextBoxColumn numAccessible;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
    }
}
