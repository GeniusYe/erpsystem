﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClientSystem.WareHouseManage.Connector;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.WareHouseManage.Transmission;

namespace ClientSystem.WareHouseManage.Stuff
{
    public partial class DraftPrintForm : Form
    {
        //transmission
        private Transmission.Transmission mainTransmission;
        private List<Transmission.Connector> draftConnectors = new List<Transmission.Connector>();

        //identities
        private Identity.IdentityCheck identityCheck = new Identity.IdentityCheck();

        //show the DraftDetailForm for thread safety
        protected delegate void DOperatedState(OperatedState operatedState);
        protected delegate void DIdentityCheck(Identity.IdentityCheckForm identityCheckForm);

        //delegates called by transmission
        protected Connector.DraftConnector.DOperatedState EOperatedState;
        protected Connector.DraftConnector.DIdentityCheck EIdentityCheck;

        // used to shut all the windows
        private List<DraftDetailForm> detailForms = new List<DraftDetailForm>();
        private List<ViewAllForm> viewAllForms = new List<ViewAllForm>();
        private List<ClientSystem.WareHouseInOut.Stuff.ViewDetailForm> viewDetailForms = new List<ClientSystem.WareHouseInOut.Stuff.ViewDetailForm>();
        private DraftChooseBox draftChooseBox;

        public DraftPrintForm()
        {
            InitializeComponent();

            this.mainTransmission = Transmission.Transmission.mainTransmission;
            if (mainTransmission != null)
                mainTransmission.addConnectors(SharedObject.Normal.SystemID.WareHouseManage, draftConnectors);
            else
                throw new Transmission.TransmissionIsNullException();

            draftChooseBox = new DraftChooseBox(mainTransmission);
            this.draftChooseBox.Location = new System.Drawing.Point(12, 12);
            this.draftChooseBox.Name = "draftChooseBox";
            this.draftChooseBox.TabIndex = 2;
            this.Controls.Add(draftChooseBox);

            EOperatedState = new Connector.DraftConnector.DOperatedState(state => this.Invoke(new DOperatedState(responseForOperatedState), new object[] { state }));
            EIdentityCheck = new Connector.DraftConnector.DIdentityCheck(identityCheckForm => this.Invoke(new DIdentityCheck(responseForIdentityCheck), new object[] { identityCheckForm }));

            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DraftPrintForm_FormClosing);
            draftChooseBox.DoubleClick += new DraftChooseBox.DoubleClickEventHandler(draftChooseBox_DoubleClick);
        }

        private void DraftPrintForm_FormClosing(object sender, EventArgs e)
        {
            draftChooseBox.Dispose();
            while (draftConnectors.Count > 0)
                draftConnectors[0].Dispose();
            mainTransmission.removeConnectors(SharedObject.Normal.SystemID.WareHouseManage, draftConnectors);
            shutWindows();
        }

        //used for display
        private void draftChooseBox_DoubleClick(DefaultDraft draft)
        {
            if (draft != null && draft.id > 0)
            {
                DraftDetailForm draftDetailForm = new DraftDetailForm(detailForms);
                draftDetailForm.visibleNum = true;
                draftDetailForm.visiblePrice = true;
                draftDetailForm.changeDraft(draft.id);
                draftDetailForm.Show();
            }
            else
                MessageBox.Show("请选择配件");
        }
        
        private void editButton_Click(object sender, EventArgs e)
        {
            draftChooseBox_DoubleClick(draftChooseBox.getChosenDefaultDraft());
        }
        
        ///This method is used for response to server
        private void responseForOperatedState(OperatedState operatedState)
        {
            String str = Function.OperatedStateStringBuilder(operatedState);
            if (str.Length > 0)
                MessageBox.Show(str);
        }

        ///This method is used for response to server
        private void responseForIdentityCheck(Identity.IdentityCheckForm identityCheckForm)
        {
            identityCheckForm.Show();
        }
        
        private void addButton_Click(object sender, EventArgs e)
        {
            DraftContainer m = draftChooseBox.getChosenObjectAccordingToContainer(2);
            if (m != null)
            {
                DraftDetailForm draftDetailForm = new DraftDetailForm(detailForms);
                draftDetailForm.visibleNum = true;
                draftDetailForm.visiblePrice = true;
                draftDetailForm.newDraft(m.id);
                draftDetailForm.Show();
            }
        }
        
        private void submitButton_Click(object sender, EventArgs e)
        {
            DefaultDraft defaultDraft = draftChooseBox.getChosenDefaultDraft();
            if (defaultDraft != null)
            {
                if (defaultDraft.state == 0)
                {
                    Connector.DraftConnector draftConnector = new Connector.DraftConnector(Instruction.draftSubmit, mainTransmission, draftConnectors);
                    draftConnector.EOperatedState += EOperatedState;
                    draftConnector.EIdentityCheck += EIdentityCheck;
                    draftConnector.submitDraft(defaultDraft);
                }
                else
                    MessageBox.Show("配件已经被审核");
            }
            else
                MessageBox.Show("请选择配件");
        }

        private void delButton_Click(object sender, EventArgs e)
        {
            DefaultDraft defaultDraft = draftChooseBox.getChosenDefaultDraft();
            if (defaultDraft != null)
            {
                Connector.DraftConnector draftConnector = new Connector.DraftConnector(Instruction.draftDel, mainTransmission, draftConnectors);
                draftConnector.EOperatedState += EOperatedState;
                draftConnector.submitDraft(defaultDraft);
            }
            else
                MessageBox.Show("请选择配件");
        }
        
        private void shutWindows()
        {
            foreach (DraftDetailForm f in detailForms)
            {
                f.Close();
                f.Dispose();
            }
            foreach (ViewAllForm f in viewAllForms)
            {
                f.Close();
                f.Dispose();
            }
            foreach (ClientSystem.WareHouseInOut.Stuff.ViewDetailForm f in viewDetailForms)
            {
                f.Close();
                f.Dispose();
            }
        }

        private void shutWindowsButton_Click(object sender, EventArgs e)
        {
            shutWindows();
        }

        private void viewAllButton_Click(object sender, EventArgs e)
        {
            ViewAllForm f = new ViewAllForm(mainTransmission);
            viewAllForms.Add(f);
            int k = draftChooseBox.getChosenContainer();
            DraftContainer t = draftChooseBox.getChosenObjectAccordingToContainer(k);
            if (t != null)
            {
                f.rangeType = k + 1;
                f.range = t.id;
            }
            f.Show();
        }

        private void ViewAllButtonAll_Click(object sender, EventArgs e)
        {
            ViewAllForm viewAllForm = new ViewAllForm(mainTransmission);
            viewAllForms.Add(viewAllForm);
            viewAllForm.rangeType = 0;
            viewAllForm.range = 0;
            viewAllForm.Show();
        }

        private void detailButton_Click(object sender, EventArgs e)
        {
            DefaultDraft draft = draftChooseBox.getChosenDefaultDraft();
            if (draft != null)
            {
                ClientSystem.WareHouseInOut.Stuff.ViewDetailForm viewDetailForm = new WareHouseInOut.Stuff.ViewDetailForm(mainTransmission);
                viewDetailForms.Add(viewDetailForm);
                viewDetailForm.getDetailAccount(draft.id);
                viewDetailForm.Show();
            }
            else
                MessageBox.Show("请选择配件");
        }
    }
}
