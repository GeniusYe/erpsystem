﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.WareHouseManage.Stuff;

namespace ClientSystem.WareHouseManage.Stuff
{
    /// <summary>
    /// Class DraftContainers contains the three containers and the draft displayed on the screen now
    /// </summary>
    class DraftContainers
    {
        /// <summary>
        /// it contains the three containers _container[0], _container[1], _container[2]
        /// </summary>
        public List<List<DraftContainer>> _container { get; private set; }

        public List<DefaultDraft> _draft { get; set; }

        public DraftContainers()
        {
            _container = new List<List<DraftContainer>>();
            _container.Add(new List<DraftContainer>());
            _container.Add(new List<DraftContainer>());
            _container.Add(new List<DraftContainer>());
            _draft = new List<DefaultDraft>();
        }
    }
}
