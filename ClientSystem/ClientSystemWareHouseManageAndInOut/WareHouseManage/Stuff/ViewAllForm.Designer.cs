﻿namespace ClientSystem.WareHouseManage.Stuff
{
    partial class ViewAllForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.viewAllDraftGridView = new System.Windows.Forms.DataGridView();
            this.amountAllLabel = new System.Windows.Forms.Label();
            this.amountAllNumericLabel = new System.Windows.Forms.Label();
            this.amountAllChineseLabel = new System.Windows.Forms.Label();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.refreshButton = new System.Windows.Forms.Button();
            this.refreshNowButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.viewAllDraftGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // viewAllDraftGridView
            // 
            this.viewAllDraftGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.viewAllDraftGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.viewAllDraftGridView.Location = new System.Drawing.Point(24, 25);
            this.viewAllDraftGridView.Margin = new System.Windows.Forms.Padding(6);
            this.viewAllDraftGridView.Name = "viewAllDraftGridView";
            this.viewAllDraftGridView.RowTemplate.Height = 23;
            this.viewAllDraftGridView.Size = new System.Drawing.Size(1109, 555);
            this.viewAllDraftGridView.TabIndex = 0;
            // 
            // amountAllLabel
            // 
            this.amountAllLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.amountAllLabel.AutoSize = true;
            this.amountAllLabel.Location = new System.Drawing.Point(281, 601);
            this.amountAllLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.amountAllLabel.Name = "amountAllLabel";
            this.amountAllLabel.Size = new System.Drawing.Size(70, 25);
            this.amountAllLabel.TabIndex = 1;
            this.amountAllLabel.Text = "label1";
            // 
            // amountAllNumericLabel
            // 
            this.amountAllNumericLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.amountAllNumericLabel.AutoSize = true;
            this.amountAllNumericLabel.Location = new System.Drawing.Point(403, 601);
            this.amountAllNumericLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.amountAllNumericLabel.Name = "amountAllNumericLabel";
            this.amountAllNumericLabel.Size = new System.Drawing.Size(70, 25);
            this.amountAllNumericLabel.TabIndex = 2;
            this.amountAllNumericLabel.Text = "label1";
            this.amountAllNumericLabel.Click += new System.EventHandler(this.amountAllNumericLabel_Click);
            // 
            // amountAllChineseLabel
            // 
            this.amountAllChineseLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.amountAllChineseLabel.AutoSize = true;
            this.amountAllChineseLabel.Location = new System.Drawing.Point(667, 601);
            this.amountAllChineseLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.amountAllChineseLabel.Name = "amountAllChineseLabel";
            this.amountAllChineseLabel.Size = new System.Drawing.Size(70, 25);
            this.amountAllChineseLabel.TabIndex = 3;
            this.amountAllChineseLabel.Text = "label1";
            this.amountAllChineseLabel.Click += new System.EventHandler(this.amountAllChineseLabel_Click);
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dateTimePicker.CustomFormat = "yyyy年MM月";
            this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dateTimePicker.Location = new System.Drawing.Point(24, 640);
            this.dateTimePicker.Margin = new System.Windows.Forms.Padding(6);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(334, 31);
            this.dateTimePicker.TabIndex = 4;
            // 
            // refreshButton
            // 
            this.refreshButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.refreshButton.Location = new System.Drawing.Point(370, 632);
            this.refreshButton.Margin = new System.Windows.Forms.Padding(6);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(254, 50);
            this.refreshButton.TabIndex = 5;
            this.refreshButton.Text = "加载选定月份库存信息";
            this.refreshButton.UseVisualStyleBackColor = true;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // refreshNowButton
            // 
            this.refreshNowButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.refreshNowButton.Location = new System.Drawing.Point(636, 632);
            this.refreshNowButton.Margin = new System.Windows.Forms.Padding(6);
            this.refreshNowButton.Name = "refreshNowButton";
            this.refreshNowButton.Size = new System.Drawing.Size(254, 50);
            this.refreshNowButton.TabIndex = 6;
            this.refreshNowButton.Text = "加载当前库存信息";
            this.refreshNowButton.UseVisualStyleBackColor = true;
            this.refreshNowButton.Click += new System.EventHandler(this.refreshNowButton_Click);
            // 
            // ViewAllForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1158, 697);
            this.Controls.Add(this.refreshNowButton);
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.dateTimePicker);
            this.Controls.Add(this.amountAllChineseLabel);
            this.Controls.Add(this.amountAllNumericLabel);
            this.Controls.Add(this.amountAllLabel);
            this.Controls.Add(this.viewAllDraftGridView);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "ViewAllForm";
            this.Text = "配件库存信息查询";
            ((System.ComponentModel.ISupportInitialize)(this.viewAllDraftGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView viewAllDraftGridView;
        private System.Windows.Forms.Label amountAllLabel;
        private System.Windows.Forms.Label amountAllNumericLabel;
        private System.Windows.Forms.Label amountAllChineseLabel;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.Button refreshNowButton;
        private System.Windows.Forms.DateTimePicker dateTimePicker;

    }
}