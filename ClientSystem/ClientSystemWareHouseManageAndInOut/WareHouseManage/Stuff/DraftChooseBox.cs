﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.WareHouseManage.Transmission;

namespace ClientSystem.WareHouseManage.Stuff
{
    /// <summary>
    /// This userControl is used to let user choose draft
    /// DoubleClick is called when user doubleclick the draft and it returns the draft which user click on
    /// 
    /// </summary>
    partial class DraftChooseBox : UserControl
    {
        //The delegates and events
        public delegate void DoubleClickEventHandler(DefaultDraft draft);
        public delegate void ScanResultEventHandler(DefaultDraft draft);
        public delegate void SearchEventHandler(string str);
        public new event DoubleClickEventHandler DoubleClick;
        public event ScanResultEventHandler Scan;
        public event SearchEventHandler Search;

        //containerListBoxList including all the three containerListBox
        private List<ListBox> containerListBoxList;

        //DisplayContainerEventHandler used to called the displayContainer the display the containers read just now for thread safety
        private delegate void DisplayContainerEventHandler(List<DraftContainer> list, int i);
        private delegate void DisplayDefaultDraftEventHandler(List<DefaultDraft> list);
        private delegate void DSpecialOneDraft(Draft draft);
        private delegate void DOperatedState(OperatedState instrState);

        //used to recover the selectedindex when refresh the draftChooseList
        private int olddraftSelectedIndex = -1;
        private int oldContainer3ID = 0;
        private int containerChosen = 0;
        List<DefaultDraft> draftList;

        //transmission
        private Transmission.Transmission mainTransmission;
        private List<Transmission.Connector> draftConnectors = new List<Transmission.Connector>();

        //the delegates called by transmission
        private Connector.DraftConnector.DContainer1List EContainer1List;
        private Connector.DraftConnector.DContainer2List EContainer2List;
        private Connector.DraftConnector.DContainer3List EContainer3List;
        private Connector.DraftConnector.DDraftList EDraftList;
        private Connector.DraftConnector.DSpecialOneDraft ESpecialOneDraft, ESpecialOneDraftScanResult;
        private Connector.DraftConnector.DOperatedState EOperateState;

        public DraftChooseBox(Transmission.Transmission mainTransmission)
        {
            InitializeComponent();

            this.mainTransmission = mainTransmission;
            if (mainTransmission != null)
                mainTransmission.addConnectors(SharedObject.Normal.SystemID.WareHouseManage, draftConnectors);
            else
                throw new Transmission.TransmissionIsNullException();

            EContainer1List = new Connector.DraftConnector.DContainer1List(receiveContainer1);
            EContainer2List = new Connector.DraftConnector.DContainer2List(receiveContainer2);
            EContainer3List = new Connector.DraftConnector.DContainer3List(receiveContainer3);
            EDraftList = new Connector.DraftConnector.DDraftList(receiveDisplayDraft);
            ESpecialOneDraft = new Connector.DraftConnector.DSpecialOneDraft(draft => this.Invoke(new DSpecialOneDraft(responseForAskForSpecialOneDraft), new object[] { draft }));
            ESpecialOneDraftScanResult = new Connector.DraftConnector.DSpecialOneDraft(draft => this.Invoke(new DSpecialOneDraft(responseForScanningResult), new object[] { draft }));
            EOperateState = new Connector.DraftConnector.DOperatedState(state => this.Invoke(new DOperatedState(responseForOperatedState), new object[] { state }));

            containerListBoxList = new List<ListBox>();
            containerListBoxList.Add(containerListBox1);
            containerListBoxList.Add(containerListBox2);
            containerListBoxList.Add(containerListBox3);

            Connector.DraftConnector draftConnector = new Connector.DraftConnector(Instruction.draftSpecialOne, mainTransmission, draftConnectors);
            draftConnector.ESpecialOneDraft += ESpecialOneDraft;

            this.draftListGridView.DoubleClick += new EventHandler(draftListGridView_DoubleClick);
            this.Disposed += new EventHandler(DraftChooseBox_Disposed);
            this.Load += new EventHandler(DraftChooseBox_Load);
        }

        private void DraftChooseBox_Load(object sender, EventArgs e)
        {
            if (mainTransmission != null && mainTransmission.connected)
            {
                Connector.DraftConnector draftConnector = new Connector.DraftConnector(Instruction.draftContainer1List, mainTransmission, draftConnectors);
                draftConnector.EContainer1List += EContainer1List;
                draftConnector.getContainer1();
            }
        }

        public void DraftChooseBox_Disposed(Object sender, EventArgs args)
        {
            while (draftConnectors.Count > 0)
                draftConnectors[0].Dispose();
            mainTransmission.removeConnectors(SharedObject.Normal.SystemID.WareHouseManage, draftConnectors);
        }

        /// These methods are used to show the containers
        /// These methods are called by mainTransmission
        private void receiveContainer1(List<DraftContainer> list)
        {
            this.Invoke(new DisplayContainerEventHandler(displayContainer), new object[] { list, 0 });
        }
        private void receiveContainer2(List<DraftContainer> list)
        {
            this.Invoke(new DisplayContainerEventHandler(displayContainer), new object[] { list, 1 });
        }
        private void receiveContainer3(List<DraftContainer> list)
        {
            this.Invoke(new DisplayContainerEventHandler(displayContainer), new object[] { list, 2 });
        }
        /// This method is used to receive draft from server
        /// This method is called by mainTransmission
        private void receiveDisplayDraft(List<DefaultDraft> list)
        {
            if (list != null)
                this.Invoke(new DisplayDefaultDraftEventHandler(displayDraft), new object[] { list });
        }

        /// This method is used to show a special draft
        private void responseForAskForSpecialOneDraft(Draft draft)
        {
            if (draft != null)
            {
                refreshDraft(draft);
            }
        }

        /// This method is used to show a special draft
        private void responseForScanningResult(Draft draft)
        {
            if (draft != null && Scan != null)
            {
                Scan(draft);
            }
        }
        ///This method is used for response to server
        private void responseForOperatedState(OperatedState operatedState)
        {
            String str = Function.OperatedStateStringBuilder(operatedState);
            if (str.Length > 0)
                MessageBox.Show(str);
            //setAccessiable(null);
        }

        /// Method displayContainer is used to display the containers read just now
        /// <param name="i">0->container1  1->container2  2->container3</param>
        private void displayContainer(List<DraftContainer> list, int i)
        {
            containerListBoxList[i].Items.Clear();
            if (list != null)
            {
                foreach (DraftContainer container in list)
                {
                    containerListBoxList[i].Items.Add(container);
                }
                if (list.Count == 1)
                {
                    containerListBoxList[i].SelectedIndex = 0;
                }
            }
        }

        /// This method is used to display the draft read just now from the tcpClient
        private void displayDraft(List<DefaultDraft> list)
        {
            this.draftList = list;
            if (list != null)
            {
                draftListGridView.Rows.Clear();
                foreach (DefaultDraft draft in list)
                    draftListGridView.Rows.Add(
                        new Object[] {
                            draft.id,
                            draft.name,
                            draft.specification + "->" + draft.hiddenSpecification,
                            draft.numAccessiable,
                            (double)draft.priceOutH / 100
                        });
            }
        }

        public void refreshDraft(Draft draft)
        {
            int k = draftListGridView.Rows.Count;
            for (int i = 0; i < k; i++)
            {
                if (this.draftList[i].id == draft.id)
                {
                    draftListGridView.Rows[i].Cells["DraftName"].Value = draft.name;
                    draftListGridView.Rows[i].Cells["Specification"].Value = draft.specification + "->" + draft.hiddenSpecification;
                    break;
                }
            }
        }

        public int getChosenContainer()
        {
            return containerChosen;
        }

        /// <summary>
        /// Method getChosenContainer is used to get the container which is chosen by user
        /// </summary>
        /// <param name="i">0->container1  1->container2  2->container3</param>
        /// <returns></returns>
        public DraftContainer getChosenObjectAccordingToContainer(int i)
        {
            if (i < 0 || i > 2)
                return null;
            int j = containerListBoxList[i].SelectedIndex;
            if (j >= 0)
                return (DraftContainer)containerListBoxList[i].Items[j];
            else
                return null;
        }

        /// <summary>
        /// Method getChosenDefaultDraft is used to get the DefaultDraft which is chosen by user
        /// </summary>
        /// <returns></returns>
        public DefaultDraft getChosenDefaultDraft()
        {
            if (draftListGridView.SelectedRows.Count > 0)
            {
                int j = draftListGridView.SelectedRows[0].Index;
                return draftList[j];
            }
            else
                return null;
        }

        private void draftListGridView_DoubleClick(object sender, EventArgs e)
        {
            Object obj = this;
            if (DoubleClick != null)
            {
                DefaultDraft a = getChosenDefaultDraft();
                if (a != null)
                    DoubleClick(a);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            if (mainTransmission != null && mainTransmission.connected)
            {
                string traString = searchTextBox.Text.Trim();
                int i;
                if (!Int32.TryParse(traString, out i) && traString.Length < 2)
                {
                    MessageBox.Show("对不起，为防止产生过多的搜索结果，您的输入至少要有3个字符");
                    return;
                }
                Connector.DraftConnector draftConnector = new Connector.DraftConnector(Instruction.draftDisplayDraftListSearch, mainTransmission, draftConnectors);
                draftConnector.EDraftList += EDraftList;
                draftConnector.getDisplayDraftListSearch(traString);
                if (Search != null)
                    Search(searchTextBox.Text);
            }
        }

        public void scanDraftByID(int id)
        {
            if (mainTransmission != null && mainTransmission.connected)
            {
                Connector.DraftConnector draftConnector = new Connector.DraftConnector(Instruction.draftSpecialOne, mainTransmission, draftConnectors);
                draftConnector.ESpecialOneDraft += ESpecialOneDraftScanResult;
                draftConnector.EOperatedState += EOperateState;
                draftConnector.getSpecialOneDraft(id);
            }
        }

        private void containerListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            containerChosen = 0;
            if (mainTransmission != null && mainTransmission.connected)
            {
                if (containerListBox1.SelectedIndex < 0) return;
                containerListBoxList[1].Items.Clear();
                containerListBoxList[2].Items.Clear();
                draftListGridView.Rows.Clear();
                Connector.DraftConnector draftConnector = new Connector.DraftConnector(Instruction.draftContainer2List, mainTransmission, draftConnectors);
                draftConnector.EContainer2List += EContainer2List;
                draftConnector.getContainer2(((DraftContainer)containerListBox1.SelectedItem).id);
            }
        }

        private void containerListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            containerChosen = 1;
            if (mainTransmission != null && mainTransmission.connected)
            {
                if (containerListBox2.SelectedIndex < 0) return;
                containerListBoxList[2].Items.Clear();
                draftListGridView.Rows.Clear();
                Connector.DraftConnector draftConnector = new Connector.DraftConnector(Instruction.draftContainer3List, mainTransmission, draftConnectors);
                draftConnector.EContainer3List += EContainer3List;
                draftConnector.getContainer3(((DraftContainer)containerListBox2.SelectedItem).id);
            }
        }

        private void containerListBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            containerChosen = 2;
            if (mainTransmission != null && mainTransmission.connected)
            {
                if (containerListBox3.SelectedIndex < 0) return;
                draftListGridView.Rows.Clear();
                oldContainer3ID = ((DraftContainer)containerListBox3.SelectedItem).id;
                if (draftListGridView.SelectedRows.Count > 0)
                    olddraftSelectedIndex = draftListGridView.Rows[0].Index;
                Connector.DraftConnector draftConnector = new Connector.DraftConnector(Instruction.draftDisplayDraftList, mainTransmission, draftConnectors);
                draftConnector.EDraftList += EDraftList;
                draftConnector.getDisplayDraftList(oldContainer3ID);
            }
        }
    }
}