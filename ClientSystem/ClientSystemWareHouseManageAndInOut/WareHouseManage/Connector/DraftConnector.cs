﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.WareHouseManage.Transmission;

namespace ClientSystem.WareHouseManage.Connector
{
    public class DraftConnector : Transmission.Connector
    {
        //delegate for receiving somekind of object
        public delegate void DContainer1List(List<DraftContainer> lists);
        public delegate void DContainer2List(List<DraftContainer> lists);
        public delegate void DContainer3List(List<DraftContainer> lists);
        public delegate void DDraftList(List<DefaultDraft> lists);
        public delegate void DSpecialOneDraft(Draft draft);
        public delegate void DViewAllDraft(List<ViewAllDraft> viewAllDraftList);
        public event DContainer1List EContainer1List = null;
        public event DContainer2List EContainer2List = null;
        public event DContainer3List EContainer3List = null;
        public event DDraftList EDraftList = null;
        public event DSpecialOneDraft ESpecialOneDraft = null;
        public event DViewAllDraft EViewAllDraft = null;

        public DraftConnector(Instruction mainInstruction, Transmission.Transmission mainTransmission, List<Transmission.Connector> draftConnectors)
            : base(mainInstruction, mainTransmission, SharedObject.Normal.SystemID.WareHouseManage, draftConnectors) { }

        protected override bool ResponseMethod(Object obj, Instruction mainInstruction, OperatedState operatedState)
        {
            switch (mainInstruction)
            {
                case Instruction.draftContainer1List:
                case Instruction.draftContainer2List:
                case Instruction.draftContainer3List:
                    TSCDraftContainerList transSCDraftContainerList = null;
                    if (obj != null && obj is TSCDraftContainerList)
                        transSCDraftContainerList = (TSCDraftContainerList)obj;
                    switch (mainInstruction)
                    {
                        case Instruction.draftContainer1List:
                            if (EContainer1List != null)
                                EContainer1List(transSCDraftContainerList.draftContainers);
                            break;
                        case Instruction.draftContainer2List:
                            if (EContainer2List != null)
                                EContainer2List(transSCDraftContainerList.draftContainers);
                            break;
                        case Instruction.draftContainer3List:
                            if (EContainer3List != null)
                                EContainer3List(transSCDraftContainerList.draftContainers);
                            break;
                    }
                    break;
                case Instruction.draftDisplayDraftList:
                case Instruction.draftDisplayDraftListSearch:
                    if (EDraftList != null)
                    {
                        TSCDefaultDraftList transSCDefaultDraftList = null;
                        if (obj != null && obj is TSCDefaultDraftList)
                            transSCDefaultDraftList = (TSCDefaultDraftList)obj;
                        if (transSCDefaultDraftList != null)
                            EDraftList(transSCDefaultDraftList.draftList);
                    }
                    break;
                case Instruction.draftViewAll:
                    if (EViewAllDraft != null)
                    {
                        TSCViewAll TSCViewAll = null;
                        if (obj != null && obj is TSCViewAll)
                            TSCViewAll = (TSCViewAll)obj;
                        if (TSCViewAll != null)
                            EViewAllDraft(TSCViewAll.viewAllDraft);
                    }
                    break;
                case Instruction.draftSpecialOne:
                    if (ESpecialOneDraft != null)
                    {
                        TSCDraft transSCDraft = null;
                        if (obj != null && obj is TSCDraft)
                            transSCDraft = (TSCDraft)obj;
                        id = transSCDraft.draft.id;
                        if (transSCDraft != null)
                            ESpecialOneDraft(transSCDraft.draft);
                    }
                    break;
                case Instruction.draftAdd:
                case Instruction.draftEdit:
                    break;
            }
            if (operatedState.state == SharedObject.Main.Transmission.OperatedState.OperatedStateEnum.success || operatedState.state == SharedObject.Main.Transmission.OperatedState.OperatedStateEnum.normal)
            {
                switch (mainInstruction)
                {
                    case Instruction.draftAdd:
                    case Instruction.draftEdit:
                        if (id > 0)
                        {
                            this.mainInstruction = Instruction.draftSpecialOne;
                            getSpecialOneDraft(id);
                            return false;
                        }
                        break;
                }
            }
            return true;
        }

        public void getContainer1()
        {
            sendRequestObj(null, new byte[] { });
        }

        public void getContainer2(int id)
        {
            sendRequestObj(null, id);
        }

        public void getContainer3(int id)
        {
            sendRequestObj(null, id);
        }

        public void getDisplayDraftList(int id)
        {
            this.id = id;
            sendRequestObj(null, id);
        }

        public void getDisplayDraftListSearch(string searchStr)
        {
            TCSSearchString transCSSearchString = new TCSSearchString();
            transCSSearchString.traString = searchStr;
            sendRequestObj(transCSSearchString, new byte[] { });
        }

        public void getSpecialOneDraft(int id)
        {
            this.id = id;
            sendRequestObj(null, id);
        }

        public void getViewDraftList(int rangeType, int range, DateTime date)
        {
            TCSViewAll tCSViewAll = new TCSViewAll();
            tCSViewAll.rangeType = rangeType;
            tCSViewAll.range = range;
            tCSViewAll.date = date;
            sendRequestObj(tCSViewAll, new byte[] { });
        }

        public void addEditDraft(Draft draft)
        {
            id = draft.id;
            TCSDraftCreate tSCFittignsCreate = new TCSDraftCreate();
            tSCFittignsCreate.draft = draft;
            sendRequestObj(tSCFittignsCreate, new byte[] { });
        }

        public void submitDraft(DefaultDraft draft)
        {
            id = draft.id;
            TCSDraftControl tCSDraftControl = new TCSDraftControl();
            tCSDraftControl.draft = new DraftCSControl(draft);
            sendRequestObj(tCSDraftControl, new byte[] { });
        }
    }
}
