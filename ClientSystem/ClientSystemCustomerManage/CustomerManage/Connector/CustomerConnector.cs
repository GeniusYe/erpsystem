﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SharedObject.Main.Transmission;
using SharedObject.CustomerManage.Stuff;
using SharedObject.CustomerManage.Transmission;

namespace ClientSystem.CustomerManage.Connector
{
    public class CustomerConnector : Transmission.Connector
    {
        private static List<DefaultCustomer> allCustomerList;
        private static List<DefaultCustomer> inSourceList;
        private static List<DefaultCustomer> outCustomerList;

        //delegate for receiving somekind of object
        public delegate void DCustomerList(List<DefaultCustomer> lists);
        public delegate void DSpecialOneCustomer(Customer customer);
        public event DCustomerList ECustomerList = null;
        public event DSpecialOneCustomer ESpecialOneCustomer = null;

        public CustomerConnector(Instruction mainInstruction, Transmission.Transmission mainTransmission, List<Transmission.Connector> customerConnectors)
            : base(mainInstruction, mainTransmission, SharedObject.Normal.SystemID.CustomerManage, customerConnectors) { }

        protected override bool ResponseMethod(Object obj, Instruction mainInstruction, OperatedState operatedState)
        {
            switch (mainInstruction)
            {
                case Instruction.rights:
                    break;
                case Instruction.customerInSourceList:
                case Instruction.customerOutCustomerList:
                case Instruction.customerAllCustomersList:
                    if (ECustomerList != null)
                    {
                        TSCCustomersList tSCCustomersList = null;
                        if (obj != null && obj is TSCCustomersList)
                            tSCCustomersList = (TSCCustomersList)obj;
                        switch (mainInstruction)
                        {
                            case Instruction.customerInSourceList:
                                inSourceList = tSCCustomersList.customersList;
                                break;
                            case Instruction.customerOutCustomerList:
                                outCustomerList = tSCCustomersList.customersList;
                                break;
                            case Instruction.customerAllCustomersList:
                                allCustomerList = tSCCustomersList.customersList;
                                if (inSourceList == null)
                                    inSourceList = new List<DefaultCustomer>();
                                else
                                    inSourceList.Clear();
                                var inSource = (from customer in allCustomerList where customer.ifInSource == true select customer);
                                foreach (DefaultCustomer customer in inSource)
                                    inSourceList.Add(customer);
                                if (outCustomerList == null)
                                    outCustomerList = new List<DefaultCustomer>();
                                else
                                    outCustomerList.Clear();
                                var outCustomer = (from customer in allCustomerList where customer.ifCustomer == true select customer);
                                foreach (DefaultCustomer customer in outCustomer)
                                    outCustomerList.Add(customer);
                                break;
                        }
                        if (ECustomerList != null)
                            getWarrantCustomersList();
                    }
                    break;
                case Instruction.customerSpecialOneCustomer:
                case Instruction.customerAdd:
                    if (ESpecialOneCustomer != null)
                    {
                        TSCCustomer tSCCustomer = null;
                        if (obj != null && obj is TSCCustomer)
                            tSCCustomer = (TSCCustomer)obj;
                        if (tSCCustomer != null)
                        {
                            Customer customer = tSCCustomer.customer;
                            if (customer != null)
                            {
                                int i;
                                for (i = 0; i < allCustomerList.Count; i++)
                                    if (allCustomerList[i].id == customer.id)
                                    {
                                        allCustomerList[i] = customer;
                                        break;
                                    }
                                if (i == allCustomerList.Count)
                                    allCustomerList.Add(customer);
                            }
                            ESpecialOneCustomer(tSCCustomer.customer);
                        }
                    }
                    break;
                default:
                    throw new ConnectorMainInstructionNotMatchException();
            }
            return true;
        }

        public void getWarrantCustomersList()
        {
            List<DefaultCustomer> list = new List<DefaultCustomer>();
            switch (mainInstruction)
            {
                case Instruction.customerInSourceList:
                    if (inSourceList != null && ECustomerList != null)
                    {
                        foreach (DefaultCustomer cus in inSourceList)
                            list.Add(cus);
                        ECustomerList(list);
                        this.Dispose();
                        return;
                    }
                    break;
                case Instruction.customerOutCustomerList:
                    if (outCustomerList != null && ECustomerList != null)
                    {
                        foreach (DefaultCustomer cus in outCustomerList)
                            list.Add(cus);
                        ECustomerList(list);
                        this.Dispose();
                        return;
                    }
                    break;
                case Instruction.customerAllCustomersList:
                    if (allCustomerList != null && ECustomerList != null)
                    {
                        foreach (DefaultCustomer cus in allCustomerList)
                            list.Add(cus);
                        ECustomerList(list);
                        this.Dispose();
                        return;
                    }
                    break;
            }    
            sendRequestObj(null, new byte[] { });
        }

        public void refreshWarrantCustomersList()
        {
            sendRequestObj(null, new byte[] { });
        }

        public void getSpecialOneCustomer(int id)
        {
            sendRequestObj(null, id);
        }

        public void addCustomer(Customer customer)
        {
            TCSCustomer tCSCustomer = new TCSCustomer();
            tCSCustomer.customer = customer;
            sendRequestObj(tCSCustomer, new byte[] { });
        }
    }
}
