﻿namespace ClientSystem.CustomerManage.Stuff
{
    partial class CustomerDetailBox
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.informationTextBox = new System.Windows.Forms.TextBox();
            this.giveAwayAmountTextBox = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.informationLabel = new System.Windows.Forms.Label();
            this.giveAwayAmountLabel = new System.Windows.Forms.Label();
            this.inSourceCheckBox = new System.Windows.Forms.CheckBox();
            this.outCustomerCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(50, 3);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(89, 21);
            this.nameTextBox.TabIndex = 1;
            // 
            // informationTextBox
            // 
            this.informationTextBox.Location = new System.Drawing.Point(50, 30);
            this.informationTextBox.Multiline = true;
            this.informationTextBox.Name = "informationTextBox";
            this.informationTextBox.Size = new System.Drawing.Size(164, 112);
            this.informationTextBox.TabIndex = 3;
            // 
            // giveAwayAmountTextBox
            // 
            this.giveAwayAmountTextBox.Enabled = false;
            this.giveAwayAmountTextBox.Location = new System.Drawing.Point(86, 148);
            this.giveAwayAmountTextBox.Name = "giveAwayAmountTextBox";
            this.giveAwayAmountTextBox.Size = new System.Drawing.Size(128, 21);
            this.giveAwayAmountTextBox.TabIndex = 5;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(3, 6);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(41, 12);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "姓名：";
            // 
            // informationLabel
            // 
            this.informationLabel.AutoSize = true;
            this.informationLabel.Location = new System.Drawing.Point(3, 33);
            this.informationLabel.Name = "informationLabel";
            this.informationLabel.Size = new System.Drawing.Size(41, 12);
            this.informationLabel.TabIndex = 2;
            this.informationLabel.Text = "说明：";
            // 
            // giveAwayAmountLabel
            // 
            this.giveAwayAmountLabel.AutoSize = true;
            this.giveAwayAmountLabel.Location = new System.Drawing.Point(3, 151);
            this.giveAwayAmountLabel.Name = "giveAwayAmountLabel";
            this.giveAwayAmountLabel.Size = new System.Drawing.Size(77, 12);
            this.giveAwayAmountLabel.TabIndex = 4;
            this.giveAwayAmountLabel.Text = "剩余赠送额：";
            // 
            // inSourceCheckBox
            // 
            this.inSourceCheckBox.AutoSize = true;
            this.inSourceCheckBox.Location = new System.Drawing.Point(5, 175);
            this.inSourceCheckBox.Name = "inSourceCheckBox";
            this.inSourceCheckBox.Size = new System.Drawing.Size(96, 16);
            this.inSourceCheckBox.TabIndex = 6;
            this.inSourceCheckBox.Text = "是否入库用户";
            this.inSourceCheckBox.UseVisualStyleBackColor = true;
            // 
            // outCustomerCheckBox
            // 
            this.outCustomerCheckBox.AutoSize = true;
            this.outCustomerCheckBox.Location = new System.Drawing.Point(118, 175);
            this.outCustomerCheckBox.Name = "outCustomerCheckBox";
            this.outCustomerCheckBox.Size = new System.Drawing.Size(96, 16);
            this.outCustomerCheckBox.TabIndex = 7;
            this.outCustomerCheckBox.Text = "是否出库用户";
            this.outCustomerCheckBox.UseVisualStyleBackColor = true;
            // 
            // CustomerDetailBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.outCustomerCheckBox);
            this.Controls.Add(this.inSourceCheckBox);
            this.Controls.Add(this.giveAwayAmountLabel);
            this.Controls.Add(this.informationLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.giveAwayAmountTextBox);
            this.Controls.Add(this.informationTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Name = "CustomerDetailBox";
            this.Size = new System.Drawing.Size(218, 193);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox informationTextBox;
        private System.Windows.Forms.TextBox giveAwayAmountTextBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label informationLabel;
        private System.Windows.Forms.Label giveAwayAmountLabel;
        private System.Windows.Forms.CheckBox inSourceCheckBox;
        private System.Windows.Forms.CheckBox outCustomerCheckBox;
    }
}
