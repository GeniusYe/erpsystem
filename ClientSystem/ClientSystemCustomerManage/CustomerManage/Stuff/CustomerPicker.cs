﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.CustomerManage.Stuff;

namespace ClientSystem.CustomerManage.Stuff
{
    public partial class CustomerPicker : UserControl
    {
        private List<DefaultCustomer> _customerList;
        private DefaultCustomer chosenCustomer;
        
        public delegate void DCustomerChosen(DefaultCustomer customer);
        public event DCustomerChosen ECustomerChosen;
        private bool ifTrigerCustomerChosen = true;

        public List<DefaultCustomer> customerList
        {
            get
            {
                return _customerList;
            }
            set
            {
                this._customerList = value;
                displayCustomerList();
            }
        }

        public CustomerPicker()
        {
            InitializeComponent();
            this.customerComboBox.SelectedIndexChanged += new EventHandler(customerComboBox_SelectedIndexChanged);
            this.customerComboBox.TextChanged += new EventHandler(this.customerComboBox_TextChanged);
        }

        private void customerComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DefaultCustomer defaultCustomer = (DefaultCustomer)customerComboBox.SelectedItem;
            if (defaultCustomer != null)
            {
                chosenCustomer = defaultCustomer;
                if (ifTrigerCustomerChosen && ECustomerChosen != null)
                    ECustomerChosen(chosenCustomer);
            }
        }

        private void customerComboBox_TextChanged(object sender, EventArgs e)
        {
            DefaultCustomer defaultCustomer = (DefaultCustomer)customerComboBox.SelectedItem;
            if (defaultCustomer == null)
            {
                chosenCustomer = null;
                customerComboBox.Items.Clear();
                var list = from customer in _customerList where customer.name.Contains(customerComboBox.Text) select customer;
                foreach (DefaultCustomer customer in list)
                    customerComboBox.Items.Add(customer);
                if (customerComboBox.Items.Count == 1 && ((DefaultCustomer)customerComboBox.Items[0]).name == customerComboBox.Text)
                    chosenCustomer = (DefaultCustomer)customerComboBox.Items[0];
                if (ifTrigerCustomerChosen && ECustomerChosen != null)
                    ECustomerChosen(chosenCustomer);
                customerComboBox.Select(customerComboBox.Text.Length, 0);
            }
        }

        private void displayCustomerList()
        {
            if (customerList != null)
            {
                customerComboBox.Items.Clear();
                foreach (DefaultCustomer customer in customerList)
                    customerComboBox.Items.Add(customer);
            }
        }

        public DefaultCustomer getChosenCustomer()
        {
            return chosenCustomer;
        }

        public void setTarget(int? id)
        {
            if (id == null)
                customerComboBox.SelectedIndex = -1;
            if (customerList != null)
            {
                ifTrigerCustomerChosen = false;
                int i;
                for (i = 0; i < customerList.Count; i++)
                    if (customerList[i].id == id)
                    {
                        customerComboBox.SelectedIndex = i;
                        break;
                    }
                if (i == customerList.Count)
                    customerComboBox.Text = "";
                ifTrigerCustomerChosen = true;
            }
        }

        private void CustomerPicker_LostFocus(object sender, System.EventArgs e)
        {
            if (chosenCustomer == null)
                customerComboBox.Text = "";
            if (ECustomerChosen != null)
                ECustomerChosen(chosenCustomer);
        }
    }
}
