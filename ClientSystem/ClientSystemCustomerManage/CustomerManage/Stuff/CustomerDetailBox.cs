﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.CustomerManage.Stuff;
using SharedObject.CustomerManage.Transmission;

namespace ClientSystem.CustomerManage.Stuff
{
    public partial class CustomerDetailBox : UserControl
    {
        private Customer _customer;

        public Customer customer
        {
            get
            {
                if (_customer == null)
                    _customer = new Customer();
                _customer.name = nameTextBox.Text;
                _customer.information = informationTextBox.Text;
                _customer.ifCustomer = outCustomerCheckBox.Checked;
                _customer.ifInSource = inSourceCheckBox.Checked;
                return _customer;
            }
            set
            {
                this._customer = value;
                displayCustomer();
            }
        }

        public void displayCustomer()
        {
            if (_customer == null)
            {
                this.nameTextBox.Text = "";
                this.informationTextBox.Text = "";
                this.inSourceCheckBox.Checked = false;
                this.outCustomerCheckBox.Checked = false;
                this.giveAwayAmountTextBox.Text = ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(0);
                return;
            }
            this.nameTextBox.Text = _customer.name;
            this.informationTextBox.Text = _customer.information;
            this.inSourceCheckBox.Checked = _customer.ifInSource == true ? true : false;
            this.outCustomerCheckBox.Checked = _customer.ifCustomer == true ? true : false;
            this.giveAwayAmountTextBox.Text = (_customer.giveAwayAmount != null && _customer.giveAwayAlready != null) ? ClassObjectToNum.Money.ClassMoney.MoneyNumToDisplayString(_customer.giveAwayAmount.Value - _customer.giveAwayAlready.Value) : "0.00";
        }

        public CustomerDetailBox()
        {
            InitializeComponent();
        }
    }
}
