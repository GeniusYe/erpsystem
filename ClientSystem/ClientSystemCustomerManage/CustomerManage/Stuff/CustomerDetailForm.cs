﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.CustomerManage.Stuff;

namespace ClientSystem.CustomerManage.Stuff
{
    public partial class CustomerDetailForm : Form
    {
        public Customer customer
        {
            get
            {
                return customerDetailBox.customer;
            }
            set
            {
                customerDetailBox.customer = value;
            }
        }

        //delegates and events
        public delegate void AddGiveAwayAmountEventHandler(Customer customer);         //when release submitButton
        public delegate void OkEventHandler(Customer customer);             //when release okButton
        public delegate void CancelEventHandler(Customer customer);         //when release cancelButton
        public event CancelEventHandler Cancel;
        //public event AddGiveAwayAmountEventHandler AddGiveAwayAmount;
        public event OkEventHandler Ok;

        public CustomerDetailForm()
        {
            InitializeComponent();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (Ok != null)
                Ok(customer);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (Cancel != null)
                Cancel(customer);
            this.Dispose();
        }
    }
}
