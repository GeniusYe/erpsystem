﻿namespace ClientSystem.CustomerManage.Stuff
{
    partial class CustomerDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomerDetailForm));
            this.customerDetailBox = new ClientSystem.CustomerManage.Stuff.CustomerDetailBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.addGiveAwayAmountButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // customerDetailBox
            // 
            this.customerDetailBox.customer = ((SharedObject.CustomerManage.Stuff.Customer)(resources.GetObject("customerDetailBox.customer")));
            this.customerDetailBox.Location = new System.Drawing.Point(12, 12);
            this.customerDetailBox.Name = "customerDetailBox";
            this.customerDetailBox.Size = new System.Drawing.Size(218, 193);
            this.customerDetailBox.TabIndex = 5;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(236, 41);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(88, 23);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "取消";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(236, 12);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(88, 23);
            this.OkButton.TabIndex = 3;
            this.OkButton.Text = "确定";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // addGiveAwayAmountButton
            // 
            this.addGiveAwayAmountButton.Location = new System.Drawing.Point(236, 70);
            this.addGiveAwayAmountButton.Name = "addGiveAwayAmountButton";
            this.addGiveAwayAmountButton.Size = new System.Drawing.Size(88, 23);
            this.addGiveAwayAmountButton.TabIndex = 4;
            this.addGiveAwayAmountButton.Text = "增加折让额";
            this.addGiveAwayAmountButton.UseVisualStyleBackColor = true;
            // 
            // CustomerDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 217);
            this.Controls.Add(this.customerDetailBox);
            this.Controls.Add(this.addGiveAwayAmountButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.OkButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "CustomerDetailForm";
            this.Text = "CustomerDetailForm";
            this.ResumeLayout(false);

        }

        #endregion

        private CustomerDetailBox customerDetailBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button addGiveAwayAmountButton;

    }
}