﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharedObject.Main.Transmission;
using SharedObject.CustomerManage.Stuff;

namespace ClientSystem.CustomerManage.Stuff
{
    public partial class CustomerPrintForm : Form
    {
        private Transmission.Transmission mainTransmission;

        private List<Transmission.Connector> customerConnectors = new List<Transmission.Connector>();

        private List<DefaultCustomer> customerList;

        private Connector.CustomerConnector.DCustomerList ECustomerList;
        private Connector.CustomerConnector.DSpecialOneCustomer ESpecialOneCustomer;
        private Connector.CustomerConnector.DOperatedState EOperatedState;
        
        private delegate void DCustomerList(List<DefaultCustomer> customerList);
        private delegate void DSpeicalOneCustomer(Customer customer);
        private delegate void DOperatedState(SharedObject.Main.Transmission.OperatedState operatedState);

        private int _editingState = 0;//0 普通 1 添加 2 编辑
        private int editingState
        {
            get
            {
                return _editingState;
            }
            set
            {
                _editingState = value;
                switch (_editingState)
                {
                    case 0:
                        customerDetailBox1.Enabled = false;
                        OkButton.Enabled = false;
                        cancelButton.Enabled = false;
                        addGiveAwayAmountButton.Enabled = false;
                        break;
                    case 1:
                        customerDetailBox1.Enabled = true;
                        OkButton.Enabled = true;
                        cancelButton.Enabled = true;
                        addGiveAwayAmountButton.Enabled = true;
                        break;
                }
            }
        }

        public CustomerPrintForm()
        {
            InitializeComponent();

            mainTransmission = Transmission.Transmission.mainTransmission;
            if (mainTransmission != null)
                mainTransmission.addConnectors(SharedObject.Normal.SystemID.CustomerManage, customerConnectors);
            else
                throw new Transmission.TransmissionIsNullException();

            ECustomerList = new Connector.CustomerConnector.DCustomerList(list => this.Invoke(new DCustomerList(responseForCustomerList), new object[] { list }));
            ESpecialOneCustomer = new Connector.CustomerConnector.DSpecialOneCustomer(customer => this.Invoke(new DSpeicalOneCustomer(cus => this.responseForSpecialOneCustomer(cus)), new object[] { customer }));
            EOperatedState = new Connector.CustomerConnector.DOperatedState(operatedState => this.Invoke(new DOperatedState(responseForOperatedState), new object[] { operatedState }));

            this.customerListbox.Click += new EventHandler(customerListbox_Click);
            
            this.Load += new EventHandler(CustomerPrintForm_Load);
            this.FormClosing += new FormClosingEventHandler(CustomerPrintForm_FormClosing);  
        }

        private void customerListbox_Click(object sender, EventArgs e)
        {
            if (customerListbox.SelectedIndex >= 0 && customerListbox.SelectedIndex < customerList.Count)
            {
                customerDetailBox1.customer = null;
                Connector.CustomerConnector connector = new Connector.CustomerConnector(SharedObject.Main.Transmission.Instruction.customerSpecialOneCustomer, mainTransmission, customerConnectors);
                connector.ESpecialOneCustomer += ESpecialOneCustomer;
                connector.EOperatedState += EOperatedState;
                connector.getSpecialOneCustomer(customerList[customerListbox.SelectedIndex].id);
            }
        }

        ///This method is used for response to server
        private void responseForOperatedState(OperatedState operatedState)
        {
            String str = Function.OperatedStateStringBuilder(operatedState);
            if (str.Length > 0)
                MessageBox.Show(str);
        }

        private void CustomerPrintForm_Load(object sender, EventArgs e)
        {
            editingState = 0;
            Connector.CustomerConnector connector = new Connector.CustomerConnector(SharedObject.Main.Transmission.Instruction.customerAllCustomersList, mainTransmission, customerConnectors);
            connector.ECustomerList += ECustomerList;
            connector.getWarrantCustomersList();
        }

        private void CustomerPrintForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            mainTransmission.removeConnectors(SharedObject.Normal.SystemID.CustomerManage, customerConnectors);
        }

        private void responseForCustomerList(List<DefaultCustomer> customerList)
        {
            this.customerListbox.Items.Clear();
            if (customerListbox != null)
            {
                this.customerList = customerList;
                foreach (DefaultCustomer customer in customerList)
                    customerListbox.Items.Add(customer.name);
            }
        }

        private void responseForSpecialOneCustomer(Customer customer)
        {
            if (customer != null)
            {
                customerDetailBox1.customer = customer;
                int i;
                for (i = 0; i < customerList.Count; i++)
                    if (customerList[i].id == customer.id)
                    {
                        customerList[i] = customer;
                        break;
                    }
                if (i == customerList.Count)
                {
                    customerList.Add(customer);
                    customerListbox.Items.Add(customer.name);
                }
                editingState = 0;
            }
        }

        private void addNewCustomerButton_Click(object sender, EventArgs e)
        {
            editingState = 1;
            customerDetailBox1.customer = null;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (editingState == 1)
            {
                Connector.CustomerConnector connector = new Connector.CustomerConnector(Instruction.customerAdd, mainTransmission, customerConnectors);
                connector.EOperatedState += EOperatedState;
                connector.ESpecialOneCustomer += ESpecialOneCustomer;
                connector.addCustomer(customerDetailBox1.customer);
            }
        }

        private void refreshCustomerListButton_Click(object sender, EventArgs e)
        {
            Connector.CustomerConnector connector = new Connector.CustomerConnector(SharedObject.Main.Transmission.Instruction.customerAllCustomersList, mainTransmission, customerConnectors);
            connector.ECustomerList += ECustomerList;
            connector.refreshWarrantCustomersList();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            editingState = 0;
        }
    }
}
