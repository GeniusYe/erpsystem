﻿namespace ClientSystem.CustomerManage.Stuff
{
    partial class CustomerPrintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomerPrintForm));
            this.customerListbox = new System.Windows.Forms.ListBox();
            this.addGiveAwayAmountButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.addNewCustomerButton = new System.Windows.Forms.Button();
            this.customerDetailBox1 = new ClientSystem.CustomerManage.Stuff.CustomerDetailBox();
            this.refreshCustomerListButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // customerListbox
            // 
            this.customerListbox.FormattingEnabled = true;
            this.customerListbox.ItemHeight = 12;
            this.customerListbox.Location = new System.Drawing.Point(12, 12);
            this.customerListbox.Name = "customerListbox";
            this.customerListbox.Size = new System.Drawing.Size(185, 220);
            this.customerListbox.TabIndex = 0;
            // 
            // addGiveAwayAmountButton
            // 
            this.addGiveAwayAmountButton.Location = new System.Drawing.Point(200, 240);
            this.addGiveAwayAmountButton.Name = "addGiveAwayAmountButton";
            this.addGiveAwayAmountButton.Size = new System.Drawing.Size(88, 23);
            this.addGiveAwayAmountButton.TabIndex = 6;
            this.addGiveAwayAmountButton.Text = "增加折让额";
            this.addGiveAwayAmountButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(333, 240);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(88, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "取消";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(333, 211);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(88, 23);
            this.OkButton.TabIndex = 5;
            this.OkButton.Text = "确定";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // addNewCustomerButton
            // 
            this.addNewCustomerButton.Location = new System.Drawing.Point(106, 240);
            this.addNewCustomerButton.Name = "addNewCustomerButton";
            this.addNewCustomerButton.Size = new System.Drawing.Size(88, 23);
            this.addNewCustomerButton.TabIndex = 8;
            this.addNewCustomerButton.Text = "增加";
            this.addNewCustomerButton.UseVisualStyleBackColor = true;
            this.addNewCustomerButton.Click += new System.EventHandler(this.addNewCustomerButton_Click);
            // 
            // customerDetailBox1
            // 
            this.customerDetailBox1.customer = ((SharedObject.CustomerManage.Stuff.Customer)(resources.GetObject("customerDetailBox1.customer")));
            this.customerDetailBox1.Location = new System.Drawing.Point(203, 12);
            this.customerDetailBox1.Name = "customerDetailBox1";
            this.customerDetailBox1.Size = new System.Drawing.Size(218, 193);
            this.customerDetailBox1.TabIndex = 1;
            // 
            // refreshCustomerListButton
            // 
            this.refreshCustomerListButton.Location = new System.Drawing.Point(12, 240);
            this.refreshCustomerListButton.Name = "refreshCustomerListButton";
            this.refreshCustomerListButton.Size = new System.Drawing.Size(88, 23);
            this.refreshCustomerListButton.TabIndex = 9;
            this.refreshCustomerListButton.Text = "刷新";
            this.refreshCustomerListButton.UseVisualStyleBackColor = true;
            this.refreshCustomerListButton.Click += new System.EventHandler(this.refreshCustomerListButton_Click);
            // 
            // CustomerPrintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 279);
            this.Controls.Add(this.refreshCustomerListButton);
            this.Controls.Add(this.addNewCustomerButton);
            this.Controls.Add(this.addGiveAwayAmountButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.customerDetailBox1);
            this.Controls.Add(this.customerListbox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CustomerPrintForm";
            this.Text = "客户管理";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox customerListbox;
        private CustomerDetailBox customerDetailBox1;
        private System.Windows.Forms.Button addGiveAwayAmountButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button addNewCustomerButton;
        private System.Windows.Forms.Button refreshCustomerListButton;
    }
}