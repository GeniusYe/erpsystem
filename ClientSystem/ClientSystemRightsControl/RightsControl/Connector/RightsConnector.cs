﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SharedObject.Main.Transmission;
using SharedObject.RightsControl.Stuff;
using SharedObject.RightsControl.Transmission;

namespace ClientSystem.RightsControl.Connector
{
    public class RightsConnector : Transmission.Connector
    {
        //delegate for receiving somekind of object
        public delegate void DAddUser();
        public event DAddUser EAddUser = null;

        public RightsConnector(Instruction mainInstruction, Transmission.Transmission mainTransmission, List<Transmission.Connector> warrantConnectors)
            : base(mainInstruction, mainTransmission, SharedObject.Normal.SystemID.RightsControl, warrantConnectors) { }

        protected override bool ResponseMethod(Object obj, Instruction mainInstruction, OperatedState operatedState)
        {
            switch (mainInstruction)
            {
                case Instruction.rightsAddUser:
                    if (EAddUser != null)
                    {
                        EAddUser();
                    }
                    break;
                default:
                    throw new ConnectorMainInstructionNotMatchException();
            }
            return true;
        }

        public void addNewUser(User user)
        {
            if (user == null)
                return;
            TCSUser tCSUser = new TCSUser();
            tCSUser.user = user;
            sendRequestObj(tCSUser, null);
        }
    }
}
