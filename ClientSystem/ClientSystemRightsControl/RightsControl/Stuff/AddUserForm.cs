﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClientSystem.Identity;
using SharedObject.Main.Transmission;
using SharedObject.RightsControl.Stuff;

namespace ClientSystem.RightsControl.Stuff
{
    public partial class AddUserForm : Form
    {
        private CardReadForm cardReadForm;

        private String cardInformation;

        private Transmission.Transmission mainTransmission;
        private List<Transmission.Connector> rightsControlConnectors = new List<Transmission.Connector>();

        protected delegate void DOperatedState(OperatedState instrState);

        protected Connector.RightsConnector.DOperatedState EOperateState;

        public AddUserForm(Transmission.Transmission mainTransmission)
        {
            InitializeComponent();

            this.mainTransmission = mainTransmission;
            if (this.mainTransmission != null)
                this.mainTransmission.addConnectors(SharedObject.Normal.SystemID.RightsControl, rightsControlConnectors);
            else
                throw new Transmission.TransmissionIsNullException();

            EOperateState = new Connector.RightsConnector.DOperatedState(state => this.Invoke(new DOperatedState(responseForOperatedState), new object[] { state }));

            this.FormClosing += new FormClosingEventHandler(AddUserForm_FormClosing);

            readCardButton.Text = "点击读卡";
        }

        private void AddUserForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            while (rightsControlConnectors.Count > 0)
                rightsControlConnectors[0].Dispose();
            if (this.mainTransmission != null)
                this.mainTransmission.removeConnectors(SharedObject.Normal.SystemID.RightsControl, rightsControlConnectors);
        }

        private void responseForOperatedState(OperatedState operatedState)
        {
            String str = Function.OperatedStateStringBuilder(operatedState);
            if (str.Length > 0)
                MessageBox.Show(str);
            this.Close();
        }

        private void readCardButton_Click(object sender, EventArgs e)
        {
            if (cardReadForm != null)
                cardReadForm.Dispose();
            cardReadForm = new CardReadForm();
            cardReadForm.FormClosed += new FormClosedEventHandler(cardReadForm_FormClosed);
            cardReadForm.Show();
        }

        private void cardReadForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            cardInformation = String.Empty;
            if (this.cardReadForm.cardInfoRead != String.Empty)
            {
                cardInformation = Function.getMd5Hash(this.cardReadForm.cardInfoRead.ToString());
                MessageBox.Show("读取成功");
                this.readCardButton.Text = "已读卡，点击重新读取";
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (cardReadForm != null)
                cardReadForm.Dispose();
            this.Close();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Connector.RightsConnector connector = new Connector.RightsConnector(Instruction.rightsAddUser, mainTransmission, rightsControlConnectors);
            connector.EOperatedState += EOperateState;
            User user = new User();
            user.name = nameTextBox.Text;
            if (usernameTextBox.Text != String.Empty)
                user.username = usernameTextBox.Text;
            if (passwordTextBox.Text != String.Empty)
                user.password = Function.getMd5Hash(passwordTextBox.Text);
            user.passwordLogin = passwordLoginCheckBox.Checked;
            user.passwordAuthorize = passwordAuthorizeCheckBox.Checked;
            if (cardInformation != String.Empty)
                user.cardInformation = cardInformation;
            user.cardLogin = cardLoginCheckBox.Checked;
            user.cardAuthorize = cardAuthorizeCheckBox.Checked;
            connector.addNewUser(user);
        }
    }
}
