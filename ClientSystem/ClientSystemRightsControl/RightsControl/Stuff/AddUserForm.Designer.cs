﻿namespace ClientSystem.RightsControl.Stuff
{
    partial class AddUserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelUsername = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelCard = new System.Windows.Forms.Label();
            this.passwordLoginCheckBox = new System.Windows.Forms.CheckBox();
            this.passwordAuthorizeCheckBox = new System.Windows.Forms.CheckBox();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.cardLoginCheckBox = new System.Windows.Forms.CheckBox();
            this.cardAuthorizeCheckBox = new System.Windows.Forms.CheckBox();
            this.readCardButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.confirmPasswordTextBox = new System.Windows.Forms.TextBox();
            this.labelConfirmPassword = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(92, 12);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(161, 21);
            this.nameTextBox.TabIndex = 0;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(12, 15);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(41, 12);
            this.labelName.TabIndex = 11;
            this.labelName.Text = "姓名：";
            // 
            // labelUsername
            // 
            this.labelUsername.AutoSize = true;
            this.labelUsername.Location = new System.Drawing.Point(12, 64);
            this.labelUsername.Name = "labelUsername";
            this.labelUsername.Size = new System.Drawing.Size(41, 12);
            this.labelUsername.TabIndex = 12;
            this.labelUsername.Text = "账号：";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(12, 93);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(41, 12);
            this.labelPassword.TabIndex = 13;
            this.labelPassword.Text = "密码：";
            // 
            // labelCard
            // 
            this.labelCard.AutoSize = true;
            this.labelCard.Location = new System.Drawing.Point(12, 169);
            this.labelCard.Name = "labelCard";
            this.labelCard.Size = new System.Drawing.Size(29, 12);
            this.labelCard.TabIndex = 15;
            this.labelCard.Text = "卡：";
            // 
            // passwordLoginCheckBox
            // 
            this.passwordLoginCheckBox.AutoSize = true;
            this.passwordLoginCheckBox.Location = new System.Drawing.Point(14, 39);
            this.passwordLoginCheckBox.Name = "passwordLoginCheckBox";
            this.passwordLoginCheckBox.Size = new System.Drawing.Size(72, 16);
            this.passwordLoginCheckBox.TabIndex = 1;
            this.passwordLoginCheckBox.Text = "密码登录";
            this.passwordLoginCheckBox.UseVisualStyleBackColor = true;
            // 
            // passwordAuthorizeCheckBox
            // 
            this.passwordAuthorizeCheckBox.AutoSize = true;
            this.passwordAuthorizeCheckBox.Location = new System.Drawing.Point(129, 39);
            this.passwordAuthorizeCheckBox.Name = "passwordAuthorizeCheckBox";
            this.passwordAuthorizeCheckBox.Size = new System.Drawing.Size(72, 16);
            this.passwordAuthorizeCheckBox.TabIndex = 2;
            this.passwordAuthorizeCheckBox.Text = "密码授权";
            this.passwordAuthorizeCheckBox.UseVisualStyleBackColor = true;
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Location = new System.Drawing.Point(92, 61);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(161, 21);
            this.usernameTextBox.TabIndex = 3;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(92, 90);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '*';
            this.passwordTextBox.Size = new System.Drawing.Size(161, 21);
            this.passwordTextBox.TabIndex = 4;
            // 
            // cardLoginCheckBox
            // 
            this.cardLoginCheckBox.AutoSize = true;
            this.cardLoginCheckBox.Location = new System.Drawing.Point(14, 144);
            this.cardLoginCheckBox.Name = "cardLoginCheckBox";
            this.cardLoginCheckBox.Size = new System.Drawing.Size(72, 16);
            this.cardLoginCheckBox.TabIndex = 6;
            this.cardLoginCheckBox.Text = "密码登录";
            this.cardLoginCheckBox.UseVisualStyleBackColor = true;
            // 
            // cardAuthorizeCheckBox
            // 
            this.cardAuthorizeCheckBox.AutoSize = true;
            this.cardAuthorizeCheckBox.Location = new System.Drawing.Point(129, 144);
            this.cardAuthorizeCheckBox.Name = "cardAuthorizeCheckBox";
            this.cardAuthorizeCheckBox.Size = new System.Drawing.Size(72, 16);
            this.cardAuthorizeCheckBox.TabIndex = 7;
            this.cardAuthorizeCheckBox.Text = "密码授权";
            this.cardAuthorizeCheckBox.UseVisualStyleBackColor = true;
            // 
            // readCardButton
            // 
            this.readCardButton.Location = new System.Drawing.Point(92, 164);
            this.readCardButton.Name = "readCardButton";
            this.readCardButton.Size = new System.Drawing.Size(161, 23);
            this.readCardButton.TabIndex = 8;
            this.readCardButton.Text = "读卡按钮";
            this.readCardButton.UseVisualStyleBackColor = true;
            this.readCardButton.Click += new System.EventHandler(this.readCardButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(36, 193);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 9;
            this.addButton.Text = "确定";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(157, 193);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "取消";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // confirmPasswordTextBox
            // 
            this.confirmPasswordTextBox.Location = new System.Drawing.Point(92, 117);
            this.confirmPasswordTextBox.Name = "confirmPasswordTextBox";
            this.confirmPasswordTextBox.PasswordChar = '*';
            this.confirmPasswordTextBox.Size = new System.Drawing.Size(161, 21);
            this.confirmPasswordTextBox.TabIndex = 5;
            // 
            // labelConfirmPassword
            // 
            this.labelConfirmPassword.AutoSize = true;
            this.labelConfirmPassword.Location = new System.Drawing.Point(12, 120);
            this.labelConfirmPassword.Name = "labelConfirmPassword";
            this.labelConfirmPassword.Size = new System.Drawing.Size(65, 12);
            this.labelConfirmPassword.TabIndex = 14;
            this.labelConfirmPassword.Text = "重复密码：";
            // 
            // AddUserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 229);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.readCardButton);
            this.Controls.Add(this.cardAuthorizeCheckBox);
            this.Controls.Add(this.cardLoginCheckBox);
            this.Controls.Add(this.passwordAuthorizeCheckBox);
            this.Controls.Add(this.passwordLoginCheckBox);
            this.Controls.Add(this.labelCard);
            this.Controls.Add(this.labelConfirmPassword);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelUsername);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.confirmPasswordTextBox);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.usernameTextBox);
            this.Controls.Add(this.nameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AddUserForm";
            this.Text = "新建用户";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelCard;
        private System.Windows.Forms.CheckBox passwordLoginCheckBox;
        private System.Windows.Forms.CheckBox passwordAuthorizeCheckBox;
        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.CheckBox cardLoginCheckBox;
        private System.Windows.Forms.CheckBox cardAuthorizeCheckBox;
        private System.Windows.Forms.Button readCardButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox confirmPasswordTextBox;
        private System.Windows.Forms.Label labelConfirmPassword;
    }
}