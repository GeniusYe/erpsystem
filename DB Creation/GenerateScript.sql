USE [ERPSystem]
GO
/****** Object:  UserDefinedFunction [dbo].[isInDate]    Script Date: 11/18/2019 10:13:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[isInDate]
(
 @d1 dateTime, --日期1(min)
 @d2 dateTime,--日期2(max)
 @y int, --年份
 @m int --月份
)

returns bit

as 
begin

 declare @retV bit
 declare @t1 int
 declare @t2 int
 declare @t3 int


 set @t1 = year(@d1) * 12 + month(@d1)
 set @t2 = year(@d2) * 12 + month(@d2)
 set @t3 = @y * 12 + @m

 if( @t3 >= @t1 AND @t3 <= @t2 )
  set @retV = 1
 else
  set @retV = 0

 return @retV

END
GO
/****** Object:  Table [dbo].[Customers.Information]    Script Date: 11/18/2019 10:13:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers.Information](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[frequency] [int] NOT NULL,
	[vip_level] [int] NULL,
	[state] [int] NOT NULL,
	[inSource] [bit] NULL,
	[outCustomer] [bit] NULL,
	[giveAwayAmount] [int] NULL,
	[giveAwayAlready] [int] NULL,
	[creator] [int] NULL,
	[cookie] [int] NOT NULL,
	[contact] [varchar](20) NULL,
	[phone] [varchar](40) NULL,
	[address] [varchar](100) NULL,
	[distance] [int] NULL,
	[machine_model] [varchar](50) NULL,
 CONSTRAINT [PK_Customers.Information] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers.Rights]    Script Date: 11/18/2019 10:13:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers.Rights](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[userID] [int] NOT NULL,
	[rLogIn] [tinyint] NULL,
	[rInSourceList] [tinyint] NULL,
	[rOutCustomerList] [tinyint] NULL,
	[rCustomerList] [tinyint] NULL,
	[rSpecialOneCustomer] [tinyint] NULL,
	[rAdd] [tinyint] NULL,
	[rEdit] [tinyint] NULL,
 CONSTRAINT [PK_Customers.Rights] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Draft.Amount]    Script Date: 11/18/2019 10:13:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Draft.Amount](
	[IDDraft] [int] NOT NULL,
	[num] [int] NULL,
	[value] [int] NULL,
	[year] [int] NOT NULL,
	[month] [int] NOT NULL,
 CONSTRAINT [PK_Draft.Amount] PRIMARY KEY CLUSTERED 
(
	[year] ASC,
	[month] ASC,
	[IDDraft] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Draft.Container]    Script Date: 11/18/2019 10:13:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Draft.Container](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[BelongsTo] [int] NULL,
	[Depth] [int] NULL,
 CONSTRAINT [PK_Draft.Container] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Draft.Draft]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Draft.Draft](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[specification] [varchar](100) NULL,
	[hiddenSpecification] [varchar](50) NULL,
	[belongsTo] [int] NOT NULL,
	[state] [int] NOT NULL,
	[location] [varchar](20) NULL,
	[usingTarget] [varchar](50) NULL,
	[numAll] [int] NOT NULL,
	[numAccessiable] [int] NOT NULL,
	[numInHouse] [int] NOT NULL,
	[numWarning] [int] NOT NULL,
	[numOriginal] [int] NOT NULL,
	[priceOutH] [int] NOT NULL,
	[priceOutL] [int] NOT NULL,
	[cookie] [int] NOT NULL,
	[Del] [bit] NOT NULL,
	[value] [int] NOT NULL,
	[is_servicefee] [bit] NOT NULL,
	[unit] [varchar](20) NULL,
 CONSTRAINT [PK_Draft.Draft] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Draft.OperationRecord]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Draft.OperationRecord](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[draftId] [int] NOT NULL,
	[operationOperator] [int] NOT NULL,
	[operation] [int] NOT NULL,
	[operatedTime] [datetime2](7) NOT NULL,
	[isAuthorize] [bit] NOT NULL,
 CONSTRAINT [PK_Draft.OperationRecord] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Draft.Rights]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Draft.Rights](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[userID] [int] NOT NULL,
	[rLogIn] [tinyint] NULL,
	[rGetSpecialOne] [tinyint] NULL,
	[rAdd] [tinyint] NULL,
	[rEdit] [tinyint] NULL,
	[rSubmit] [tinyint] NULL,
	[rDisplayPrice] [tinyint] NULL,
	[rDisplayNum] [tinyint] NULL,
	[rViewAll] [tinyint] NULL,
	[rDel] [tinyint] NULL,
 CONSTRAINT [PK_Draft.Rights] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Draft.SpecialPrice]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Draft.SpecialPrice](
	[target] [int] NOT NULL,
	[IDDraft] [int] NOT NULL,
	[priceOutH] [int] NULL,
	[priceOutL] [int] NULL,
 CONSTRAINT [PK_Draft.SpecialPrice] PRIMARY KEY NONCLUSTERED 
(
	[target] ASC,
	[IDDraft] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ERPSystemNew.Rights]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ERPSystemNew.Rights](
	[user_id] [int] NOT NULL,
	[rCustomerOperator] [tinyint] NULL,
	[rAttendantOperator] [tinyint] NULL,
	[rBonusOperator] [tinyint] NULL,
	[rBonusAdmin] [tinyint] NULL,
	[rTicketOperator] [tinyint] NULL,
	[rTicketAdmin] [tinyint] NULL,
	[rTicketFaultOperator] [tinyint] NULL,
	[rTicketFaultAdmin] [tinyint] NULL,
	[rDraftOperator] [tinyint] NULL,
 CONSTRAINT [PK_ERPSystemNew.Rights] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Identity.Identity]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Identity.Identity](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](30) NOT NULL,
	[cardInformation] [nchar](32) NULL,
	[userName] [varchar](16) NULL,
	[password] [nchar](32) NULL,
	[cardAuthorize] [bit] NULL,
	[passwordAuthorize] [bit] NULL,
	[cardLogin] [bit] NULL,
	[passwordLogin] [bit] NULL,
 CONSTRAINT [PK_Identity.Identity] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RightsControl.Rights]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RightsControl.Rights](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[rAdd] [tinyint] NULL,
 CONSTRAINT [PK_RightsControl.Rights] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[System.AuthorizeLog]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[System.AuthorizeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[operator] [int] NOT NULL,
	[originalOperator] [int] NOT NULL,
	[systemID] [int] NOT NULL,
	[instructionID] [int] NOT NULL,
	[errorID] [int] NOT NULL,
	[errorWarrantID] [int] NULL,
	[para] [int] NULL,
	[date] [datetime] NULL,
 CONSTRAINT [PK_System.AuthorizeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[System.LoginLog]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[System.LoginLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[time] [datetime] NOT NULL,
	[type] [int] NOT NULL,
 CONSTRAINT [PK_System.LoginLog] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ticket.Fault]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ticket.Fault](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[working_hour] [int] NULL,
 CONSTRAINT [PK_Ticket.Fault] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ticket.Ticket]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ticket.Ticket](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[state] [int] NOT NULL,
	[customer_id] [int] NOT NULL,
	[attendant_id] [int] NOT NULL,
	[operator] [int] NOT NULL,
	[contact] [varchar](20) NOT NULL,
	[phone] [varchar](40) NOT NULL,
	[address] [varchar](100) NOT NULL,
	[truck] [int] NOT NULL,
	[distance] [int] NOT NULL,
	[comment] [varchar](50) NOT NULL,
	[working_hour] [int] NOT NULL,
	[amount_servicefee_working] [int] NULL,
	[amount_servicefee_truck] [int] NULL,
	[submit_date] [datetime2](7) NOT NULL,
	[finish_date] [datetime2](7) NULL,
	[solved_fault] [varchar](255) NULL,
	[remaining_fault] [varchar](255) NULL,
	[machine_model] [varchar](50) NULL,
	[machine_worked_hour] [int] NULL,
 CONSTRAINT [PK_Ticket.Ticket] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.Amount]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.Amount](
	[year] [int] NOT NULL,
	[month] [int] NOT NULL,
	[amountIn] [int] NULL,
	[amountAdjustIn] [int] NULL,
	[amountSell] [int] NULL,
	[amountCost] [int] NULL,
	[amountService] [int] NULL,
	[amountCostService] [int] NULL,
	[amountGift] [int] NULL,
	[amountCostGift] [int] NULL,
	[amountAdjustOut] [int] NULL,
 CONSTRAINT [PK_Warrant.Amount] PRIMARY KEY NONCLUSTERED 
(
	[year] ASC,
	[month] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_Warrant.Amount] UNIQUE CLUSTERED 
(
	[year] ASC,
	[month] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.Attendants]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.Attendants](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](20) NULL,
 CONSTRAINT [PK_Warrant.Attendants] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.Bonus]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.Bonus](
	[warrant_id] [int] NOT NULL,
	[warrant_type] [int] NOT NULL,
	[amount_draft] [int] NOT NULL,
	[amount_servicefee] [int] NOT NULL,
	[repay_delay] [int] NOT NULL,
	[attendant_num] [int] NOT NULL,
	[customer_id] [int] NULL,
	[year] [int] NOT NULL,
	[month] [int] NOT NULL,
	[bonus_draft] [int] NULL,
	[bonus_servicefee] [int] NULL,
	[calculation_type] [int] NULL,
 CONSTRAINT [PK_Warrant.Bonus] PRIMARY KEY CLUSTERED 
(
	[warrant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.Bonus.Addition]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.Bonus.Addition](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[year] [int] NOT NULL,
	[month] [int] NOT NULL,
	[attendant_id] [int] NOT NULL,
	[bonus] [int] NOT NULL,
	[reason] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Warrant.Bonus.Addition] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.BonusRatio]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.BonusRatio](
	[year] [int] NOT NULL,
	[month] [int] NOT NULL,
	[type] [int] NOT NULL,
	[ratio_draft_imme] [int] NULL,
	[ratio_draft_normal] [int] NOT NULL,
	[ratio_draft_penalty] [int] NULL,
	[ratio_servicefee_imme] [int] NULL,
	[ratio_servicefee_normal] [int] NOT NULL,
	[ratio_servicefee_penalty] [int] NULL,
 CONSTRAINT [PK_Warrant.BonusRatio] PRIMARY KEY CLUSTERED 
(
	[year] ASC,
	[month] ASC,
	[type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.In.Item]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.In.Item](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDDraft] [int] NOT NULL,
	[IDWarrant] [int] NOT NULL,
	[price] [int] NOT NULL,
	[num] [int] NOT NULL,
	[remark] [nchar](40) NULL,
	[currentNumAll] [int] NULL,
	[currentValue] [int] NULL,
	[priceWithTax] [int] NOT NULL,
 CONSTRAINT [PK_Warrant.In.Item] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.In.OperationRecord]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.In.OperationRecord](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[warrantId] [int] NULL,
	[operationOperator] [int] NULL,
	[operation] [tinyint] NULL,
	[operatedTime] [datetime2](7) NULL,
	[isAuthorize] [bit] NULL,
	[parameter1] [int] NULL,
	[parameter2] [varchar](100) NULL,
 CONSTRAINT [PK_Warrant.In.OperationRecord] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.In.Warrant]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.In.Warrant](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[state] [int] NOT NULL,
	[type] [int] NOT NULL,
	[target] [int] NULL,
	[cookie] [int] NOT NULL,
	[comment] [varchar](80) NULL,
	[amountIn] [int] NULL,
	[submitDate] [datetime2](7) NULL,
	[number] [nchar](13) NOT NULL,
	[amountWithTaxIn] [int] NULL,
 CONSTRAINT [PK_Warrant.In.Warrant] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [iw_unique_number] UNIQUE NONCLUSTERED 
(
	[number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.Out.Attendant]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.Out.Attendant](
	[warrant_id] [int] NOT NULL,
	[attendant_id] [int] NOT NULL,
 CONSTRAINT [PK_Warrant.Out.Attendant] PRIMARY KEY CLUSTERED 
(
	[warrant_id] ASC,
	[attendant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.Out.Item]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.Out.Item](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDDraft] [int] NOT NULL,
	[IDWarrant] [int] NOT NULL,
	[price] [int] NOT NULL,
	[num] [int] NOT NULL,
	[numReturn] [int] NOT NULL,
	[remark] [nchar](40) NULL,
	[priceCost] [int] NULL,
	[priceAdjustMannual] [int] NULL,
	[currentNumAll] [int] NULL,
	[currentValue] [int] NULL,
 CONSTRAINT [PK_Warrant.Out.Item] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.Out.OperationRecord]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.Out.OperationRecord](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[warrantId] [int] NULL,
	[operationOperator] [int] NULL,
	[operation] [tinyint] NULL,
	[operatedTime] [datetime2](7) NULL,
	[isAuthorize] [bit] NULL,
	[parameter1] [int] NULL,
	[parameter2] [varchar](100) NULL,
 CONSTRAINT [PK_Warrant.Out.OperationRecord] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.Out.Warrant]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.Out.Warrant](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[state] [int] NOT NULL,
	[type] [int] NOT NULL,
	[payState] [int] NOT NULL,
	[target] [int] NULL,
	[cookie] [int] NOT NULL,
	[comment] [varchar](80) NULL,
	[amountTotal] [int] NULL,
	[amountServicefee] [int] NULL,
	[amountPermit] [int] NULL,
	[amountCost] [int] NULL,
	[amountPaid] [int] NULL,
	[submitDate] [datetime2](7) NULL,
	[lastPaidDate] [datetime2](7) NULL,
	[submitCustomerPageDate] [datetime2](7) NULL,
	[number] [nchar](13) NOT NULL,
 CONSTRAINT [PK_Warrant.Out.Warrant] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [ow_unique_number] UNIQUE NONCLUSTERED 
(
	[number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrant.Rights]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrant.Rights](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[userID] [int] NOT NULL,
	[rViewDetail] [tinyint] NULL,
	[rViewOneCycle] [tinyint] NULL,
	[rInLogIn] [tinyint] NULL,
	[rInSpecialOne] [tinyint] NULL,
	[rInAdd] [tinyint] NULL,
	[rInEdit] [tinyint] NULL,
	[rInSubmit] [tinyint] NULL,
	[rInCancel] [tinyint] NULL,
	[rInPrint] [tinyint] NULL,
	[rInMoney] [tinyint] NULL,
	[rInComplete] [tinyint] NULL,
	[rInCustomers] [tinyint] NULL,
	[rOutLogIn] [tinyint] NULL,
	[rOutSpecialOne] [tinyint] NULL,
	[rOutAdd] [tinyint] NULL,
	[rOutEdit] [tinyint] NULL,
	[rOutCancel] [tinyint] NULL,
	[rOutSubmitCustomer] [tinyint] NULL,
	[rOutPrintCustomer] [tinyint] NULL,
	[rOutForcePass] [tinyint] NULL,
	[rOutTakeOut] [tinyint] NULL,
	[rOutReturn] [tinyint] NULL,
	[rOutSubmit] [tinyint] NULL,
	[rOutPrint] [tinyint] NULL,
	[rOutMoney] [tinyint] NULL,
	[rOutComplete] [tinyint] NULL,
	[rOutCustomers] [tinyint] NULL,
	[rOutDiscount] [tinyint] NULL,
	[rOutReturnPayCheck] [tinyint] NULL,
	[rAttendantsList] [tinyint] NULL,
 CONSTRAINT [PK_Warrant.Rights] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Customers.Rights]  WITH CHECK ADD  CONSTRAINT [FK_Customers.Rights_Identity.Identity] FOREIGN KEY([userID])
REFERENCES [dbo].[Identity.Identity] ([ID])
GO
ALTER TABLE [dbo].[Customers.Rights] CHECK CONSTRAINT [FK_Customers.Rights_Identity.Identity]
GO
ALTER TABLE [dbo].[Draft.Amount]  WITH CHECK ADD  CONSTRAINT [FK_Draft.Amount_Draft.Draft] FOREIGN KEY([IDDraft])
REFERENCES [dbo].[Draft.Draft] ([ID])
GO
ALTER TABLE [dbo].[Draft.Amount] CHECK CONSTRAINT [FK_Draft.Amount_Draft.Draft]
GO
ALTER TABLE [dbo].[Draft.Container]  WITH CHECK ADD  CONSTRAINT [FK_Draft.Container_Draft.Container] FOREIGN KEY([BelongsTo])
REFERENCES [dbo].[Draft.Container] ([ID])
GO
ALTER TABLE [dbo].[Draft.Container] CHECK CONSTRAINT [FK_Draft.Container_Draft.Container]
GO
ALTER TABLE [dbo].[Draft.Draft]  WITH CHECK ADD  CONSTRAINT [FK_Draft.Draft_Draft.Container] FOREIGN KEY([belongsTo])
REFERENCES [dbo].[Draft.Container] ([ID])
GO
ALTER TABLE [dbo].[Draft.Draft] CHECK CONSTRAINT [FK_Draft.Draft_Draft.Container]
GO
ALTER TABLE [dbo].[Draft.OperationRecord]  WITH CHECK ADD  CONSTRAINT [FK_Draft.OperationRecord_Draft.Draft] FOREIGN KEY([draftId])
REFERENCES [dbo].[Draft.Draft] ([ID])
GO
ALTER TABLE [dbo].[Draft.OperationRecord] CHECK CONSTRAINT [FK_Draft.OperationRecord_Draft.Draft]
GO
ALTER TABLE [dbo].[Draft.OperationRecord]  WITH CHECK ADD  CONSTRAINT [FK_Draft.OperationRecord_Identity.Identity] FOREIGN KEY([operationOperator])
REFERENCES [dbo].[Identity.Identity] ([ID])
GO
ALTER TABLE [dbo].[Draft.OperationRecord] CHECK CONSTRAINT [FK_Draft.OperationRecord_Identity.Identity]
GO
ALTER TABLE [dbo].[Draft.Rights]  WITH CHECK ADD  CONSTRAINT [FK_Draft.Rights_Identity.Identity] FOREIGN KEY([userID])
REFERENCES [dbo].[Identity.Identity] ([ID])
GO
ALTER TABLE [dbo].[Draft.Rights] CHECK CONSTRAINT [FK_Draft.Rights_Identity.Identity]
GO
ALTER TABLE [dbo].[Draft.SpecialPrice]  WITH CHECK ADD  CONSTRAINT [FK_Draft.SpecialPrice_Customers.Information] FOREIGN KEY([target])
REFERENCES [dbo].[Customers.Information] ([ID])
GO
ALTER TABLE [dbo].[Draft.SpecialPrice] CHECK CONSTRAINT [FK_Draft.SpecialPrice_Customers.Information]
GO
ALTER TABLE [dbo].[Draft.SpecialPrice]  WITH CHECK ADD  CONSTRAINT [FK_Draft.SpecialPrice_Draft.Draft] FOREIGN KEY([IDDraft])
REFERENCES [dbo].[Draft.Draft] ([ID])
GO
ALTER TABLE [dbo].[Draft.SpecialPrice] CHECK CONSTRAINT [FK_Draft.SpecialPrice_Draft.Draft]
GO
ALTER TABLE [dbo].[ERPSystemNew.Rights]  WITH CHECK ADD  CONSTRAINT [FK_ERPSystemNew.Rights_Identity.Identity] FOREIGN KEY([user_id])
REFERENCES [dbo].[Identity.Identity] ([ID])
GO
ALTER TABLE [dbo].[ERPSystemNew.Rights] CHECK CONSTRAINT [FK_ERPSystemNew.Rights_Identity.Identity]
GO
ALTER TABLE [dbo].[RightsControl.Rights]  WITH CHECK ADD  CONSTRAINT [FK_RightsControl.Rights_Identity.Identity] FOREIGN KEY([userId])
REFERENCES [dbo].[Identity.Identity] ([ID])
GO
ALTER TABLE [dbo].[RightsControl.Rights] CHECK CONSTRAINT [FK_RightsControl.Rights_Identity.Identity]
GO
ALTER TABLE [dbo].[Ticket.Ticket]  WITH CHECK ADD  CONSTRAINT [FK_Ticket.Ticket_Customers.Information] FOREIGN KEY([customer_id])
REFERENCES [dbo].[Customers.Information] ([ID])
GO
ALTER TABLE [dbo].[Ticket.Ticket] CHECK CONSTRAINT [FK_Ticket.Ticket_Customers.Information]
GO
ALTER TABLE [dbo].[Ticket.Ticket]  WITH CHECK ADD  CONSTRAINT [FK_Ticket.Ticket_Identity.Identity] FOREIGN KEY([operator])
REFERENCES [dbo].[Identity.Identity] ([ID])
GO
ALTER TABLE [dbo].[Ticket.Ticket] CHECK CONSTRAINT [FK_Ticket.Ticket_Identity.Identity]
GO
ALTER TABLE [dbo].[Ticket.Ticket]  WITH CHECK ADD  CONSTRAINT [FK_Ticket.Ticket_Warrant.Attendants] FOREIGN KEY([attendant_id])
REFERENCES [dbo].[Warrant.Attendants] ([id])
GO
ALTER TABLE [dbo].[Ticket.Ticket] CHECK CONSTRAINT [FK_Ticket.Ticket_Warrant.Attendants]
GO
ALTER TABLE [dbo].[Warrant.Bonus]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.Bonus_Customers.Information] FOREIGN KEY([customer_id])
REFERENCES [dbo].[Customers.Information] ([ID])
GO
ALTER TABLE [dbo].[Warrant.Bonus] CHECK CONSTRAINT [FK_Warrant.Bonus_Customers.Information]
GO
ALTER TABLE [dbo].[Warrant.Bonus]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.Bonus_Warrant.Out.Warrant] FOREIGN KEY([warrant_id])
REFERENCES [dbo].[Warrant.Out.Warrant] ([ID])
GO
ALTER TABLE [dbo].[Warrant.Bonus] CHECK CONSTRAINT [FK_Warrant.Bonus_Warrant.Out.Warrant]
GO
ALTER TABLE [dbo].[Warrant.Bonus.Addition]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.Bonus.Addition_Warrant.Attendants] FOREIGN KEY([attendant_id])
REFERENCES [dbo].[Warrant.Attendants] ([id])
GO
ALTER TABLE [dbo].[Warrant.Bonus.Addition] CHECK CONSTRAINT [FK_Warrant.Bonus.Addition_Warrant.Attendants]
GO
ALTER TABLE [dbo].[Warrant.In.Item]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.In.Item_Draft.Draft] FOREIGN KEY([IDDraft])
REFERENCES [dbo].[Draft.Draft] ([ID])
GO
ALTER TABLE [dbo].[Warrant.In.Item] CHECK CONSTRAINT [FK_Warrant.In.Item_Draft.Draft]
GO
ALTER TABLE [dbo].[Warrant.In.Item]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.In.Item_Warrant.In.Warrant] FOREIGN KEY([IDWarrant])
REFERENCES [dbo].[Warrant.In.Warrant] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Warrant.In.Item] CHECK CONSTRAINT [FK_Warrant.In.Item_Warrant.In.Warrant]
GO
ALTER TABLE [dbo].[Warrant.In.OperationRecord]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.In.OperationRecord_Identity.Identity] FOREIGN KEY([operationOperator])
REFERENCES [dbo].[Identity.Identity] ([ID])
GO
ALTER TABLE [dbo].[Warrant.In.OperationRecord] CHECK CONSTRAINT [FK_Warrant.In.OperationRecord_Identity.Identity]
GO
ALTER TABLE [dbo].[Warrant.In.OperationRecord]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.In.OperationRecord_Warrant.In.Warrant] FOREIGN KEY([warrantId])
REFERENCES [dbo].[Warrant.In.Warrant] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Warrant.In.OperationRecord] CHECK CONSTRAINT [FK_Warrant.In.OperationRecord_Warrant.In.Warrant]
GO
ALTER TABLE [dbo].[Warrant.In.Warrant]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.In.Warrant_Customers.Information] FOREIGN KEY([target])
REFERENCES [dbo].[Customers.Information] ([ID])
GO
ALTER TABLE [dbo].[Warrant.In.Warrant] CHECK CONSTRAINT [FK_Warrant.In.Warrant_Customers.Information]
GO
ALTER TABLE [dbo].[Warrant.Out.Attendant]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.Out.Attendant_Warrant.Attendants] FOREIGN KEY([attendant_id])
REFERENCES [dbo].[Warrant.Attendants] ([id])
GO
ALTER TABLE [dbo].[Warrant.Out.Attendant] CHECK CONSTRAINT [FK_Warrant.Out.Attendant_Warrant.Attendants]
GO
ALTER TABLE [dbo].[Warrant.Out.Attendant]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.Out.Attendant_Warrant.Out.Attendant] FOREIGN KEY([warrant_id])
REFERENCES [dbo].[Warrant.Out.Warrant] ([ID])
GO
ALTER TABLE [dbo].[Warrant.Out.Attendant] CHECK CONSTRAINT [FK_Warrant.Out.Attendant_Warrant.Out.Attendant]
GO
ALTER TABLE [dbo].[Warrant.Out.Item]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.Out.Item_Draft.Draft] FOREIGN KEY([IDDraft])
REFERENCES [dbo].[Draft.Draft] ([ID])
GO
ALTER TABLE [dbo].[Warrant.Out.Item] CHECK CONSTRAINT [FK_Warrant.Out.Item_Draft.Draft]
GO
ALTER TABLE [dbo].[Warrant.Out.Item]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.Out.Item_Warrant.Out.Warrant] FOREIGN KEY([IDWarrant])
REFERENCES [dbo].[Warrant.Out.Warrant] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Warrant.Out.Item] CHECK CONSTRAINT [FK_Warrant.Out.Item_Warrant.Out.Warrant]
GO
ALTER TABLE [dbo].[Warrant.Out.OperationRecord]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.Out.OperationRecord_Identity.Identity] FOREIGN KEY([operationOperator])
REFERENCES [dbo].[Identity.Identity] ([ID])
GO
ALTER TABLE [dbo].[Warrant.Out.OperationRecord] CHECK CONSTRAINT [FK_Warrant.Out.OperationRecord_Identity.Identity]
GO
ALTER TABLE [dbo].[Warrant.Out.OperationRecord]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.Out.OperationRecord_Warrant.Out.Warrant] FOREIGN KEY([warrantId])
REFERENCES [dbo].[Warrant.Out.Warrant] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Warrant.Out.OperationRecord] CHECK CONSTRAINT [FK_Warrant.Out.OperationRecord_Warrant.Out.Warrant]
GO
ALTER TABLE [dbo].[Warrant.Out.Warrant]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.Out.Warrant_Customers.Information] FOREIGN KEY([target])
REFERENCES [dbo].[Customers.Information] ([ID])
GO
ALTER TABLE [dbo].[Warrant.Out.Warrant] CHECK CONSTRAINT [FK_Warrant.Out.Warrant_Customers.Information]
GO
ALTER TABLE [dbo].[Warrant.Rights]  WITH CHECK ADD  CONSTRAINT [FK_Warrant.Rights_Identity.Identity] FOREIGN KEY([userID])
REFERENCES [dbo].[Identity.Identity] ([ID])
GO
ALTER TABLE [dbo].[Warrant.Rights] CHECK CONSTRAINT [FK_Warrant.Rights_Identity.Identity]
GO
/****** Object:  StoredProcedure [dbo].[CheckNum]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CheckNum]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Fittings.Fittings] SET tempInt = numOriginal

MERGE INTO dbo.[Fittings.Fittings] f
	USING (SELECT fi.ID, SUM(wit.num) as num FROM dbo.[Fittings.Fittings] fi
	JOIN dbo.[Warrant.In.Item] wit
		ON wit.IDfittings = fi.ID
	JOIN dbo.[Warrant.In.Warrant] wi
		ON wi.ID = wit.IDWarrant AND wi.state >= 9 AND YEAR(wi.dateSubmit) < 2012 AND MONTH(wi.dateSubmit) <= 9
	GROUP BY fi.ID) fi
	ON fi.ID = f.ID
	WHEN MATCHED THEN
		UPDATE SET f.tempInt = f.numOriginal + fi.num;

MERGE INTO dbo.[Fittings.Fittings] f
	USING (SELECT fi.ID, SUM(wot.num - wot.numReturn) as num FROM dbo.[Fittings.Fittings] fi
	JOIN dbo.[Warrant.Out.Item] wot
		ON wot.IDfittings = fi.ID
	JOIN dbo.[Warrant.Out.Warrant] wo
		ON wo.ID = wot.IDWarrant AND wo.state >= 9 AND YEAR(wo.dateSubmit) < 2012 AND MONTH(wo.dateSubmit) <= 9
	GROUP BY fi.ID) fo
	ON fo.ID = f.ID
	WHEN MATCHED THEN
		UPDATE SET f.tempInt = f.tempInt - fo.num;

SELECT * FROM dbo.[Fittings.Fittings] WHERE tempInt <> numOriginal201110
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Bonus_Value_Set]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Bonus_Value_Set]
	-- Add the parameters for the stored procedure here
	@warrant_id int,
	@year int,
	@month int,
	@bonus_draft int,
	@bonus_servicefee int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.Bonus]
		SET bonus_draft = @bonus_draft, bonus_servicefee = @bonus_servicefee
		WHERE warrant_id = @warrant_id AND year = @year AND month = @month
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Customer_Add]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Customer_Add]
	-- Add the parameters for the stored procedure here
	@name varchar(40),
	@ifInSource bit = 1,
	@ifOutCustomer bit = 1,
	@creator int,
	@cookieNew int,
	@vip_level int = 0,
	@contact varchar(20) = null,
	@phone varchar(40) = null,
	@address varchar(100) = null,
	@distance int = null,
	@machine_model varchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF NOT EXISTS(SELECT * FROM dbo.[Customers.Information] WHERE name = @name)
	BEGIN
		INSERT INTO dbo.[Customers.Information] (name, frequency, state, creator, cookie, inSource, outCustomer, vip_level, contact, phone, address, distance, machine_model)
			VALUES (@name, 0, 0, @creator, @cookieNew, 1, 1, @vip_level, @contact, @phone, @address, @distance, @machine_model)
		DECLARE @ID int = @@IDENTITY

		SELECT @ID
	END
	ELSE
	BEGIN
		SELECT -1
	END
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Customer_Edit]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Customer_Edit]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(50),
	@vip_level int,
	@contact varchar(20) = null,
	@phone varchar(40) = null,
	@address varchar(100) = null,
	@distance int = null,
	@machine_model varchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF NOT EXISTS(SELECT * FROM dbo.[Customers.Information] WHERE name = @name AND ID <> @id)
	BEGIN
		UPDATE dbo.[Customers.Information] SET name = @name, vip_level = @vip_level, contact = @contact, phone = @phone, address = @address, distance = @distance, machine_model = @machine_model WHERE ID = @id
	END
	ELSE
	BEGIN
		SELECT -1
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Customer_GetDefaultOnes]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Customer_GetDefaultOnes]
	-- Add the parameters for the stored procedure here
	@set int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF @set = 0
    BEGIN
		SELECT ID, RTRIM(name) AS name, state, inSource, outCustomer, vip_level, contact, phone, address, distance, machine_model FROM dbo.[Customers.Information] WHERE inSource = 1 ORDER BY frequency DESC;
	END
	ELSE IF @set = 1
	BEGIN 
		SELECT ID, RTRIM(name) AS name, state, inSource, outCustomer, vip_level, contact, phone, address, distance, machine_model FROM dbo.[Customers.Information] WHERE outCustomer = 1 ORDER BY frequency DESC;
	END 
	ELSE
	BEGIN
		SELECT ID, RTRIM(name) AS name, state, inSource, outCustomer, vip_level, contact, phone, address, distance, machine_model FROM dbo.[Customers.Information] ORDER BY frequency DESC;
	END
		
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Customer_GetInfo]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Customer_GetInfo]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ID, name, state, inSource, outCustomer, creator, frequency, giveAwayAmount, giveAwayAlready, cookie 
		FROM dbo.[Customers.Information]
		WHERE ID = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Customer_GetRights]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Customer_GetRights]
	-- Add the parameters for the stored procedure here
	@userID int,
	@instructionID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		CASE @instructionID
			WHEN 0 THEN rLogIn
			WHEN 1 THEN rInSourceList
			WHEN 2 THEN rOutCustomerList
			WHEN 3 THEN rCustomerList
			WHEN 4 THEN rSpecialOneCustomer 
			WHEN 5 THEN rAdd
			WHEN 6 THEN rEdit
			END 'r'
		FROM dbo.[Customers.Rights] WHERE userID = @userID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Customer_Read]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Customer_Read]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ID, RTRIM(name) AS name, state, inSource, outCustomer, frequency, creator, giveAwayAmount, giveAwayAlready, vip_level, cookie, contact, phone, address, distance, machine_model
		FROM dbo.[Customers.Information]
		WHERE ID = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_Add]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_Add]
	-- Add the parameters for the stored procedure here
	@cookieNew int,
	@name varchar(50) = NULL,
	@specification varchar(100) = NULL,
	@hiddenSpecification varchar(50) = NULL,
	@belongsTo int,
	@location varchar(20) = NULL,
	@usingTarget varchar(50) = NULL,
	@editor int,
	@numWarning int,
	@priceOutH int = 0,
	@priceOutL int = 0,
	@is_servicefee bit = false,
	@unit varchar(20) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[Draft.Draft] (
		name,
		specification,
		hiddenSpecification,
		belongsTo,
		state,
		location,
		usingTarget,
		numAll,
		numAccessiable,
		numOriginal,
		numWarning,
		numInHouse,
		value,
		priceOutH,
		priceOutL,
		cookie,
		Del,
		is_servicefee,
		unit
	) Values (
		@name,
		@specification,
		@hiddenSpecification,
		@belongsTo,
		0,
		@location,
		@usingTarget,
		0,
		0,
		0,
		@numWarning,
		0,
		0,
		@priceOutH,
		@priceOutL,
		@cookieNew,
		0,
		@is_servicefee,
		@unit
	)
		
	DECLARE @result int = @@IDENTITY
			
	EXEC dbo.SP_Draft_AddOperationRecord @draftId = @result, @operationOperator = @editor, @operation = 0, @isAuthorize = 0
	
	SELECT @result
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_AddOperationRecord]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_AddOperationRecord]
	-- Add the parameters for the stored procedure here
	@draftId int,
	@operationOperator int,
	@operation int,
	@isAuthorize bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[Draft.OperationRecord] (draftId, operation, operationOperator, isAuthorize, operatedTime) VALUES(@draftId, @operation, @operationOperator, @isAuthorize, GETDATE())
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_Del]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_Del]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--UPDATE dbo.[Draft.Draft] SET Del = 1, cookie = @cookieNew
	--	WHERE ID = @id And cookie = @cookieOri AND numAccessiable = 0 AND numAll = 0
		
	SELECT @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_Edit]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_Edit]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@location varchar(20) = NULL,
	@usingTarget varchar(50) = NULL,
	@editor int,
	@name varchar(50) = NULL,
	@specification varchar(100) = NULL,
	@hiddenSpecification varchar(50) = NULL,
	@numWarning int,
	@priceOutH int = 0,
	@priceOutL int = 0,
	@is_servicefee bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Draft.Draft]
		SET location = @location,
			usingTarget = @usingTarget,
			numWarning = @numWarning,
			cookie = @cookieNew,
			is_servicefee = @is_servicefee,
			priceOutH = (CASE @priceOutH WHEN 0 THEN priceOutH ELSE @priceOutH END),
			priceOutL = (CASE @priceOutL WHEN 0 THEN priceOutL ELSE @priceOutL END),
			specification = ISNULL(@specification, specification),
			hiddenSpecification = ISNULL(@hiddenSpecification, hiddenSpecification),
			name = ISNULL(@name, name)
		WHERE ID = @id AND 
			  cookie = @cookieOri
		
	DECLARE @result int = @@ROWCOUNT
			
	EXEC dbo.SP_Draft_AddOperationRecord @draftId = @id, @operationOperator = @editor, @operation = 1, @isAuthorize = 0
	
	SELECT @result
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_GetDefaultOnes]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_GetDefaultOnes]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		ID,
		name,
		specification,
		RTRIM(hiddenSpecification) + '	' + RTRIM(usingTarget) AS hiddenSpecification,
		priceOutH,
		priceOutL,
		state,
		belongsTo,
		cookie,
		numAll,
		numAccessiable
	FROM dbo.[Draft.Draft]
	WHERE belongsTo = @id AND Del = 0
	ORDER BY ID DESC
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_GetDefaultOnesBySearching]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_GetDefaultOnesBySearching]
	-- Add the parameters for the stored procedure here
	@searchingString nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		ID,
		name,
		specification,
		RTRIM(hiddenSpecification) + '	' + RTRIM(usingTarget) AS hiddenSpecification,
		priceOutH,
		priceOutL,
		state,
		belongsTo,
		cookie,
		numAll,
		numAccessiable
	FROM dbo.[Draft.Draft] 
	WHERE 
		(Name like @searchingString OR specification like @searchingString) 
		AND Del = 0 
	ORDER BY ID DESC
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_GetInfo]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_GetInfo]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT d.ID, d.state, dor.operationOperator AS editor, d.cookie
		FROM dbo.[Draft.Draft] d
		LEFT JOIN dbo.[Draft.OperationRecord] dor
			ON dor.operation = 0 AND dor.draftId = d.ID
		WHERE d.ID = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_GetRights]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_GetRights]
	-- Add the parameters for the stored procedure here
	@userID int,
	@instructionID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		CASE @instructionID
			WHEN 0 THEN	rLogIn
			WHEN 1 THEN rGetSpecialOne
			WHEN 2 THEN rAdd
			WHEN 3 THEN rEdit
			WHEN 4 THEN rSubmit
			WHEN 5 THEN rDisplayPrice
			WHEN 6 THEN rDisplayNum
			WHEN 7 THEN rViewAll
			WHEN 8 THEN rDel
		END 'R'
		FROM dbo.[Draft.Rights]
		WHERE userID = @userID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_GetSpecialPrice]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_GetSpecialPrice]
	-- Add the parameters for the stored procedure here
	@target int,
	@IDDraft int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT priceOutH, priceOutL FROM dbo.[Draft.SpecialPrice] WHERE target = @target AND IDDraft = @IDDraft
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_Query_Total_Sale]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[SP_Draft_Query_Total_Sale]
	-- Add the parameters for the stored procedure here
	@startDate DateTime,
	@stopDate DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		d.ID,
		d.Name as name,
		d.specification,
		d.hiddenSpecification,
		d.unit,
		d.priceOutH,
		d.priceOutL,
		d.value,
		d.numAll,
		d.numAccessiable,
		sell_info.num,
		sell_info.num_return,
		sell_info.ori_price,
		sell_info.sold_price,
		sell_info.cost_price
	FROM (SELECT
			woi.IDDraft AS id,
			SUM(woi.num) AS num,
			SUM(woi.numReturn) AS num_return,
			SUM(woi.price * (woi.num - woi.numReturn)) AS ori_price,
			SUM(woi.priceCost) AS cost_price,
			SUM(CASE WHEN woi.priceAdjustMannual IS NOT NULL THEN woi.priceAdjustMannual ELSE woi.price END * (woi.num - woi.numReturn)) AS sold_price
		FROM dbo.[Warrant.Out.Item] AS woi
			LEFT JOIN dbo.[Warrant.Out.Warrant] AS wow
				ON woi.IDWarrant = wow.ID 
		WHERE
			wow.state >= 9 AND 
			wow.type IN (0, 1, 2, 4) AND
			wow.submitDate > @startDate AND 
			wow.submitDate < @stopDate
		GROUP BY woi.IDDraft) AS sell_info
	LEFT JOIN dbo.[Draft.Draft] AS d 
		ON sell_info.id = d.ID

END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_Read]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_Read]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
			d.ID,
			d.name,
			d.specification,
			d.state,
			d.cookie,
			d.location,
			d.usingTarget,
			d.numAll,
			d.numAccessiable,
			d.numInHouse,
			d.numWarning, 
			d.value,
			d.priceOutH,
			d.priceOutL, 
			d.hiddenSpecification,
			d.belongsTo,
			c1.Name + '->' + c2.Name + '->' + c3.Name AS belongsToString,
			d.is_servicefee,
			d.unit
		FROM dbo.[Draft.Draft] AS d 
		LEFT JOIN dbo.[Draft.Container] AS c3
			ON d.belongsTo = c3.ID
		LEFT JOIN dbo.[Draft.Container] AS c2
			ON c3.BelongsTo = c2.ID
		LEFT JOIN dbo.[Draft.Container] AS c1
			ON c2.BelongsTo = c1.ID
		WHERE d.ID = @id;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_ReadContainer]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_ReadContainer]
	-- Add the parameters for the stored procedure here
	@depth int = 0,
	@id int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF @depth = 0
    BEGIN
		SELECT ID, Name FROM dbo.[Draft.Container] WHERE belongsTo IS NULL
	END
	ELSE
	BEGIN
		SELECT ID, Name FROM dbo.[Draft.Container] WHERE belongsTo = @id AND depth = @depth
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_ReadOperationRecord]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_ReadOperationRecord]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT dor.ID, dor.draftId, dor.operationOperator, dor.operation, dor.operatedTime, RTRIM(u.name) AS operationOperatorName
		FROM dbo.[Draft.OperationRecord] dor
		LEFT JOIN dbo.[Identity.Identity] u
			ON u.ID = dor.operationOperator
		WHERE dor.draftId = @id
		ORDER BY dor.operatedTime 
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_SaveDataForMonthCheck]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Draft_SaveDataForMonthCheck]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @dateNow datetime = GETDATE()
	DECLARE @dateLast datetime = (SELECT TOP 1 time FROM dbo.[System.LoginLog] WHERE type = 2 ORDER BY time DESC)

	-- From March 2019, we switch from 28th business month to calendar month
	-- which means March 1st - March 31st is a month
	--             April 1st - April 30th is a month
	
	-- Get the latest month that should be calculated
	SET @dateNow = DATEADD(MM, DATEDIFF(MM, 0, @dateNow) - 1, 0)
	-- Get the latest month that has been calculated
	DECLARE @dateCalMonth datetime = DATEADD(MM, DATEDIFF(MM, 0, @dateLast) - 1, 0)

	-- The following algorithm is stopped by March (no included), 2019
	-- 2013-03-05 -> 2013-02-01
	-- 2013-03-31 -> 2013-03-01
	-- Get the latest month that should be calculated
	-- SET @dateNow = DATEADD(MM, DATEDIFF(MM, 0, DATEADD(DD, -28, @dateNow)), 0)
	-- Get the latest month that has been calculated
	-- DECLARE @dateCalMonth datetime = DATEADD(MM, DATEDIFF(MM, 0, DATEADD(DD, -28, @dateLast)), 0)

	DECLARE @ifAddLog bit = 0

	WHILE @dateCalMonth < @dateNow
	BEGIN
		SET @dateCalMonth = DATEADD(MM, 1, @dateCalMonth)
		
		DECLARE @year int = DATEPART(YEAR, @dateCalMonth)
		DECLARE @month int = DATEPART(MONTH, @dateCalMonth)

		INSERT INTO [Draft.Amount] (IDDraft, num, value, year, month) SELECT ID, numAll, value, @year, @month FROM [Draft.Draft]
		
		SET @ifAddLog = 1
	END
	
	IF @ifAddLog = 1
	BEGIN
		EXEC dbo.[SP_System_AddLoginLog] @type = 2
	END

END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_Submit]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Draft_Submit]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Draft.Draft] SET state = 1, cookie = @cookieNew
		WHERE ID = @id And cookie = @cookieOri AND state = 0
		
	DECLARE @result int = @@ROWCOUNT
			
	EXEC dbo.SP_Draft_AddOperationRecord @draftId = @id, @operationOperator = @operator, @operation = 8, @isAuthorize = 0
	
	SELECT @result
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_ViewAll]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Draft_ViewAll]
	-- Add the parameters for the stored procedure here
	@searchDate datetime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

    IF @searchDate IS NULL
    BEGIN
		SELECT
			d.ID,
			d.name,
			d.specification,
			d.state,
			d.location,
			d.numAll,
			d.value,
			d.hiddenSpecification,
			d.unit,
			d.priceOutH,
			d.priceOutL
		FROM dbo.[Draft.Draft] d
		LEFT JOIN dbo.[Draft.Container] dc3 ON d.belongsTo = dc3.ID AND dc3.Depth = 2
		LEFT JOIN dbo.[Draft.Container] dc2 ON dc3.BelongsTo = dc2.ID AND dc2.Depth = 1
		WHERE d.Del = 0 
		ORDER BY dc2.belongsTo, dc3.BelongsTo, d.belongsTo, d.ID
	END
	ELSE
	BEGIN
		DECLARE @year nchar(4) = CAST(DATEPART(YY, @searchDate) AS INT)
		DECLARE @month nchar(2) = CAST(DATEPART(MM, @searchDate) AS INT)

		SELECT
			d.ID,
			d.name,
			d.specification,
			d.state,
			d.location,
			da.num AS numAll,
			da.value AS value,
			d.hiddenSpecification,
			d.unit,
			d.priceOutH,
			d.priceOutL
		FROM dbo.[Draft.Amount] da
		LEFT JOIN dbo.[Draft.Draft] d ON d.ID = da.IDDraft
		LEFT JOIN dbo.[Draft.Container] dc3 ON d.belongsTo = dc3.ID AND dc3.Depth = 2
		LEFT JOIN dbo.[Draft.Container] dc2 ON dc3.BelongsTo = dc2.ID AND dc2.Depth = 1
		WHERE d.Del = 0 AND da.year = @year AND da.month = @month
		ORDER BY dc2.belongsTo, dc3.BelongsTo, d.belongsTo, d.ID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_ViewAll1]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_Draft_ViewAll1]
	-- Add the parameters for the stored procedure here
	@id int,
	@searchDate datetime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

    IF @searchDate IS NULL
    BEGIN
		SELECT
			d.ID,
			d.name,
			d.specification,
			d.state,
			d.location,
			d.numAll,
			d.value,
			d.hiddenSpecification,
			d.unit,
			d.priceOutH,
			d.priceOutL
		FROM dbo.[Draft.Draft] d
		LEFT JOIN dbo.[Draft.Container] dc3 ON d.belongsTo = dc3.ID AND dc3.Depth = 2
		LEFT JOIN dbo.[Draft.Container] dc2 ON dc3.BelongsTo = dc2.ID AND dc2.Depth = 1
		WHERE d.Del = 0 AND dc2.BelongsTo = @id
		ORDER BY dc3.BelongsTo, d.belongsTo, d.ID
	END
	ELSE
	BEGIN
		DECLARE @year nchar(4) = CAST(DATEPART(YY, @searchDate) AS INT)
		DECLARE @month nchar(2) = CAST(DATEPART(MM, @searchDate) AS INT)

		SELECT
			d.ID,
			d.name,
			d.specification,
			d.state,
			d.location,
			da.num AS numAll,
			da.value AS value,
			d.hiddenSpecification,
			d.unit,
			d.priceOutH,
			d.priceOutL
		FROM dbo.[Draft.Amount] da
		LEFT JOIN dbo.[Draft.Draft] d ON d.ID = da.IDDraft
		LEFT JOIN dbo.[Draft.Container] dc3 ON d.belongsTo = dc3.ID AND dc3.Depth = 2
		LEFT JOIN dbo.[Draft.Container] dc2 ON dc3.BelongsTo = dc2.ID AND dc2.Depth = 1
		WHERE d.Del = 0 AND dc2.BelongsTo = @id AND da.year = @year AND da.month = @month
		ORDER BY dc3.BelongsTo, d.belongsTo, d.ID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_ViewAll2]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Draft_ViewAll2]
	-- Add the parameters for the stored procedure here
	@id int,
	@searchDate datetime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

    IF @searchDate IS NULL
    BEGIN
		SELECT
			d.ID,
			d.name,
			d.specification,
			d.state,
			d.location,
			d.numAll,
			d.value,
			d.hiddenSpecification,
			d.unit,
			d.priceOutH,
			d.priceOutL
		FROM dbo.[Draft.Draft] d
		LEFT JOIN dbo.[Draft.Container] dc3 ON d.belongsTo = dc3.ID AND dc3.Depth = 2
		WHERE d.Del = 0 AND dc3.BelongsTo = @id
		ORDER BY d.belongsTo, d.ID
	END
	ELSE
	BEGIN
		DECLARE @year nchar(4) = CAST(DATEPART(YY, @searchDate) AS INT)
		DECLARE @month nchar(2) = CAST(DATEPART(MM, @searchDate) AS INT)

		SELECT
			d.ID,
			d.name,
			d.specification,
			d.state,
			d.location,
			da.num AS numAll,
			da.value AS value,
			d.hiddenSpecification,
			d.unit,
			d.priceOutH,
			d.priceOutL
		FROM dbo.[Draft.Amount] da
		LEFT JOIN dbo.[Draft.Draft] d ON d.ID = da.IDDraft
		LEFT JOIN dbo.[Draft.Container] dc3 ON d.belongsTo = dc3.ID AND dc3.Depth = 2
		WHERE d.Del = 0 AND dc3.BelongsTo = @id AND da.year = @year AND da.month = @month
		ORDER BY d.belongsTo, d.ID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Draft_ViewAll3]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Draft_ViewAll3]
	-- Add the parameters for the stored procedure here
	@id int,
	@searchDate datetime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

    IF @searchDate IS NULL
    BEGIN
		SELECT
			d.ID,
			d.name,
			d.specification,
			d.state,
			d.location,
			d.numAll,
			d.value,
			d.hiddenSpecification,
			d.unit,
			d.priceOutH,
			d.priceOutL
		FROM dbo.[Draft.Draft] d
		WHERE d.Del = 0 AND d.BelongsTo = @id
		ORDER BY ID
	END
	ELSE
	BEGIN
		DECLARE @year nchar(4) = CAST(DATEPART(YY, @searchDate) AS INT)
		DECLARE @month nchar(2) = CAST(DATEPART(MM, @searchDate) AS INT)

		SELECT
			d.ID,
			d.name,
			d.specification,
			d.state,
			d.location,
			da.num AS numAll,
			da.value AS value,
			d.hiddenSpecification,
			d.unit,
			d.priceOutH,
			d.priceOutL
		FROM dbo.[Draft.Amount] da
		LEFT JOIN dbo.[Draft.Draft] d ON d.ID = da.IDDraft
		WHERE d.Del = 0 AND d.BelongsTo = @id AND da.year = @year AND da.month = @month
		ORDER BY ID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_ERPSystemNew_Rights_Read]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_ERPSystemNew_Rights_Read]
	-- Add the parameters for the stored procedure here
	@user_id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		rCustomerOperator,
		rAttendantOperator,
		rBonusOperator,
		rBonusAdmin,
		rTicketOperator,
		rTicketAdmin,
		rTicketFaultOperator,
		rTicketFaultAdmin,
		rDraftOperator
	FROM dbo.[ERPSystemNew.Rights]
	WHERE user_id = @user_id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Identity_Read]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Identity_Read]
	-- Add the parameters for the stored procedure here
	@type int,
	-------------------
	-- 0 card
	-- 1 password
	-------------------
	@cardInformation char(32) = '',
	@userName char(16) = '',
	@password char(32) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF @type = 0
    BEGIN
		SELECT ID, RTRIM(name) AS name, cardInformation, cardLogin, cardAuthorize, passwordLogin, passwordAuthorize 
			FROM dbo.[Identity.Identity]
			WHERE (NOT cardInformation IS NULL) AND cardInformation = @cardInformation
	END
	ELSE IF @type = 1
	BEGIN
		SELECT ID, RTRIM(name) AS name, cardInformation, cardLogin, cardAuthorize, passwordLogin, passwordAuthorize 
			FROM dbo.[Identity.Identity]
			WHERE (NOT userName IS NULL) AND (NOT password IS NULL) AND userName = @userName AND password = @password
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_RightsControl_Add]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_RightsControl_Add]
	-- Add the parameters for the stored procedure here
	@name nchar(100),
	@username nchar(16) = NULL,
	@password nchar(32) = NULL,
	@passwordLogin tinyint = NULL,
	@passwordAuthorize tinyint = NULL,
	@cardInformation nchar(32) = NULL,
	@cardLogin tinyint = NULL,
	@cardAuthorize tinyint = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    INSERT INTO dbo.[Identity.Identity] (name, userName, password, passwordLogin, passwordAuthorize, cardInformation, cardLogin, cardAuthorize)
		VALUES (@name, @username, @password, @passwordLogin, @passwordAuthorize, @cardInformation, @cardLogin, @cardAuthorize)
		
	DECLARE @RESULT int = @@IDENTITY
	
	SELECT @RESULT
END
GO
/****** Object:  StoredProcedure [dbo].[SP_RightsControl_GetRights]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_RightsControl_GetRights]
	-- Add the parameters for the stored procedure here
	@userID int,
	@instructionID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		CASE @instructionID
			WHEN 2 THEN rAdd
		END 'r'
		FROM dbo.[RightsControl.Rights] WHERE userID = @userID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_System_AddLoginLog]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_System_AddLoginLog]
	-- Add the parameters for the stored procedure here
	@type int,
	--	=======================
	--  1 normalLogin
	--  2 saveFittingsAmount
	--	3 saveWarrantAmount
	--	=======================
	@para1 int = 0,
	@para2 int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[System.LoginLog] (time, type) VALUES(GETDATE(), @type)
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_System_AuthorizeAddLog]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,> 
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_System_AuthorizeAddLog]
	-- Add the parameters for the stored procedure here
	@operator int,
	@originalOperator int,
	@systemID int,
	@instructionID int,
	@errorID int,
	@errorWarrantID int = NULL,
	@para int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[System.AuthorizeLog] 
		(operator, originalOperator, systemID, instructionID, errorID, date, errorWarrantID, para)
		VALUES (@operator, @originalOperator, @systemID, @instructionID, @errorID,
			GETDATE(), @errorWarrantID, @para)
			
END
GO
/****** Object:  StoredProcedure [dbo].[SP_System_MonthlySave]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_System_MonthlySave]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	EXEC dbo.[SP_Draft_SaveDataForMonthCheck]
	
	EXEC dbo.[SP_Warrant_SaveAmountForMonthCheck]

	EXEC dbo.[SP_Warrant_FinishForMonthlyResult]

	EXEC dbo.[SP_Ticket_FinishForMonthlyResult]
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_Add]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_Add]
	-- Add the parameters for the stored procedure here
	@customer_id int,
	@attendant_id int,
	@operator int,
	@contact varchar(20),
	@phone varchar(40),
	@address varchar(100),
	@truck int,
	@distance int,
	@comment varchar(50),
	@working_hour int,
	@solved_fault varchar(255),
	@machine_model varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC dbo.[SP_System_MonthlySave]

    -- Insert statements for procedure here
	INSERT INTO dbo.[Ticket.Ticket] 
		(state, customer_id, attendant_id, operator, contact, phone, address, truck, distance, comment,
			working_hour, submit_date, solved_fault, machine_model)
		VALUES (0, @customer_id, @attendant_id, @operator, @contact, @phone, @address, @truck, @distance, @comment,
			@working_hour, GETDATE(), @solved_fault, @machine_model)
	DECLARE @ID int = @@IDENTITY

	SELECT @ID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_Cancel]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_Cancel]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF DATEDIFF(DAY, (SELECT submit_date FROM dbo.[Ticket.Ticket] WHERE id = @id), GETDATE()) > 30
	BEGIN
		SELECT -2
	END
	ELSE
	BEGIN
		UPDATE dbo.[Ticket.Ticket] SET state = 3 WHERE id = @id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_Collect]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_Collect]
	-- Add the parameters for the stored procedure here
	@startDate DateTime,
	@stopDate DateTime,
	@col_restrict int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SET @startDate = DATEADD(DD, DATEDIFF(DD, 0, @startDate), 0)
	SET @stopDate = DATEADD(DD, DATEDIFF(DD, 0, @stopDate) + 1, 0)

	IF @col_restrict = 1
	BEGIN
		SELECT tt.*, ci.name AS name FROM (
			SELECT COUNT(*) AS count_all, SUM(tt.working_hour) AS sum_working_hour, customer_id AS id
				FROM dbo.[Ticket.Ticket] tt
					WHERE tt.submit_date BETWEEN @startDate AND @stopDate AND tt.state <> 3
					GROUP BY customer_id
				) AS tt
			LEFT JOIN dbo.[Customers.Information] ci ON ci.ID = tt.id
			ORDER BY count_all DESC
	END
	ELSE IF @col_restrict = 2
	BEGIN
		SELECT tt.*, wa.name AS name FROM (
			SELECT COUNT(*) AS count_all, SUM(tt.working_hour) AS sum_working_hour, attendant_id AS id
				FROM dbo.[Ticket.Ticket] tt
					LEFT JOIN dbo.[Warrant.Attendants] wa ON wa.ID = tt.attendant_id AND tt.state <> 3
					WHERE tt.submit_date BETWEEN @startDate AND @stopDate
					GROUP BY attendant_id
				) AS tt
			LEFT JOIN dbo.[Warrant.Attendants] wa ON wa.ID = tt.id
			ORDER BY count_all DESC
	END
	ELSE IF @col_restrict = 3
	BEGIN
		SELECT COUNT(*) AS count_all, SUM(tt.working_hour) AS sum_working_hour, truck AS id, '' AS name
			FROM dbo.[Ticket.Ticket] tt
				WHERE tt.submit_date BETWEEN @startDate AND @stopDate AND tt.state <> 3
				GROUP BY truck
				ORDER BY count_all DESC
	END

		
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_Copy]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_Copy]
	-- Add the parameters for the stored procedure here
	@id int,
	@operator int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC dbo.[SP_System_MonthlySave]

    -- Insert statements for procedure here
	INSERT INTO dbo.[Ticket.Ticket] 
		(state, customer_id, attendant_id, operator, contact, phone, address, truck, distance, comment,
			working_hour, submit_date, solved_fault, machine_model)
		SELECT 0, customer_id, attendant_id, @operator, contact, phone, address, truck, distance, comment,
			working_hour, GETDATE(), solved_fault, machine_model
			FROM dbo.[Ticket.Ticket] WHERE id = @id

	DECLARE @new_ID int = @@IDENTITY

	SELECT @new_ID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_Edit_Finished]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_Edit_Finished]
	-- Add the parameters for the stored procedure here
	@id int,
	@comment varchar(50),
	@working_hour int,
	@solved_fault varchar(255),
	@remaining_fault varchar(255),
	@machine_model varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC dbo.[SP_System_MonthlySave]

    -- Insert statements for procedure here
	UPDATE dbo.[Ticket.Ticket] 
		SET comment = @comment, working_hour = @working_hour, solved_fault = @solved_fault, remaining_fault = @remaining_fault, machine_model = @machine_model
		WHERE id = @id AND state = 1
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_Edit_Initial]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_Edit_Initial]
	-- Add the parameters for the stored procedure here
	@id int,
	@customer_id int,
	@attendant_id int,
	@operator int,
	@contact varchar(20),
	@phone varchar(40),
	@address varchar(100),
	@truck int,
	@distance int,
	@comment varchar(50),
	@working_hour int,
	@solved_fault varchar(255),
	@machine_model varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Ticket.Ticket] 
		SET customer_id = @customer_id, attendant_id = @attendant_id, operator = @operator,
			contact = @contact, phone = @phone, address = @address, truck = @truck, distance = @distance,
			comment = @comment, working_hour = @working_hour, submit_date = GETDATE(),
			solved_fault = @solved_fault, machine_model = @machine_model
		WHERE id = @id AND state = 0
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_Fault_Add]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_Fault_Add]
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@working_hour int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF NOT EXISTS(SELECT * FROM dbo.[Ticket.Fault] WHERE name = @name)
	BEGIN
		INSERT INTO dbo.[Ticket.Fault] (name, working_hour)
			VALUES (@name, @working_hour)
		DECLARE @ID int = @@IDENTITY

		SELECT @ID
	END
	ELSE
	BEGIN
		SELECT -1
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_Fault_Edit]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_Fault_Edit]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(50),
	@working_hour int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF NOT EXISTS(SELECT * FROM dbo.[Ticket.Fault] WHERE name = @name AND id <> @id)
	BEGIN
		UPDATE dbo.[Ticket.Fault] SET name = @name, working_hour = @working_hour WHERE id = @id
	END
	ELSE
	BEGIN
		SELECT -1
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_Fault_GetDefaultOnes]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_Fault_GetDefaultOnes]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT id, name, working_hour FROM dbo.[Ticket.Fault]
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_Fault_Read]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_Fault_Read]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT id, name, working_hour FROM dbo.[Ticket.Fault] WHERE id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_FinishForMonthlyResult]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_FinishForMonthlyResult]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here	SET NOCOUNT ON;

	DECLARE @cycle_start int = 15

	DECLARE @dateNow datetime = GETDATE()
	DECLARE @dateLast datetime = (SELECT TOP 1 time FROM dbo.[System.LoginLog] WHERE type = 5 ORDER BY time DESC)

	-- Get the latest month that should be calculated
	-- For this circumstance, we have a cycle from 29 to 28 but we shutdown discount at 5th of next month
	-- 2013-03-06 -> 2013-02-01
	-- 2013-03-05 -> 2013-01-01
	-- 2013-03-31 -> 2013-02-01
	SET @dateNow = DATEADD(MM, DATEDIFF(MM, 0, DATEADD(DD, -@cycle_start, @dateNow)) - 1, 0)

	-- Get the latest month that has been calculated
	-- 2013-03-06 -> 2013-02-01
	-- 2013-03-05 -> 2013-01-01
	-- 2013-03-31 -> 2013-02-01
	DECLARE @dateCalMonth datetime = DATEADD(MM, DATEDIFF(MM, 0, DATEADD(DD, -@cycle_start, @dateLast)) - 1, 0)

	DECLARE @ifAddLog bit = 0

	WHILE @dateCalMonth < @dateNow
	BEGIN
						
		SET @dateCalMonth = DATEADD(MM, 1, @dateCalMonth)

		-- From March 2019, we switch from 28th business month to calendar month
		-- which means March 1st - March 31st is a month
		--             April 1st - April 30th is a month
		DECLARE @dateCalStart datetime = @dateCalMonth
		DECLARE @dateCalStop datetime = DATEADD(MM, 1, @dateCalStart)

		-- The following algorithm is stopped by March (no included), 2019
		-- Get the latest month that should be calculated
		-- 2013-03-05 -> 2013-03-01
		-- 2013-03-31 -> 2013-03-01
		-- SET @dateCalStart = DATEADD(DD, 28, DATEADD(MM, -1, @dateCalMonth))
		-- SET @dateCalStop = DATEADD(DD, 28, @dateCalMonth)

		DECLARE @year int = DATEPART(YEAR, @dateCalMonth)
		DECLARE @month int = DATEPART(MONTH, @dateCalMonth)

		UPDATE dbo.[Ticket.Ticket] SET state = 10 WHERE state = 1 AND submit_date < @dateCalStop
		
		SET @ifAddLog = 1

	END
	IF @ifAddLog = 1 
	BEGIN
		EXEC dbo.[SP_System_AddLoginLog] @type = 5
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_GetDefaultOnes]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_GetDefaultOnes]
	-- Add the parameters for the stored procedure here
	@startDate DateTime = NULL,
	@stopDate DateTime = NULL,
	@col_restrict int = NULL,
	@col_restrict_id int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	IF @startDate IS NULL OR @stopDate IS NULL
	BEGIN
		SELECT tt.id, tt.state, tt.customer_id, ci.name as customer_name, tt.attendant_id, wa.name as attendant_name, tt.truck, tt.working_hour, tt.submit_date
			FROM dbo.[Ticket.Ticket] tt
			LEFT JOIN dbo.[Customers.Information] ci ON ci.ID = tt.customer_id
			LEFT JOIN dbo.[Warrant.Attendants] wa ON wa.id = tt.attendant_id
				WHERE submit_date IS NULL
				ORDER BY id DESC
	END
	ELSE
	BEGIN
		SET @startDate = DATEADD(DD, DATEDIFF(DD, 0, @startDate), 0)
		SET @stopDate = DATEADD(DD, DATEDIFF(DD, 0, @stopDate) + 1, 0)
		IF @col_restrict IS NOT NULL
		BEGIN
			SELECT tt.id, tt.state, tt.customer_id, ci.name as customer_name, tt.attendant_id, wa.name as attendant_name, tt.truck, tt.working_hour, tt.submit_date
				FROM dbo.[Ticket.Ticket] tt
				LEFT JOIN dbo.[Customers.Information] ci ON ci.ID = tt.customer_id
				LEFT JOIN dbo.[Warrant.Attendants] wa ON wa.id = tt.attendant_id
					WHERE tt.submit_date BETWEEN @startDate AND @stopDate
						AND (CASE @col_restrict WHEN 1 THEN customer_id WHEN 2 THEN attendant_id WHEN 3 THEN truck END) = @col_restrict_id
					ORDER BY id DESC
		END
		ELSE
		BEGIN
			SELECT tt.id, tt.state, tt.customer_id, ci.name as customer_name, tt.attendant_id, wa.name as attendant_name, tt.truck, tt.working_hour, tt.submit_date
				FROM dbo.[Ticket.Ticket] tt
				LEFT JOIN dbo.[Customers.Information] ci ON ci.ID = tt.customer_id
				LEFT JOIN dbo.[Warrant.Attendants] wa ON wa.id = tt.attendant_id
					WHERE tt.submit_date BETWEEN @startDate AND @stopDate
					ORDER BY id DESC
		END
	END

END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_Print]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_Print]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Ticket.Ticket] SET state = 1 WHERE id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ticket_Read]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Ticket_Read]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tt.id, tt.state, tt.customer_id, ci.name as customer_name, tt.attendant_id, wa.name as attendant_name, tt.operator, ii.name AS operator_name, 
		tt.contact, tt.phone, tt.address, tt.machine_model, tt.truck, tt.distance, tt.comment, tt.working_hour, tt.amount_servicefee_working, tt.amount_servicefee_truck,
		tt.submit_date, tt.finish_date, tt.solved_fault, tt.remaining_fault, tt.machine_model, tt.machine_worked_hour
		FROM dbo.[Ticket.Ticket] tt
		LEFT JOIN dbo.[Customers.Information] ci ON ci.ID = tt.customer_id
		LEFT JOIN dbo.[Warrant.Attendants] wa ON wa.id = tt.attendant_id
		LEFT JOIN dbo.[Identity.Identity] ii ON ii.ID = tt.operator
			WHERE tt.id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Attendants_Add]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Attendants_Add]
	-- Add the parameters for the stored procedure here
	@name varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
    IF NOT EXISTS(SELECT * FROM dbo.[Warrant.Attendants] WHERE name = @name)
	BEGIN
		INSERT INTO dbo.[Warrant.Attendants] (name)
			VALUES (@name)
		DECLARE @ID int = @@IDENTITY

		SELECT @ID
	END
	ELSE
	BEGIN
		SELECT -1
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Attendants_Edit]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Attendants_Edit]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF NOT EXISTS(SELECT * FROM dbo.[Warrant.Attendants] WHERE name = @name AND id <> @id)
	BEGIN
		UPDATE dbo.[Warrant.Attendants] SET name = @name WHERE id = @id
	END
	ELSE
	BEGIN
		SELECT -1
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Attendants_GetDefaultOnes]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Attendants_GetDefaultOnes]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT id, name FROM [dbo].[Warrant.Attendants]
END

GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Attendants_Read]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Attendants_Read]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT id, name FROM dbo.[Warrant.Attendants] WHERE id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Bonus_Addition_Add]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Bonus_Addition_Add]
	-- Add the parameters for the stored procedure here
	@year int,
	@month int,
	@attendant_id int,
	@bonus int,
	@reason varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[Warrant.Bonus.Addition] (year, month, attendant_id, bonus, reason) VALUES (@year, @month, @attendant_id, @bonus, @reason)
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Bonus_Addition_Edit]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Bonus_Addition_Edit]
	-- Add the parameters for the stored procedure here
	@id int,
	@year int,
	@month int,
	@attendant_id int,
	@bonus int,
	@reason varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.Bonus.Addition] SET bonus = @bonus, reason = @reason WHERE id = @id AND year = @year AND month = @month AND attendant_id = @attendant_id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Bonus_Addition_GetByAttendantAndDate]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Bonus_Addition_GetByAttendantAndDate]
	-- Add the parameters for the stored procedure here
	@attendant_id int,
	@year int,
	@month int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT wb.id, wb.attendant_id, wb.bonus, wb.reason
		FROM dbo.[Warrant.Bonus.Addition] AS wb
		WHERE attendant_id = @attendant_id AND year = @year AND month = @month
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Bonus_GetByAttendantAndDate]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Bonus_GetByAttendantAndDate]
	-- Add the parameters for the stored procedure here
	@attendant_id int,
	@year int,
	@month int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT wb.warrant_id, wb.warrant_type, wb.customer_id, wb.amount_draft, wb.amount_servicefee, wb.repay_delay, wb.attendant_num, wb.bonus_draft, wb.bonus_servicefee, wb.calculation_type, ci.name AS customer_name
		FROM dbo.[Warrant.Bonus] AS wb 
			LEFT JOIN dbo.[Warrant.Out.Attendant] AS woa ON woa.warrant_id = wb.warrant_id
			LEFT JOIN dbo.[Customers.Information] AS ci ON wb.customer_id = ci.ID
		WHERE woa.attendant_id = @attendant_id AND year = @year AND month = @month
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Bonus_GetByDate]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Bonus_GetByDate]
	-- Add the parameters for the stored procedure here
	@year int,
	@month int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

    -- Insert statements for procedure here
	SELECT wb.num_bonus, wb.attendant_id, wa.name AS attendant_name, wb.bonus_draft, wb.bonus_servicefee, wba.bonus_addition FROM 
		(SELECT COUNT(*) AS num_bonus, SUM(wb.bonus_draft / wb.attendant_num) AS bonus_draft, SUM(wb.bonus_servicefee / wb.attendant_num) AS bonus_servicefee, woa.attendant_id
			FROM dbo.[Warrant.Bonus] wb
			JOIN dbo.[Warrant.Out.Attendant] woa ON woa.warrant_id = wb.warrant_id
			WHERE wb.year = @year AND wb.month = @month
			GROUP BY woa.attendant_id) wb
		FULL JOIN (SELECT SUM(bonus) AS bonus_addition, attendant_id FROM dbo.[Warrant.Bonus.Addition] WHERE year = @year AND month = @month GROUP BY attendant_id) AS wba ON wba.attendant_id = wb.attendant_id
		LEFT JOIN dbo.[Warrant.Attendants] wa ON wa.id = wb.attendant_id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Bonus_GetByDateForCalculation]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Bonus_GetByDateForCalculation]
	-- Add the parameters for the stored procedure here
	@year int,
	@month int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT wb.warrant_id, wb.warrant_type, wb.amount_draft, wb.amount_servicefee, wb.bonus_draft, wb.bonus_servicefee, c.vip_level, wb.repay_delay
		FROM dbo.[Warrant.Bonus] wb
			JOIN dbo.[Customers.Information] c ON c.ID = wb.customer_id
		WHERE year = @year AND month = @month
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Bonus_Value_Set]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Bonus_Value_Set]
	-- Add the parameters for the stored procedure here
	@warrant_id int,
	@year int,
	@month int,
	@calculation_type int,
	@bonus_draft int,
	@bonus_servicefee int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.Bonus]
		SET bonus_draft = @bonus_draft, bonus_servicefee = @bonus_servicefee, calculation_type = @calculation_type
		WHERE warrant_id = @warrant_id AND year = @year AND month = @month
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_BonusRatio_Read]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_BonusRatio_Read]
	-- Add the parameters for the stored procedure here
	@year int,
	@month int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (SELECT COUNT(*) FROM dbo.[Warrant.BonusRatio] WHERE year = @year AND month = @month) = 0
	BEGIN
		DECLARE @lastmonth int, @lastyear int
		IF @month = 1
		BEGIN 
			SET @lastmonth = 12
			SET @lastyear = @year - 1
		END
		ELSE
		BEGIN
			SET @lastmonth = @month - 1
			SET @lastyear = @year
		END
		INSERT INTO dbo.[Warrant.BonusRatio] 
			SELECT @year, @month, type, ratio_draft_imme, ratio_draft_normal, ratio_draft_penalty,
				ratio_servicefee_imme, ratio_servicefee_normal, ratio_servicefee_penalty
				FROM dbo.[Warrant.BonusRatio] WHERE year = @lastyear AND month = @lastmonth

		IF (SELECT COUNT(*) FROM dbo.[Warrant.BonusRatio] WHERE year = @year AND month = @month) < 3
		BEGIN
			INSERT INTO dbo.[Warrant.BonusRatio] VALUES 
				(@year, @month, 0, null, 0, null, null, 0, null),
				(@year, @month, 1, null, 0, null, null, 0, null),
				(@year, @month, 2, 0, 0, 0, 0, 0, 0)
		END
	END

	SELECT * FROM dbo.[Warrant.BonusRatio] WHERE year = @year AND month = @month
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_BonusRatio_Set]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_BonusRatio_Set]
	-- Add the parameters for the stored procedure here
	@year int,
	@month int,
	@type int,
	@draft_imme int = null,
	@draft_normal int,
	@draft_penalty int = null,
	@servicefee_imme int = null,
	@servicefee_normal int,
	@servicefee_penalty int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @type = 2
	BEGIN
		UPDATE dbo.[Warrant.BonusRatio] SET 
			ratio_draft_imme = @draft_imme, ratio_draft_normal = @draft_normal, ratio_draft_penalty = @draft_penalty,
			ratio_servicefee_imme = @servicefee_imme, ratio_servicefee_normal = @servicefee_normal, ratio_servicefee_penalty = @servicefee_penalty
			WHERE year = @year AND month = @month AND type = @type
	END
	ELSE
	BEGIN
		UPDATE dbo.[Warrant.BonusRatio] SET ratio_draft_normal = @draft_normal, ratio_servicefee_normal = @servicefee_normal
			WHERE year = @year AND month = @month AND type = @type
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_FinishForMonthlyResult]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery2.sql|7|0|C:\Users\家杰\AppData\Local\Temp\~vs75FE.sql
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_FinishForMonthlyResult]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @cycle_start int = 5

	DECLARE @dateNow datetime = GETDATE()
	DECLARE @dateLast datetime = (SELECT TOP 1 time FROM dbo.[System.LoginLog] WHERE type = 4 ORDER BY time DESC)

	-- Get the latest month that should be calculated
	-- For this circumstance, we have a cycle from 29 to 28 but we shutdown discount at 5th of next month
	-- 2013-03-06 -> 2013-02-01
	-- 2013-03-05 -> 2013-01-01
	-- 2013-03-31 -> 2013-02-01
	SET @dateNow = DATEADD(MM, DATEDIFF(MM, 0, DATEADD(DD, -@cycle_start, @dateNow)) - 1, 0)

	-- Get the latest month that has been calculated
	-- 2013-03-06 -> 2013-02-01
	-- 2013-03-05 -> 2013-01-01
	-- 2013-03-31 -> 2013-02-01
	DECLARE @dateCalMonth datetime = DATEADD(MM, DATEDIFF(MM, 0, DATEADD(DD, -@cycle_start, @dateLast)) - 1, 0)

	DECLARE @ifAddLog bit = 0
	DECLARE @row nchar(6)
	
	DECLARE @numIn int
	DECLARE @numAdjustIn int
	DECLARE @numSell int
	DECLARE @numCost int
	DECLARE @numService int
	DECLARE @numCostService int
	DECLARE @numGift int
	DECLARE @numCostGift int
	DECLARE @numAdjustOut int

	WHILE @dateCalMonth < @dateNow
	BEGIN
		SET @dateCalMonth = DATEADD(MM, 1, @dateCalMonth)
		DECLARE @dateCalStart datetime = @dateCalMonth
		DECLARE @dateCalStop datetime = DATEADD(MM, 1, @dateCalMonth)

		-- The following algorithm is stopped by March (no included), 2019
		-- SET @dateCalStart = DATEADD(DD, 28, DATEADD(MM, -1, @dateCalMonth))
		-- SET @dateCalStop = DATEADD(DD, 28, @dateCalMonth)
						
		DECLARE @year int = DATEPART(YEAR, @dateCalMonth)
		DECLARE @month int = DATEPART(MONTH, @dateCalMonth)

		SET @numSell = 0
		SET @numSell = ISNULL((SELECT SUM(ISNULL(amountPermit, amountTotal))
			FROM dbo.[Warrant.Out.Warrant] ow
			WHERE ow.submitDate BETWEEN @dateCalStart AND @dateCalStop
				AND (ow.type = 0 OR ow.type = 1) AND ow.state >= 9), 0)
					
		UPDATE dbo.[Warrant.Amount] SET amountSell = @numSell WHERE year = @year AND month = @month

		UPDATE dbo.[Warrant.Out.Warrant] SET state = 14 WHERE state >= 9 AND submitDate BETWEEN @dateCalStart AND @dateCalStop

		UPDATE dbo.[Warrant.Out.Warrant] SET state = 20 WHERE state = 14 AND submitDate BETWEEN @dateCalStart AND @dateCalStop AND ISNULL(amountPaid, 0) = ISNULL(amountPermit, amountTotal) 

		INSERT INTO dbo.[Warrant.Bonus] 
			(warrant_id, warrant_type, amount_draft, amount_servicefee, customer_id, repay_delay, attendant_num, year, month) 
			SELECT wow.ID, (CASE WHEN ISNULL(wow.amountPermit, wow.amountTotal) >= wow.amountTotal * 0.3 THEN wow.type ELSE 20 END), wow.amountTotal - wow.amountServicefee, wow.amountServicefee, wow.target, ISNULL(DATEDIFF(DD, wow.submitDate, wow.lastPaidDate), 0), (SELECT COUNT(*) FROM dbo.[Warrant.Out.Attendant] woa WHERE woa.warrant_id = wow.ID), @year, @month
				FROM dbo.[Warrant.Out.Warrant] wow 
					LEFT JOIN dbo.[Customers.Information] ci ON ci.ID = wow.target
				WHERE wow.type IN (0, 1) AND
						(
						(wow.state = 20)											-- Paid Warrant
						OR 
						(wow.state = 14 AND ci.vip_level >= 5)						-- VIP Warrant
						)
					AND wow.submitDate BETWEEN @dateCalStart AND @dateCalStop
					AND (SELECT COUNT(*) FROM dbo.[Warrant.Out.Attendant] woa WHERE woa.warrant_id = wow.ID) > 0

		INSERT INTO dbo.[Warrant.Bonus] 
			(warrant_id, warrant_type, amount_draft, amount_servicefee, customer_id, repay_delay, attendant_num, year, month) 
			SELECT wow.ID, wow.type, wow.amountCost, 0, wow.target, 0, (SELECT COUNT(*) FROM dbo.[Warrant.Out.Attendant] woa WHERE woa.warrant_id = wow.ID), @year, @month
				FROM dbo.[Warrant.Out.Warrant] wow 
					LEFT JOIN dbo.[Customers.Information] ci ON ci.ID = wow.target
				WHERE wow.type IN (2)
					AND (wow.state = 14)											-- Service Warrant
					AND wow.submitDate BETWEEN @dateCalStart AND @dateCalStop	
					AND (SELECT COUNT(*) FROM dbo.[Warrant.Out.Attendant] woa WHERE woa.warrant_id = wow.ID) > 0

		UPDATE dbo.[Warrant.In.Warrant] SET state = 14 WHERE state >= 9 AND submitDate BETWEEN @dateCalStart AND @dateCalStop

		SET @ifAddLog = 1
	END
	IF @ifAddLog = 1 
	BEGIN
		EXEC dbo.[SP_System_AddLoginLog] @type = 4
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_GetRights]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_GetRights]
	-- Add the parameters for the stored procedure here
	@userID int,
	@instructionID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
    -- Insert statements for procedure here
	SELECT 
		CASE @instructionID
			WHEN 1 THEN rViewDetail
			WHEN 2 THEN rViewOneCycle
			WHEN 10 THEN rInLogIn
			WHEN 11 THEN rInSpecialOne
			WHEN 12 THEN rInAdd
			WHEN 13 THEN rInEdit 
			WHEN 14 THEN rInCancel
			WHEN 17 THEN rInSubmit
			WHEN 18 THEN rInPrint
			WHEN 19 THEN rInMoney 
			WHEN 20 THEN rInComplete
			WHEN 30 THEN rOutLogIn
			WHEN 31 THEN rOutSpecialOne
			WHEN 32 THEN rOutAdd
			WHEN 33 THEN rOutEdit
			WHEN 34 THEN rOutCancel
			WHEN 35 THEN rOutSubmitCustomer
			WHEN 36 THEN rOutPrintCustomer
			WHEN 37 THEN rOutSubmit
			WHEN 38 THEN rOutPrint
			WHEN 39 THEN rOutMoney 
			WHEN 40 THEN rOutComplete
			WHEN 43 THEN rOutForcePass
			WHEN 44 THEN rOutTakeOut
			WHEN 45 THEN rOutReturn
			WHEN 50 THEN rOutDiscount
			WHEN 51 THEN rOutReturnPayCheck
			WHEN 52 THEN rAttendantsList
			END 'r'
		FROM dbo.[Warrant.Rights] WHERE userID = @userID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_Add]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_Add] 
	-- Add the parameters for the stored procedure here
	@target int = NULL,
	@type int,
	@operator int,
	@cookieNew int,
	@comment varchar(80) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @time datetime2 = GETDATE()

	DECLARE @seconds int = 
		DATEDIFF(
			SECOND,
			DATEADD(
				DAY,
				DATEDIFF(DAY, 0, @time),
				0
			),
			@time
		)

	DECLARE @number nchar(13) = CONCAT(
		FORMAT(@time, 'yyyyMMdd'),
		FORMAT(@seconds, '#####')
	)

    -- Insert statements for procedure here
	INSERT INTO dbo.[Warrant.In.Warrant] (
		state,
		target,
		type,
		cookie,
		comment,
		number
	) Values(
		0,
		@target,
		@type,
		@cookieNew,
		@comment,
		@number
	)

	DECLARE @ID int = @@IDENTITY
	
	EXEC dbo.SP_Warrant_In_AddOperationRecord @operationOperator = @operator, @operation = 0, @isAuthorize = 0, @warrantId = @ID
	
	SELECT @ID
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_AddItem]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_AddItem]
	-- Add the parameters for the stored procedure here
	@IDDraft int,
	@IDWarrant int,
	@price int,
	@priceWithTax int,
	@num int,
	@remark varchar(20) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[Warrant.In.Item] (
		IDDraft,
		IDWarrant,
		price,
		priceWithTax,
		num,
		remark
	) Values (
		@IDDraft,
		@IDWarrant,
		@price,
		@priceWithTax,
		@num,
		@remark
	)
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_AddOperationRecord]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_AddOperationRecord]
	-- Add the parameters for the stored procedure here
	@operationOperator int,
	@operation int,
	@warrantId int,
	@isAuthorize bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[Warrant.In.OperationRecord] (warrantId, operation, operationOperator, isAuthorize, operatedTime) VALUES(@warrantId, @operation, @operationOperator, @isAuthorize, GETDATE())
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_Cancel]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_Cancel]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.In.Warrant] SET state = 2, cookie = @cookieNew
		WHERE ID = @id AND cookie = @cookieOri AND state = 0
		
	SELECT @@ROWCOUNT
	
	EXEC dbo.SP_Warrant_In_AddOperationRecord @operationOperator = @operator, @operation = 2, @isAuthorize = 0, @warrantId = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_CustomersAddFrequency]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_CustomersAddFrequency]
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	MERGE INTO dbo.[Customers.Information] cus
		USING dbo.[Warrant.In.Warrant] iw
			ON iw.ID = @id AND iw.target = cus.ID
			WHEN MATCHED THEN UPDATE SET cus.frequency += 1;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_Edit]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_Edit]
	-- Add the parameters for the stored procedure here
	@id int,
	@target int = NULL,
	@type int,
	@operator int,
	@cookieOri int,
	@cookieNew int,
	@comment varchar(80) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.In.Warrant] Set target = @target, type = @type, cookie = @cookieNew, Comment = @comment
		Where ID = @id And cookie = @cookieOri AND state = 0
	
	SELECT @@ROWCOUNT
	
	EXEC dbo.SP_Warrant_In_AddOperationRecord @operationOperator = @operator, @operation = 1, @isAuthorize = 0, @warrantId = @id
	
	EXEC dbo.[SP_Warrant_In_EditDelItem] @id = @id
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_EditDelItem]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_EditDelItem]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE dbo.[Warrant.In.Item] WHERE IDWarrant = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_GetDefaultOnes]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_GetDefaultOnes]
	-- Add the parameters for the stored procedure here
	@startDate DateTime = NULL,
	@stopDate DateTime = NULL,
	@target int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	CREATE TABLE #TempTable(
		ID int,
		state int,
		type int,
		target int,
		amountIn int,
		amountWithTaxIn int,
		number nchar(13)
	)
	
    -- Insert statements for procedure here
	IF @startDate IS NULL OR @stopDate IS NULL
	BEGIN
		INSERT INTO #TempTable 
			SELECT
				iw.ID,
				iw.state,
				iw.type,
				iw.target,
				iw.amountIn,
				iw.amountWithTaxIn,
				iw.number
			FROM dbo.[Warrant.In.Warrant] iw
	END
	ELSE
	BEGIN
		SET @startDate = DATEADD(DD, DATEDIFF(DD, 0, @startDate), 0)
		SET @stopDate = DATEADD(DD, DATEDIFF(DD, 0, @stopDate) + 1, 0)
		INSERT INTO #TempTable 
			SELECT
				iw.ID,
				iw.state,
				iw.type,
				iw.target,
				iw.amountIn,
				iw.amountWithTaxIn,
				iw.number
			FROM dbo.[Warrant.In.Warrant] iw
			WHERE (iw.submitDate BETWEEN @startDate AND @stopDate) OR (iw.state < 10 AND iw.state != 2)
	END
	
	IF @target = 0
	BEGIN
		SELECT * FROM #TempTable ORDER BY ID
	END
	ELSE
		BEGIN SELECT * FROM #TempTable WHERE target = @target ORDER BY ID
	END
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_GetInfo]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_GetInfo]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT wiw.ID, wiw.state, wiw.cookie, wior.operationOperator AS creator
		FROM dbo.[Warrant.In.Warrant] wiw
		LEFT JOIN dbo.[Warrant.In.OperationRecord] wior
			ON wior.warrantId = wiw.ID AND wior.operation = 0
	WHERE wiw.ID = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_Print]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_Print]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.In.Warrant] SET state = 10, cookie = @cookieNew
		WHERE ID = @id AND cookie = @cookieOri AND state >= 9 AND state <= 10
	
	SELECT @@ROWCOUNT
		
	EXEC dbo.SP_Warrant_In_AddOperationRecord @operationOperator = @operator, @operation = 9, @isAuthorize = 0, @warrantId = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_Read]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_Read]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		iw.ID,
		iw.target,
		iw.state,
		iw.type,
		iw.submitDate,
		iw.comment,
		iw.cookie,
		iw.amountIn,
		iw.number
	FROM dbo.[Warrant.In.Warrant] iw
	WHERE iw.ID = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_ReadItem]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_ReadItem]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
			iwi.ID,
			iwi.IDDraft,
			iwi.IDWarrant,
			iwi.price,
			iwi.priceWithTax,
			iwi.num,
			iwi.remark,
			RTRIM(d.Name) AS name,
			RTRIM(d.specification) AS specification,
			RTRIM(d.hiddenSpecification) + '	' + RTRIM(d.usingTarget) AS hiddenSpecification,
			RTRIM(d.unit) AS unit
		FROM dbo.[Warrant.In.Item] iwi
		LEFT JOIN dbo.[Draft.Draft] d ON
			d.ID = iwi.IDDraft
		WHERE iwi.IDWarrant = @id
		ORDER BY iwi.ID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_ReadOperationRecord]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_ReadOperationRecord]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
			
	SELECT wir.ID, wir.warrantId, wir.operationOperator, wir.operation, wir.operatedTime, wir.parameter1, wir.parameter2, u.name AS operationOperatorName
		FROM dbo.[Warrant.In.OperationRecord] wir
		LEFT JOIN dbo.[Identity.Identity] u
			ON u.ID = wir.operationOperator
		WHERE wir.warrantId = @id
		ORDER BY wir.operatedTime
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_Submit]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_Submit]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC dbo.[SP_System_MonthlySave]
	
    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.In.Warrant] SET state = 9, cookie = @cookieNew, submitDate = GETDATE()
		WHERE ID = @id AND cookie = @cookieOri AND state = 0
	
	IF @@ROWCOUNT = 1
	BEGIN
		EXEC dbo.[SP_Warrant_In_CustomersAddFrequency] @id = @id

		MERGE INTO dbo.[Draft.Draft] d
			USING dbo.[Warrant.In.Item] iwi
			ON iwi.IDDraft = d.ID
				WHEN MATCHED AND iwi.IDWarrant = @id THEN
				UPDATE SET d.value += iwi.price * iwi.num, d.numAccessiable += iwi.num, d.numAll += iwi.num, d.numInHouse += iwi.num;
		
		MERGE INTO dbo.[Warrant.In.Item] iwi
			USING dbo.[Draft.Draft] d
			ON iwi.IDWarrant = @id And d.ID = iwi.IDDraft
				WHEN MATCHED THEN
				UPDATE SET iwi.currentValue = d.value, iwi.currentNumAll = d.numAll;
		
		DECLARE @amountIn int = 
			ISNULL((SELECT SUM(num * price) 
				FROM dbo.[Warrant.In.Item] WHERE IDWarrant = @id), 0)

		DECLARE @amountWithTaxIn int = 
			ISNULL((SELECT SUM(num * priceWithTax) 
				FROM dbo.[Warrant.In.Item] WHERE IDWarrant = @id), 0)
		
		UPDATE dbo.[Warrant.In.Warrant]
			SET
				amountIn = @amountIn,
				amountWithTaxIn = @amountWithTaxIn
			WHERE ID = @id
		
		EXEC dbo.SP_Warrant_In_AddOperationRecord @operationOperator = @operator, @operation = 8, @isAuthorize = 0, @warrantId = @id
		
		SELECT 1
	END
	ELSE 
		SELECT 0
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_SubmitReadForCheck]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_SubmitReadForCheck]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT d.numAccessiable, d.numAll, d.value, iwi.num, iwi.price, d.Name, d.specification, d.Del
		FROM dbo.[Warrant.In.Item] iwi
		JOIN dbo.[Draft.Draft] d
			ON d.ID = iwi.IDDraft
		WHERE iwi.IDWarrant = @id
		
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_ViewDetail]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_ViewDetail]
	-- Add the parameters for the stored procedure here
	@id int,
	@startDate dateTime,
	@stopDate dateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SET @startDate = DATEADD(DD, DATEDIFF(DD, 0, @startDate), 0)
	SET @stopDate = DATEADD(DD, DATEDIFF(DD, 0, @stopDate) + 1, 0)
	
	SELECT
		RTRIM(d.Name) AS name,
		RTRIM(d.specification) AS specification,
		iwi.IDWarrant,
		iwi.num,
		iwi.price,
		iwi.priceWithTax,
		iwi.remark,
		iw.submitDate AS dateSubmit,
		iwi.currentNumAll, iwi.currentValue
	FROM dbo.[Warrant.In.Item] iwi
	JOIN dbo.[Warrant.In.Warrant] iw ON iw.ID = iwi.IDWarrant
	JOIN dbo.[Draft.Draft] d ON d.ID = @id
	WHERE
		iwi.IDDraft = @id AND
		iw.submitDate BETWEEN @startDate AND @stopDate
	ORDER BY iw.submitDate

END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_In_ViewOneCycle]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_In_ViewOneCycle]
	-- Add the parameters for the stored procedure here
	@date datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- From March 2019, we switch from 28th business month to calendar month
	-- which means March 1st - March 31st is a month
	--             April 1st - April 30th is a month

	-- Get the latest month that should be calculated
	-- 2013-03-05 -> 2013-03-01
	-- 2013-03-31 -> 2013-03-01
	DECLARE @dateNow datetime = DATEADD(MM, DATEDIFF(MM, 0, @date), 0)

	DECLARE @startDate datetime = @dateNow
	DECLARE @stopDate datetime = DATEADD(MM, 1, @dateNow)
    
	-- The following algorithm is stopped by March (no included), 2019
	-- DECLARE @startDate datetime = DATEADD(DD, 28, DATEADD(MM, -1, @dateNow))
	-- DECLARE @stopDate datetime = DATEADD(DD, 28, @dateNow)
	
    SELECT
		iw.ID,
		iw.state,
		iw.type,
		iw.target,
		iw.amountIn,
		iw.submitDate,
		ci.name AS target_name,
		iw.number
	FROM dbo.[Warrant.In.Warrant] iw
	LEFT JOIN dbo.[Customers.Information] ci
		ON ci.ID = iw.target
	WHERE (iw.submitDate BETWEEN @startDate AND @stopDate) AND iw.state >= 9
	ORDER BY ID		
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_Add]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_Add] 
	-- Add the parameters for the stored procedure here
	@target int = NULL,
	@operator int,
	@cookieNew int,
	@type int,
	@comment varchar(80) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @time datetime2 = GETDATE()

	DECLARE @seconds int = 
		DATEDIFF(
			SECOND,
			DATEADD(
				DAY,
				DATEDIFF(DAY, 0, @time),
				0
			),
			@time
		)

	DECLARE @number nchar(13) = CONCAT(
		FORMAT(@time, 'yyyyMMdd'),
		FORMAT(@seconds, '#####')
	)

    -- Insert statements for procedure here
	INSERT INTO dbo.[Warrant.Out.Warrant] (
		state,
		target,
		type,
		payState,
		cookie,
		comment,
		number
	) Values(
		0,
		@target,
		@type,
		0,
		@cookieNew,
		@comment,
		@number
	)
	
	DECLARE @ID int = @@IDENTITY
	
	EXEC dbo.SP_Warrant_Out_AddOperationRecord @operationOperator = @operator, @operation = 0, @isAuthorize = 0, @warrantId = @ID
	
	SELECT @ID
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_AddAttendant]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_AddAttendant]
	-- Add the parameters for the stored procedure here
	@warrant_id int,
	@attendant_id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[Warrant.Out.Attendant] (warrant_id, attendant_id) 
		VALUES (@warrant_id, @attendant_id)
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_AddItem]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_AddItem]
	-- Add the parameters for the stored procedure here
	@IDDraft int,
	@IDWarrant int,
	@num int,
	@priceAdjustMannual int = NULL,
	@remark varchar(20) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[Warrant.Out.Item] (IDDraft, IDWarrant, price, num, numReturn, priceAdjustMannual, remark) 
		Values (@IDDraft, @IDWarrant, 
		(SELECT CASE ow.type WHEN 1 THEN d.priceOutL ELSE d.priceOutH END 
			FROM dbo.[Draft.Draft] d 
				LEFT JOIN dbo.[Warrant.Out.Warrant] ow ON ow.ID = @IDWarrant
			WHERE d.ID = @IDDraft), 
		@num, 0, @priceAdjustMannual, @remark)
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_AddOperationRecord]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_AddOperationRecord]
	-- Add the parameters for the stored procedure here
	@operationOperator int,
	@operation int,
	@warrantId int,
	@isAuthorize bit,
	@parameter1 int = NULL,
	@parameter2 varchar(100) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[Warrant.Out.OperationRecord] (warrantId, operation, operationOperator, isAuthorize, operatedTime, parameter1, parameter2) VALUES(@warrantId, @operation, @operationOperator, @isAuthorize, GETDATE(), @parameter1, @parameter2)
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_Cancel]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_Cancel]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.Out.Warrant] SET state = 2, cookie = @cookieNew
		WHERE ID = @id AND cookie = @cookieOri AND state = 0
		
	SELECT @@ROWCOUNT
		
	EXEC dbo.SP_Warrant_Out_AddOperationRecord @operationOperator = @operator, @operation = 2, @isAuthorize = 0, @warrantId = @id
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_CustomersAddFrequency]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_CustomersAddFrequency]
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	MERGE INTO dbo.[Customers.Information] cus
		USING dbo.[Warrant.Out.Warrant] ow
			ON ow.ID = @id AND ow.target = cus.ID
			WHEN MATCHED THEN UPDATE SET cus.frequency += 1;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_Discount]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_Discount]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int,
	@amountPermit int,
	@reason varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @row int

	UPDATE dbo.[Warrant.Out.Warrant] SET amountPermit = @amountPermit, cookie = @cookieNew
		WHERE ID = @id AND cookie = @cookieOri AND state < 13
			
	DECLARE @RESULT int = @@ROWCOUNT
	
	EXEC dbo.[SP_Warrant_Out_UpdatePayState] @id = @id
		
	EXEC dbo.SP_Warrant_Out_AddOperationRecord @operationOperator = @operator, @operation = 12, @isAuthorize = 0, @warrantId = @id, @parameter1 = @amountPermit, @parameter2 = @reason
		
	SELECT @RESULT
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_Edit]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_Edit]
	-- Add the parameters for the stored procedure here
	@id int,
	@target int = NULL,
	@operator int,
	@type int,
	@cookieOri int,
	@cookieNew int,
	@comment varchar(80) = ''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.Out.Warrant] Set target = @target, type = @type, cookie = @cookieNew, Comment = @comment
		Where ID = @id And cookie = @cookieOri AND state = 0
	
	SELECT @@ROWCOUNT
	
	EXEC dbo.SP_Warrant_Out_AddOperationRecord @operationOperator = @operator, @operation = 1, @isAuthorize = 0, @warrantId = @id
	
	EXEC dbo.[SP_Warrant_Out_EditDelItem] @id = @id
	
	EXEC dbo.[SP_Warrant_Out_EditDelAttendant] @id = @id
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_EditDelAttendant]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_EditDelAttendant]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE dbo.[Warrant.Out.Attendant] WHERE warrant_id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_EditDelItem]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_EditDelItem]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE dbo.[Warrant.Out.Item] WHERE IDWarrant = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_ForcePass]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_ForcePass]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.Out.Warrant] SET payState = 2, cookie = @cookieNew
		WHERE ID = @id AND cookie = @cookieOri AND (state = 4 OR state = 5)
	SELECT @@ROWCOUNT
		
	EXEC dbo.SP_Warrant_Out_AddOperationRecord @operationOperator = @operator, @operation = 5, @isAuthorize = 0, @warrantId = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_GetDefaultOnes]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_GetDefaultOnes]
	-- Add the parameters for the stored procedure here
	@startDate DateTime = NULL,
	@stopDate DateTime = NULL,
	@target int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #TempTable(
		ID int,
		state int,
		type int,
		payState int,
		target int,
		amountTotal int,
		amountPermit int,
		amountCost int,
		number nchar(13)
	)

	IF @startDate IS NULL OR @stopDate IS NULL
	BEGIN
		INSERT INTO #TempTable 
			SELECT
				ow.ID,
				ow.state,
				ow.type,
				ow.payState,
				ow.target,
				ow.amountTotal,
				ow.amountPermit,
				ow.amountCost,
				ow.number
			FROM dbo.[Warrant.Out.Warrant] ow
	END
	ELSE
	BEGIN
		SET @startDate = DATEADD(DD, DATEDIFF(DD, 0, @startDate), 0)
		SET @stopDate = DATEADD(DD, DATEDIFF(DD, 0, @stopDate) + 1, 0)
		INSERT INTO #TempTable 
			SELECT
				ow.ID,
				ow.state,
				ow.type,
				ow.payState,
				ow.target,
				ow.amountTotal,
				ow.amountPermit,
				ow.amountCost,
				ow.number
			FROM dbo.[Warrant.Out.Warrant] ow
			WHERE (ow.submitDate BETWEEN @startDate AND @stopDate) OR (ow.state < 10 AND ow.state != 2)
	END
	
	IF @target = 0
	BEGIN
		SELECT * FROM #TempTable ORDER BY ID
	END
	ELSE
		BEGIN SELECT * FROM #TempTable WHERE target = @target ORDER BY ID
	END
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_GetInfo]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_GetInfo]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT wow.ID, wow.state, wow.type, wow.payState, wow.cookie, woor1.operationOperator AS creator, woor2.operationOperator AS returnOperator
		FROM dbo.[Warrant.Out.Warrant] wow
		LEFT JOIN dbo.[Warrant.Out.OperationRecord] woor1
			ON woor1.warrantId = wow.ID AND woor1.operation = 0
		LEFT JOIN dbo.[Warrant.Out.OperationRecord] woor2
			ON woor2.warrantId = wow.ID AND woor2.operation = 7
	WHERE wow.ID = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_GetInfoIncludingAmount]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_GetInfoIncludingAmount]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT wow.ID, wow.state, wow.type, wow.payState, wow.cookie, wow.amountTotal, wow.amountPermit, wow.amountPaid, woor1.operationOperator AS creator, woor2.operationOperator AS returnOperator
		FROM dbo.[Warrant.Out.Warrant] wow
		LEFT JOIN dbo.[Warrant.Out.OperationRecord] woor1
			ON woor1.warrantId = wow.ID AND woor1.operation = 0
		LEFT JOIN dbo.[Warrant.Out.OperationRecord] woor2
			ON woor2.warrantId = wow.ID AND woor2.operation = 7
	WHERE wow.ID = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_PayCheck]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_PayCheck]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int,
	@amountPaid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @dateNow datetime = GETDATE()

	DECLARE @bonus_cycle_start int = 15
	
	IF @amountPaid > 0
	BEGIN
		UPDATE dbo.[Warrant.Out.Warrant] SET lastPaidDate = @dateNow
			WHERE ID = @id AND ISNULL(amountPaid, 0) < ISNULL(amountPermit, amountTotal) 
	END

	UPDATE dbo.[Warrant.Out.Warrant] SET amountPaid = ISNULL(amountPaid, 0) + @amountPaid, cookie = @cookieNew
		WHERE ID = @id AND cookie = @cookieOri AND state < 20
			
	DECLARE @RESULT int = @@ROWCOUNT

	UPDATE dbo.[Warrant.Out.Warrant] SET state = 20
		WHERE ID = @id AND state = 14 AND ISNULL(amountPaid, 0) = ISNULL(amountPermit, amountTotal)

	-- find the correct billing cycle
	-- 2014-05-10 -> 2014-04-01
	-- 2014-05-12 -> 2014-05-01
	SET @dateNow = DATEADD(MM, DATEDIFF(MM, 0, DATEADD(DD, -@bonus_cycle_start, @dateNow)), 0)

	INSERT INTO dbo.[Warrant.Bonus] 
		(warrant_id, warrant_type, amount_draft, amount_servicefee, customer_id, repay_delay, attendant_num, year, month) 
		SELECT wow.ID, wow.type, wow.amountTotal - wow.amountServicefee, wow.amountServicefee, wow.target, DATEDIFF(DD, wow.submitDate, wow.lastPaidDate), (SELECT COUNT(*) FROM dbo.[Warrant.Out.Attendant] woa WHERE woa.warrant_id = wow.ID), YEAR(@dateNow), MONTH(@dateNow)
			FROM dbo.[Warrant.Out.Warrant] wow 
			WHERE wow.ID = @id 
				AND (wow.state = 20 AND wow.type IN (0, 1))
				AND (SELECT COUNT(*) FROM dbo.[Warrant.Out.Attendant] woa WHERE woa.warrant_id = @id) > 0
	
	EXEC dbo.[SP_Warrant_Out_UpdatePayState] @id = @id
	
	EXEC dbo.SP_Warrant_Out_AddOperationRecord @operationOperator = @operator, @operation = 11, @isAuthorize = 0, @warrantId = @id, @parameter1 = @amountPaid
		
	SELECT @RESULT
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_Print]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_Print]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.Out.Warrant] SET state = 10, cookie = @cookieNew
		WHERE ID = @id AND cookie = @cookieOri AND state >= 9 AND state <= 10
	
	SELECT @@ROWCOUNT
	
	EXEC dbo.SP_Warrant_Out_AddOperationRecord @operationOperator = @operator, @operation = 9, @isAuthorize = 0, @warrantId = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_PrintCustomerPage]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_PrintCustomerPage]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.Out.Warrant] SET state = 5, cookie = @cookieNew
		WHERE ID = @id AND cookie = @cookieOri AND (state = 5 OR state = 4)
	
	SELECT @@ROWCOUNT
		
	EXEC dbo.SP_Warrant_Out_AddOperationRecord @operationOperator = @operator, @operation = 4, @isAuthorize = 0, @warrantId = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_Read]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_Read]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		ow.ID,
		ow.target,
		ow.state,
		ow.type,
		ow.payState,
		ow.submitDate,
		ow.comment,
		ow.cookie,
		ow.amountTotal,
		ow.amountPermit,
		ow.amountPaid,
		ow.amountCost,
		ow.number
	FROM dbo.[Warrant.Out.Warrant] ow
	WHERE ow.ID = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_ReadItem]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_ReadItem]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
			owi.ID,
			owi.IDDraft,
			owi.IDWarrant,
			owi.price,
			owi.priceCost,
			owi.priceAdjustMannual,
			owi.num, owi.numReturn,
			owi.remark,
			RTRIM(d.Name) AS name,
			RTRIM(d.specification) AS specification,
			RTRIM(d.hiddenSpecification) + '	' + RTRIM(d.usingTarget) AS hiddenSpecification,
			d.priceOutH,
			d.priceOutL,
			RTRIM(d.unit) AS unit
		FROM dbo.[Warrant.Out.Item] owi
		LEFT JOIN dbo.[Draft.Draft] d ON
			d.ID = owi.IDDraft
		WHERE owi.IDWarrant = @id
		ORDER BY owi.ID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_ReadOperationRecord]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_ReadOperationRecord]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT wor.ID, wor.warrantId, wor.operationOperator, wor.operation, wor.operatedTime, wor.parameter1, wor.parameter2, u.name AS operationOperatorName
		FROM dbo.[Warrant.Out.OperationRecord] wor
		LEFT JOIN dbo.[Identity.Identity] u
			ON u.ID = wor.operationOperator
		WHERE wor.warrantId = @id
		ORDER BY wor.operatedTime
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_ReadWarrantAttendant]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_ReadWarrantAttendant]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT woa.attendant_id as id, wa.name
		FROM dbo.[Warrant.Out.Attendant] woa
		LEFT JOIN dbo.[Warrant.Attendants] wa
			ON woa.attendant_id = wa.id
		WHERE woa.warrant_id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_Return]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_Return]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.Out.Warrant] SET state = 8, cookie = @cookieNew
		WHERE ID = @id AND cookie = @cookieOri AND (state = 7 OR state = 8)
	
	SELECT @@ROWCOUNT
	
	EXEC dbo.[SP_Warrant_Out_EditDelAttendant] @id = @id
		
	EXEC dbo.SP_Warrant_Out_AddOperationRecord @operationOperator = @operator, @operation = 7, @isAuthorize = 0, @warrantId = @id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_ReturnItem]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_ReturnItem]	
	-- Add the parameters for the stored procedure here
	@warrantID int,
	@ID int,
	@number int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @num int = (SELECT woi.num FROM dbo.[Warrant.Out.Item] woi WHERE woi.ID = @ID)
    IF @num < @number
    BEGIN
		SELECT -2			--Return More Than Num
	END
	ELSE
	BEGIN
		UPDATE dbo.[Warrant.Out.Item] SET numReturn = @number WHERE ID = @ID AND IDWarrant = @warrantID
		SELECT 1
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_Submit]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_Submit]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int,
	@ifIgnoreSubmitLate int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC dbo.[SP_System_MonthlySave]

    -- Insert statements for procedure here
    
    DECLARE @row int
    
	IF @ifIgnoreSubmitLate = 1
	BEGIN
		UPDATE dbo.[Warrant.Out.Warrant] SET state = 9, cookie = @cookieNew, submitDate = GETDATE() WHERE ID = @id AND cookie = @cookieOri AND (state = 7 Or state = 8)
		SET @row = @@ROWCOUNT
	END
	ELSE
	BEGIN
		IF DATEDIFF(HOUR, (SELECT woor.operatedTime FROM dbo.[Warrant.Out.OperationRecord] woor WHERE woor.operation = 3 AND woor.warrantId = @id), GETDATE()) > 72
		BEGIN
			SELECT -2
		END
		UPDATE dbo.[Warrant.Out.Warrant] SET state = 9, cookie = @cookieNew, submitDate = GETDATE() WHERE ID = @id AND cookie = @cookieOri AND (state = 7 Or state = 8)
		SET @row = @@ROWCOUNT
	END
	
	IF @row = 1
	BEGIN
		EXEC dbo.[SP_Warrant_Out_CustomersAddFrequency] @id = @id
				
		DECLARE @type int = (SELECT type FROM dbo.[Warrant.Out.Warrant] WHERE ID = @id)
		
		MERGE INTO dbo.[Warrant.Out.Item] owi
		USING dbo.[Draft.Draft] d
		ON owi.IDWarrant = @id And d.ID = owi.IDDraft
			WHEN MATCHED THEN
				UPDATE SET owi.priceCost = (d.value / d.numAll) * (owi.num - owi.numReturn);
			
		MERGE INTO dbo.[Draft.Draft] d
		USING dbo.[Warrant.Out.Item] owi
		ON owi.IDWarrant = @id AND owi.IDDraft = d.ID AND d.numAll >=  owi.num
			WHEN MATCHED THEN
				UPDATE SET d.value -= priceCost, d.numAll -= (owi.num - owi.numReturn), d.numAccessiable += owi.numReturn, d.numInHouse += owi.numReturn;

		MERGE INTO dbo.[Warrant.Out.Item] owi
		USING dbo.[Draft.Draft] d
		ON owi.IDWarrant = @id And d.ID = owi.IDDraft
			WHEN MATCHED THEN
				UPDATE SET owi.currentValue = d.value, owi.currentNumAll = d.numAll;
				
		DECLARE @amountTotal int = 
			ISNULL((SELECT SUM((num - numReturn) * ISNULL(priceAdjustMannual, price)) 
				FROM dbo.[Warrant.Out.Item] WHERE IDWarrant = @id), 0)
		SET @amountTotal = @amountTotal / 100 * 100

		DECLARE @amountServicefee int = 
			ISNULL((SELECT SUM((num - numReturn) * ISNULL(priceAdjustMannual, price)) 
				FROM dbo.[Warrant.Out.Item] woi
					LEFT JOIN dbo.[Draft.Draft] d ON d.ID = woi.IDDraft
				WHERE d.is_servicefee = 1 AND IDWarrant = @id), 0)

		DECLARE @amountCost int =
			ISNULL((SELECT SUM(priceCost) 
				FROM dbo.[Warrant.Out.Item] WHERE IDWarrant = @id), 0)

		UPDATE dbo.[Warrant.Out.Warrant]
			SET amountTotal = @amountTotal, amountServicefee = @amountServicefee, amountCost = @amountCost
			WHERE ID = @id
			
		EXEC dbo.SP_Warrant_Out_AddOperationRecord @operationOperator = @operator, @operation = 8, @isAuthorize = 0, @warrantId = @id
			
		EXEC dbo.[SP_Warrant_Out_UpdatePayState] @id = @id
			
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_SubmitCustomerPage]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_SubmitCustomerPage]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.Out.Warrant] SET state = 4, cookie = @cookieNew, submitCustomerPageDate = GETDATE() WHERE ID = @id AND cookie = @cookieOri AND state = 0
	
	IF @@ROWCOUNT = 1
	BEGIN
		MERGE INTO dbo.[Draft.Draft] d
		USING dbo.[Warrant.Out.Item] owi
		ON owi.IDWarrant = @id AND owi.IDDraft = d.ID
			WHEN MATCHED THEN
				UPDATE SET d.numAccessiable -= owi.num;
		
		DECLARE @type int = (SELECT type FROM dbo.[Warrant.Out.Warrant] WHERE ID = @id)
		
		MERGE INTO dbo.[Warrant.Out.Item] owi
		USING dbo.[Draft.Draft] d
		ON owi.IDWarrant = @id And d.ID = owi.IDDraft
			WHEN MATCHED THEN
				UPDATE SET owi.price = ISNULL(owi.priceAdjustMannual, (CASE @type WHEN 1 THEN d.priceOutL ELSE d.priceOutH END));
		
		DECLARE @target int = (SELECT target FROM dbo.[Warrant.Out.Warrant] WHERE ID = @id)
		MERGE INTO dbo.[Draft.SpecialPrice] sp
			USING (SELECT * FROM dbo.[Warrant.Out.Item] owi2 WHERE owi2.IDWarrant = @id AND owi2.priceAdjustMannual > 0 AND @target > 0) owi
				ON (EXISTS(SELECT ID FROM dbo.[Draft.SpecialPrice] WHERE sp.target = @target AND sp.IDDraft = owi.IDDraft)) 
				WHEN MATCHED THEN 
					UPDATE SET sp.priceOutH = owi.priceAdjustMannual, sp.priceOutL = owi.priceAdjustMannual
				WHEN NOT MATCHED THEN
					INSERT(target, IDDraft, priceOutH, priceOutL) VALUES(@target, owi.IDDraft, owi.priceAdjustMannual, owi.priceAdjustMannual);
				
		DECLARE @amountTotal int = 
			ISNULL((SELECT SUM(num * ISNULL(priceAdjustMannual, price)) 
				FROM dbo.[Warrant.Out.Item] WHERE IDWarrant = @id), 0)
		-- Do not remove the change
		-- SET @amountTotal = @amountTotal / 100 * 100

		UPDATE dbo.[Warrant.Out.Warrant]
			SET amountTotal = @amountTotal
			WHERE ID = @id
					
		EXEC dbo.SP_Warrant_Out_AddOperationRecord @operationOperator = @operator, @operation = 3, @isAuthorize = 0, @warrantId = @id
		
		SELECT 1
	END 
	ELSE
	BEGIN 
		SELECT 0
	END
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_SubmitReadForCheck]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_SubmitReadForCheck]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT d.numAccessiable, d.numAll, d.value, d.state, owi.num, owi.price, owi.priceAdjustMannual, owi.numReturn, d.Name, d.specification,
		CASE ow.type WHEN 0 THEN d.priceOutL ELSE d.priceOutH END AS standardPrice,
		CASE ow.type WHEN 0 THEN sp.priceOutL ELSE sp.priceOutH END AS specialPrice
		FROM dbo.[Warrant.Out.Item] owi
		JOIN dbo.[Draft.Draft] d
			ON d.ID = owi.IDDraft
		JOIN dbo.[Warrant.Out.Warrant] ow
			ON ow.ID = @id
		LEFT JOIN dbo.[Draft.SpecialPrice] sp
			ON sp.IDDraft = owi.IDDraft AND sp.target = ow.target
		WHERE owi.IDWarrant = @id
		
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_TakeOut]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_TakeOut]
	-- Add the parameters for the stored procedure here
	@id int,
	@cookieOri int,
	@cookieNew int,
	@operator int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[Warrant.Out.Warrant] SET state = 7, cookie = @cookieNew
		WHERE ID = @id AND cookie = @cookieOri AND state = 5 AND (payState >= 1 AND payState <= 4)
		
	DECLARE @RESULT int = @@ROWCOUNT
	
	MERGE INTO dbo.[Draft.Draft] d
	USING dbo.[Warrant.Out.Item] owi
	ON owi.IDWarrant = @id AND owi.IDDraft = d.ID AND d.numInHouse >=  owi.num
		WHEN MATCHED THEN
			UPDATE SET d.numInHouse -= owi.num;
			
	EXEC dbo.SP_Warrant_Out_AddOperationRecord @operationOperator = @operator, @operation = 6, @isAuthorize = 0, @warrantId = @id
	
	SELECT @RESULT
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_UpdatePayState]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_UpdatePayState]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	DECLARE @AmountPaid int = (SELECT wow.amountPaid FROM dbo.[Warrant.Out.Warrant] wow WHERE ID = @id)
	
	DECLARE @AmountPermit int = (SELECT wow.amountPermit FROM dbo.[Warrant.Out.Warrant] wow WHERE ID = @id) 
	DECLARE @AmountTotal int = (SELECT wow.amountTotal FROM dbo.[Warrant.Out.Warrant] wow WHERE ID = @id) 
	
	DECLARE @AmountShould int
	IF @AmountPermit IS NULL
	BEGIN
		SET @AmountShould = @AmountTotal
	END
	ELSE
	BEGIN
		SET @AmountShould = @AmountPermit
	END
	
	-- PAYSTATE 0 NOTPAID 1 PAID 2 FORCEPASS 3 PARTPAID 4 OVERPAID
	IF @AmountPaid > 0 AND @AmountPaid < @AmountShould
	BEGIN
		UPDATE dbo.[Warrant.Out.Warrant] SET payState = 3 WHERE ID = @id	
	END
	ELSE IF @AmountPaid = @AmountShould
	BEGIN
		UPDATE dbo.[Warrant.Out.Warrant] SET payState = 1 WHERE ID = @id
	END
	ELSE IF @AmountPaid > @AmountShould
	BEGIN
		UPDATE dbo.[Warrant.Out.Warrant] SET payState = 4 WHERE ID = @id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_ViewDetail]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_ViewDetail]
	-- Add the parameters for the stored procedure here
	@id int,
	@startDate dateTime,
	@stopDate dateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SET @startDate = DATEADD(DD, DATEDIFF(DD, 0, @startDate), 0)
	SET @stopDate = DATEADD(DD, DATEDIFF(DD, 0, @stopDate) + 1, 0)
	
	SELECT
		RTRIM(d.Name) AS name,
		RTRIM(d.specification) AS specification,
		owi.IDWarrant,
		owi.num,
		owi.numReturn,
		owi.price,
		owi.priceAdjustMannual,
		owi.priceCost,
		owi.remark,
		ow.submitCustomerPageDate AS dateSubmitCustomerPage,
		ow.submitDate AS dateSubmit,
		owi.currentNumAll,
		owi.currentValue
	FROM dbo.[Warrant.Out.Item] owi
		JOIN dbo.[Warrant.Out.Warrant] ow ON ow.ID = owi.IDWarrant
		JOIN dbo.[Draft.Draft] d ON d.ID = @id
	WHERE owi.IDDraft = @id AND ow.submitDate BETWEEN @startDate AND @stopDate
	ORDER BY ow.submitDate

END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_Out_ViewOneCycle]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_Out_ViewOneCycle]
	-- Add the parameters for the stored procedure here
	@date datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- From March 2019, we switch from 28th business month to calendar month
	-- which means March 1st - March 31st is a month
	--             April 1st - April 30th is a month

	-- Get the latest month that should be calculated
	-- 2013-03-05 -> 2013-03-01
	-- 2013-03-31 -> 2013-03-01
	DECLARE @dateNow datetime = DATEADD(MM, DATEDIFF(MM, 0, @date), 0)

	DECLARE @startDate datetime = @dateNow
	DECLARE @stopDate datetime = DATEADD(MM, 1, @dateNow)
    
	-- The following algorithm is stopped by March (no included), 2019
	-- DECLARE @startDate datetime = DATEADD(DD, 28, DATEADD(MM, -1, @dateNow))
	-- DECLARE @stopDate datetime = DATEADD(DD, 28, @dateNow)

	SELECT
		woa.warrant_id,
		wa.name AS attendant_name
	INTO #attendant_name_temp_table
	FROM [Warrant.Out.Attendant] woa 
	JOIN [Warrant.Attendants] wa 
		ON wa.id = woa.attendant_id

	-- With SQL Server 2017 or newer, I can just use STRING_AGG to aggregate
	--   multiple attendant's name for a particular warrant
	--   For example:
	--        warrant_id,     attendant_id
	--            1                2
	--            1                3
	--            1                4
	--            2                5
	--
	--    Query result is
	--            1              2,3,4
	--            2                5
	--
	--    Further, we can join their names 
	--            1            Alice, Bob, Charlie
	--            2            Devonte
	-- 
	-- However, before SQL Server 2017, STRING_AGG is not supported
	-- The work around is the following:

	SELECT warrant_id, attendant_names = STUFF((SELECT ', ' + attendant_name
								   FROM  #attendant_name_temp_table As P1
								   WHERE P1.warrant_id = P2.warrant_id
								   FOR XML PATH(''), TYPE).value('.[1]', 'varchar(max)'), 1, 2, '')
	INTO #warrant_attendant_names_temp_table
	FROM #attendant_name_temp_table as P2
	GROUP BY P2.warrant_id;

	SELECT
		ow.ID,
		ow.state,
		ow.type,
		ow.payState,
		ow.target,
		ci.name AS target_name,
		ow.submitCustomerPageDate,
		ow.submitDate,
		ow.amountTotal,
		ow.amountPermit,
		ow.amountServicefee,
		ow.amountCost,
		ow.amountPaid,
		attendant_info.attendant_names,
		ow.number
	FROM dbo.[Warrant.Out.Warrant] ow
	LEFT JOIN dbo.[Customers.Information] ci
		ON ci.ID = ow.target
	LEFT JOIN #warrant_attendant_names_temp_table AS attendant_info
		ON attendant_info.warrant_id = ow.ID
	WHERE (ow.submitDate BETWEEN @startDate AND @stopDate) AND ow.state >= 9
	ORDER BY ow.ID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Warrant_SaveAmountForMonthCheck]    Script Date: 11/18/2019 10:13:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery2.sql|7|0|C:\Users\家杰\AppData\Local\Temp\~vs75FE.sql
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Warrant_SaveAmountForMonthCheck]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @dateNow datetime = GETDATE()
	DECLARE @dateLast datetime = (SELECT TOP 1 time FROM dbo.[System.LoginLog] WHERE type = 3 ORDER BY time DESC)

	-- From March 2019, we switch from 28th business month to calendar month
	-- which means March 1st - March 31st is a month
	--             April 1st - April 30th is a month
	
	-- Get the latest month that should be calculated
	SET @dateNow = DATEADD(MM, DATEDIFF(MM, 0, @dateNow) - 1, 0)
	-- Get the latest month that has been calculated
	DECLARE @dateCalMonth datetime = DATEADD(MM, DATEDIFF(MM, 0, @dateLast) - 1, 0)

	-- The following algorithm is stopped by March (no included), 2019
	-- 2013-03-05 -> 2013-02-01
	-- 2013-03-31 -> 2013-03-01
	-- Get the latest month that should be calculated
	-- SET @dateNow = DATEADD(MM, DATEDIFF(MM, 0, DATEADD(DD, -28, @dateNow)), 0)
	-- Get the latest month that has been calculated
	-- DECLARE @dateCalMonth datetime = DATEADD(MM, DATEDIFF(MM, 0, DATEADD(DD, -28, @dateLast)), 0)

	DECLARE @ifAddLog bit = 0
	DECLARE @row nchar(6)
	DECLARE @addRowInstruction nchar(500)
	
	DECLARE @numIn int
	DECLARE @numAdjustIn int
	DECLARE @numSell int
	DECLARE @numCost int
	DECLARE @numService int
	DECLARE @numCostService int
	DECLARE @numGift int
	DECLARE @numCostGift int
	DECLARE @numAdjustOut int

	WHILE @dateCalMonth < @dateNow
	BEGIN

		SET @dateCalMonth = DATEADD(MM, 1, @dateCalMonth)
		DECLARE @dateCalStart datetime = @dateCalMonth
		DECLARE @dateCalStop datetime = DATEADD(MM, 1, @dateCalMonth)
		DECLARE @year int = DATEPART(YEAR, @dateCalMonth)
		DECLARE @month int = DATEPART(MONTH, @dateCalMonth)

		SET @numIn = 0
		SET @numAdjustIn = 0
		SET @numSell = 0
		SET @numCost = 0
		SET @numService = 0
		SET @numCostService = 0
		SET @numGift = 0
		SET @numCostGift = 0
		SET @numAdjustOut = 0
		
		SET @numIn = ISNULL((SELECT SUM(iw.amountIn) 
			FROM dbo.[Warrant.In.Warrant] iw
			WHERE iw.submitDate BETWEEN @dateCalStart AND @dateCalStop
				AND iw.type = 3 AND iw.state >= 9), 0)
		
		SET @numAdjustIn = ISNULL((SELECT SUM(iw.amountIn) 
			FROM dbo.[Warrant.In.Warrant] iw
			WHERE iw.submitDate BETWEEN @dateCalStart AND @dateCalStop
				AND iw.type = 5 AND iw.state >= 9), 0)
				
		SET @numCost = ISNULL((SELECT SUM(amountCost)
			FROM dbo.[Warrant.Out.Warrant] ow
			WHERE ow.submitDate BETWEEN @dateCalStart AND @dateCalStop
				AND (ow.type = 0 OR ow.type = 1) AND ow.state >= 9), 0)
				
		SET @numService = ISNULL((SELECT SUM(ISNULL(amountPermit, amountTotal))
			FROM dbo.[Warrant.Out.Warrant] ow
			WHERE ow.submitDate BETWEEN @dateCalStart AND @dateCalStop
				AND ow.type = 2 AND ow.state >= 9), 0)
		SET @numCostService = ISNULL((SELECT SUM(amountCost)
			FROM dbo.[Warrant.Out.Warrant] ow
			WHERE ow.submitDate BETWEEN @dateCalStart AND @dateCalStop
				AND ow.type = 2 AND ow.state >= 9), 0)
				
		SET @numGift = ISNULL((SELECT SUM(ISNULL(amountPermit, amountTotal))
			FROM dbo.[Warrant.Out.Warrant] ow
			WHERE ow.submitDate BETWEEN @dateCalStart AND @dateCalStop
				AND ow.type = 4 AND ow.state >= 9), 0)
		SET @numCostGift = ISNULL((SELECT SUM(amountCost)
			FROM dbo.[Warrant.Out.Warrant] ow
			WHERE ow.submitDate BETWEEN @dateCalStart AND @dateCalStop
				AND ow.type = 4 AND ow.state >= 9), 0)
				
		SET @numAdjustOut = ISNULL((SELECT SUM(amountCost)
			FROM dbo.[Warrant.Out.Warrant] ow
			WHERE ow.submitDate BETWEEN @dateCalStart AND @dateCalStop
				AND ow.type = 6 AND ow.state >= 9), 0)
					
		INSERT INTO dbo.[Warrant.Amount] (year, month, amountIn, amountAdjustIn, amountCost, amountService, amountCostService, amountGift, amountCostGift, amountAdjustOut) 
			VALUES (@year, @month, @numIn, @numAdjustIn, @numCost, @numService, @numCostService, @numGift, @numCostGift, @numAdjustOut)

		SET @ifAddLog = 1
	END
	IF @ifAddLog = 1 
	BEGIN
		EXEC dbo.[SP_System_AddLoginLog] @type = 3
	END
END
GO
