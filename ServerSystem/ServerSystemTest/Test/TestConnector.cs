﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SharedObject.Main.Transmission;

namespace ServerSystem.Connector.Test
{
    public class TestConnector : Connector
    {
        public TestConnector(byte[] instrData, List<Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            : base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.test:
                    break;
                default:
                    throw new Exception();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            switch (mainInstruction)
            {
                case Instruction.test:
                    StreamWriter writer = new StreamWriter("TransmissionTest.txt", true);
                    writer.WriteLine("成功收到长度为" + ((int)obj + 16) + "（不包括指令16位）的测试数据");
                    writer.Close();
                    this.Dispose();
                    break;
                default:
                    throw new Exception();
            }
            return new AWithOperatedState();
        }
    }
}
