﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerSystem.RightsControl.Action
{
    public class RightsControlAction<T> : ServerSystem.Action.Action<T>
    {
        protected new ERPSystemEntities entities = new ERPSystemEntities();

        public RightsControlAction(SharedObject.Main.Transmission.Instruction mainInstruction, SharedObject.Identity.Stuff.IdentityMark identity)
            : base(SharedObject.Normal.SystemID.RightsControl, mainInstruction, identity)
        {
            base.entities = new ERPSystemEntities();
            this.entities = (ERPSystemEntities)base.entities;
        }

        protected override T mainMethod() { return default(T); }

        protected bool getInfo(int id, out int state, out int editor, out int cookieOriRead)
        {
            //bool t = false;
            state = -1;
            editor = 0;
            cookieOriRead = -1;
           /* SP_Customer_GetInfo_Result getInfoResult = (from result in entities.SP_RightsControl_GetInfo(id) select result).First();
            if (getInfoResult != null)
            {
                cookieOriRead = getInfoResult.cookie == null ? -1 : getInfoResult.cookie.Value;
                state = getInfoResult.state;
                editor = getInfoResult.creator == null ? 0 : getInfoResult.creator.Value;
                return true;
            }*/
            return false;
        }

        public RightState CheckIdentityRightsSafely(SharedObject.Identity.Stuff.IdentityMark identity, getRightsInstruction instruction)
        {
            RightState rightState = RightState.Level0;
            if (identity == null)
                return rightState;
            byte? rightResult = entities.SP_RightsControl_GetRights(identity.ID, (int)instruction).First();
            if (rightResult == null)
                rightState = RightState.Level0;
            else if (rightResult > 0 && rightResult < 4)
                rightState = (RightState)rightResult.Value;
            return rightState;
        }

        public enum getRightsInstruction
        {
            rLogIn = 0,
            rGetSpecialOne = 1,
            rAdd = 2,
            rEdit = 3,
            rResetPassword = 4
        }
    }
}
