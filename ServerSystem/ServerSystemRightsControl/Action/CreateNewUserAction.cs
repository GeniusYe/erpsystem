﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.RightsControl.Stuff;
using ServerSystem.RightsControl.Service;

namespace ServerSystem.RightsControl.Action
{
    public class CreateNewUserAction : RightsControlAction<Object>
    {
        public User user { get; set; }

        public CreateNewUserAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.rightsAddUser:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override Object mainMethod()
        {
            if (user == null)
                throw new ParameterNotMatchException();
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.rightsAddUser:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rAdd);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            else if (rightState < RightState.Level3)
            {
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
                throw new NotImplementedException();
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            CreateNewUserService service = new CreateNewUserService(entities);
            switch (mainInstruction)
            {
                case Instruction.rightsAddUser:
                    service.addUserService(identity, user);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = service.operatedState;
            return null;
        }
    }
}
