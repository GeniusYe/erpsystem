﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using SharedObject.Normal;
using SharedObject.Main.Transmission;
using SharedObject.RightsControl.Stuff;
using SharedObject.RightsControl.Transmission;

namespace ServerSystem.RightsControl.Service
{
    class CreateNewUserService : Service
    {
        public CreateNewUserService(ERPSystemEntities entities)
            : base(entities) { }

        public void addUserService(SharedObject.Identity.Stuff.IdentityMark identity, User user)
        {
            if (user == null)
                throw new ParameterNotMatchException();
            int? i = entities.SP_RightsControl_Add(user.name, user.username, user.password, (byte)(user.passwordLogin == true ? 1 : 0), (byte)(user.passwordAuthorize == true ? 1 : 0), user.cardInformation, (byte)(user.cardLogin == true ? 1 : 0), (byte)(user.cardAuthorize == true ? 1 : 0)).First();
            if (i > 0)
                operatedState.state = OperatedState.OperatedStateEnum.success;
            else
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
        }
    }
}
