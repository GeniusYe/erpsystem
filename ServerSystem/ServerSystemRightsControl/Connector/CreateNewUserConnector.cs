﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.RightsControl.Stuff;
using SharedObject.RightsControl.Transmission;
using ServerSystem.RightsControl.Action;

namespace ServerSystem.RightsControl.Connector
{
    public class CreateNewUserConnector : ServerSystem.Connector.Connector
    {
        public CreateNewUserConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            : base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case SharedObject.Main.Transmission.Instruction.rightsAddUser:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            TCSUser tCSUser;
            if (obj is TCSUser)
                tCSUser = (TCSUser)obj;
            else
                throw new ParameterNotMatchException();

            switch (mainInstruction)
            {
                case Instruction.rightsAddUser:
                    RightsControl.Action.CreateNewUserAction action = new CreateNewUserAction(identity, mainInstruction);
                    action.user = tCSUser.user;
                    action.execute();
                    this.operatedState = action.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            return new AWithOperatedState();
        }
    }
}
