﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseManage.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ServerSystem.WareHouseManage.Action
{
    class ControlOneDraftAction : WareHouseManageAction<Object>
    {
        public DraftCSControl draft { get; set; }

        public ControlOneDraftAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.draftSubmit:
                case Instruction.draftDel:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override Object mainMethod()
        {
            if (draft == null)
                throw new ParameterNotMatchException();
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.draftSubmit:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rSubmit);
                    break;
                case Instruction.draftDel:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rDel);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            else if (rightState == RightState.Level1)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
            else
            {
                int state, cookieOriRead, creator;
                getInfo(draft.id, out state, out creator, out cookieOriRead);
                if (mainInstruction == Instruction.draftSubmit && state != 0)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.draftAlreadySubmitted);
                if (rightState == RightState.Level1)
                {
                    if (creator != identity.ID)
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
                }
                if (cookieOriRead != draft.cookie)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.cookieWrong);
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            ControlOneDraftService service = new ControlOneDraftService(entities);
            switch (mainInstruction)
            {
                case Instruction.draftSubmit:
                    service.submitDraft(identity, draft);
                    break;
                case Instruction.draftDel:
                    service.delDraft(draft);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = service.operatedState;
            return null;
        }
    }
}
