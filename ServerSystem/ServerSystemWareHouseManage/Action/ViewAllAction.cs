﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseManage.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.WareHouseManage.Transmission;

namespace ServerSystem.WareHouseManage.Action
{
    class ViewAllAction : WareHouseManageAction<List<ViewAllDraft>>
    {
        public int rangeType { get; set; }
        public int range { get; set; }
        public DateTime date { get; set; }

        public ViewAllAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.draftViewAll:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override List<ViewAllDraft> mainMethod()
        {
            List<ViewAllDraft> viewAllDraft;
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.draftViewAll:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rViewAll);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
            {
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
                return null;
            }
            else if (rightState == RightState.Level1)
                if (date != default(DateTime))
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.draftCannotLoadPreviousInformation);
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            bool numDisplay, priceInDisplay;
            RightState numRightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rDisplayNum);
            RightState priceInRightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rDisplayPrice);
            if (numRightState != RightState.Level0)
                numDisplay = true;
            else
                numDisplay = false;
            if (priceInRightState != RightState.Level0)
                priceInDisplay = true;
            else
                priceInDisplay = false;
            switch (mainInstruction)
            {
                case Instruction.draftViewAll:
                    ViewAllService service = new ViewAllService(entities);
                    service.numDisplay = numDisplay;
                    service.priceDisplay = priceInDisplay;
                    service.rangeType = rangeType;
                    service.range = range;
                    service.date = date;
                    viewAllDraft = service.getViewDraftList(identity);
                    this.operatedState = service.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.success)
                operatedState.state = OperatedState.OperatedStateEnum.normal;
            return viewAllDraft;
        }
    }
}
