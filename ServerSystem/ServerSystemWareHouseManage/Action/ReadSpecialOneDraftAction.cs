﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseManage.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.WareHouseManage.Transmission;

namespace ServerSystem.WareHouseManage.Action
{
    class ReadSpecialOneDraftAction : WareHouseManageAction<Draft>
    {
        public int id { get; set; }

        public ReadSpecialOneDraftAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.draftSpecialOne:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override Draft mainMethod()
        {
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.draftSpecialOne:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rGetSpecialOne);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
            {
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
                return null;
            }
            else if (rightState == RightState.Level1)
            {
                int state, cookieOriRead, editor;
                bool t = true;
                getInfo(id, out state, out editor, out cookieOriRead);
                if (editor != identity.ID)
                {
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
                    t = false;
                }
                if (!t)
                    return null;
            }
            bool numDisplay, priceInDisplay;
            RightState numRightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rDisplayNum);
            RightState priceInRightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rDisplayPrice);
            if (numRightState != RightState.Level0)
                numDisplay = true;
            else
                numDisplay = false;
            if (priceInRightState != RightState.Level0)
                priceInDisplay = true;
            else
                priceInDisplay = false;
            Draft draft;
            switch (mainInstruction)
            {
                case Instruction.draftSpecialOne:
                    ReadSpecialOneDraftService service = new ReadSpecialOneDraftService(entities);
                    draft = service.getSpecialOneDraft(id, identity, numDisplay, priceInDisplay);
                    this.operatedState = service.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            return draft;
        }
    }
}
