﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseManage.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ServerSystem.WareHouseManage.Action
{
    class ReadDraftListAction : WareHouseManageAction<List<DefaultDraft>>
    {

        public int id { get; set; }
        public string searchStr { get; set; }

        public ReadDraftListAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.draftDisplayDraftList:
                case Instruction.draftDisplayDraftListSearch:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override List<DefaultDraft> mainMethod()
        {
            if (id == 0 && searchStr == null)
                throw new ParameterNotMatchException();
            List<DefaultDraft> draftList = null;
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.draftDisplayDraftList:
                case Instruction.draftDisplayDraftListSearch:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rLogIn);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
            {
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
                draftList = null;
            }
            else
            {
                ReadDraftListService service = new ReadDraftListService(entities);
                switch (mainInstruction)
                {
                    case Instruction.draftDisplayDraftList:
                        draftList = service.getDefaultDraft(id);
                        break;
                    case Instruction.draftDisplayDraftListSearch:
                        draftList = service.getDefaultDraftSearch(searchStr);
                        break;
                }
                this.operatedState = service.operatedState;
            }
            return draftList;
        }
    }
}
