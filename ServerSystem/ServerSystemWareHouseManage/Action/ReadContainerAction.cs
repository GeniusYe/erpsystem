﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseManage.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ServerSystem.WareHouseManage.Action
{
    class ReadContainerAction : WareHouseManageAction<List<DraftContainer>>
    {
        private int depth;
        public int id { get; set; }

        public ReadContainerAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            :base(Instruction.NotSet, identity)
        {
            this.mainInstruction = instruction;
            switch (mainInstruction)
            {
                case Instruction.draftContainer1List:
                    this.depth = 0;
                    break;
                case Instruction.draftContainer2List:
                    this.depth = 1;
                    break;
                case Instruction.draftContainer3List:
                    this.depth = 2;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override List<DraftContainer> mainMethod()
        {
            List<DraftContainer> draftContainer;
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.draftContainer1List:
                case Instruction.draftContainer2List:
                case Instruction.draftContainer3List:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rLogIn);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            ReadContainerService service = new ReadContainerService(entities);
            draftContainer = service.getContainer(depth, id);
            this.operatedState = service.operatedState;
            return draftContainer;
        }
    }
}
