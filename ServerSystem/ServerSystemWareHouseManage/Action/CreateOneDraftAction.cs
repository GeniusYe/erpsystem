﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseManage.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ServerSystem.WareHouseManage.Action
{
    class CreateOneDraftAction : WareHouseManageAction<Object>
    {
        public Draft draft { get; set; }

        public CreateOneDraftAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.draftAdd:
                case Instruction.draftEdit:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override Object mainMethod()
        {
            if (draft == null)
                throw new ParameterNotMatchException();
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.draftAdd:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rAdd);
                    break;
                case Instruction.draftEdit:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rEdit);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            else
            {
                if (mainInstruction == Instruction.draftEdit)
                {
                    int state, cookieOriRead, editor;
                    getInfo(draft.id, out state, out editor, out cookieOriRead);
                    if (rightState < RightState.Level3)
                    {
                        if (state != 0 &&
                            (this.draft.specification != null ||
                             this.draft.hiddenSpecification != null ||
                             this.draft.name != null))
                        {
                            operatedState.addStates(
                                OperatedState.OperatedConfirmingEnum.draftEditSubmitDraftConsistentInfo);
                        }

                        if (state != 0 &&
                            (this.draft.priceOutH != 0 || this.draft.priceOutL != 0))
                        {
                            operatedState.addStates(
                                OperatedState.OperatedConfirmingEnum.draftEditSubmitDraftPriceChange);
                        }
                    }
                    if (cookieOriRead != draft.cookie)
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.cookieWrong);
                }
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            bool numDisplay, priceInDisplay;
            RightState numRightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rDisplayNum);
            RightState priceInRightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rDisplayPrice);
            if (numRightState != RightState.Level0)
                numDisplay = true;
            else
                numDisplay = false;
            if (priceInRightState != RightState.Level0)
                priceInDisplay = true;
            else
                priceInDisplay = false;
            CreateOneDraftService service = new CreateOneDraftService(entities);
            switch (mainInstruction)
            {
                case Instruction.draftAdd:
                    service.addDraft(draft, identity, numDisplay, priceInDisplay);
                    break;
                case Instruction.draftEdit:
                    service.editDraft(draft, identity, numDisplay, priceInDisplay);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = service.operatedState;
            return null;
        }
    }
}
