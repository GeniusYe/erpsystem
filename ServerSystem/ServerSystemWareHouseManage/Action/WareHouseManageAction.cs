﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerSystem.WareHouseManage.Action
{
    public class WareHouseManageAction<T> : ServerSystem.Action.Action<T>
    {
        protected new ERPSystemEntities entities;

        public WareHouseManageAction(SharedObject.Main.Transmission.Instruction mainInstruction, SharedObject.Identity.Stuff.IdentityMark identity)
            : base(SharedObject.Normal.SystemID.WareHouseManage, mainInstruction, identity)
        {
            base.entities = new ERPSystemEntities();
            this.entities = (ERPSystemEntities)base.entities;
        }

        protected override T mainMethod() { return default(T); }

        protected bool getInfo(int id, out int state, out int editor, out int cookieOriRead)
        {
            state = -1;
            editor = 0;
            cookieOriRead = -1;
            SP_Draft_GetInfo_Result getInfoResult = entities.SP_Draft_GetInfo(id).First();
            if (getInfoResult != null)
            {
                cookieOriRead = getInfoResult.cookie;
                state = getInfoResult.state;
                editor = getInfoResult.editor == null ? 0 : getInfoResult.editor.Value;
                return true;
            }
            return false;
        }

        public RightState CheckIdentityRightsSafely(SharedObject.Identity.Stuff.IdentityMark identity, getRightsInstruction instruction)
        {
            RightState rightState = RightState.Level0;
            if (identity == null)
                return rightState;
            byte? rightResult = entities.SP_Draft_GetRights(identity.ID, (int)instruction).First();
            if (rightResult == null)
                rightState = RightState.Level0;
            else if (rightResult > 0 && rightResult < 4)
                rightState = (RightState)rightResult.Value;
            return rightState;
        }

        public enum getRightsInstruction
        {
            rLogIn = 0,
            rGetSpecialOne = 1,
            rAdd = 2,
            rEdit = 3,                  //1-himself 2-himself and edit location or something    3-edit name specification 
            rSubmit = 4,
            rDisplayPrice = 5,
            rDisplayNum = 6,
            rViewAll = 7,
            rDel = 8
        }
    }
}
