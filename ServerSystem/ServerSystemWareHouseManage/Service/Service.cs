﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerSystem.WareHouseManage.Service
{
    class Service : ServerSystem.Service.Service 
    {
        protected new ERPSystemEntities entities;

        public Service(ERPSystemEntities entities)
            : base()
        {
            this.entities = entities;
        }
    }
}
