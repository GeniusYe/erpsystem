﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ServerSystem.WareHouseManage.Service
{
    class ControlOneDraftService : Service
    {
        public ControlOneDraftService(ERPSystemEntities entities)
            : base(entities) { }

        /// <summary>
        /// submit a draft to SQL server
        /// </summary>
        public void submitDraft(SharedObject.Identity.Stuff.IdentityMark identity, DraftCSControl draft)
        {
            if (draft != null && draft.id > 0)
            {
                int id = draft.id;
                int cookieOri = draft.cookie, cookieNew;
                cookieNew = generateNewCookie(cookieOri);
                int? i = entities.SP_Draft_Submit(id, cookieOri, cookieNew, identity.ID).First();
                if (i == 1)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        public void delDraft(DraftCSControl draft)
        {
            if (draft != null && draft.id > 0)
            {
                int id = draft.id;
                int cookieOri = draft.cookie, cookieNew;
                cookieNew = generateNewCookie(cookieOri);
                SP_Draft_Read_Result draft_result = entities.SP_Draft_Read(id).First();
                int numAccessiable = draft_result.numAccessiable;
                int num = draft_result.numAll;
                if (num != 0 || numAccessiable != 0)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.draftNumNotZero);
                if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                    return;
                int? i = entities.SP_Draft_Del(id, cookieOri, cookieNew).First();
                if (i == 1)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }
    }
}
