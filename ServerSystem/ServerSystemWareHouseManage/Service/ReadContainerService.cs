﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ServerSystem.WareHouseManage.Service
{
    class ReadContainerService : Service
    {
        public ReadContainerService(ERPSystemEntities entities)
            : base(entities) { }

        public List<DraftContainer> getContainer(int depth, int i)
        {
            List<DraftContainer> tempDraftContainerList = new List<DraftContainer>();
            operatedState.state = SharedObject.Main.Transmission.OperatedState.OperatedStateEnum.normal;
            IEnumerable<SP_Draft_ReadContainer_Result> spContainers = entities.SP_Draft_ReadContainer(depth, i);
            foreach (SP_Draft_ReadContainer_Result spContainer in spContainers)
            {
                DraftContainer container = new DraftContainer() { id = spContainer.ID, name = spContainer.Name };
                tempDraftContainerList.Add(container);
            }
            operatedState.state = OperatedState.OperatedStateEnum.success;
            return tempDraftContainerList;
        }

    }
}
