﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ServerSystem.WareHouseManage.Service
{
    class ReadSpecialOneDraftService : Service
    {
        public ReadSpecialOneDraftService(ERPSystemEntities entities)
            : base(entities) { }

        /// <summary>
        /// get special one draft from SQL server
        /// </summary>
        public Draft getSpecialOneDraft(int i, SharedObject.Identity.Stuff.IdentityMark identity, bool numDisplay, bool priceDisplay)
        {
            Draft draft = new Draft();
            var result = (from spDraft in entities.SP_Draft_Read(i)
                          select new Draft()
                          {
                              id = spDraft.ID,
                              name = spDraft.name,
                              belongsTo = spDraft.belongsTo,
                              belongsToString = spDraft.belongsToString,
                              state = spDraft.state,
                              specification = spDraft.specification,
                              location = spDraft.location,
                              usingTarget = spDraft.usingTarget,
                              priceOutH = spDraft.priceOutH,
                              priceOutL = spDraft.priceOutL,
                              hiddenSpecification = spDraft.hiddenSpecification,
                              cookie = spDraft.cookie,
                              numAccessiable = numDisplay ? spDraft.numAccessiable : 0,
                              numAll = numDisplay ? spDraft.numAll : 0,
                              numWarning = numDisplay ? spDraft.numWarning : 0,
                              value = priceDisplay ? spDraft.value : 0,
                              is_servicefee = spDraft.is_servicefee,
                              unit = spDraft.unit,
                          });
            try
            {
                draft = result.First();
                ReadOperationRecord(draft);
                operatedState.state = SharedObject.Main.Transmission.OperatedState.OperatedStateEnum.normal;
            }
            catch
            {
                operatedState.addStates(SharedObject.Main.Transmission.OperatedState.OperatedConfirmingEnum.draftNotFound);
            }
            return draft;
        }

        private void ReadOperationRecord(Draft draft)
        {
            IEnumerable<SP_Draft_ReadOperationRecord_Result> spOperatedRecordList = entities.SP_Draft_ReadOperationRecord(draft.id);
            draft.operationRecord = new List<DraftOperationRecord>();
            foreach (SP_Draft_ReadOperationRecord_Result spOperatedResult in spOperatedRecordList)
            {
                DraftOperationRecord record = new DraftOperationRecord() { ID = spOperatedResult.ID, draftId = spOperatedResult.draftId, operation = (DraftOperationEnum)spOperatedResult.operation, operationOperator = spOperatedResult.operationOperator, operationOperatorName = spOperatedResult.operationOperatorName, operatedTime = spOperatedResult.operatedTime };
                draft.operationRecord.Add(record);
            }
        }
    }
}
