﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ServerSystem.WareHouseManage.Service
{
    class ReadDraftListService : Service
    {
        public ReadDraftListService(ERPSystemEntities entities)
            : base(entities) { }

        /// <summary>
        /// get DefaultDraft from SQL server
        /// </summary>
        public List<DefaultDraft> getDefaultDraft(int i)
        {
            IEnumerable<DefaultDraft> spDefaultDraft = 
                from result in entities.SP_Draft_GetDefaultOnes(i)
                select new DefaultDraft() {
                    id = result.ID,
                    name = result.name,
                    belongsTo = result.belongsTo,
                    state = result.state,
                    specification = result.specification,
                    hiddenSpecification = result.hiddenSpecification,
                    priceOutH = result.priceOutH,
                    priceOutL = result.priceOutL,
                    cookie = result.cookie,
                    numAccessiable = result.numAccessiable,
                    numAll = result.numAll
                };
            List<DefaultDraft> draftList = new List<DefaultDraft>();
            foreach (DefaultDraft defaultDraft in spDefaultDraft)
                draftList.Add(defaultDraft);
            operatedState.state = OperatedState.OperatedStateEnum.success;
            return draftList;
        }
        /// <summary>
        /// get DefaultDraft from SQL server
        /// </summary>
        public List<DefaultDraft> getDefaultDraftSearch(string searchString)
        {
            string searchStringTrim = searchString.Trim();
            int i;
            if (searchStringTrim.Length >= 2 || Int32.TryParse(searchStringTrim, out i))
            {
                IEnumerable<DefaultDraft> spDefaultDraft =
                    from result in entities.SP_Draft_GetDefaultOnesBySearching(searchStringTrim)
                        select new DefaultDraft() {
                            id = result.ID,
                            name = result.name,
                            belongsTo = result.belongsTo,
                            state = result.state,
                            specification = result.specification,
                            hiddenSpecification = result.hiddenSpecification,
                            priceOutH = result.priceOutH,
                            priceOutL = result.priceOutL,
                            cookie = result.cookie,
                            numAccessiable = result.numAccessiable,
                            numAll = result.numAll
                        };
                List<DefaultDraft> draftList = new List<DefaultDraft>();
                foreach (DefaultDraft defaultDraft in spDefaultDraft)
                    draftList.Add(defaultDraft);
                operatedState.state = OperatedState.OperatedStateEnum.success;
                return draftList;
            }
            else
                throw new ParameterNotMatchException();
        }
    }
}
