﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Normal;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ServerSystem.WareHouseManage.Service
{
    class CreateOneDraftService : Service 
    {
        public CreateOneDraftService(ERPSystemEntities entities)
            : base(entities) { }

        /// <summary>
        /// update a draft to SQL server
        /// </summary>
        public void editDraft(Draft draft, SharedObject.Identity.Stuff.IdentityMark identity, bool numDisplay, bool priceDisplay)
        {
            if (draft != null && draft.id > 0)
            {
                int cookieOri = draft.cookie;
                int? i = entities.SP_Draft_Edit(
                        draft.id,
                        cookieOri,
                        generateNewCookie(cookieOri),
                        draft.location,
                        draft.usingTarget,
                        identity.ID,
                        draft.name,
                        draft.specification,
                        draft.hiddenSpecification,
                        draft.numWarning,
                        priceDisplay == true ? draft.priceOutH : (int?)null,
                        priceDisplay == true ? draft.priceOutL : (int?)null,
                        draft.is_servicefee).First();
                if (i == 1)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        /// <summary>
        /// add a new draft to SQL server
        /// </summary>
        public void addDraft(Draft draft, SharedObject.Identity.Stuff.IdentityMark identity, bool numDisplay, bool priceDisplay)
        {
            if (draft != null)
            {
                int? i = entities.SP_Draft_Add(
                    generateNewCookie(0),
                    draft.name,
                    draft.specification,
                    draft.hiddenSpecification,
                    draft.belongsTo,
                    draft.location,
                    draft.usingTarget,
                    identity.ID,
                    draft.numWarning,
                    priceDisplay == true ? draft.priceOutH : (int?)null,
                    priceDisplay == true ? draft.priceOutL : (int?)null,
                    draft.is_servicefee,
                    draft.unit).First();
                if (i > 0)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }
    }
}
