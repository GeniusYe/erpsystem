﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace ServerSystem.WareHouseManage.Service
{
    class ViewAllService : Service
    {
        public bool numDisplay { get; set; }
        public bool priceDisplay { get; set; }
        public int rangeType { get; set; }
        public int range { get; set; }
        public DateTime? date { get; set; }

        public ViewAllService(ERPSystemEntities entities)
            : base(entities) { }

        public List<ViewAllDraft> getViewDraftList(SharedObject.Identity.Stuff.IdentityMark identity)
        {
            ViewAllDraft draft;
            List<ViewAllDraft> viewDraftList = null;
            IEnumerable<SP_Draft_View_All_Result> spViewAllDraft = null;
            if (date == default(DateTime)) date = null;
            if (rangeType == 3)
                if (range >= 0)
                {
                    spViewAllDraft = entities.SP_Draft_ViewAll3(range, date);
                }
                else
                    throw new Exception();
            else if (rangeType == 2)
                if (range >= 0)
                {
                    spViewAllDraft = entities.SP_Draft_ViewAll2(range, date);
                }
                else
                    throw new Exception();
            else if (rangeType == 1)
                if (range >= 0)
                {
                    spViewAllDraft = entities.SP_Draft_ViewAll1(range, date);
                }
                else
                    throw new Exception();
            else if (rangeType == 0)
                spViewAllDraft = entities.SP_Draft_ViewAll(date);
            else
                throw new Exception();

            viewDraftList = new List<ViewAllDraft>();
            foreach (SP_Draft_View_All_Result spDraft in spViewAllDraft)
            {
                draft = new ViewAllDraft() {
                    id = spDraft.ID,
                    name = spDraft.name,
                    state = spDraft.state,
                    specification = spDraft.specification,
                    hiddenSpecification = spDraft.hiddenSpecification,
                    unit = spDraft.unit,
                    priceOutH = spDraft.priceOutH,
                    priceOutL = spDraft.priceOutL,
                };
                if (!numDisplay)
                    draft.numAll = 0;
                else
                    draft.numAll = spDraft.numAll;
                if (!priceDisplay)
                    draft.value = 0;
                else
                    draft.value = spDraft.value;
                viewDraftList.Add(draft);
            }
            operatedState.state = SharedObject.Main.Transmission.OperatedState.OperatedStateEnum.success;
            return viewDraftList;
        }
    }
}
