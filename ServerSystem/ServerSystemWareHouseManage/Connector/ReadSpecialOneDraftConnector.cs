﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseManage.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.WareHouseManage.Transmission;

namespace ServerSystem.WareHouseManage.Connector
{
    public class ReadSpecialOneDraftConnector : ServerSystem.Connector.Connector
    {
        public ReadSpecialOneDraftConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.draftSpecialOne:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            Draft draft;

            WareHouseManage.Action.ReadSpecialOneDraftAction action = new ReadSpecialOneDraftAction(identity, mainInstruction);

            switch (mainInstruction)
            {
                case Instruction.draftSpecialOne:
                    action.id = id;
                    draft = action.execute();
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            TSCDraft returnObj = new TSCDraft();
            returnObj.draft = draft;
            this.operatedState = action.operatedState; 
            return returnObj;
        }
    }
}
