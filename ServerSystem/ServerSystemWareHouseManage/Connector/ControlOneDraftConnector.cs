﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseManage.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.WareHouseManage.Transmission;

namespace ServerSystem.WareHouseManage.Connector
{ 
    public class ControlOneDraftConnector : ServerSystem.Connector.Connector
    {
        public ControlOneDraftConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.draftSubmit:
                case Instruction.draftDel:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            TCSDraftControl tCSDraftControl;

            if (obj is TCSDraftControl)
                tCSDraftControl = (TCSDraftControl)obj;
            else
                throw new ParameterNotMatchException();

            WareHouseManage.Action.ControlOneDraftAction action = new ControlOneDraftAction(identity, mainInstruction);

            switch (mainInstruction)
            {
                case Instruction.draftSubmit:
                case Instruction.draftDel:
                    action.draft = tCSDraftControl.draft;
                    action.execute();
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = action.operatedState;
            return new AWithOperatedState();
        }
    }
}
