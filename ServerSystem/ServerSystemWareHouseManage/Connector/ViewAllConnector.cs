﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseManage.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.WareHouseManage.Transmission;

namespace ServerSystem.WareHouseManage.Connector
{
    public class ViewAllConnector : ServerSystem.Connector.Connector
    {
        public ViewAllConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.draftViewAll:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            TCSViewAll tCSViewAll;

            if (obj is TCSViewAll)
                tCSViewAll = (TCSViewAll)obj;
            else
                throw new ParameterNotMatchException();

            WareHouseManage.Action.ViewAllAction action = new ViewAllAction(identity, mainInstruction);

            TSCViewAll returnObj = new TSCViewAll();
            switch (mainInstruction)
            {
                case Instruction.draftViewAll:
                    action.range = tCSViewAll.range;
                    action.rangeType = tCSViewAll.rangeType;
                    action.date = tCSViewAll.date;
                    returnObj.viewAllDraft = action.execute();
                    this.operatedState = action.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            return returnObj;
        }
    }
}
