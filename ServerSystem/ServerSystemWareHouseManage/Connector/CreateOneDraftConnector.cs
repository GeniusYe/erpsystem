﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseManage.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.WareHouseManage.Transmission;

namespace ServerSystem.WareHouseManage.Connector
{
    public class CreateOneDraftConnector : ServerSystem.Connector.Connector
    {
        public CreateOneDraftConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.draftAdd:
                case Instruction.draftEdit:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            TCSDraftCreate tCSDraftCreate;

            if (obj is TCSDraftCreate)
                tCSDraftCreate = (TCSDraftCreate)obj;
            else
                throw new ParameterNotMatchException();

            WareHouseManage.Action.CreateOneDraftAction action = new CreateOneDraftAction(identity, mainInstruction);

            switch (mainInstruction)
            {
                case Instruction.draftAdd:
                case Instruction.draftEdit:
                    action.draft = tCSDraftCreate.draft;
                    action.execute();
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = action.operatedState;
            return new AWithOperatedState();
        }
    }
}
