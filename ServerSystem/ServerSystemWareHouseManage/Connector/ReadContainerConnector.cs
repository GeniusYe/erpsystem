﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseManage.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.WareHouseManage.Transmission;

namespace ServerSystem.WareHouseManage.Connector
{
    public class ReadContainerConnector : ServerSystem.Connector.Connector
    {
        public ReadContainerConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.draftContainer1List:
                case Instruction.draftContainer2List:
                case Instruction.draftContainer3List:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }
        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            WareHouseManage.Action.ReadContainerAction action = new ReadContainerAction(identity, mainInstruction);
            switch (mainInstruction)
            {
                case Instruction.draftContainer1List:
                    action.id = 0;
                    break;
                case Instruction.draftContainer2List:
                case Instruction.draftContainer3List:
                    action.id = id;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            TSCDraftContainerList returnObj = new TSCDraftContainerList();
            returnObj.draftContainers = action.execute();
            this.operatedState = action.operatedState;
            return returnObj;
        }
    }
}
