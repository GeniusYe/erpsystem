﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseManage.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;
using SharedObject.WareHouseManage.Transmission;

namespace ServerSystem.WareHouseManage.Connector
{
    public class ReadDraftListConnector : ServerSystem.Connector.Connector
    {
        public ReadDraftListConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.draftDisplayDraftList:
                case Instruction.draftDisplayDraftListSearch:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            List<DefaultDraft> draftList;

            WareHouseManage.Action.ReadDraftListAction action = new ReadDraftListAction(identity, mainInstruction);
         
            switch (mainInstruction)
            {
                case Instruction.draftDisplayDraftList:
                    action.id = id;
                    draftList = action.execute();
                    break;
                case Instruction.draftDisplayDraftListSearch:
                    TCSSearchString tCSSearchString;
                    if (obj is TCSSearchString)
                        tCSSearchString = (TCSSearchString)obj;
                    else
                        throw new ParameterNotMatchException();
                    action.searchStr = tCSSearchString.traString;
                    draftList = action.execute();
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            TSCDefaultDraftList returnObj = new TSCDefaultDraftList();
            returnObj.draftList = draftList;
            this.operatedState = action.operatedState;
            return returnObj;
        }
    }
}
