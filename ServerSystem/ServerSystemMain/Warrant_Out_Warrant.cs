//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServerSystem
{
    using System;
    using System.Collections.Generic;
    
    public partial class Warrant_Out_Warrant
    {
        public Warrant_Out_Warrant()
        {
            this.Warrant_Out_Item = new HashSet<Warrant_Out_Item>();
            this.Warrant_Out_OperationRecord = new HashSet<Warrant_Out_OperationRecord>();
            this.Warrant_Attendants = new HashSet<Warrant_Attendants>();
        }
    
        public int ID { get; set; }
        public int state { get; set; }
        public int type { get; set; }
        public int payState { get; set; }
        public Nullable<int> target { get; set; }
        public int cookie { get; set; }
        public string comment { get; set; }
        public Nullable<int> amountTotal { get; set; }
        public Nullable<int> amountServicefee { get; set; }
        public Nullable<int> amountPermit { get; set; }
        public Nullable<int> amountCost { get; set; }
        public Nullable<int> amountPaid { get; set; }
        public Nullable<System.DateTime> submitDate { get; set; }
        public Nullable<System.DateTime> lastPaidDate { get; set; }
        public Nullable<System.DateTime> submitCustomerPageDate { get; set; }
        public string number { get; set; }
    
        public virtual Customers_Information Customers_Information { get; set; }
        public virtual Warrant_Bonus Warrant_Bonus { get; set; }
        public virtual ICollection<Warrant_Out_Item> Warrant_Out_Item { get; set; }
        public virtual ICollection<Warrant_Out_OperationRecord> Warrant_Out_OperationRecord { get; set; }
        public virtual ICollection<Warrant_Attendants> Warrant_Attendants { get; set; }
    }
}
