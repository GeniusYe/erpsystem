//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServerSystem
{
    using System;
    
    public partial class SP_Warrant_Out_ViewOneCycle_Result
    {
        public int ID { get; set; }
        public int state { get; set; }
        public int type { get; set; }
        public int payState { get; set; }
        public Nullable<int> target { get; set; }
        public Nullable<int> amountTotal { get; set; }
        public Nullable<int> amountPermit { get; set; }
        public Nullable<int> amountCost { get; set; }
        public Nullable<int> amountPaid { get; set; }
        public Nullable<System.DateTime> submitDate { get; set; }
        public Nullable<int> amountServicefee { get; set; }
        public string attendant_names { get; set; }
        public string target_name { get; set; }
        public Nullable<System.DateTime> submitCustomerPageDate { get; set; }
        public string number { get; set; }
    }
}
