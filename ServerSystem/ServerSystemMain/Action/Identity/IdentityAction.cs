﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.Service.Identity;
using SharedObject.Main.Transmission;
using SharedObject.Identity.Stuff;
using SharedObject.Identity.Transmission;

namespace ServerSystem.Action.Identity
{
    class IdentityAction : Action<SharedObject.Identity.Stuff.IdentityMark>
    {
        public int type { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string cardInformation { get; set; }

        public IdentityAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(SharedObject.Normal.SystemID.Identity, instruction, identity)
        {
            base.entities = new ERPSystemEntities();
            switch (instruction)
            {
                case Instruction.askForNormalLogIn:
                case Instruction.askForSpecialLogIn:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override SharedObject.Identity.Stuff.IdentityMark mainMethod()
        {
            IdentityService identityService = new IdentityService();

            identityService.type = type;
            if (type == 0)
            {
                identityService.cardInformation = cardInformation;
            }
            else if (type == 1)
            {
                identityService.userName = userName;
                identityService.password = password;
            }
            SharedObject.Identity.Stuff.IdentityMark identity;
            bool login, authorize;
            switch (mainInstruction)
            {
                case Instruction.askForNormalLogIn:
                    identity = identityService.readIdentity(out login, out authorize);
                    this.operatedState = identityService.operatedState;
                    if (!login)
                        if (authorize)
                            operatedState.addStates(OperatedState.OperatedConfirmingEnum.canOnlyAuthorize);
                        else
                            operatedState.addStates(OperatedState.OperatedConfirmingEnum.canNotLoginOrAuthorize);
                    break;
                case Instruction.askForSpecialLogIn:
                    identity = identityService.readIdentity(out login, out authorize);
                    this.operatedState = identityService.operatedState;
                    if (!authorize)
                        if (login)
                            operatedState.addStates(OperatedState.OperatedConfirmingEnum.canOnlyLogin);
                        else
                            operatedState.addStates(OperatedState.OperatedConfirmingEnum.canNotLoginOrAuthorize);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.success)
                operatedState.state = OperatedState.OperatedStateEnum.normal;
            return identity;
        }
    }
}
