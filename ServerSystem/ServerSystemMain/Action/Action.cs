﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Transactions;
using SharedObject.Main.Transmission;

namespace ServerSystem.Action
{
    public abstract class Action<T>
    {
        protected SharedObject.Normal.SystemID systemID;
        protected Instruction mainInstruction;
        protected SharedObject.Identity.Stuff.IdentityMark identity;
        public OperatedState operatedState { get; protected set; }

        protected ERPSystemEntities entities;

        public Action(SharedObject.Normal.SystemID systemID, Instruction mainInstruction, SharedObject.Identity.Stuff.IdentityMark identity)
        {
            this.systemID = systemID;
            this.mainInstruction = mainInstruction;
            this.identity = identity;
            this.operatedState = new OperatedState();
        }

        public T execute()
        {
            T e;
            using (var scope = new TransactionScope(TransactionScopeOption.Required,
                new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                try
                {
                    e = mainMethod();
                    entities.SaveChanges();
                    scope.Complete();
                }
                catch (Exception exception)
                {
                    operatedState = new OperatedState();
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.exception);
                    e = default(T);
                    log(exception);
                }
            }
            return e;
        }

        protected abstract T mainMethod();

        public enum RightState
        {
            Level0,
            Level1,
            Level2,
            Level3
        }

        private void log(Exception e)
        {
            Global.GlobalParameters.log(e);
        }

        public class InstructionNotMatchException : Exception
        {
        }

        public class ParameterNotMatchException : Exception
        {
        }

        public class GetInfoException : Exception
        {
        }
    }
}
