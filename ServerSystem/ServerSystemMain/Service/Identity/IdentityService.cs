﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.Identity.Stuff;

namespace ServerSystem.Service.Identity
{
    class IdentityService : Service
    {
        public string cardInformation { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public int type { get; set; }

        public IdentityService()
            : base() { }

        public IdentityMark readIdentity(out bool login, out bool authorize)
        {
            login = false;
            authorize = false;
            if (type == 0)
            {
                var user = from i in entities.SP_Identity_Read(type, cardInformation, userName, password) select new { ID = i.ID, name = i.name, cardLogin = i.cardLogin, cardAuthorize = i.cardAuthorize };
                foreach (var result in user)
                {
                    IdentityMark identity = new IdentityMark() { ID = result.ID, name = result.name };
                    bool? t = result.cardLogin;
                    login = t == true ? true : false;
                    t = result.cardAuthorize;
                    authorize = t == true ? true : false;
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                    return identity;
                }
            }
            else if (type == 1)
            {
                var user = from i in entities.SP_Identity_Read(type, cardInformation, userName, password) select new { ID = i.ID, name = i.name, passwordLogin = i.passwordLogin, passwordAuthorize = i.passwordAuthorize };
                foreach (var result in user)
                {
                    IdentityMark identity = new IdentityMark() { ID = result.ID, name = result.name };
                    bool? t = result.passwordLogin;
                    login = t == true ? true : false;
                    t = result.passwordAuthorize;
                    authorize = t == true ? true : false;
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                    return identity;
                }
            }
            else
                throw new ParameterNotMatchException();
            return null;
        }
    }
}
