﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;

namespace ServerSystem.Service
{
    public abstract class Service
    {
        protected ERPSystemEntities entities;

        public OperatedState operatedState { get; protected set; }

        public Service()
        {
            this.operatedState = new OperatedState();
            this.entities = new ERPSystemEntities();
        }

        protected int generateNewCookie(int cookieOri)
        {
            Random ro = new Random(System.DateTime.Now.GetHashCode());
            int cookie = ro.Next();
            if (cookie == cookieOri) cookie++;
            return cookie;
        }

        public class ParameterNotMatchException : Exception
        {
        }    
    }
}
