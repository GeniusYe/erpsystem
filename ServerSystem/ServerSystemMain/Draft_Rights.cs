//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServerSystem
{
    using System;
    using System.Collections.Generic;
    
    public partial class Draft_Rights
    {
        public int ID { get; set; }
        public int userID { get; set; }
        public Nullable<byte> rLogIn { get; set; }
        public Nullable<byte> rGetSpecialOne { get; set; }
        public Nullable<byte> rAdd { get; set; }
        public Nullable<byte> rEdit { get; set; }
        public Nullable<byte> rSubmit { get; set; }
        public Nullable<byte> rDisplayPrice { get; set; }
        public Nullable<byte> rDisplayNum { get; set; }
        public Nullable<byte> rViewAll { get; set; }
        public Nullable<byte> rDel { get; set; }
    
        public virtual Identity_Identity Identity_Identity { get; set; }
    }
}
