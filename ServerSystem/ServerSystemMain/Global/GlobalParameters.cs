﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerSystem.Global
{
    public static class GlobalParameters
    {
        public static string logFileName = "Exception Log.log";

        public static int localPort = 8000;

        public static int disposePartMessageTimeOut = 10000;

        public static int connectorTimeOut = 600000;

        public static int tcpBufferSize = 2097152;

        public static string certificateName = "ERPSystem";

        public static void log(Exception e)
        {
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.AppendLine(e.Message);
            if (e.InnerException != null)
                bodyBuilder.AppendLine(e.InnerException.Message);
            bodyBuilder.AppendLine(e.StackTrace);
            sendMail(new String[] { "netaccount@geniusye.com" }, "您的ERPSystemServer程序，在" + DateTime.Now + "时发生错误", bodyBuilder.ToString());
        }

        public static bool sendMail(String[] to, String subject, String body)
        {
            try
            {
                SMTPAssistant.MailSend mail = new SMTPAssistant.MailSend();
                foreach (String str in to)
                    mail.addToAddress(new System.Net.Mail.MailAddress(str));
                mail.subject = subject;
                mail.enableSsl = false;
                mail.hostName = "smtp.ym.163.com";
                mail.hostPort = 25;
                mail.username = "datasender@youlinjixie.com";
                mail.password = "5cteQHGC3t";
                mail.isBodyHtml = false;
                mail.fromAddress = new System.Net.Mail.MailAddress("datasender@youlinjixie.com", "发送者", Encoding.UTF8);
                mail.body = body;
                mail.sendMail(true);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
