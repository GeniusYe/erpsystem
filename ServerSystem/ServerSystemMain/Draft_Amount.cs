//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServerSystem
{
    using System;
    using System.Collections.Generic;
    
    public partial class Draft_Amount
    {
        public int IDDraft { get; set; }
        public Nullable<int> num { get; set; }
        public Nullable<int> value { get; set; }
        public int year { get; set; }
        public int month { get; set; }
    
        public virtual Draft_Draft Draft_Draft { get; set; }
    }
}
