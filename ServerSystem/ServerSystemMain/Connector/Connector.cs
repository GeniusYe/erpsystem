﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ServerSystem.Action;
using SharedObject.Main.Transmission;
using TransmissionReceiveMessage;

namespace ServerSystem.Connector
{
    public abstract class Connector
    {
        protected SharedObject.Normal.SystemID systemID;
        protected Instruction mainInstruction;
        protected SecondaryInstruction mainSecondaryInstruction;
        protected Identity.IdentityConnector.IdentityState identityState = new Identity.IdentityConnector.IdentityState();
        protected int cookie;
        protected int id;
        private List<Connector> connectors;
        private Object receiveObj = null;
        protected byte[] instrData = new byte[0];
        private System.Threading.Timer timer;
        protected bool ifAuthorize = false;

        public OperatedState operatedState { get; protected set; }

        public Connector(byte[] instrData, List<Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
        {
            this.instrData = (byte[])instrData.Clone();
            this.connectors = connectors;
            this.connectors.Add(this);
            this.systemID = (SharedObject.Normal.SystemID)instrData[1];
            this.mainInstruction = (Instruction)instrData[2];
            this.mainSecondaryInstruction = (SecondaryInstruction)instrData[3];
            this.cookie = ReceiveMessage.getCookie(instrData);
            this.id = ReceiveMessage.convertBytesToInt(instrData, 8);
            this.identityState.normalIdentity = normalIdentity;
            timer = new System.Threading.Timer(timer_Elapsed, null, Global.GlobalParameters.connectorTimeOut, System.Threading.Timeout.Infinite);
        }

        void timer_Elapsed(object state)
        {
            timer.Dispose();
            this.Dispose();
        }

        public void Dispose()
        {
            this.timer.Dispose();
            lock (this.connectors)
            {
                this.connectors.Remove(this);
            }
        }

        public MemoryStream AuthorizedRedo(ServerSystem.Connector.Identity.LogInIdentity specialIdentity)
        {
            this.identityState.specialIdentities = specialIdentity;
            this.instrData[3] = (int)SecondaryInstruction.sendAuthorizeResponse;
            this.mainSecondaryInstruction = (SecondaryInstruction)instrData[3];
            return this.execute(this.instrData, this.receiveObj);
        }

        public MemoryStream execute(byte[] instrData, MemoryStream receiveStream)
        {
            if (receiveStream.Length != 0)
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                try
                {
                    this.receiveObj = binaryFormatter.Deserialize(receiveStream);
                }
                catch
                {
                    this.receiveObj = (int)receiveStream.Length;
                }
            }
            return execute(this.instrData, this.receiveObj);
        }

        private MemoryStream execute(byte[] instrData, Object obj)
        {
            this.timer.Change(600000, System.Threading.Timeout.Infinite);
            
            ServerSystem.Connector.Identity.LogInIdentity identity = null;
            if (mainSecondaryInstruction == SecondaryInstruction.sendRequire)
            {
                ifAuthorize = false;
                identity = this.identityState.normalIdentity;
            }
            else if (mainSecondaryInstruction == SecondaryInstruction.sendAuthorizeResponse)
            {
                ifAuthorize = true;
                identity = identityState.specialIdentities;
                if (identityState.specialIdentities == null)
                    identity = identityState.normalIdentity;
            }
            else
            {
                return new MemoryStream();
            }
            MemoryStream returnStream = new MemoryStream();
            returnStream.Position = 20;
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            AWithOperatedState returnObj = mainMethod(identity, obj);
            returnObj.operatedState = this.operatedState;
            binaryFormatter.Serialize(returnStream, returnObj);
            identityState.specialIdentities = null;
            setReturnStream(systemID, mainInstruction, returnStream, cookie);
            int i = 0;
            IEnumerator<OperatedState.OperatedConfirmingItem> iterator = operatedState.GetEnumerator();
            while (iterator.MoveNext())
            {
                if (!iterator.Current.ifFatal())
                {
                    i++;
                }
            }
            if (!(i == operatedState.Count && operatedState.Count > 0 && operatedState.state == OperatedState.OperatedStateEnum.notSuccess))
                this.Dispose();
            return returnStream;
        }

        protected abstract AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, Object obj);

        private void packOperatedStateOnlyReturnStream(SharedObject.Normal.SystemID systemID, Instruction mainInstruction, MemoryStream returnStream, BinaryFormatter binaryFormatter, int cookie, OperatedState operatedState)
        {
            AWithOperatedState operatedStateObj = new AWithOperatedState();
            operatedStateObj.operatedState = operatedState;
            returnStream.Position = 20;
            binaryFormatter.Serialize(returnStream, operatedStateObj);
            setReturnStream(systemID, mainInstruction, returnStream, cookie);
        }

        private void setReturnStream(SharedObject.Normal.SystemID systemID, Instruction mainInstruction, MemoryStream returnStream, int cookie)
        {
            returnStream.Position = 4;
            returnStream.Write(TransmissionFunction.createInstrData(systemID, mainInstruction, SecondaryInstruction.sendResponse, cookie), 0, 16);
            returnStream.Position = 0;
            returnStream.Write(ReceiveMessage.convertIntToBytes((int)returnStream.Length - 4), 0, 4);
            returnStream.Position = 0;
        }

        public bool ifMatch(Instruction mainInstruction, int cookie)
        {
            if (mainInstruction == this.mainInstruction && cookie == this.cookie)
                return true;
            else
                return false;
        }

        public class InstructionNotMatchException : Exception
        {
        }

        public class ParameterNotMatchException : Exception
        {
        }
    }
}
