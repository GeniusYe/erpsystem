﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Identity.Stuff;

namespace ServerSystem.Connector.Identity
{
    public class LogInIdentity : SharedObject.Identity.Stuff.IdentityMark
    {
        public LogInIdentity(IdentityMark identity)
            : base(identity.name, identity.ID, identity.cookie)
        { }

        public bool check(IdentityMark identity)
        {
            if (identity == null)
                return false;
            IdentityMark i = identity;
            if (i.ID == identity.ID && i.cookie == identity.cookie)
                return true;
            else
                return false;
        }
    }
}
