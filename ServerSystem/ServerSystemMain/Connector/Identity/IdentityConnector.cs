﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.Action;
using SharedObject.Main.Transmission;
using SharedObject.Identity.Stuff;
using SharedObject.Identity.Transmission;

namespace ServerSystem.Connector.Identity
{
    public class IdentityConnector : Connector
    {
        private List<Connector> connectors;
        public System.IO.MemoryStream authorizeExecuteStream { get; private set; }

        public IdentityConnector(byte[] instrData, List<Connector> connectors, Identity.IdentityConnector.IdentityState identityState)
            : base(instrData, connectors, null)
        {
            this.connectors = connectors;
            this.identityState = identityState;
            switch (mainInstruction)
            {
                case Instruction.askForNormalLogIn:
                case Instruction.askForSpecialLogIn:
                    break;
                default:
                    throw new Exception();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark iden, object obj)
        {
            TCSIdentity tCSIdentity;

            if (obj is TCSIdentity)
                tCSIdentity = (TCSIdentity)obj;
            else
                throw new ParameterNotMatchException();

            Action.Identity.IdentityAction action = new Action.Identity.IdentityAction(null, mainInstruction);

            SharedObject.Identity.Stuff.IdentityMark identity = null;
            if (tCSIdentity.authorizeType == SharedObject.Identity.Transmission.TCSIdentity.AuthorizeType.card)
            {
                action.type = 0;
                action.cardInformation = tCSIdentity.cardInformation;
                identity = action.execute();
            }
            else if (tCSIdentity.authorizeType == SharedObject.Identity.Transmission.TCSIdentity.AuthorizeType.password)
            {
                action.type = 1;
                action.userName = tCSIdentity.userName;
                action.password = tCSIdentity.password;
                identity = action.execute();
            }
            this.operatedState = action.operatedState;
            TSCIdentity tSCIdentity;
            tSCIdentity = new TSCIdentity();
            tSCIdentity.identity = identity;
            tSCIdentity.version = SharedObject.Normal.Version.version;
            tSCIdentity.operatedState = operatedState;
            if (identity != null && (operatedState.state == OperatedState.OperatedStateEnum.success || operatedState.state == OperatedState.OperatedStateEnum.normal))
            {
                LogInIdentity logInIdentity = new LogInIdentity(identity);
                if (mainInstruction == Instruction.askForSpecialLogIn)
                {
                    identityState.specialIdentities = logInIdentity;
                    int cookieToBeRedo = tCSIdentity.cookieToBeRedo;
                    SharedObject.Normal.SystemID systemToBeRedo = tCSIdentity.systemNumber;
                    Instruction instructionToBeRedo = (Instruction)tCSIdentity.instructionNumber;
                    if (connectors != null)
                        lock (connectors)
                        {
                            foreach (Connector connector in connectors)
                                if (connector.ifMatch((Instruction)instructionToBeRedo, cookieToBeRedo))
                                {
                                    authorizeExecuteStream = connector.AuthorizedRedo(logInIdentity);
                                    break;
                                }
                        }
                }
                else
                    identityState.normalIdentity = logInIdentity;
            }
            return tSCIdentity;
        }

        public class IdentityState
        {
            public ServerSystem.Connector.Identity.LogInIdentity specialIdentities { get; set; }
            public ServerSystem.Connector.Identity.LogInIdentity normalIdentity { get; set; }
        }
    }
}
