//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServerSystem
{
    using System;
    
    public partial class SP_Ticket_Collect_Result
    {
        public Nullable<int> count_all { get; set; }
        public Nullable<int> sum_working_hour { get; set; }
        public int id { get; set; }
        public string name { get; set; }
    }
}
