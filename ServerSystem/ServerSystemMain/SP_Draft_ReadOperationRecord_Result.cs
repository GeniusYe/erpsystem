//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServerSystem
{
    using System;
    
    public partial class SP_Draft_ReadOperationRecord_Result
    {
        public int ID { get; set; }
        public int draftId { get; set; }
        public int operationOperator { get; set; }
        public int operation { get; set; }
        public System.DateTime operatedTime { get; set; }
        public string operationOperatorName { get; set; }
    }
}
