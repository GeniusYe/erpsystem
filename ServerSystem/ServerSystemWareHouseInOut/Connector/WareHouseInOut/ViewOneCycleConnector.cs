﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ServerSystem.WareHouseInOut.Connector
{
    public class ViewOneCycleConnector : ServerSystem.Connector.Connector
    {
        public ViewOneCycleConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.warrantViewOneCycle:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            TCSViewOneCycle tCSViewOneCycle;
            if (obj is TCSViewOneCycle)
                tCSViewOneCycle = (TCSViewOneCycle)obj;
            else
                throw new ParameterNotMatchException();

            ViewOneCycle viewOneCycle;

            switch (mainInstruction)
            {
                case Instruction.warrantViewOneCycle:
                    WareHouseInOut.Action.ViewOneCycleAction action = new ViewOneCycleAction(identity, mainInstruction);
                    action.date = tCSViewOneCycle.date;
                    viewOneCycle = action.execute();
                    this.operatedState = action.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            TSCViewOneCycle returnObj = new TSCViewOneCycle();
            returnObj.viewOneCycle = viewOneCycle;
            return returnObj;
        }

    }
}
