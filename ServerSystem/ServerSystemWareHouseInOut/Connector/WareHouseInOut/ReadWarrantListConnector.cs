﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ServerSystem.WareHouseInOut.Connector
{
    public class ReadWarrantListConnector : ServerSystem.Connector.Connector
    {
        public ReadWarrantListConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.inWarrantList:
                case Instruction.outWarrantList:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            List<DefaultWarrant> warrantList;

            WareHouseInOut.Action.ReadWarrantListAction action = new ReadWarrantListAction(identity, mainInstruction);

            switch (mainInstruction)
            {
                case Instruction.inWarrantList:
                case Instruction.outWarrantList:
                    TCSDefaultWarrantsRequirement tCSDefaultWarrantsRequirement;
                    if (obj is TCSDefaultWarrantsRequirement)
                        tCSDefaultWarrantsRequirement = (TCSDefaultWarrantsRequirement)obj;
                    else
                        throw new ParameterNotMatchException();
                    action.startDate = tCSDefaultWarrantsRequirement.dateRange.startDate;
                    action.stopDate = tCSDefaultWarrantsRequirement.dateRange.stopDate;
                    action.target = tCSDefaultWarrantsRequirement.target;
                    warrantList = action.execute();
                    break;
                default:
                    throw new InstructionNotMatchException();
            }

            this.operatedState = action.operatedState;
            TSCDefaultWarrantList returnObj = new TSCDefaultWarrantList();
            returnObj.defaultWarrantList = warrantList;
            return returnObj;
        }
    }
}
