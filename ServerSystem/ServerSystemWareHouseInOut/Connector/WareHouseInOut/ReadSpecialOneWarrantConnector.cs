﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ServerSystem.WareHouseInOut.Connector
{
    public class ReadSpecialOneWarrantConnector : ServerSystem.Connector.Connector
    {
        public ReadSpecialOneWarrantConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.inWarrantSpecialOne:
                case Instruction.outWarrantSpecialOne:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            AWithOperatedState returnObj;

            switch (mainInstruction)
            {
                case Instruction.inWarrantSpecialOne:
                    WareHouseInOut.Action.ReadSpecialOneInWarrantAction action1 = new ReadSpecialOneInWarrantAction(identity, mainInstruction);
                    action1.id = id;
                    TSCInWarrant tSCInWarrant = new TSCInWarrant();
                    tSCInWarrant.inWarrant = action1.execute();
                    returnObj = tSCInWarrant;
                    this.operatedState = action1.operatedState;
                    break;
                case Instruction.outWarrantSpecialOne:
                    WareHouseInOut.Action.ReadSpecialOneOutWarrantAction action2 = new ReadSpecialOneOutWarrantAction(identity, mainInstruction);
                    action2.id = id;
                    TSCOutWarrant tSCOutWarrant = new TSCOutWarrant();
                    tSCOutWarrant.outWarrant = action2.execute();
                    returnObj = tSCOutWarrant;
                    this.operatedState = action2.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            return returnObj;
        }
    }
}
