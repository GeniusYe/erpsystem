﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ServerSystem.WareHouseInOut.Connector
{
    public class ViewDetailConnector : ServerSystem.Connector.Connector
    {
        public ViewDetailConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.warrantViewDetail:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            TCSViewDetail tCSViewDetail;
            if (obj is TCSViewDetail)
                tCSViewDetail = (TCSViewDetail)obj;
            else
                throw new ParameterNotMatchException();

            DetailAccount detailAccount;

            switch (mainInstruction)
            {
                case Instruction.warrantViewDetail:
                    WareHouseInOut.Action.ViewDetailAction action = new ViewDetailAction(identity, mainInstruction);
                    action.id = tCSViewDetail.ID;
                    action.startDate = tCSViewDetail.dateRange.startDate;
                    action.stopDate = tCSViewDetail.dateRange.stopDate;
                    detailAccount = action.execute();
                    this.operatedState = action.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            TSCViewDetail returnObj = new TSCViewDetail();
            returnObj.detailAccount = detailAccount;
            return returnObj;
        }
    }
}
