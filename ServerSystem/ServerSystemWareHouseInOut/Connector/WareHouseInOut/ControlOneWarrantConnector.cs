﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ServerSystem.WareHouseInOut.Connector
{
    public class ControlOneWarrantConnector : ServerSystem.Connector.Connector
    {
        public ControlOneWarrantConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.inWarrantSubmit:
                case Instruction.inWarrantPrint:
                case Instruction.outWarrantSubmitCustomerPage:
                case Instruction.outWarrantPrintCustomerPage:
                case Instruction.outWarrantForcePass:
                case Instruction.outWarrantTakeOut:
                case Instruction.outWarrantSubmit:
                case Instruction.outWarrantPrint:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            TCSWarrantControl tCSWarrantControl;
            if (obj is TCSWarrantControl)
                tCSWarrantControl = (TCSWarrantControl)obj;
            else
                throw new ParameterNotMatchException();

            switch (mainInstruction)
            {
                case Instruction.inWarrantSubmit:
                case Instruction.outWarrantSubmitCustomerPage:
                case Instruction.outWarrantSubmit:
                    WareHouseInOut.Action.SubmitOneWarrantAction action1 = new SubmitOneWarrantAction(identity, mainInstruction);
                    action1.warrant = tCSWarrantControl.warrant;
                    action1.ifForce = ifAuthorize;
                    action1.execute();
                    this.operatedState = action1.operatedState;
                    break;
                case Instruction.inWarrantPrint:
                case Instruction.outWarrantPrintCustomerPage:
                case Instruction.outWarrantForcePass:
                case Instruction.outWarrantTakeOut:
                case Instruction.outWarrantPrint:
                    WareHouseInOut.Action.ControlOneWarrantAction action2 = new ControlOneWarrantAction(identity, mainInstruction);
                    action2.warrant = tCSWarrantControl.warrant;
                    action2.execute();
                    this.operatedState = action2.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            return new AWithOperatedState();
        }
    }
}
