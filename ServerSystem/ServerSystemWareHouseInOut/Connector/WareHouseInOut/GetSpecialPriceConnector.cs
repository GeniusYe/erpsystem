﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ServerSystem.WareHouseInOut.Connector
{
    public class GetSpecialPriceConnector : ServerSystem.Connector.Connector
    {
        public GetSpecialPriceConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.outWarrantSpecialPrice:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            TCSGetSpecialPrice tCSGetSpecialPrice;
            if (obj is TCSGetSpecialPrice)
                tCSGetSpecialPrice = (TCSGetSpecialPrice)obj;
            else
                throw new ParameterNotMatchException();

            SpecialPriceSC specialPriceSC;

            switch (mainInstruction)
            {
                case Instruction.outWarrantSpecialPrice:
                    WareHouseInOut.Action.GetSpecialPriceAction action = new GetSpecialPriceAction(identity, mainInstruction);
                    action.specialPriceInfo = tCSGetSpecialPrice.getSpecialPriceInfo;
                    specialPriceSC = action.execute();
                    this.operatedState = action.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            TSCSpecialPrice tSCSpecialPrice = new TSCSpecialPrice();
            tSCSpecialPrice.specialPrice = specialPriceSC;
            return tSCSpecialPrice;
        }
    }
}
