﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ServerSystem.WareHouseInOut.Connector
{
    public class ReadAttendantsConnector : ServerSystem.Connector.Connector
    {
        public ReadAttendantsConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.warrantAttendantsList:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            WareHouseInOut.Action.ReadAttendantsAction action;

            switch (mainInstruction)
            {
                case Instruction.warrantAttendantsList:
                    action = new ReadAttendantsAction(identity, Instruction.warrantAttendantsList);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            TSCAttendantsList returnObj = new TSCAttendantsList();
            returnObj.attendantsList = action.execute();
            this.operatedState = action.operatedState;
            return returnObj;
        }
    }
}
