﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ServerSystem.WareHouseInOut.Connector
{
    public class MoneyOperationConnector : ServerSystem.Connector.Connector
    {
        public MoneyOperationConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            : base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.outWarrantDiscount:
                case Instruction.outWarrantPayCheck:
                case Instruction.outWarrantReturnPayCheck:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            WareHouseInOut.Action.MoneyOperationAction action = new MoneyOperationAction(identity, mainInstruction);

            AWithOperatedState returnObj;

            switch (mainInstruction)
            {
                case Instruction.outWarrantDiscount:
                    TCSDiscountChange tCSDiscountChange;
                    if (obj is TCSDiscountChange)
                        tCSDiscountChange = (TCSDiscountChange)obj;
                    else
                        throw new ParameterNotMatchException();
                    action.warrant = tCSDiscountChange.warrant;
                    action.discountItem = tCSDiscountChange.discountItem;
                    action.execute();
                    this.operatedState = action.operatedState;
                    returnObj = new TSCDiscountChange();
                    ((TSCDiscountChange)returnObj).discountItem = action.discountItem;
                    break;
                case Instruction.outWarrantPayCheck:
                case Instruction.outWarrantReturnPayCheck:
                    TCSPayCheck tCSPayCheck;
                    if (obj is TCSPayCheck)
                        tCSPayCheck = (TCSPayCheck)obj;
                    else
                        throw new ParameterNotMatchException();
                    action.warrant = tCSPayCheck.warrant;
                    action.payCheckItem = tCSPayCheck.payCheckItem;
                    action.execute();
                    this.operatedState = action.operatedState;
                    returnObj = new TSCPayCheckChange();
                    ((TSCPayCheckChange)returnObj).payCheckItem = action.payCheckItem;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            return returnObj;
        }
    }
}

