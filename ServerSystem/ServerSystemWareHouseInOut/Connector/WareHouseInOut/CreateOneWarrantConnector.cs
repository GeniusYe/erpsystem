﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ServerSystem.WareHouseInOut.Connector
{
    public class CreateOneWarrantConnector : ServerSystem.Connector.Connector
    {
        public CreateOneWarrantConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.inWarrantAdd:
                case Instruction.inWarrantEdit:
                case Instruction.outWarrantAdd:
                case Instruction.outWarrantEdit:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {

            AWithOperatedState returnObj;

            switch (mainInstruction)
            {
                case Instruction.inWarrantAdd:
                case Instruction.inWarrantEdit:
                    TCSInWarrantCreate tCSInWarrantCreate;
                    if (obj is TCSInWarrantCreate)
                        tCSInWarrantCreate = (TCSInWarrantCreate)obj;
                    else
                        throw new ParameterNotMatchException();
                    returnObj = new TSCInWarrant();
                    WareHouseInOut.Action.CreateOneInWarrantAction action1 = new CreateOneInWarrantAction(identity, mainInstruction);
                    action1.warrant = tCSInWarrantCreate.inWarrant;
                    ((TSCInWarrant)returnObj).inWarrant = action1.execute();
                    this.operatedState = action1.operatedState;
                    break;
                case Instruction.outWarrantAdd:
                case Instruction.outWarrantEdit:
                    TCSOutWarrantCreate tCSOutWarrantCreate;
                    if (obj is TCSOutWarrantCreate)
                        tCSOutWarrantCreate = (TCSOutWarrantCreate)obj;
                    else
                        throw new ParameterNotMatchException();
                    returnObj = new TSCOutWarrant();
                    WareHouseInOut.Action.CreateOneOutWarrantAction action2 = new CreateOneOutWarrantAction(identity, mainInstruction);
                    action2.warrant = tCSOutWarrantCreate.outWarrant;
                    ((TSCOutWarrant)returnObj).outWarrant = action2.execute();
                    this.operatedState = action2.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            return returnObj;
        }
    }
}
