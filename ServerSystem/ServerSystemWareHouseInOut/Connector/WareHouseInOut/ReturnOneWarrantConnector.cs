﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Action;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.WareHouseInOut.Transmission;

namespace ServerSystem.WareHouseInOut.Connector
{
    public class ReturnOneWarrantConnector : ServerSystem.Connector.Connector
    {
        public ReturnOneWarrantConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.outWarrantReturn:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            TCSOutWarrantReturnNumber tCSOutWarrantReturnNumber;
            if (obj is TCSOutWarrantReturnNumber)
                tCSOutWarrantReturnNumber = (TCSOutWarrantReturnNumber)obj;
            else
                throw new ParameterNotMatchException();

            switch (mainInstruction)
            {
                case Instruction.outWarrantReturn:
                    WareHouseInOut.Action.ReturnOneWarrantAction action = new ReturnOneWarrantAction(identity, mainInstruction);
                    action.warrant = tCSOutWarrantReturnNumber.warrant;
                    action.returnNumberPair = tCSOutWarrantReturnNumber.transCSOutWarrantReturnNumberPairs;
                    action.attendants = tCSOutWarrantReturnNumber.attendants;
                    action.execute();
                    this.operatedState = action.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            return new AWithOperatedState();
        }
    }
}
