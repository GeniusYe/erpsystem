﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class SubmitOneWarrantAction : WareHouseInOutAction<Object>
    {
        public WarrantCSControl warrant { get; set; }
        public bool ifForce { get; set; }

        public SubmitOneWarrantAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.inWarrantSubmit:
                case Instruction.outWarrantSubmitCustomerPage:
                case Instruction.outWarrantSubmit:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override Object mainMethod()
        {
            if (warrant == null)
                throw new ParameterNotMatchException();
            RightState rightState;
            WarrantType warrantType;
            switch (mainInstruction)
            {
                case Instruction.inWarrantSubmit:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rInSubmit);
                    warrantType = WarrantType.In;
                    break;
                case Instruction.outWarrantSubmitCustomerPage:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutSubmitCustomerPage);
                    warrantType = WarrantType.Out;
                    break;
                case Instruction.outWarrantSubmit:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutSubmit);
                    warrantType = WarrantType.Out;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            else
            {
                DefaultWarrant.WarrantState state;
                OutWarrant.OutWarrantPayState? payState;
                DefaultWarrant.SpecificWarrantType specificWarrantType;
                int cookieOriRead, creator;
                int? returnOperator;
                getInfo(warrantType, warrant.ID, out state, out creator, out returnOperator, out cookieOriRead, out payState, out specificWarrantType);
                if (cookieOriRead != warrant.cookie)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.cookieWrong);
                if (rightState < RightState.Level2)
                {
                    if (identity.ID != creator)
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
                }
                if (rightState < RightState.Level3)
                {
                    if (mainInstruction == Instruction.outWarrantSubmit && returnOperator != null)
                    {
                        if (identity.ID != returnOperator)
                            operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
                    }
                    else if (mainInstruction == Instruction.outWarrantSubmitCustomerPage)
                    {
                        // adjust warrant requires approval by admin
                        if (specificWarrantType == DefaultWarrant.SpecificWarrantType.AdjustOut)
                        {
                            operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
                        }
                    }
                    else if (mainInstruction == Instruction.inWarrantSubmit)
                    {
                        // in warrant requires admin approval
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
                    }
                    
                }
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            if (mainInstruction == Instruction.inWarrantSubmit)
            {                
                if (!ifForce)
                {
                    int priceOld, priceNew;
                    bool del;
                    string name;
                    IEnumerable<SP_Warrant_In_SubmitReadForCheck_Result> spResultList = entities.SP_Warrant_In_SubmitReadForCheck(warrant.ID);
                    foreach (SP_Warrant_In_SubmitReadForCheck_Result spResult in spResultList)
                    {
                        name = spResult.Name.Trim() + " " + spResult.specification.Trim();
                        priceOld = spResult.numAll == 0 ? 0 : spResult.value / spResult.numAll;
                        priceNew = spResult.price;
                        del = spResult.Del;
                        if (del)
                            operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantTakeInAlreadyDeletedDraft, name);
                        if ((priceNew > priceOld || priceNew < priceOld * 0.7) && priceOld != 0)
                            operatedState.addStates(SharedObject.Main.Transmission.OperatedState.OperatedConfirmingEnum.warrantTooMuchPriceChange, name);
                    }
                }
            }
            else if (mainInstruction == Instruction.outWarrantSubmitCustomerPage)
            {
                int priceCost, standardPrice, num;
                int? specialPrice, priceAdjust;
                string name;
                IEnumerable<SP_Warrant_Out_SubmitReadForCheck_Result> spResultList = entities.SP_Warrant_Out_SubmitReadForCheck(warrant.ID);
                foreach (SP_Warrant_Out_SubmitReadForCheck_Result spResult in spResultList)
                {
                    name = spResult.Name.Trim() + " " + spResult.specification.Trim();
                    num = spResult.num;
                    if ((!ifForce) && spResult.state != 1)
                        operatedState.addStates(SharedObject.Main.Transmission.OperatedState.OperatedConfirmingEnum.warrantNotSubmittedDraft, name);
                    if ((!ifForce) || (rightState < RightState.Level3))
                    {
                        priceCost = spResult.numAll == 0 ? 0 : spResult.value / spResult.numAll;
                        standardPrice = spResult.standardPrice;
                        specialPrice = spResult.specialPrice;
                        priceAdjust = spResult.priceAdjustMannual;

                        if (((priceAdjust != null && priceAdjust != 0 && specialPrice == 0) || priceAdjust != specialPrice) && rightState < RightState.Level3)
                            operatedState.addStates(SharedObject.Main.Transmission.OperatedState.OperatedConfirmingEnum.warrantPriceAdjusted, name);

                        if (priceAdjust != null && priceAdjust != 0 && priceAdjust < priceCost)
                            operatedState.addStates(SharedObject.Main.Transmission.OperatedState.OperatedConfirmingEnum.warrantLowerPriceAdjustThanPriceIn, name);
                        else if (priceAdjust != null && priceAdjust != 0 && priceAdjust < standardPrice)
                            operatedState.addStates(SharedObject.Main.Transmission.OperatedState.OperatedConfirmingEnum.warrantLowerPriceAdjustThanPrice, name);
                    }
                }
            }
            else if (mainInstruction == Instruction.outWarrantSubmit)
            {

            }
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            ControlOneWarrantService service;
            switch (mainInstruction)
            {
                case Instruction.inWarrantSubmit:
                    service = new ControlOneWarrantService(entities, WarrantType.In);
                    service.submitInWarrant(identity, warrant);
                    break;
                case Instruction.outWarrantSubmitCustomerPage:
                    service = new ControlOneWarrantService(entities, WarrantType.Out);
                    service.submitOutWarrantCustomerPage(identity, warrant);
                    break;
                case Instruction.outWarrantSubmit:
                    service = new ControlOneWarrantService(entities, WarrantType.Out);
                    service.submitOutWarrant(identity, warrant, ifForce);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = service.operatedState;
            return null;
        }
    }
}
