﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    abstract class WareHouseInOutAction<T> : ServerSystem.Action.Action<T>
    {
        protected new ERPSystemEntities entities = new ERPSystemEntities();

        public WareHouseInOutAction(SharedObject.Main.Transmission.Instruction mainInstruction, SharedObject.Identity.Stuff.IdentityMark identity)
            : base(SharedObject.Normal.SystemID.WareHouseInOut, mainInstruction, identity)
        {
            base.entities = new ERPSystemEntities();
            this.entities = (ERPSystemEntities)base.entities;
        }

        protected override abstract T mainMethod();

        protected bool getInfo(WarrantType warrantType, int id, out DefaultWarrant.WarrantState state, out int creator, out int? returnOperator, out int cookieOriRead, out OutWarrant.OutWarrantPayState? payState, out DefaultWarrant.SpecificWarrantType specificWarrantType, object[] obj = null)
        {
            state = DefaultWarrant.WarrantState.Abnormal;
            creator = -1;
            cookieOriRead = -1;
            returnOperator = null;
            payState = null;
            specificWarrantType = DefaultWarrant.SpecificWarrantType.NotSuitable;
            if (warrantType == WarrantType.In)
            {
                SP_Warrant_In_GetInfo_Result result = entities.SP_Warrant_In_GetInfo(id).First();
                cookieOriRead = result.cookie;
                state = DefaultWarrant.IntToState(result.state);
                creator = result.creator.Value;
                specificWarrantType =  DefaultWarrant.SpecificWarrantType.In;
            }
            else
            {
                
                switch (mainInstruction)
                {
                    case SharedObject.Main.Transmission.Instruction.outWarrantDiscount:
                    case SharedObject.Main.Transmission.Instruction.outWarrantPayCheck:
                    case SharedObject.Main.Transmission.Instruction.outWarrantReturnPayCheck:
                        SP_Warrant_Out_GetInfoIncludingAmount_Result resultIncludingAmount;
                        resultIncludingAmount = entities.SP_Warrant_Out_GetInfoIncludingAmount(id).First();
                        cookieOriRead = resultIncludingAmount.cookie;
                        state = DefaultWarrant.IntToState(resultIncludingAmount.state);
                        specificWarrantType = DefaultWarrant.IntToSpecificWarrantType(resultIncludingAmount.type);
                        creator = resultIncludingAmount.creator.Value;
                        int? amountTotal = resultIncludingAmount.amountTotal;
                        int? amountPermit = resultIncludingAmount.amountPermit;
                        int? amountPaid = resultIncludingAmount.amountPaid;
                        if (amountTotal != null && obj != null && obj.Length > 0)
                        {
                            obj[0] = amountTotal;
                            obj[1] = amountPermit;
                            obj[2] = amountPaid;
                        }
                        break;
                    default:
                        SP_Warrant_Out_GetInfo_Result result = entities.SP_Warrant_Out_GetInfo(id).First();
                        cookieOriRead = result.cookie;
                        state = DefaultWarrant.IntToState(result.state);
                        creator = result.creator.Value;
                        specificWarrantType = DefaultWarrant.IntToSpecificWarrantType(result.type);
                        if (mainInstruction == SharedObject.Main.Transmission.Instruction.outWarrantSubmit)
                            returnOperator = result.returnOperator;
                        else if (mainInstruction == SharedObject.Main.Transmission.Instruction.outWarrantTakeOut)
                            payState = OutWarrant.IntToPayState(result.payState);
                        break;
                }
                return true;
            }
            return false;
        }

        public RightState CheckIdentityRightsSafely(SharedObject.Identity.Stuff.IdentityMark identity, getRightsInstruction instruction)
        {
            RightState rightState = RightState.Level0;
            if (identity == null)
                return rightState;
            byte? rightResult = entities.SP_Warrant_GetRights(identity.ID, (int)instruction).First();
            if (rightResult == null)
                rightState = RightState.Level0;
            else if (rightResult > 0 && rightResult < 4)
                rightState = (RightState)rightResult.Value;
            return rightState;
        }

        public RightState getWareHouseManageRights(WareHouseManage.Action.WareHouseManageAction<T>.getRightsInstruction getRightsInstruction)
        {
            WareHouseManage.Action.WareHouseManageAction<T> action = new WareHouseManage.Action.WareHouseManageAction<T>(SharedObject.Main.Transmission.Instruction.NotSet, identity);
            RightState priceRightState = action.CheckIdentityRightsSafely(identity, getRightsInstruction);
            return priceRightState;
        }

        public enum getRightsInstruction
        {
            rViewDetail = 1,
            rViewOneCycle = 2,
            rInLogIn = 10,
            rInDisplaySpecialOne = 11,
            rInAdd = 12,
            rInEdit = 13,
            rInCancel = 14,
            rInSubmit = 17,
            rInPrint = 18,
            rInMoney = 19,
            rInComplete = 20,
            rInCustomers = 21,
            rOutLogIn = 30,
            rOutDisplaySpecialOne = 31,
            rOutAdd = 32,
            rOutEdit = 33,
            rOutCancel = 34,
            rOutSubmitCustomerPage = 35,
            rOutPrintCustomerPage = 36,
            rOutSubmit = 37,
            rOutPrint = 38,
            rOutMoney = 39,
            rOutComplete = 40,
            rOutCustomers = 41,
            rOutForcePass = 43,
            rOutTakeOut = 44,
            rOutReturn = 45,
            rOutDiscount = 50,
            rOutReturnPayCheck = 51,
            rAttendantsList = 52,
        }
    }
}
