﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class ReadSpecialOneInWarrantAction : WareHouseInOutAction<InWarrant>
    {
        public int id { get; set; }

        public ReadSpecialOneInWarrantAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.inWarrantSpecialOne:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override InWarrant mainMethod()
        {
            RightState rightState, priceRightState;
            WarrantType warrantType;
            switch (mainInstruction)
            {
                case Instruction.inWarrantSpecialOne:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rInDisplaySpecialOne);
                    warrantType = WarrantType.In;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            priceRightState = getWareHouseManageRights(WareHouseManage.Action.WareHouseManageAction<InWarrant>.getRightsInstruction.rDisplayPrice);
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            else if (rightState == RightState.Level1)
            {
                DefaultWarrant.WarrantState state;
                OutWarrant.OutWarrantPayState? payState;
                DefaultWarrant.SpecificWarrantType specificWarrantType;
                int cookieOriRead, creator;
                int? returnOperator;
                getInfo(warrantType, id, out state, out creator, out returnOperator, out cookieOriRead, out payState, out specificWarrantType);
                if (creator != identity.ID)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            ReadSpecialOneWarrantService service;
            InWarrant warrant;
            bool priceDisplay;
            if (priceRightState != RightState.Level0)
                priceDisplay = true;
            else
                priceDisplay = false;
            switch (mainInstruction)
            {
                case Instruction.inWarrantSpecialOne:
                    service = new ReadSpecialOneWarrantService(entities, WarrantType.In);
                    warrant = service.getSpecialOneInWarrant(identity, id, priceDisplay);
                    this.operatedState = service.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            return warrant;
        }
    }
}
