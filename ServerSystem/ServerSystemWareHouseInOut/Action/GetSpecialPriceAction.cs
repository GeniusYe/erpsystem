﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class GetSpecialPriceAction : WareHouseInOutAction<SpecialPriceSC>
    {
        public GetSpecialPriceCS specialPriceInfo { get; set; }

        public GetSpecialPriceAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.outWarrantSpecialPrice:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override SpecialPriceSC mainMethod()
        {
            if (specialPriceInfo == null)
                throw new ParameterNotMatchException();
            GetSpecialPriceService service;
            SpecialPriceSC specialPriceSC;
            switch (mainInstruction)
            {
                case Instruction.outWarrantSpecialPrice:
                    service = new GetSpecialPriceService(entities, WarrantType.Out);
                    specialPriceSC = service.getSpecialPrice(identity, specialPriceInfo);
                    this.operatedState = service.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            return specialPriceSC;
        }
    }
}
