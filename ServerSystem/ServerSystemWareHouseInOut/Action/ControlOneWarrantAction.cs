﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class ControlOneWarrantAction : WareHouseInOutAction<Object>
    {
        public WarrantCSControl warrant { get; set; }

        public ControlOneWarrantAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.inWarrantPrint:
                case Instruction.outWarrantPrintCustomerPage:
                case Instruction.outWarrantForcePass:
                case Instruction.outWarrantTakeOut:
                case Instruction.outWarrantPrint:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override Object mainMethod()
        {
            if (warrant == null)
                throw new ParameterNotMatchException();
            RightState rightState;
            WarrantType warrantType;
            switch (mainInstruction)
            {
                case Instruction.inWarrantPrint:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rInPrint);
                    warrantType = WarrantType.In;
                    break;
                case Instruction.outWarrantPrintCustomerPage:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutPrintCustomerPage);
                    warrantType = WarrantType.Out;
                    break;
                case Instruction.outWarrantForcePass:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutForcePass);
                    warrantType = WarrantType.Out;
                    break;
                case Instruction.outWarrantTakeOut:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutTakeOut);
                    warrantType = WarrantType.Out;
                    break;
                case Instruction.outWarrantReturn:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutReturn);
                    warrantType = WarrantType.Out;
                    break;
                case Instruction.outWarrantPrint:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutPrint);
                    warrantType = WarrantType.Out;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            DefaultWarrant.WarrantState state;
            OutWarrant.OutWarrantPayState? payState;
            DefaultWarrant.SpecificWarrantType specificWarrantType;
            int cookieOriRead, creator;
            int? returnOperator;
            getInfo(warrantType, warrant.ID, out state, out creator, out returnOperator, out cookieOriRead, out payState, out specificWarrantType);
            if (cookieOriRead != warrant.cookie)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.cookieWrong);
            switch (mainInstruction)
            {
                case Instruction.inWarrantPrint:
                    if (state != DefaultWarrant.WarrantState.Submitted && state != DefaultWarrant.WarrantState.Printed)
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.stateWrong);
                    break;
                case Instruction.outWarrantPrintCustomerPage:
                    if (state != DefaultWarrant.WarrantState.SubmittedCustomerPage && state != DefaultWarrant.WarrantState.PrintedCustomerPage)
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.stateWrong);
                    break;
                case Instruction.outWarrantForcePass:
                    if (state != DefaultWarrant.WarrantState.PrintedCustomerPage)
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.stateWrong);
                    if (rightState < RightState.Level2)
                    {
                        if (specificWarrantType != DefaultWarrant.SpecificWarrantType.Service)
                        {
                            operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
                        }
                    }
                    break;
                case Instruction.outWarrantTakeOut:
                    if (state != DefaultWarrant.WarrantState.PrintedCustomerPage)
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.stateWrong);
                    if (payState != OutWarrant.OutWarrantPayState.ForcePass && payState != OutWarrant.OutWarrantPayState.Paid && payState != OutWarrant.OutWarrantPayState.OverPaid)
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantNotPaid);
                    break;
                case Instruction.outWarrantPrint:
                    if (state != DefaultWarrant.WarrantState.Submitted && state != DefaultWarrant.WarrantState.Printed)
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.stateWrong);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level1)
            {
                if ((mainInstruction == Instruction.outWarrantSubmit && returnOperator != identity.ID) || (mainInstruction != Instruction.outWarrantSubmit && creator != identity.ID))
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            ControlOneWarrantService service;
            switch (mainInstruction)
            {
                case Instruction.inWarrantPrint:
                    service = new ControlOneWarrantService(entities, WarrantType.In);
                    service.printWarrant(identity, warrant);
                    break;
                case Instruction.outWarrantPrintCustomerPage:
                    service = new ControlOneWarrantService(entities, WarrantType.Out);
                    service.printCustomerPageWarrant(identity, warrant);
                    break;
                case Instruction.outWarrantForcePass:
                    service = new ControlOneWarrantService(entities, WarrantType.Out);
                    service.forcePassWarrant(identity, warrant);
                    break;
                case Instruction.outWarrantTakeOut:
                    service = new ControlOneWarrantService(entities, WarrantType.Out);
                    service.takeOutWarrant(identity, warrant);
                    break;
                case Instruction.outWarrantPrint:
                    service = new ControlOneWarrantService(entities, WarrantType.Out);
                    service.printWarrant(identity, warrant);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = service.operatedState;
            return null;
        }
    }
}      