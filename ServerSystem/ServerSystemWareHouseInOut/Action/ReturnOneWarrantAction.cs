﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class ReturnOneWarrantAction : WareHouseInOutAction<Object>
    {
        public WarrantCSControl warrant { get; set; }
        public List<DefaultAttendant> attendants { get; set; }
        public List<TransReturnNumberPair> returnNumberPair { get; set; }

        public ReturnOneWarrantAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.outWarrantReturn:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override Object mainMethod()
        {
            if (warrant == null || returnNumberPair == null)
                throw new ParameterNotMatchException();
            RightState rightState;
            WarrantType warrantType;
            switch (mainInstruction)
            {
                case Instruction.outWarrantReturn:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutReturn);
                    warrantType = WarrantType.Out;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            else
            {
                DefaultWarrant.WarrantState state;
                OutWarrant.OutWarrantPayState? payState;
                DefaultWarrant.SpecificWarrantType specificWarrantType;
                int cookieOriRead, creator;
                int? returnOperator;
                getInfo(warrantType, warrant.ID, out state, out creator, out returnOperator, out cookieOriRead, out payState, out specificWarrantType);
                if (cookieOriRead != warrant.cookie)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.cookieWrong);
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            ControlOneWarrantService service;
            switch (mainInstruction)
            {
                case Instruction.outWarrantReturn:
                    service = new ControlOneWarrantService(entities, WarrantType.Out);
                    service.returnWarrant(identity, warrant, returnNumberPair, attendants);
                    this.operatedState = service.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            return null;
        }
    }
}
