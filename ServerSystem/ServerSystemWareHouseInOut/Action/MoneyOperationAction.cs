﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class MoneyOperationAction : WareHouseInOutAction<Object>
    {
        public WarrantCSControl warrant { get; set; }
        public DiscountItem discountItem { get; set; }
        public PayCheckItem payCheckItem { get; set; }

        public MoneyOperationAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.outWarrantDiscount:
                case Instruction.outWarrantPayCheck:
                case Instruction.outWarrantReturnPayCheck:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override Object mainMethod()
        {
            if (warrant == null)
                throw new ParameterNotMatchException();
            if ((mainInstruction == Instruction.outWarrantPayCheck || mainInstruction == Instruction.outWarrantReturnPayCheck) && payCheckItem == null)
                throw new ParameterNotMatchException();
            if (mainInstruction == Instruction.outWarrantDiscount && discountItem == null)
                throw new ParameterNotMatchException();
            RightState rightState;
            WarrantType warrantType;
            switch (mainInstruction)
            {
                case Instruction.outWarrantDiscount:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutDiscount);
                    warrantType = WarrantType.Out;
                    break;
                case Instruction.outWarrantPayCheck:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutMoney);
                    warrantType = WarrantType.Out;
                    break;
                case Instruction.outWarrantReturnPayCheck:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutReturnPayCheck);
                    warrantType = WarrantType.Out;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            else if (rightState < RightState.Level2 && mainInstruction == Instruction.outWarrantDiscount)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            DefaultWarrant.WarrantState state;
            OutWarrant.OutWarrantPayState? payState;
            DefaultWarrant.SpecificWarrantType specificWarrantType;
            int cookieOriRead, creator;
            int? returnOperator;
            object[] optionalParameter = new object[10];
            getInfo(warrantType, warrant.ID, out state, out creator, out returnOperator, out cookieOriRead, out payState, out specificWarrantType, optionalParameter);
            if (cookieOriRead != warrant.cookie)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.cookieWrong);
            if (optionalParameter != null && optionalParameter.Length >= 3)
            {
                int amountTotal = (int)optionalParameter[0];
                int? amountPermit = (int?)optionalParameter[1];
                int? amountPaid = (int?)optionalParameter[2];

                if (mainInstruction == Instruction.outWarrantDiscount)
                {
                    switch (state)
                    {
                        case DefaultWarrant.WarrantState.SubmittedCustomerPage:
                        case DefaultWarrant.WarrantState.PrintedCustomerPage:
                        case DefaultWarrant.WarrantState.TakenOut:
                        case DefaultWarrant.WarrantState.Returned:
                        case DefaultWarrant.WarrantState.Submitted:
                        case DefaultWarrant.WarrantState.Printed:
                            break;
                        default:
                            operatedState.addStates(OperatedState.OperatedConfirmingEnum.stateWrong);
                            break;
                    }
                    switch (specificWarrantType)
                    {
                        case DefaultWarrant.SpecificWarrantType.withReceipt:
                        case DefaultWarrant.SpecificWarrantType.withoutReceipt:
                            break;
                        default:
                            operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantNotSupportDiscount);
                            break;
                    }

                    if (discountItem.amountPermit != null && (discountItem.amountPermit > amountTotal || discountItem.amountPermit < 0))
                    {
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantPermitNotLegal);
                    }
                    // extra permission check
                    // operator can discount up to 2% and 100 whichever smaller
                    if (rightState < RightState.Level3 && 
                        (discountItem.amountPermit < amountTotal * 0.98 ||
                         amountTotal - discountItem.amountPermit > 10000))
                    {
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantTooMuchDiscount);
                    }
                }
                else if (mainInstruction == Instruction.outWarrantPayCheck)
                {
                    if (payCheckItem.amountPaid < 0)
                    {
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantPayAmountNotLegal);
                    }
                }
                else if (mainInstruction == Instruction.outWarrantReturnPayCheck)
                {
                    int amountShould = amountPermit == null ? amountTotal : amountPermit.Value;
                    int originalPaid = amountPaid == null ? 0 : amountPaid.Value;
                    if (originalPaid + payCheckItem.amountPaid < amountShould)          //payCheckItem.amountPaid is a negative number
                    {
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantNotSoMuchPayCheckToReturn);
                    }
                }
            }
            else
                throw new GetInfoException();
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            ControlOneWarrantService service;
            switch (mainInstruction)
            {
                case Instruction.outWarrantDiscount:
                    service = new ControlOneWarrantService(entities, WarrantType.Out);
                    service.discountWarrant(identity, this.discountItem, warrant);
                    break;
                case Instruction.outWarrantPayCheck:
                    service = new ControlOneWarrantService(entities, WarrantType.Out);
                    service.payCheckWarrant(identity, this.payCheckItem, warrant);
                    break;
                case Instruction.outWarrantReturnPayCheck:
                    service = new ControlOneWarrantService(entities, WarrantType.Out);
                    service.payCheckWarrant(identity, this.payCheckItem, warrant);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = service.operatedState;
            return null;
        }
    }
}      