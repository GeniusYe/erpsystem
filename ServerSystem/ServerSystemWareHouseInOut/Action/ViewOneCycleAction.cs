﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class ViewOneCycleAction : WareHouseInOutAction<ViewOneCycle>
    {
        public DateTime date{get;set;}

        public ViewOneCycleAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.warrantViewOneCycle:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override ViewOneCycle mainMethod()
        {
            if (date == null)
                throw new ParameterNotMatchException();
            DateTime stopDate = date.Date;
            stopDate = stopDate.AddDays(-date.Day + 28);
            DateTime startDate = stopDate.AddMonths(-1).AddDays(1);
            RightState rightState;
            rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rViewOneCycle);
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            RightState priceRightState = getWareHouseManageRights(WareHouseManage.Action.WareHouseManageAction<ViewOneCycle>.getRightsInstruction.rDisplayPrice);
            bool priceDisplay = false; 
            if (priceRightState > RightState.Level0)
                priceDisplay = true;
            ReadWarrantListService service = new ReadWarrantListService(entities, default(WarrantType));
            ViewOneCycle viewOneCycle = service.getOneCycleWarrant(identity, date, priceDisplay);
            this.operatedState = service.operatedState;
            return viewOneCycle;
        }
    }
}
