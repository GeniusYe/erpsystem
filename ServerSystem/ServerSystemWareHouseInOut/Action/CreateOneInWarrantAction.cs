﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class CreateOneInWarrantAction : WareHouseInOutAction<InWarrant>
    {
        public InWarrantCSCreate warrant { get; set; }

        public CreateOneInWarrantAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.inWarrantAdd:
                case Instruction.inWarrantEdit:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override InWarrant mainMethod()
        {
            if (warrant == null)
                throw new ParameterNotMatchException();
            RightState rightState;
            WarrantType warrantType;
            switch (mainInstruction)
            {
                case Instruction.inWarrantAdd:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rInAdd);
                    warrantType = WarrantType.In;
                    break;
                case Instruction.inWarrantEdit:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rInEdit);
                    warrantType = WarrantType.In;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (warrant == null)
                throw new ParameterNotMatchException();
            if (warrant.type != DefaultWarrant.SpecificWarrantType.In && warrant.type != DefaultWarrant.SpecificWarrantType.AdjustIn)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantTypeNotMatched);
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            if (mainInstruction == Instruction.inWarrantEdit)
            {
                DefaultWarrant.WarrantState state;
                OutWarrant.OutWarrantPayState? payState;
                DefaultWarrant.SpecificWarrantType specificWarrantType;
                int cookieOriRead, creator;
                int? returnOperator;
                getInfo(warrantType, warrant.ID, out state, out creator, out returnOperator, out cookieOriRead, out payState, out specificWarrantType);
                if (cookieOriRead != warrant.cookie)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.cookieWrong);
                if (rightState == RightState.Level1)
                {
                    if (creator != identity.ID)
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
                }
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            CreateOneWarrantService service;
            int id;
            switch (mainInstruction)
            {
                case Instruction.inWarrantEdit:
                    service = new CreateOneWarrantService(entities, WarrantType.In);
                    service.editInWarrant(identity, warrant, out id);
                    break;
                case Instruction.inWarrantAdd:
                    service = new CreateOneWarrantService(entities, WarrantType.In);
                    service.addInWarrant(identity, warrant, out id);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = service.operatedState;
            if (operatedState.state == OperatedState.OperatedStateEnum.normal || operatedState.state == OperatedState.OperatedStateEnum.success)
            {
                ReadSpecialOneWarrantService service2 = new ReadSpecialOneWarrantService(entities, WarrantType.In);
                return service2.getSpecialOneInWarrant(identity, id, true);
            }
            else
                return null;
        }
    }
}
