﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class ReadWarrantListAction : WareHouseInOutAction<List<DefaultWarrant>>
    {
        public DateTime startDate { get; set; }
        public DateTime stopDate { get; set; }
        public int target { get; set; }

        public ReadWarrantListAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.inWarrantList:
                case Instruction.outWarrantList:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override List<DefaultWarrant> mainMethod()
        {
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.inWarrantList:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rInLogIn);
                    break;
                case Instruction.outWarrantList:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutLogIn);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            RightState priceRightState = getWareHouseManageRights(WareHouseManage.Action.WareHouseManageAction<List<DefaultWarrant>>.getRightsInstruction.rDisplayPrice);
            bool priceDisplay;
            if (priceRightState != RightState.Level0)
                priceDisplay = true;
            else
                priceDisplay = false;
            ReadWarrantListService service;
            switch (mainInstruction)
            {
                case Instruction.inWarrantList:
                    service = new ReadWarrantListService(entities, WarrantType.In);
                    break;
                case Instruction.outWarrantList:
                    service = new ReadWarrantListService(entities, WarrantType.Out);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            List<DefaultWarrant> warrantList = service.getDefaultWarrant(identity, startDate, stopDate, target, priceDisplay);
            this.operatedState = service.operatedState;
            return warrantList;
        }
    }
}
