﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class ReadSpecialOneOutWarrantAction : WareHouseInOutAction<OutWarrant>
    {
        public int id { get; set; }

        public ReadSpecialOneOutWarrantAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.outWarrantSpecialOne:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override OutWarrant mainMethod()
        {
            RightState rightState, priceRightState;
            WarrantType warrantType;
            switch (mainInstruction)
            {
                case Instruction.outWarrantSpecialOne:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutDisplaySpecialOne);
                    warrantType = WarrantType.Out;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            else if (rightState == RightState.Level1)
            {
                DefaultWarrant.WarrantState state;
                OutWarrant.OutWarrantPayState? payState;
                DefaultWarrant.SpecificWarrantType specificWarrantType;
                int cookieOriRead, creator;
                int? returnOperator;
                getInfo(warrantType, id, out state, out creator, out returnOperator, out cookieOriRead, out payState, out specificWarrantType);
                if (creator != identity.ID)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            ReadSpecialOneWarrantService service;
            OutWarrant warrant;
            priceRightState = getWareHouseManageRights(WareHouseManage.Action.WareHouseManageAction<OutWarrant>.getRightsInstruction.rDisplayPrice);
            bool priceDisplay;
            if (priceRightState != RightState.Level0)
                priceDisplay = true;
            else
                priceDisplay = false;
            switch (mainInstruction)
            {
                case Instruction.outWarrantSpecialOne:
                    service = new ReadSpecialOneWarrantService(entities, WarrantType.Out);
                    warrant = service.getSpecialOneOutWarrant(identity, id, priceDisplay);
                    this.operatedState = service.operatedState;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            return warrant;
        }
    }
}
