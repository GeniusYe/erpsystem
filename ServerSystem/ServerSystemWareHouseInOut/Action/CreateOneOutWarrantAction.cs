﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class CreateOneOutWarrantAction : WareHouseInOutAction<OutWarrant>
    {
        public OutWarrantCSCreate warrant { get; set; }

        public CreateOneOutWarrantAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.outWarrantAdd:
                case Instruction.outWarrantEdit:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override OutWarrant mainMethod()
        {
            if (warrant == null)
                throw new ParameterNotMatchException();
            RightState rightState;
            WarrantType warrantType;
            switch (mainInstruction)
            {
                case Instruction.outWarrantAdd:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutAdd);
                    warrantType = WarrantType.Out;
                    break;
                case Instruction.outWarrantEdit:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutEdit);
                    warrantType = WarrantType.Out;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (warrant == null)
                throw new ParameterNotMatchException();
            if (warrant.type != DefaultWarrant.SpecificWarrantType.withoutReceipt 
                && warrant.type != DefaultWarrant.SpecificWarrantType.withReceipt
                && warrant.type != DefaultWarrant.SpecificWarrantType.Service
                && warrant.type != DefaultWarrant.SpecificWarrantType.Gift
                && warrant.type != DefaultWarrant.SpecificWarrantType.AdjustOut)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantTypeNotMatched);
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            if (mainInstruction == Instruction.outWarrantEdit)
            {
                DefaultWarrant.WarrantState state;
                OutWarrant.OutWarrantPayState? payState;
                DefaultWarrant.SpecificWarrantType specificWarrantType;
                int cookieOriRead, creator;
                int? returnOperator;
                getInfo(warrantType, warrant.ID, out state, out creator, out returnOperator, out cookieOriRead, out payState, out specificWarrantType);
                if (cookieOriRead != warrant.cookie)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.cookieWrong);
                if (rightState == RightState.Level1)
                {
                    if (creator != identity.ID)
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
                }
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            CreateOneWarrantService service;
            int id;
            switch (mainInstruction)
            {
                case Instruction.outWarrantEdit:
                    service = new CreateOneWarrantService(entities, WarrantType.Out);
                    service.editOutWarrant(identity, warrant, out id);
                    break;
                case Instruction.outWarrantAdd:
                    service = new CreateOneWarrantService(entities, WarrantType.Out);
                    service.addOutWarrant(identity, warrant, out id);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = service.operatedState;
            if (operatedState.state == OperatedState.OperatedStateEnum.normal || operatedState.state == OperatedState.OperatedStateEnum.success)
            {
                ReadSpecialOneWarrantService service2 = new ReadSpecialOneWarrantService(entities, WarrantType.Out);
                return service2.getSpecialOneOutWarrant(identity, id, true);
            }
            else
                return null;
        }
    }
}
