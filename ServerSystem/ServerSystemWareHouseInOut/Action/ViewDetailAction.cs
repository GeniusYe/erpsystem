﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class ViewDetailAction : WareHouseInOutAction<DetailAccount>
    {
        public DateTime startDate { get; set; }
        public DateTime stopDate { get; set; }
        public int id { get; set; }

        public ViewDetailAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.warrantViewDetail:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override DetailAccount mainMethod()
        {
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.warrantViewDetail:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rViewDetail);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            ViewDetailService service = new ViewDetailService(entities, WarrantType.Out);   //no matter sell or in
            DetailAccount detailAccount;
            switch (mainInstruction)
            {
                case Instruction.warrantViewDetail:
                    detailAccount = service.getDetailAccount(startDate, stopDate, id);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = service.operatedState;
            return detailAccount;
        }
    }
}
