﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.WareHouseInOut.Service;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Action
{
    class ReadAttendantsAction : WareHouseInOutAction<List<DefaultAttendant>>
    {
        public ReadAttendantsAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.warrantAttendantsList:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override List<DefaultAttendant> mainMethod()
        {
            List<DefaultAttendant> attendantList = null;
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.warrantAttendantsList:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rAttendantsList);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            ReadAttendantsService service = new ReadAttendantsService(entities);
            switch (mainInstruction)
            {
                case Instruction.warrantAttendantsList:
                    attendantList = service.getDefaultAttendants(identity);
                    break;
            }
            this.operatedState = service.operatedState;
            return attendantList;
        }
    }
}
