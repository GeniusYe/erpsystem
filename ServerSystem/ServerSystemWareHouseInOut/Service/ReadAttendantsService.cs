﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Service
{
    class ReadAttendantsService : Service
    {
        public ReadAttendantsService(ERPSystemEntities entities)
            : base(entities) { }

        /// <summary>
        /// get DefaultInWarrant from SQL server
        /// set 0 in 1 out 2 inout
        /// </summary>
        public List<DefaultAttendant> getDefaultAttendants(SharedObject.Identity.Stuff.IdentityMark identity)
        {
            //DateTime nowDate = DatabaseAssistant.ReadAssistant.getSqlServerDate(CreateNewCommand());
            List<DefaultAttendant> attendantList = new List<DefaultAttendant>();
            IEnumerable<DefaultAttendant> spAttendantList = from spDefaultAttendant in entities.SP_Warrant_Attendants_GetDefaultOnes() select new DefaultAttendant() { id = spDefaultAttendant.id, name = spDefaultAttendant.name };
            foreach (DefaultAttendant cus in spAttendantList)
                attendantList.Add(cus);
            operatedState.state = OperatedState.OperatedStateEnum.success;
            return attendantList;
        }
    }
}
