﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Normal;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Service
{
    class CreateOneWarrantService : WareHouseInOutService
    {
        public CreateOneWarrantService(ERPSystemEntities entities, WarrantType warrantType)
            : base(entities, warrantType) { }

        /// <summary>
        /// edit a inWarrant
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="inWarrant">the inWarrant to be save into database to cover the old one</param>
        /// <returns></returns>
        public void editInWarrant(SharedObject.Identity.Stuff.IdentityMark identity, InWarrantCSCreate inWarrant, out int id)
        {
            id = 0;
            if (inWarrant != null && inWarrant.ID > 0)
            {
                bool t = CheckInWarrantMultipleDraft(inWarrant);
                if (!t)
                    return;
                id = inWarrant.ID;
                int cookieOri = inWarrant.cookie, cookieNew;
                cookieNew = generateNewCookie(cookieOri);
                int? i = entities.SP_Warrant_In_Edit(id, inWarrant.target, DefaultWarrant.SpecificWarrantTypeToInt(inWarrant.type), identity.ID, cookieOri, cookieNew, inWarrant.comment).First();
                if (i == 1)
                {
                    InWarrantAddItem(inWarrant);
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                }
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        /// <summary>
        /// Add a new inWarrant to database
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="inWarrant">the inWarrant being added</param>
        /// <returns></returns>
        public void addInWarrant(SharedObject.Identity.Stuff.IdentityMark identity, InWarrantCSCreate inWarrant, out int id)
        {
            id = 0;
            if (inWarrant != null)
            {
                bool t = CheckInWarrantMultipleDraft(inWarrant);
                if (!t)
                    return;
                int cookieOri = 0, cookieNew;
                cookieNew = generateNewCookie(cookieOri);
                int? i = entities.SP_Warrant_In_Add(inWarrant.target, DefaultWarrant.SpecificWarrantTypeToInt(inWarrant.type), identity.ID, cookieNew, inWarrant.comment).First();
                if (i > 0)
                {
                    id = i.Value;
                    inWarrant.ID = id;
                    InWarrantAddItem(inWarrant);
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                }
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        private bool CheckInWarrantMultipleDraft(InWarrantCSCreate inWarrant)
        {
            bool t = true;
            HashSet<Int32> hashSet = new HashSet<int>();
            foreach (InWarrantItemCS item in inWarrant.items)
            {
                if (hashSet.Contains(item.IDdraft))
                {
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantMultipleDraftInOneWarrant);
                    t = false;
                }
                hashSet.Add(item.IDdraft);
            }
            return t;
        }

        //called by addInWarrant or editInWarrant to addInWarrantItems
        private void InWarrantAddItem(InWarrantCSCreate inWarrant)
        {
            foreach (InWarrantItemCS item in inWarrant.items)
            {
                if (item.price == 0 || item.num == 0)
                    continue;
                entities.SP_Warrant_In_AddItem(
                    item.IDdraft,
                    inWarrant.ID,
                    item.price,
                    item.priceWithTax,
                    item.num,
                    item.remark
                );
            }
        }
        /// <summary>
        /// edit a outWarrant
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="outWarrant">the inWarrant to be save into database to cover the old one</param>
        /// <returns></returns>
        public void editOutWarrant(SharedObject.Identity.Stuff.IdentityMark identity, OutWarrantCSCreate outWarrant, out int id)
        {
            id = 0;
            if (outWarrant != null && outWarrant.ID > 0)
            {
                bool t = CheckOutWarrantMultipleDraft(outWarrant);
                if (!t)
                    return;
                id = outWarrant.ID;
                int cookieOri = outWarrant.cookie, cookieNew;
                cookieNew = generateNewCookie(cookieOri);
                int? i = entities.SP_Warrant_Out_Edit(id, outWarrant.target, identity.ID, DefaultWarrant.SpecificWarrantTypeToInt(outWarrant.type), cookieOri, cookieNew, outWarrant.comment).First();
                if (i == 1)
                {
                    OutWarrantAddItem(outWarrant);
                    OutWarrantAddAttendant(outWarrant);
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                }
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }
        /// <summary>
        /// Add a new outWarrant to database
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="outWarrant">the outWarrant being added</param>
        /// <returns></returns>
        public void addOutWarrant(SharedObject.Identity.Stuff.IdentityMark identity, OutWarrantCSCreate outWarrant, out int id)
        {
            id = 0;
            if (outWarrant != null)
            {
                bool t = CheckOutWarrantMultipleDraft(outWarrant);
                if (!t)
                    return;
                int cookieOri = 0, cookieNew;
                cookieNew = generateNewCookie(cookieOri);
                int? i = entities.SP_Warrant_Out_Add(outWarrant.target, identity.ID, cookieNew, DefaultWarrant.SpecificWarrantTypeToInt(outWarrant.type), outWarrant.comment).First();
                if (i > 0)
                {
                    id = i.Value;
                    outWarrant.ID = id;
                    OutWarrantAddItem(outWarrant);
                    OutWarrantAddAttendant(outWarrant);
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                }
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        private bool CheckOutWarrantMultipleDraft(OutWarrantCSCreate outWarrant)
        {
            bool t = true;
            HashSet<Int32> hashSet = new HashSet<int>();
            foreach (OutWarrantItemCS item in outWarrant.items)
            {
                if (hashSet.Contains(item.IDdraft))
                {
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantMultipleDraftInOneWarrant);
                    t = false;
                }
                hashSet.Add(item.IDdraft);
            }
            return t;
        }

        //called by addOutWarrant or editOutWarrant to addOutWarrantItems
        private void OutWarrantAddItem(OutWarrantCSCreate outWarrant)
        {
            foreach (OutWarrantItemCS item in outWarrant.items)
            {
                if (item.price == 0 || item.num == 0)
                    continue;
                entities.SP_Warrant_Out_AddItem(item.IDdraft, outWarrant.ID, item.num, item.priceAdjustMannual, item.remark);
            }
        }

        //called by addOutWarrant or editOutWarrant to addOutWarrantItems
        private void OutWarrantAddAttendant(OutWarrantCSCreate outWarrant)
        {
            foreach (DefaultAttendant attendant in outWarrant.attendants)
            {
                entities.SP_Warrant_Out_AddAttendant(outWarrant.ID, attendant.id);
            }
        }
    }
}
