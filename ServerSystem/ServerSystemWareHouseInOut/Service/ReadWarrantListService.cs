﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Service
{
    class ReadWarrantListService : WareHouseInOutService
    {
        public ReadWarrantListService(ERPSystemEntities entities, WarrantType warrantType)
            : base(entities, warrantType) { }

        /// <summary>
        /// get DefaultInWarrant from SQL server
        /// </summary>
        public List<DefaultWarrant> getDefaultWarrant(SharedObject.Identity.Stuff.IdentityMark identity, DateTime startDate, DateTime stopDate, int target, bool priceDisplay)
        {
            if (startDate > stopDate) return null;
            List<DefaultWarrant> warrantList = new List<DefaultWarrant>();
            if (_warrantType == WarrantType.In)
            {
                IEnumerable<SP_Warrant_In_GetDefaultOnes_Result> spResultList = entities.SP_Warrant_In_GetDefaultOnes(startDate, stopDate, target);
                foreach (SP_Warrant_In_GetDefaultOnes_Result spResult in spResultList)
                {
                    DefaultWarrant warrant = readDefaultInWarrant(spResult);
                    warrantList.Add(warrant);
                }
                operatedState.state = OperatedState.OperatedStateEnum.success;
            }
            else if (_warrantType == WarrantType.Out)
            {
                IEnumerable<SP_Warrant_Out_GetDefaultOnes_Result> spResultList = entities.SP_Warrant_Out_GetDefaultOnes(startDate, stopDate, target);
                foreach (SP_Warrant_Out_GetDefaultOnes_Result spResult in spResultList)
                {
                    DefaultWarrant warrant = readDefaultOutWarrant(spResult, priceDisplay);
                    warrantList.Add(warrant);
                }
                operatedState.state = OperatedState.OperatedStateEnum.success;
            }
            else
                throw new ParameterNotMatchException();
            return warrantList;
        }

        private DefaultWarrant readDefaultInWarrant(SP_Warrant_In_GetDefaultOnes_Result result)
        {
            DefaultWarrant warrant = new DefaultWarrant() {
                ID = result.ID,
                number = result.number,
                state = DefaultWarrant.IntToState(result.state),
                type = DefaultWarrant.IntToSpecificWarrantType(result.type),
                amountIn = result.amountIn
            };
            return warrant;
        }

        private DefaultWarrant readDefaultOutWarrant(SP_Warrant_Out_GetDefaultOnes_Result result, bool priceDisplay)
        {
            DefaultWarrant warrant = new DefaultWarrant() {
                ID = result.ID,
                number = result.number,
                state = DefaultWarrant.IntToState(result.state),
                type = DefaultWarrant.IntToSpecificWarrantType(result.type),
                payState = OutWarrant.IntToPayState(result.payState),
                amountTotal = result.amountTotal,
                amountCost = priceDisplay ? result.amountCost : 0,
                amountPermit = result.amountPermit,
            };
            return warrant;
        }

        private ViewOnceCycleInWarrant readOneCycleInWarrant(SP_Warrant_In_ViewOneCycle_Result result)
        {
            ViewOnceCycleInWarrant warrant = new ViewOnceCycleInWarrant() {
                ID = result.ID,
                number = result.number,
                state = DefaultWarrant.IntToState(result.state),
                type = DefaultWarrant.IntToSpecificWarrantType(result.type),
                amountIn = result.amountIn,
                target_name = result.target_name,
                submitDate = result.submitDate,
            };
            return warrant;
        }

        private ViewOnceCycleOutWarrant readOneCycleOutWarrant(SP_Warrant_Out_ViewOneCycle_Result result, bool priceDisplay)
        {
            ViewOnceCycleOutWarrant warrant = new ViewOnceCycleOutWarrant()
            {
                ID = result.ID,
                number = result.number,
                state = DefaultWarrant.IntToState(result.state),
                type = DefaultWarrant.IntToSpecificWarrantType(result.type),
                payState = OutWarrant.IntToPayState(result.payState),
                amountTotal = result.amountTotal,
                amountCost = priceDisplay ? result.amountCost : 0,
                amountPermit = result.amountPermit,
                amountServiceFee = result.amountServicefee,
                attendant_names = result.attendant_names,
                target = result.target,
                target_name = result.target_name,
                submitCustomerPageDate = result.submitCustomerPageDate,
                submitDate = result.submitDate,
            };
            return warrant;
        }

        public ViewOneCycle getOneCycleWarrant(SharedObject.Identity.Stuff.IdentityMark identity, DateTime date, bool priceDisplay)
        {
            List<ViewOnceCycleInWarrant> inWarrantList = new List<ViewOnceCycleInWarrant>();
            List<ViewOnceCycleOutWarrant> outWarrantList = new List<ViewOnceCycleOutWarrant>();
            IEnumerable<SP_Warrant_In_ViewOneCycle_Result> inOneCycleResult = entities.SP_Warrant_In_ViewOneCycle(date);
            IEnumerable<SP_Warrant_Out_ViewOneCycle_Result> outOneCycleResult = entities.SP_Warrant_Out_ViewOneCycle(date);
            foreach (SP_Warrant_In_ViewOneCycle_Result inResult in inOneCycleResult)
                inWarrantList.Add(readOneCycleInWarrant(inResult));
            foreach (SP_Warrant_Out_ViewOneCycle_Result outResult in outOneCycleResult)
                outWarrantList.Add(readOneCycleOutWarrant(outResult, priceDisplay));

            ViewOneCycle viewOneCycle = new ViewOneCycle();
            viewOneCycle.inWarrantList = inWarrantList;
            viewOneCycle.outWarrantList = outWarrantList;
            viewOneCycle.date = date;
            operatedState.state = OperatedState.OperatedStateEnum.success;
            return viewOneCycle;
        }
    }
}
