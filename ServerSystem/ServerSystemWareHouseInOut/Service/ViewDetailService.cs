﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Service
{
    class ViewDetailService : WareHouseInOutService
    {
        public ViewDetailService(ERPSystemEntities entities, WarrantType warrantType)
            : base(entities, warrantType) { }

        public DetailAccount getDetailAccount(DateTime startDate, DateTime stopDate, int id)
        {
            DetailAccount detailAccount = new DetailAccount();
            if (id > 0)
            {
                List<DetailAccount.InDetail> inDetails = new List<DetailAccount.InDetail>();
                List<DetailAccount.OutDetail> outDetails = new List<DetailAccount.OutDetail>();
                IEnumerable<SP_Warrant_In_ViewDetail_Result> inDetailResults = entities.SP_Warrant_In_ViewDetail(id, startDate, stopDate);
                foreach (SP_Warrant_In_ViewDetail_Result inDetailResult in inDetailResults)
                {
                    DetailAccount.InDetail inDetail = new DetailAccount.InDetail()
                        {
                            name = inDetailResult.name,
                            specification = inDetailResult.specification,
                            remark = inDetailResult.remark,
                            num = inDetailResult.num,
                            price = inDetailResult.price,
                            IDWarrant = inDetailResult.IDWarrant,
                            dateTime = inDetailResult.dateSubmit.Value,
                            currentNumAll = inDetailResult.currentNumAll.Value,
                            currentValue = inDetailResult.currentValue.Value
                        };
                    inDetails.Add(inDetail);
                }
                IEnumerable<SP_Warrant_Out_ViewDetail_Result> outDetailResults = entities.SP_Warrant_Out_ViewDetail(id, startDate, stopDate);
                foreach (SP_Warrant_Out_ViewDetail_Result outDetailResult in outDetailResults)
                {
                    DetailAccount.OutDetail outDetail = new DetailAccount.OutDetail()
                        {
                            name = outDetailResult.name,
                            specification = outDetailResult.specification,
                            remark = outDetailResult.remark,
                            num = outDetailResult.num,
                            price = outDetailResult.price,
                            IDWarrant = outDetailResult.IDWarrant,
                            numReturn = outDetailResult.numReturn,
                            priceAdjustMannual = outDetailResult.priceAdjustMannual,
                            priceCost = outDetailResult.priceCost.Value,
                            dateTime = outDetailResult.dateSubmit.Value,
                            currentNumAll = outDetailResult.currentNumAll.Value,
                            currentValue = outDetailResult.currentValue.Value
                        };
                    outDetails.Add(outDetail);
                }
                detailAccount.inDetails = inDetails;
                detailAccount.outDetails = outDetails;
                operatedState.state = OperatedState.OperatedStateEnum.success;
            }
            else
                throw new ParameterNotMatchException();
            return detailAccount;
        }
    }
}
