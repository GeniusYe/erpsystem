﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Service
{
    class GetSpecialPriceService : WareHouseInOutService
    {
        public GetSpecialPriceService(ERPSystemEntities entities, WarrantType warrantType)
            : base(entities, warrantType) { }

        public SpecialPriceSC getSpecialPrice(SharedObject.Identity.Stuff.IdentityMark identity, GetSpecialPriceCS specialPriceInfo)
        {
            SpecialPriceSC specialPrice = null;
            if (specialPriceInfo != null)
            {
                int? target = specialPriceInfo.target;
                if (target == null)
                {
                    this.operatedState.addStates(OperatedState.OperatedConfirmingEnum.draftSpecialPriceTargetNotExists);
                    return null;
                }
                specialPrice = new SpecialPriceSC();
                List<SpecialPriceSC.DraftWithSpecialPrice> intPairs = new List<SpecialPriceSC.DraftWithSpecialPrice>();
                specialPrice.draftIDWithPrice = intPairs;
                foreach (Int32 draftID in specialPriceInfo.draftID)
                {
                    var spResults = from result in entities.SP_Draft_GetSpecialPrice(target, draftID) select new SpecialPriceSC.DraftWithSpecialPrice(){draftID = draftID, priceOutH = result.priceOutH, priceOutL = result.priceOutL};
                    foreach (SpecialPriceSC.DraftWithSpecialPrice price in spResults)
                    {
                        intPairs.Add(price);
                        break;
                    }
                }
                operatedState.state = OperatedState.OperatedStateEnum.success;
            }
            else
                throw new ParameterNotMatchException();
            return specialPrice;
        }
    }
}
