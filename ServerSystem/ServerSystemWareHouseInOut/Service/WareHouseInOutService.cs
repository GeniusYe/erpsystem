﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Service
{
    class WareHouseInOutService : Service
    {
        protected WarrantType _warrantType;
        public WarrantType warrantType
        {
            set
            {
                this._warrantType = value;
            }
            get
            {
                return _warrantType;
            }
        }

        public WareHouseInOutService(ERPSystemEntities entities, WarrantType warrantType)
            : base(entities)
        {
            this.warrantType = warrantType;
        }
    }
}
