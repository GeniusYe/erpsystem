﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Service
{
    class ReadSpecialOneWarrantService : WareHouseInOutService
    {
        public ReadSpecialOneWarrantService(ERPSystemEntities entities, WarrantType warrantType)
            : base(entities, warrantType) { }

        /// <summary>
        /// Get a special inWarrant according to id
        /// </summary>
        /// <param name="identity"></param>
        public InWarrant getSpecialOneInWarrant(SharedObject.Identity.Stuff.IdentityMark identity, int id, bool priceDisplay)
        {
            InWarrant inWarrant;
            if (id > 0)
            {
                inWarrant = (from result in entities.SP_Warrant_In_Read(id)
                             select new InWarrant()
                             {
                                 ID = result.ID,
                                 number = result.number,
                                 target = result.target,
                                 state = DefaultWarrant.IntToState(result.state),
                                 type = DefaultWarrant.IntToSpecificWarrantType(result.type),
                                 comment = result.comment,
                                 cookie = result.cookie,
                                 amountIn = priceDisplay ? result.amountIn : 0
                             }).First();
                IEnumerable<SP_Warrant_ReadOperationRecord_Result> operationRecords = entities.SP_Warrant_In_ReadOperationRecord(id);
                ReadOperationRecord(inWarrant, operationRecords);
                IEnumerable<SP_Warrant_In_ReadItem_Result> items = entities.SP_Warrant_In_ReadItem(id);
                foreach (SP_Warrant_In_ReadItem_Result item in items)
                {
                    InWarrantItem inWarrantItem = new InWarrantItem()
                            {
                                ID = item.ID,
                                IDdraft = item.IDDraft,
                                IDWarrant = id,
                                num = item.num,
                                remark = item.remark,
                                name = item.name,
                                specification = item.specification,
                                hiddenSpecification = item.hiddenSpecification,
                                price = priceDisplay ? item.price : 0,
                                unit = item.unit
                            };
                    inWarrant.items.Add(inWarrantItem);
                }
                operatedState.state = OperatedState.OperatedStateEnum.normal;
            }
            else
                throw new ParameterNotMatchException();
            return inWarrant;
        }

        /// <summary>
        /// Get a special outWarrant according to id
        /// </summary>
        /// <param name="identity"></param>
        public OutWarrant getSpecialOneOutWarrant(SharedObject.Identity.Stuff.IdentityMark identity, int id, bool priceDisplay)
        {
            OutWarrant outWarrant;
            if (id > 0)
            {
                outWarrant = (from result in entities.SP_Warrant_Out_Read(id)
                              select new OutWarrant()
                              {
                                  ID = result.ID,
                                  number = result.number,
                                  target = result.target,
                                  state = DefaultWarrant.IntToState(result.state),
                                  type = DefaultWarrant.IntToSpecificWarrantType(result.type),
                                  payState = OutWarrant.IntToPayState(result.payState),
                                  comment = result.comment,
                                  cookie = result.cookie,
                                  amountTotal = result.amountTotal,
                                  amountPermit = result.amountPermit,
                                  amountPaid = result.amountPaid,
                                  amountCost = priceDisplay ? result.amountCost : 0
                              }).First();
                IEnumerable<SP_Warrant_ReadOperationRecord_Result> operationRecords = entities.SP_Warrant_Out_ReadOperationRecord(id);
                ReadOperationRecord(outWarrant, operationRecords);
                IEnumerable<SP_Warrant_Out_ReadItem_Result> items = entities.SP_Warrant_Out_ReadItem(id);
                foreach (SP_Warrant_Out_ReadItem_Result item in items)
                {
                    OutWarrantItem outWarrantItem = new OutWarrantItem()
                    {
                        ID = item.ID,
                        IDdraft = item.IDDraft,
                        IDWarrant = id,
                        num = item.num,
                        numReturn = item.numReturn,
                        price = priceDisplay ? item.price : 0,
                        priceAdjustMannual = item.priceAdjustMannual,
                        remark = item.remark,
                        name = item.Name,
                        specification = item.specification,
                        hiddenSpecification = item.hiddenSpecification,
                        priceOutH = item.priceOutH.Value,
                        priceOutL = item.priceOutL.Value,
                        priceCostValue = priceDisplay ? (item.priceCost == null ? 0 : item.priceCost.Value) : 0,
                        unit = item.unit
                    };
                    outWarrant.items.Add(outWarrantItem);
                }
                IEnumerable<SP_Warrant_Out_ReadWarrantAttendant_Result> attendants = entities.SP_Warrant_Out_ReadWarrantAttendant(id);
                foreach (SP_Warrant_Out_ReadWarrantAttendant_Result attendant in attendants)
                {
                    outWarrant.addAttendants(new DefaultAttendant() { id = attendant.id, name = attendant.name });
                }
                operatedState.state = OperatedState.OperatedStateEnum.normal;
            }
            else
                throw new ParameterNotMatchException();
            return outWarrant;
        }

        private void ReadOperationRecord(AbstractWarrant warrant, IEnumerable<SP_Warrant_ReadOperationRecord_Result> operationRecords)
        {
            foreach (SP_Warrant_ReadOperationRecord_Result record in operationRecords)
            {
                WarrantOperationRecord operationRecord = new WarrantOperationRecord()
                {
                    ID = record.ID,
                    warrantId = record.warrantId.Value,
                    operation = (WarrantOperationEnum)record.operation,
                    operationOperator = record.operationOperator.Value,
                    operationOperatorName = record.operationOperatorName,
                    operatedTime = record.operatedTime.Value,
                };
                warrant.operationRecord.Add(operationRecord);
            }
        }
    }
}
