﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;

namespace ServerSystem.WareHouseInOut.Service
{
    class ControlOneWarrantService : WareHouseInOutService
    {
        public ControlOneWarrantService(ERPSystemEntities entities, WarrantType warrantType)
            : base(entities, warrantType) { }

        /// <summary>
        /// to print a Warrant with the id and cookie
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="id">instruct which warrant to be printed</param>
        /// <param name="cookieOri">to check the warrant which is to be cancelled. not for transmission</param>
        /// <returns></returns>
        public void printWarrant(SharedObject.Identity.Stuff.IdentityMark identity, WarrantCSControl warrant)
        {
            if (warrant != null && warrant.ID > 0)
            {
                int id = warrant.ID;
                int cookieOri = warrant.cookie, cookieNew = generateNewCookie(cookieOri);
                int? i;
                if (_warrantType == WarrantType.In)
                    i = entities.SP_Warrant_In_Print(id, cookieOri, cookieNew, identity.ID).First();
                else if (_warrantType == WarrantType.Out)
                    i = entities.SP_Warrant_Out_Print(id, cookieOri, cookieNew, identity.ID).First();
                else
                    throw new ParameterNotMatchException();
                if (i == 1)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        /// <summary>
        /// to payCheck a outWarrant with the id and cookie
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="id">instruct which warrant to be paychecked</param>
        /// <param name="cookieOri">to check the warrant which is to be paychecked. not for transmission</param>
        /// <returns></returns>
        public void forcePassWarrant(SharedObject.Identity.Stuff.IdentityMark identity, WarrantCSControl warrant)
        {
            if (warrant != null && warrant.ID > 0)
            {
                int id = warrant.ID;
                int cookieNew = 0, cookieOri = warrant.cookie;
                cookieNew = generateNewCookie(cookieOri);
                int? i = entities.SP_Warrant_Out_ForcePass(id, cookieOri, cookieNew, identity.ID).First();
                if (i == 1)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        /// <summary>
        /// to mark a Warrant which is taken out with the id and cookie
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="id">instruct which warrant to be taken out</param>
        /// <param name="cookieOri">to check the warrant which is to be taken out. not for transmission</param>
        /// <returns></returns>
        public void takeOutWarrant(SharedObject.Identity.Stuff.IdentityMark identity, WarrantCSControl warrant)
        {
            if (warrant != null && warrant.ID > 0)
            {
                int id = warrant.ID;
                int cookieNew = 0, cookieOri = warrant.cookie;
                cookieNew = generateNewCookie(cookieOri);
                int? i = entities.SP_Warrant_Out_TakeOut(id, cookieOri, cookieNew, identity.ID).First();
                if (i == 1)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        /// <summary>
        /// to print a Warrant with the id and cookie
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="id">instruct which warrant to be printed</param>
        /// <param name="cookieOri">to check the warrant which is to be cancelled. not for transmission</param>
        /// <returns></returns>
        public void printCustomerPageWarrant(SharedObject.Identity.Stuff.IdentityMark identity, WarrantCSControl warrant)
        {
            if (warrant != null && warrant.ID > 0)
            {
                int id = warrant.ID;
                int cookieNew = 0, cookieOri = warrant.cookie;
                cookieNew = generateNewCookie(cookieOri);
                int? i = entities.SP_Warrant_Out_PrintCustomerPage(id, cookieOri, cookieNew, identity.ID).First();
                if (i == 1)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        /// <summary>
        /// to submit a Warrant with the id and cookie
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="id">instruct which warrant to be submitted</param>
        /// <param name="cookieOri">to check the warrant which is to be cancelled. not for transmission</param>
        /// <returns></returns>
        public void submitInWarrant(SharedObject.Identity.Stuff.IdentityMark identity, WarrantCSControl warrant)
        {
            if (warrant != null && warrant.ID > 0)
            {
                int id = warrant.ID;
                int cookieNew = 0, cookieOri = warrant.cookie;
                cookieNew = generateNewCookie(cookieOri);
                int? i = entities.SP_Warrant_In_Submit(id, cookieOri, cookieNew, identity.ID).First();
                if (i == 1)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        public void submitOutWarrantCustomerPage(SharedObject.Identity.Stuff.IdentityMark identity, WarrantCSControl warrant)
        {
            if (warrant != null && warrant.ID > 0)
            {
                int id = warrant.ID;
                int cookieOri = warrant.cookie;
                int cookieNew = 0;
                cookieNew = generateNewCookie(cookieOri);
                int num, numAccessiable;
                string name;
                IEnumerable<SP_Warrant_Out_SubmitReadForCheck_Result> spResultList = entities.SP_Warrant_Out_SubmitReadForCheck(id);
                foreach (SP_Warrant_Out_SubmitReadForCheck_Result spResult in spResultList)
                {
                    num = spResult.num;
                    numAccessiable = spResult.numAccessiable;
                    name = spResult.Name.Trim() + " " + spResult.specification.Trim();
                    if (num > numAccessiable)
                        operatedState.addStates(SharedObject.Main.Transmission.OperatedState.OperatedConfirmingEnum.warrantNotEnoughDraft, name);
                }
                if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                    return;
                int? i = entities.SP_Warrant_Out_SubmitCustomerPage(id, cookieOri, cookieNew, identity.ID).First();
                if (i == 1)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        public void submitOutWarrant(SharedObject.Identity.Stuff.IdentityMark identity, WarrantCSControl warrant, bool ifIgnoreDate)
        {
            if (warrant != null && warrant.ID > 0)
            {
                int id = warrant.ID;
                int cookieOri = warrant.cookie;
                int cookieNew = 0;
                cookieNew = generateNewCookie(cookieOri);
                int num, numReturn;

                IEnumerable<SP_Warrant_Out_SubmitReadForCheck_Result> spResultList = entities.SP_Warrant_Out_SubmitReadForCheck(id);
                foreach (SP_Warrant_Out_SubmitReadForCheck_Result spResult in spResultList)
                {
                    num = spResult.num;
                    numReturn = spResult.numReturn;
                    if (numReturn > num)
                        operatedState.addStates(SharedObject.Main.Transmission.OperatedState.OperatedConfirmingEnum.warrantReturnMoreThanSell);
                }
                if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                    return;
                int? i;
                if (ifIgnoreDate)
                    i = entities.SP_Warrant_Out_Submit(id, cookieOri, cookieNew, identity.ID, 1).First();
                else
                    i = entities.SP_Warrant_Out_Submit(id, cookieOri, cookieNew, identity.ID, (int?)null).First();
                if (i == 1)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else if (i == -2)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantSubmitToLate);
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        /// <summary>
        /// to set a Warrant which is returned with the id and cookie
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="id">instruct which warrant to be returned</param>
        /// <param name="cookieOri">to check the warrant which is to be returned. not for transmission</param>
        /// <returns></returns>
        public void returnWarrant(SharedObject.Identity.Stuff.IdentityMark identity, WarrantCSControl warrant, List<TransReturnNumberPair> returnNumberPair, List<DefaultAttendant> attendants)
        {
            if (warrant != null && warrant.ID > 0)
            {
                int id = warrant.ID;
                int cookieNew = 0, cookieOri = warrant.cookie;
                cookieNew = generateNewCookie(cookieOri);
                int? i = entities.SP_Warrant_Out_Return(id, cookieOri, cookieNew, identity.ID).First();
                if (i == 1)
                {
                    foreach (TransReturnNumberPair pair in returnNumberPair)
                    {
                        i = entities.SP_Warrant_Out_ReturnItem(warrant.ID, pair.number1, pair.number2).First();
                        if (i == -2)
                            break;
                    }
                    if (i == -2)
                        operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantReturnMoreThanSell);
                    else
                        operatedState.state = OperatedState.OperatedStateEnum.success;
                    OutWarrantAddAttendant(warrant.ID, attendants);
                }
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        //called by addOutWarrant or editOutWarrant to addOutWarrantItems
        private void OutWarrantAddAttendant(int warrant_id, List<DefaultAttendant> attendants)
        {
            foreach (DefaultAttendant attendant in attendants)
            {
                entities.SP_Warrant_Out_AddAttendant(warrant_id, attendant.id);
            }
        }

        public void discountWarrant(SharedObject.Identity.Stuff.IdentityMark identity, DiscountItem discountItem, WarrantCSControl warrantCSControl)
        {
            if (discountItem != null && warrantCSControl != null && discountItem.warrantId == warrantCSControl.ID)
            {
                int id = discountItem.warrantId;
                int cookieOri = warrantCSControl.cookie;
                int cookieNew = generateNewCookie(cookieOri);
                int? i = entities.SP_Warrant_Out_Discount(id, cookieOri, cookieNew, identity.ID, discountItem.amountPermit, discountItem.reason).First();
                if (i == 1)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else if (i == -1)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.warrantWarrantTooOldToHandleDiscount);
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }

        public void payCheckWarrant(SharedObject.Identity.Stuff.IdentityMark identity, PayCheckItem payCheckItem, WarrantCSControl warrantCSControl)
        {
            if (payCheckItem != null && warrantCSControl != null && payCheckItem.warrantId == warrantCSControl.ID)
            {
                int id = payCheckItem.warrantId;
                int cookieOri = warrantCSControl.cookie;
                int cookieNew = generateNewCookie(cookieOri);
                int? i = entities.SP_Warrant_Out_PayCheck(id, cookieOri, cookieNew, identity.ID, payCheckItem.amountPaid).First();
                if (i == 1)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }
    }
}