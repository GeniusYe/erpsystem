﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.CustomerManage.Service;
using SharedObject.Main.Transmission;
using SharedObject.CustomerManage.Stuff;
 
namespace ServerSystem.CustomerManage.Action
{
    class CreateOneCustomerAction : CustomerManageAction<Customer>
    {
        public Customer customer { get; set; }

        public CreateOneCustomerAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.customerAdd:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override Customer mainMethod()
        {
            if (customer == null)
                throw new ParameterNotMatchException();
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.customerAdd:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rAdd);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            int id;
            CreateOneCustomerService service = new CreateOneCustomerService(entities);
            switch (mainInstruction)
            {
                case Instruction.customerAdd:
                    if (customer == null)
                        throw new ParameterNotMatchException();
                    service.addNewCustomer(identity, customer, out id);
                    if (id > 0)
                        customer = (new ReadSpecialOneCustomerService(entities)).getSpecialOneCustomer(identity, id);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = service.operatedState;
            return customer;
        }
    }
}
