﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.CustomerManage.Service;
using SharedObject.Main.Transmission;
using SharedObject.CustomerManage.Stuff;

namespace ServerSystem.CustomerManage.Action
{
    class ReadCustomersAction : CustomerManageAction<List<DefaultCustomer>>
    {
        private int set;

        public ReadCustomersAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.customerInSourceList:
                    set = 0;
                    break;
                case Instruction.customerOutCustomerList:
                    set = 1;
                    break;
                case Instruction.customerAllCustomersList:
                    set = 2;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override List<DefaultCustomer> mainMethod()
        {
            List<DefaultCustomer> customerList = null;
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.customerInSourceList:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rInSourceList);
                    break;
                case Instruction.customerOutCustomerList:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rOutCustomerList);
                    break;
                case Instruction.customerAllCustomersList:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rCustomerList);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            ReadCustomersService service = new ReadCustomersService(entities);
            switch (mainInstruction)
            {
                case Instruction.customerInSourceList:
                case Instruction.customerOutCustomerList:
                case Instruction.customerAllCustomersList:
                    customerList = service.getDefaultCustomers(identity, set);
                    break;
            }
            this.operatedState = service.operatedState;
            return customerList;
        }
    }
}
