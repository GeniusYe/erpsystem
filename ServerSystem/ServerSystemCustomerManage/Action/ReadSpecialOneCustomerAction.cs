﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.CustomerManage.Service;
using SharedObject.Main.Transmission;
using SharedObject.CustomerManage.Stuff;

namespace ServerSystem.CustomerManage.Action
{
    class ReadSpecialOneCustomerAction : CustomerManageAction<Customer>
    {
        public int id { get; set; }

        public ReadSpecialOneCustomerAction(SharedObject.Identity.Stuff.IdentityMark identity, Instruction instruction)
            : base(instruction, identity)
        {
            switch (instruction)
            {
                case Instruction.customerSpecialOneCustomer:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override Customer mainMethod()
        {
            Customer customer;
            RightState rightState;
            switch (mainInstruction)
            {
                case Instruction.customerSpecialOneCustomer:
                    rightState = CheckIdentityRightsSafely(identity, getRightsInstruction.rSpecialOneCustomer);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            if (rightState == RightState.Level0)
                operatedState.addStates(OperatedState.OperatedConfirmingEnum.noRights);
            else if (rightState == RightState.Level1)
            {
                int state, cookieOriRead, editor;
                getInfo(id, out state, out editor, out cookieOriRead);
                if (editor != identity.ID)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.notEnoughRights);
            }
            if (operatedState.state == OperatedState.OperatedStateEnum.notSuccess)
                return null;
            ReadSpecialOneCustomerService service = new ReadSpecialOneCustomerService(entities);
            switch (mainInstruction)
            {
                case Instruction.customerSpecialOneCustomer:
                    customer = service.getSpecialOneCustomer(identity, id);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = service.operatedState;
            if (operatedState.state == OperatedState.OperatedStateEnum.success)
                operatedState.state = OperatedState.OperatedStateEnum.normal;
            return customer;
        }

    }
}
