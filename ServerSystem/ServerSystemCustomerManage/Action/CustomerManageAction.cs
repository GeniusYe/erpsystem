﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.Action;

namespace ServerSystem.CustomerManage.Action
{
    abstract class CustomerManageAction<T> : ServerSystem.Action.Action<T>
    {
        protected new ERPSystemEntities entities = new ERPSystemEntities();

        public CustomerManageAction(SharedObject.Main.Transmission.Instruction mainInstruction, SharedObject.Identity.Stuff.IdentityMark identity)
            : base(SharedObject.Normal.SystemID.CustomerManage, mainInstruction, identity)
        {
            base.entities = new ERPSystemEntities();
            this.entities = (ERPSystemEntities)base.entities;
        }

        protected override abstract T mainMethod();

        protected bool getInfo(int id, out int state, out int editor, out int cookieOriRead)
        {
            state = -1;
            editor = 0;
            cookieOriRead = -1;
            SP_Customer_GetInfo_Result getInfoResult = entities.SP_Customer_GetInfo(id).First();
            if (getInfoResult != null)
            {
                cookieOriRead = getInfoResult.cookie;
                state = getInfoResult.state;
                editor = getInfoResult.creator == null ? 0 : getInfoResult.creator.Value;
                return true;
            }
            return false;
        }

        protected RightState CheckIdentityRightsSafely(SharedObject.Identity.Stuff.IdentityMark identity, CustomerManageAction<T>.getRightsInstruction instruction)
        {
            RightState rightState = RightState.Level0;
            if (identity == null)
                return rightState;
            byte? rightResult = entities.SP_Customer_GetRights(identity.ID, (int)instruction).First();
            if (rightResult == null)
                rightState = RightState.Level0;
            else if (rightResult > 0 && rightResult < 4)
                rightState = (RightState)rightResult.Value;
            return rightState;
        }

        public enum getRightsInstruction
        {
            rLogIn = 0,
            rInSourceList = 1,
            rOutCustomerList = 2,
            rCustomerList = 3,
            rSpecialOneCustomer = 4,
            rAdd = 5,
            rEdit = 6
        }
    }
}
