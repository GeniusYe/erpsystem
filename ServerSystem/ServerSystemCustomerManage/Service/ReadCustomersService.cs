﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.CustomerManage.Stuff;

namespace ServerSystem.CustomerManage.Service
{
    class ReadCustomersService : Service
    {
        public ReadCustomersService(ERPSystemEntities entities)
            : base(entities) { }

        /// <summary>
        /// get DefaultInWarrant from SQL server
        /// set 0 in 1 out 2 inout
        /// </summary>
        public List<DefaultCustomer> getDefaultCustomers(SharedObject.Identity.Stuff.IdentityMark identity, int set)
        {
            //DateTime nowDate = DatabaseAssistant.ReadAssistant.getSqlServerDate(CreateNewCommand());
            List<DefaultCustomer> customerList = new List<DefaultCustomer>();
            IEnumerable<DefaultCustomer> spCustomerList = from spDefaultCustomer in entities.SP_Customer_GetDefaultOnes(set) select new DefaultCustomer() { id = spDefaultCustomer.ID, name = spDefaultCustomer.name, state = DefaultCustomer.IntToState(spDefaultCustomer.state), ifInSource = spDefaultCustomer.inSource, ifCustomer = spDefaultCustomer.outCustomer };
            foreach (DefaultCustomer cus in spCustomerList)
                customerList.Add(cus);
            operatedState.state = OperatedState.OperatedStateEnum.success;
            return customerList;
        }
    }
}
