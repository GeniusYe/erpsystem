﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Normal;
using SharedObject.Main.Transmission;
using SharedObject.CustomerManage.Stuff;

namespace ServerSystem.CustomerManage.Service
{
    class CreateOneCustomerService : Service
    {
        public CreateOneCustomerService(ERPSystemEntities entities)
            : base(entities) { }

        public void addNewCustomer(SharedObject.Identity.Stuff.IdentityMark identity, Customer customer, out int id)
        {
            id = 0;
            if (customer != null)
            {
                id =  entities.SP_Customer_Add(customer.name, customer.ifInSource, customer.ifCustomer, identity.ID, generateNewCookie(0), null, "", "", "", null, "").First().Value;
                if (id > 0)
                    operatedState.state = OperatedState.OperatedStateEnum.success;
                else if (id == -1)
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.customerNameExists);
                else
                    operatedState.addStates(OperatedState.OperatedConfirmingEnum.rowCountNotRight);
            }
            else
                throw new ParameterNotMatchException();
        }
    }
}
