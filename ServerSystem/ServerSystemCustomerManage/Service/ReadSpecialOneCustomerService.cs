﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.CustomerManage.Stuff;

namespace ServerSystem.CustomerManage.Service
{
    class ReadSpecialOneCustomerService : Service
    {
        public ReadSpecialOneCustomerService(ERPSystemEntities entities)
            : base(entities) { }

        public Customer getSpecialOneCustomer(SharedObject.Identity.Stuff.IdentityMark identity, int id)
        {
            Customer customer = null;
            customer = (from spCustomer in entities.SP_Customer_Read(id) 
                       select new Customer() { id = spCustomer.ID, name = spCustomer.name, giveAwayAmount = spCustomer.giveAwayAmount, giveAwayAlready = spCustomer.giveAwayAlready, ifInSource = spCustomer.inSource, ifCustomer = spCustomer.outCustomer, state = DefaultCustomer.IntToState(spCustomer.state), creator = spCustomer.creator }).First();
            if (customer != null)
                operatedState.state = OperatedState.OperatedStateEnum.success;
            return customer;
        }
    }
}
