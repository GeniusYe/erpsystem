﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.CustomerManage.Action;
using SharedObject.Main.Transmission;
using SharedObject.CustomerManage.Stuff;
using SharedObject.CustomerManage.Transmission;

namespace ServerSystem.CustomerManage.Connector
{
    public class ReadSpecialOneCustomerConnector : ServerSystem.Connector.Connector
    {
        public ReadSpecialOneCustomerConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            : base(instrData, connectors, normalIdentity) 
        {
            switch (mainInstruction)
            {
                case Instruction.customerSpecialOneCustomer:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            CustomerManage.Action.ReadSpecialOneCustomerAction action = new ReadSpecialOneCustomerAction(identity, mainInstruction);

            switch (mainInstruction)
            {
                case Instruction.customerSpecialOneCustomer:
                    action.id = id;
                    action.execute();
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            this.operatedState = action.operatedState;
            TSCCustomer returnObj = new TSCCustomer();
            returnObj.customer = action.execute();
            return returnObj;
        }
    }
}
