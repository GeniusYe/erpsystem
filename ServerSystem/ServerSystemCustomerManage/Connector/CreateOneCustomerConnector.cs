﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.CustomerManage.Action;
using SharedObject.Main.Transmission;
using SharedObject.CustomerManage.Stuff;
using SharedObject.CustomerManage.Transmission;

namespace ServerSystem.CustomerManage.Connector
{
    public class CreateOneCustomerConnector : ServerSystem.Connector.Connector
    {
        public CreateOneCustomerConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.customerAdd:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            TCSCustomer tCSCustomer;
            if (obj is TCSCustomer)
                tCSCustomer = (TCSCustomer)obj;
            else
                throw new ParameterNotMatchException();

            CustomerManage.Action.CreateOneCustomerAction action;

            switch (mainInstruction)
            {
                case Instruction.customerAdd:
                    action = new CreateOneCustomerAction(identity, mainInstruction);
                    action.customer = tCSCustomer.customer;
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            TSCCustomer returnObj = new TSCCustomer();
            returnObj.customer = action.execute();
            this.operatedState = action.operatedState;
            return returnObj;
        }
    }
}
