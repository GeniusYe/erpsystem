﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerSystem.CustomerManage.Action;
using SharedObject.Main.Transmission;
using SharedObject.CustomerManage.Stuff;
using SharedObject.CustomerManage.Transmission;

namespace ServerSystem.CustomerManage.Connector
{
    public class ReadCustomersConnector : ServerSystem.Connector.Connector
    {
        public ReadCustomersConnector(byte[] instrData, List<ServerSystem.Connector.Connector> connectors, ServerSystem.Connector.Identity.LogInIdentity normalIdentity)
            :base(instrData, connectors, normalIdentity)
        {
            switch (mainInstruction)
            {
                case Instruction.customerInSourceList:
                case Instruction.customerOutCustomerList:
                case Instruction.customerAllCustomersList:
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
        }

        protected override AWithOperatedState mainMethod(SharedObject.Identity.Stuff.IdentityMark identity, object obj)
        {
            CustomerManage.Action.ReadCustomersAction action;

            switch (mainInstruction)
            {
                case Instruction.customerInSourceList:
                    action = new ReadCustomersAction(identity, Instruction.customerInSourceList);
                    break;
                case Instruction.customerOutCustomerList:
                    action = new ReadCustomersAction(identity, Instruction.customerOutCustomerList);
                    break;
                case Instruction.customerAllCustomersList:
                    action = new ReadCustomersAction(identity, Instruction.customerAllCustomersList);
                    break;
                default:
                    throw new InstructionNotMatchException();
            }
            TSCCustomersList returnObj = new TSCCustomersList();
            returnObj.customersList = action.execute();
            this.operatedState = action.operatedState;
            return returnObj;
        }
    }
}
