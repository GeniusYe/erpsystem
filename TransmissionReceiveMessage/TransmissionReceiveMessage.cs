﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.IO;

namespace TransmissionReceiveMessage
{
    /// <summary>
    /// Send With Such Protocal
    /// 4-byte length(not including these 4 bytes) and after that is the read data
    /// No matter seperation or joining would do harm to this function
    /// </summary>
    public class ReceiveMessage
    {
        private Stream stream;
        private byte[] data;
        int ReceiveBufferSize;

        private MemoryStream returnStreamBuffer;
        private int length, lengthReceived;
        private System.Threading.Timer disposePartMessageTimer;
        public int DisposePartMessageTimeOut { get; set; }

        public delegate void DataReceiveEventHandler(MemoryStream returnStream);

        public event DataReceiveEventHandler DataReceive;

        public ReceiveMessage(Stream stream, int ReceiveBufferSize)
        {
            this.stream = stream;
            this.ReceiveBufferSize = ReceiveBufferSize;
            this.data = new byte[ReceiveBufferSize];
            length = 0;
            lengthReceived = 0;
            DisposePartMessageTimeOut = 1000;
            disposePartMessageTimer = new System.Threading.Timer(timer_Elapsed, null, DisposePartMessageTimeOut, System.Threading.Timeout.Infinite);
        }

        public void beginRead()
        {
            lock (stream)
            {
                stream.BeginRead(data, 0, ReceiveBufferSize, receiveMessage, null);
            }
        }

        private void timer_Elapsed(object state)
        {
            this.length = 0;
            this.lengthReceived = 0;
        }

        /// <summary>
        /// receiveMessage is used to handle the message received from server
        /// </summary>
        /// <param name="ar"></param>
        public void receiveMessage(IAsyncResult ar)
        {
            lock (data)
            {
                try
                {
                    int bytesRead = 0;
                    lock (stream)
                    {
                        try
                        {
                            bytesRead = stream.EndRead(ar);
                        }
                        catch
                        {
                            bytesRead = 0;
                            returnMessage(null);
                            return;
                        }
                    }
                    if (bytesRead < 1)
                    {
                        return;
                    }
                    int lengthProcessed = 0;
                    MemoryStream returnStream;
                    if (bytesRead + lengthReceived < 20)
                    {
                        lengthReceived += bytesRead;
                        lock (stream)
                        {
                            stream.BeginRead(data,
                                             lengthReceived,
                                             ReceiveBufferSize - lengthReceived,
                                             receiveMessage,
                                             null);
                        }
                    }
                    else
                    {
                        if (lengthReceived < 20)
                            // means header hasn't been analysed yet
                        {
                            bytesRead += lengthReceived;
                            lengthReceived = 0;
                        }
                        while (lengthProcessed < bytesRead)
                        {
                            returnStream = new MemoryStream();
                            // without deducting 4 bytes for length because only 
                            // when "including header" senario met will deduct them
                            int availableLength = bytesRead - lengthProcessed;
                            if (lengthReceived == 0)
                                // means include header (20 bytes header)
                            {
                                disposePartMessageTimer.Change(DisposePartMessageTimeOut, System.Threading.Timeout.Infinite);
                                this.length = convertBytesToInt(data, lengthProcessed);
                                returnStreamBuffer = new MemoryStream();
                                availableLength -= 4;
                                if (availableLength < 0)
                                {
                                    for (int i = 0; i < bytesRead - lengthProcessed; i++)
                                    {
                                        data[i] = data[lengthProcessed + i];
                                        lengthReceived = bytesRead - lengthProcessed;
                                    }
                                    break;
                                }
                            }
                            // whether length is included in this buffer, need to preclude them
                            int receivingbuffer_offset =
                                    (lengthReceived == 0) ? 
                                        4 + lengthProcessed : 
                                        lengthProcessed;
                            if (length > lengthReceived + availableLength)
                            {
                                // not enough data to fulfill
                                returnStreamBuffer.Write(data, receivingbuffer_offset, availableLength);
                                lengthReceived += availableLength;
                                break;
                            }
                            else if (length == lengthReceived + availableLength)
                            {
                                returnStreamBuffer.Write(data, receivingbuffer_offset, availableLength);
                                returnStream = returnMessage(returnStream);
                                lengthReceived = 0;
                                break;
                            }
                            else // more data than needed
                            {
                                returnStreamBuffer.Write(data, receivingbuffer_offset, (int)length);
                                returnStream = returnMessage(returnStream);
                                lengthProcessed += 4 + length;
                                lengthReceived = 0;
                            }
                        }
                        lock (stream)
                        {
                            stream.BeginRead(data, 0, ReceiveBufferSize, receiveMessage, null);
                        }
                    }
                }
                finally
                {
                }
            }
        }

        private MemoryStream returnMessage(MemoryStream returnStream)
        {
            if (returnStreamBuffer != null)
            {
                returnStreamBuffer.Position = 0;
                returnStream = returnStreamBuffer;
                if (DataReceive != null)
                    DataReceive(returnStream);
            }
            return returnStream;
        }

        public static byte[] convertIntToBytes(int a)
        {
            byte[] bytes = new byte[4];
            convertIntToBytes(a, bytes, 0);
            return bytes;
        }
        public static byte[] convertIntToBytes(int a, byte[] bytes, int i)
        {
            if (bytes.Length - i < 4)
                return new byte[] { };
            bytes[i] = (byte)(a & 0xFF);
            bytes[i + 1] = (byte)((a >> 8) & 0xFF);
            bytes[i + 2] = (byte)((a >> 16) & 0xFF);
            bytes[i + 3] = (byte)((a >> 24) & 0xFF);
            byte[] bytes2 = new byte[4];
            int j;
            for (j = 0; j < 4; j++)
                bytes2[j] = bytes[j];
            return bytes2;
        }

        public static int convertBytesToInt(byte[] bytes, int i)
        {
            if (bytes.Length - i < 4)
                return -1;
            int a = bytes[i] + (bytes[i + 1] << 8) + (bytes[i + 2] << 16) + (bytes[i + 3] << 24);
            return a;
        }
        public static int getCookie(byte[] instrData)
        {
            return convertBytesToInt(instrData, 4);
        }
    }
}
