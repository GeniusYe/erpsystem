﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClientSystem;
using ClientSystem.Transmission;
using ClientSystem.Identity;
using ClientSystem.WareHouseInOut.Stuff;
using ClientSystem.WareHouseManage.Stuff;
using ClientSystem.CustomerManage.Stuff;
using SharedObject.Identity;
using InputBox;
using TransmissionReceiveMessage;

namespace ERPSystem
{
    public partial class MainForm : Form
    {
        private delegate void DLoggedIn(SharedObject.Identity.Stuff.IdentityMark identity);

        private bool LogInState = false;

        public MainForm()
        {
            InitializeComponent();
            this.FormClosed += new FormClosedEventHandler(MainForm_FormClosed);
            setAccessiable();
        }

        void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DraftPrintForm f1 = new DraftPrintForm();
            f1.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            WarrantFormIn f2 = new WarrantFormIn();
            f2.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            WarrantFormOut f3 = new WarrantFormOut();
            f3.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CustomerPrintForm f4 = new CustomerPrintForm();
            f4.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            string password = InputBox.InputBox.ShowInputBox("请输入调试密码", "请输入调试密码");
            while (true)
            {
                string length = InputBox.InputBox.ShowInputBox("请输入指令长度", "请输入指令长度，输入完成后按回车继续输入，您输入的连续的命令将以一个连续的二进制串发送到服务器");
                int lengthN;
                if (length != "")
                {
                    byte[] instrData = new byte[16];
                    for (int i = 0; i < 16; i++)
                        instrData[i] = (byte)(i + 1);
                    instrData[0] = (byte)1;
                    instrData[1] = (byte)SharedObject.Normal.SystemID.Test;
                    instrData[2] = (byte)SharedObject.Main.Transmission.Instruction.test;
                    instrData[3] = (byte)SharedObject.Main.Transmission.SecondaryInstruction.sendRequire;
                    lengthN = Int32.Parse(length);
                    stream.Write(ReceiveMessage.convertIntToBytes(lengthN + 16), 0, 4);
                    stream.Write(instrData, 0, 16);
                    stream.Write(new byte[lengthN], 0, lengthN);
                }
                else
                {
                    Transmission.mainTransmission.sendBytes(stream.ToArray());
                    break;
                }
            }
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            Transmission t = Transmission.mainTransmission;
            if (ClientSystem.Identity.IdentityCheck.longTermIdentity == null)
            {
                ClientSystem.Identity.IdentityCheckForm identityCheckForm;
                identityCheckForm = new ClientSystem.Identity.IdentityCheckForm(0, 0, SharedObject.Main.Transmission.Instruction.askForNormalLogIn, Transmission.mainTransmission);
                ClientSystem.Identity.Connector.IdentityConnector.MainFormShowEventHandler m = new ClientSystem.Identity.Connector.IdentityConnector.MainFormShowEventHandler(identity => this.Invoke(new DLoggedIn(loginSuccess), new object[] { identity }));
                identityCheckForm.EMainFormShow = m;
                identityCheckForm.Show();
            }
            else
            {
                MessageBox.Show("您已经登录");
            }
        }

        private void loginSuccess(SharedObject.Identity.Stuff.IdentityMark identity)
        {
            ClientSystem.Identity.IdentityCheck.longTermIdentity = identity;
            this.LogInState = true;

            setAccessiable();

            this.Text = "信息化管理平台————您已经成功登录，当前用户是：" + identity.name;

            MessageBox.Show("欢迎您：" + identity.name);
        }

        private void LogOutButton_Click(object sender, EventArgs e)
        {
            this.LogInState = false;
            setAccessiable();

            this.Text = "信息化管理平台";

            ClientSystem.Identity.IdentityCheck.longTermIdentity = null;
            Transmission.ResetMainTransmission();

            this.LogInState = false;
            setAccessiable();
            
            MessageBox.Show("退出成功");
        }

        private void setAccessiable()
        {
            if (this.LogInState)
            {
                this.LogInButton.Visible = false;
                this.LogOutButton.Visible = true;

                this.OpenDraftFormButton.Enabled = true;
                this.OpenInWarrantFormButton.Enabled = true;
                this.OpenOutWarrantFormButton.Enabled = true;
                this.OpenCustomerFormButton.Enabled = true;
                this.OpenRightsControlFormButton.Enabled = true;
            }
            else
            {
                this.LogOutButton.Visible = false;
                this.LogInButton.Visible = true;

                this.OpenDraftFormButton.Enabled = false;
                this.OpenInWarrantFormButton.Enabled = false;
                this.OpenOutWarrantFormButton.Enabled = false;
                this.OpenCustomerFormButton.Enabled = false;
                this.OpenRightsControlFormButton.Enabled = false;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            ClientSystem.RightsControl.Stuff.AddUserForm form = new ClientSystem.RightsControl.Stuff.AddUserForm(Transmission.mainTransmission);
            form.Show();
        }
    }
}
