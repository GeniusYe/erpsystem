﻿namespace ERPSystem
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OpenDraftFormButton = new System.Windows.Forms.Button();
            this.OpenInWarrantFormButton = new System.Windows.Forms.Button();
            this.OpenOutWarrantFormButton = new System.Windows.Forms.Button();
            this.OpenCustomerFormButton = new System.Windows.Forms.Button();
            this.TestButton = new System.Windows.Forms.Button();
            this.LogOutButton = new System.Windows.Forms.Button();
            this.LogInButton = new System.Windows.Forms.Button();
            this.OpenRightsControlFormButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // OpenDraftFormButton
            // 
            this.OpenDraftFormButton.Location = new System.Drawing.Point(12, 23);
            this.OpenDraftFormButton.Name = "OpenDraftFormButton";
            this.OpenDraftFormButton.Size = new System.Drawing.Size(105, 52);
            this.OpenDraftFormButton.TabIndex = 0;
            this.OpenDraftFormButton.Text = "配件";
            this.OpenDraftFormButton.UseVisualStyleBackColor = true;
            this.OpenDraftFormButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // OpenInWarrantFormButton
            // 
            this.OpenInWarrantFormButton.Location = new System.Drawing.Point(123, 23);
            this.OpenInWarrantFormButton.Name = "OpenInWarrantFormButton";
            this.OpenInWarrantFormButton.Size = new System.Drawing.Size(105, 52);
            this.OpenInWarrantFormButton.TabIndex = 1;
            this.OpenInWarrantFormButton.Text = "入库";
            this.OpenInWarrantFormButton.UseVisualStyleBackColor = true;
            this.OpenInWarrantFormButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // OpenOutWarrantFormButton
            // 
            this.OpenOutWarrantFormButton.Location = new System.Drawing.Point(234, 23);
            this.OpenOutWarrantFormButton.Name = "OpenOutWarrantFormButton";
            this.OpenOutWarrantFormButton.Size = new System.Drawing.Size(105, 52);
            this.OpenOutWarrantFormButton.TabIndex = 2;
            this.OpenOutWarrantFormButton.Text = "出库";
            this.OpenOutWarrantFormButton.UseVisualStyleBackColor = true;
            this.OpenOutWarrantFormButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // OpenCustomerFormButton
            // 
            this.OpenCustomerFormButton.Location = new System.Drawing.Point(345, 23);
            this.OpenCustomerFormButton.Name = "OpenCustomerFormButton";
            this.OpenCustomerFormButton.Size = new System.Drawing.Size(105, 52);
            this.OpenCustomerFormButton.TabIndex = 3;
            this.OpenCustomerFormButton.Text = "客户";
            this.OpenCustomerFormButton.UseVisualStyleBackColor = true;
            this.OpenCustomerFormButton.Click += new System.EventHandler(this.button4_Click);
            // 
            // TestButton
            // 
            this.TestButton.Enabled = false;
            this.TestButton.Location = new System.Drawing.Point(287, 100);
            this.TestButton.Name = "TestButton";
            this.TestButton.Size = new System.Drawing.Size(95, 52);
            this.TestButton.TabIndex = 4;
            this.TestButton.Text = "测试";
            this.TestButton.UseVisualStyleBackColor = true;
            this.TestButton.Click += new System.EventHandler(this.button5_Click);
            // 
            // LogOutButton
            // 
            this.LogOutButton.Location = new System.Drawing.Point(181, 100);
            this.LogOutButton.Name = "LogOutButton";
            this.LogOutButton.Size = new System.Drawing.Size(100, 52);
            this.LogOutButton.TabIndex = 8;
            this.LogOutButton.Text = "注销";
            this.LogOutButton.UseVisualStyleBackColor = true;
            this.LogOutButton.Click += new System.EventHandler(this.LogOutButton_Click);
            // 
            // LogInButton
            // 
            this.LogInButton.Location = new System.Drawing.Point(181, 100);
            this.LogInButton.Name = "LogInButton";
            this.LogInButton.Size = new System.Drawing.Size(100, 52);
            this.LogInButton.TabIndex = 7;
            this.LogInButton.Text = "登录";
            this.LogInButton.UseVisualStyleBackColor = true;
            this.LogInButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // OpenRightsControlFormButton
            // 
            this.OpenRightsControlFormButton.Location = new System.Drawing.Point(70, 100);
            this.OpenRightsControlFormButton.Name = "OpenRightsControlFormButton";
            this.OpenRightsControlFormButton.Size = new System.Drawing.Size(105, 52);
            this.OpenRightsControlFormButton.TabIndex = 9;
            this.OpenRightsControlFormButton.Text = "用户权限管理";
            this.OpenRightsControlFormButton.UseVisualStyleBackColor = true;
            this.OpenRightsControlFormButton.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 178);
            this.Controls.Add(this.OpenRightsControlFormButton);
            this.Controls.Add(this.LogOutButton);
            this.Controls.Add(this.LogInButton);
            this.Controls.Add(this.TestButton);
            this.Controls.Add(this.OpenCustomerFormButton);
            this.Controls.Add(this.OpenOutWarrantFormButton);
            this.Controls.Add(this.OpenInWarrantFormButton);
            this.Controls.Add(this.OpenDraftFormButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "信息化管理平台";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button OpenDraftFormButton;
        private System.Windows.Forms.Button OpenInWarrantFormButton;
        private System.Windows.Forms.Button OpenOutWarrantFormButton;
        private System.Windows.Forms.Button OpenCustomerFormButton;
        private System.Windows.Forms.Button TestButton;
        private System.Windows.Forms.Button LogOutButton;
        private System.Windows.Forms.Button LogInButton;
        private System.Windows.Forms.Button OpenRightsControlFormButton;
    }
}