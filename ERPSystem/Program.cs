﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using SharedObject.Main.Transmission;

namespace ERPSystem
{
    static class Program
    {

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            try
            {
                using (StreamReader parametersReader = new StreamReader("parameters.inf"))
                {
                    string str;
                    string[] pair;
                    while (!parametersReader.EndOfStream)
                    {
                        try
                        {
                            str = parametersReader.ReadLine();
                            pair = str.Split('=');
                            switch (pair[0])
                            {
                                case "hostname":
                                    ClientSystem.Function.remoteAddress = pair[1];
                                    break;
                                case "port":
                                    ClientSystem.Function.remotePort = Int32.Parse(pair[1]);
                                    break;
                                case "disposePartMessageTimeOut":
                                    ClientSystem.Function.disposePartMessageTimeOut = Int32.Parse(pair[1]);
                                    break;
                                case "tcpBufferSize":
                                    ClientSystem.Function.tcpBufferSize = Int32.Parse(pair[1]);
                                    break;
                                case "certificateName":
                                    ClientSystem.Function.certificateName = pair[1];
                                    break;
                                case "certificateFile":
                                    ClientSystem.Function.certificateFile = pair[1];
                                    break;
                                case "company_name":
                                    ClientSystem.Function.company_name = pair[1];
                                    break;
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            catch { }

            Application.Run(new MainForm());
        }
    }
}
