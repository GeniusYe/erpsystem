﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Identity;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseManage.Stuff;

namespace SharedObject.WareHouseManage.Transmission
{

    /// <summary>
    /// rangeType 0->viewAll 1->view all draft under container1(range) 2->view all draft under container2(range)
    /// </summary>
    [Serializable]
    public class TCSViewAll
    {
        public int rangeType { get; set; }
        public int range { get; set; }
        public DateTime date { get; set; }
    }

    [Serializable]
    public class TCSSearchString
    {
        public string traString { get; set; }
    }

    [Serializable]
    public class TCSDraftControl
    {
        public DraftCSControl draft { get; set; }
    }

    [Serializable]
    public class TCSDraftCreate
    {
        public Draft draft { get; set; }
    }

    [Serializable]
    public class TSCDraft : AWithOperatedState
    {
        public Draft draft { get; set; }
    }

    [Serializable]
    public class TSCViewAll : AWithOperatedState
    {
        public List<ViewAllDraft> viewAllDraft { get; set; }
    }

    [Serializable]
    public class TSCDefaultDraftList : AWithOperatedState
    {
        public List<DefaultDraft> draftList { get; set; }
    }

    [Serializable]
    public class TSCDraftContainerList : AWithOperatedState
    {
        public List<DraftContainer> draftContainers { get; set; }
    }
}
