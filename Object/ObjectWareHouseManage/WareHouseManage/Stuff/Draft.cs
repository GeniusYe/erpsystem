﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.WareHouseManage.Stuff
{
    [Serializable]
    public class DraftContainer
    {
        public int id { get; set; }
        public string name { get; set; }

        public override string ToString()
        {
            return id + " " + name;
        }
    }
    /**
     * This class contains a full draft
     */
    [Serializable]
    public class DraftCSControl
    {
        public int id { get; set; }
        public int cookie { get; set; }

        public DraftCSControl(DefaultDraft draft)
        {
            this.id = draft.id;
            this.cookie = draft.cookie;
        }
    }

    [Serializable]
    public class DefaultDraft
    {
        public int id { get; set; }
        public int belongsTo { get; set; }
        public string belongsToString { get; set; }
        public int state { get; set; }
        public string name { get; set; }
        public string specification { get; set; }
        public string hiddenSpecification { get; set; }
        public int priceOutH { get; set; }
        public int priceOutL { get; set; }
        public int cookie { get; set; }
        public bool is_servicefee { get; set; }
        public string unit { get; set; }
        public int numAll { get; set; }
        public int numAccessiable { get; set; }

        public override string ToString()
        {
            string str = "";
            if (state == 0)
                str = "  ";
            else
                str = "√";
            return str + " " + name + "->" + specification + "->" + hiddenSpecification;
        }
    }

    [Serializable]
    public class Draft : DefaultDraft
    {
        public string location { get; set; }
        public string usingTarget { get; set; }
        public int numWarning { get; set; }
        public int value { get; set; }
        public List<DraftOperationRecord> operationRecord { get; set; }
        public int priceIn
        {
            get
            {
                return numAll == 0 ? 0 : value / numAll;
            }
        }

        public Draft()
        {
            this.operationRecord = new List<DraftOperationRecord>();
        }
    }

    [Serializable]
    public class DraftOperationRecord
    {
        public int ID { get; set; }
        public int draftId { get; set; }
        public int operationOperator { get; set; }
        public string operationOperatorName { get; set; }
        public DraftOperationEnum operation { get; set; }
        public DateTime operatedTime { get; set; }
    }

    public enum DraftOperationEnum
    {
        create = 0,
        edit = 1,
        cancel = 2,
        submit = 8
    }

    [Serializable]
    public class ViewAllDraft
    {
        public int id { get; set; }
        public int state { get; set; }
        public string name { get; set; }
        public string specification { get; set; }
        public string hiddenSpecification { get; set; }
        public int numAll { get; set; }
        public int value { get; set; }
        public string unit { get; set; }
        public int priceIn
        {
            get
            {
                return numAll == 0 ? 0 : value / numAll;
            }
        }
        public int priceOutH { get; set; }
        public int priceOutL { get; set; }
    }



}
