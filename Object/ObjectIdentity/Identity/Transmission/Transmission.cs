﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Identity.Stuff;

namespace SharedObject.Identity.Transmission
{
    [Serializable]
    public class TCSIdentity
    {
        public AuthorizeType authorizeType { get; set; }
        public string cardInformation { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public int cookieToBeRedo { get; set; }
        public SharedObject.Normal.SystemID systemNumber { get; set; }
        public byte instructionNumber { get; set; }

        public enum AuthorizeType
        {
            card = 0,
            password = 1
        }
    }

    [Serializable]
    public class TSCIdentity : SharedObject.Main.Transmission.AWithOperatedState
    {
        public IdentityMark identity { get; set; }
        public string version { get; set; }
    }
}
