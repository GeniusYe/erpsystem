﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.Identity.Stuff
{
    [Serializable]
    public class IdentityMark
    {
        public string name { get;  set; }
        public int ID { get; set; }
        public int cookie { get; set; }

        public IdentityMark(string name, int ID, int cookie)
        {
            this.name = name;
            this.ID = ID;
            this.cookie = cookie;
        }
        public IdentityMark()
        { }
    }
}
