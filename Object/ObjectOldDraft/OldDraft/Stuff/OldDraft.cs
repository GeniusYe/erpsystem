﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.OldDraft.Stuff
{
    [Serializable]
    public class OldDraft
    {
        public int draftId { get; set; }
        public int warrantId { get; set; }
        public OldDraftStateEnum oldDraftState { get; set; }

        public enum OldDraftStateEnum
        {
            notSuitable,
            inHouse = 0,
            readyForReturning = 1,
            alreadyReturned = 2,
            alreadyGotFee = 3
        }
    }
}
