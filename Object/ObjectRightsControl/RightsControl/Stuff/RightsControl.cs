﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.RightsControl.Stuff
{
    [Serializable]
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public bool passwordLogin { get; set; }
        public bool passwordAuthorize { get; set; }
        public string cardInformation { get; set; }
        public bool cardLogin { get; set; }
        public bool cardAuthorize { get; set; }
    }
}
