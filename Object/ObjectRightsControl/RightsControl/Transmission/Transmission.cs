﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.RightsControl.Stuff;

namespace SharedObject.RightsControl.Transmission
{
    [Serializable]
    public class TCSUser
    {
        public User user { get; set; }
    }
}
