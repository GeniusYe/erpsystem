﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.CustomerManage.Stuff
{
    [Serializable]
    public class CustomerCSControl
    {
        public int id { get; set; }
        public string name { get; set; }
        public DefaultCustomer.CustomerState state { get; set; }
    }

    [Serializable]
    public class DefaultCustomer
    {
        public int id { get; set; }
        public string name { get; set; }
        public CustomerState state { get; set; }
        public bool? ifInSource { get; set; }
        public bool? ifCustomer { get; set; }

        public override string ToString()
        {
            return this.name;
        }

        public CustomerCSControl generateCustomerCSControl()
        {
            CustomerCSControl CustomerCSControl = new CustomerCSControl();
            CustomerCSControl.id = id;
            CustomerCSControl.name = name;
            CustomerCSControl.state = state;
            return CustomerCSControl;
        }

        public static CustomerState IntToState(int state)
        {
            return (CustomerState)(state);
        }

        public static int StateToInt(CustomerState state)
        {
            return (int)state;
        }

        public enum CustomerState
        {
            Abnormal = -1,
            Normal = 0
        }
    }

    [Serializable]
    public class Customer : DefaultCustomer
    {
        public string information { get; set; }

        public int? creator { get; set; }

        public int? giveAwayAmount { get; set; }
        public int? giveAwayAlready { get; set; }
    }
}
