﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Identity;
using SharedObject.Main.Transmission;
using SharedObject.CustomerManage.Stuff;

namespace SharedObject.CustomerManage.Transmission
{
    
    [Serializable]
    public class TSCCustomersList : AWithOperatedState
    {
        public List<DefaultCustomer> customersList { get; set; }
    }

    [Serializable]
    public class TSCCustomer : AWithOperatedState
    {
        public Customer customer { get; set; }
    }

    [Serializable]
    public class TCSCustomer
    {
        public Customer customer { get; set; }
    }
    
}
