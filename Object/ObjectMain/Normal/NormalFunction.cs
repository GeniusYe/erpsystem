﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.Normal
{
    public static class NormalFunction
    {
        public static string TrimLeft(this string a, int i)
        {
            if (a == null)
                return a;
            string b = a.Trim();
            if (b.Length > i)
                return b.Substring(0, i);
            else
                return b;
        }
    }
}
