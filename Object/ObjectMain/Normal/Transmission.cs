﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.Normal
{
    [Serializable]
    public enum SystemID
    {
        Identity = 0,
        Test = 1,
        WareHouseManage = 4,
        WareHouseInOut = 5,
        CustomerManage = 6,
        RightsControl = 7
    }

    [Serializable]
    public class TCSDateRange
    {
        private DateTime _startDate, _stopDate;
        public DateTime startDate
        {
            get
            {
                if (_startDate == null)
                    return DateTime.Now;
                else
                    return _startDate;
            }
            set
            {
                this._startDate = value;
            }
        }
        public DateTime stopDate
        {
            get
            {
                if (_stopDate == null)
                    return DateTime.Now;
                else
                    return _stopDate;
            }
            set
            {
                this._stopDate = value;
            }
        }
    }
}
