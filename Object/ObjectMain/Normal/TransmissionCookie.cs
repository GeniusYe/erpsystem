﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SharedObject.Normal
{
    public class TransmissionCookie
    {
        static List<Int32> usedSeed = new List<int>();

        private int cookie;
        private Random ro;
        private int seed;

        public TransmissionCookie()
        {
            int i;
            do
            {
                i = System.DateTime.Now.GetHashCode();
            }
            while(usedSeed.Contains(i));
            usedSeed.Add(i);
            seed = i;
            ro = new Random(i);
        }

        ~TransmissionCookie()
        {
            usedSeed.Remove(seed);
        }

        public int generateNewCookie()
        {
            int i = ro.Next();
            cookie = i;
            return i;
        }

        public void setCookie(int cookie)
        {
            this.cookie = cookie;
        }

        public int getCookie()
        {
            return cookie;
        }

        public bool contains(int cookie)
        {
            if (this.cookie != 0 && this.cookie == cookie)
                return true;
            else
                return false;
        }
    }
}
