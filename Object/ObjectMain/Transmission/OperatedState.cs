﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.Main.Transmission
{
    [Serializable]
    public class OperatedState
    {
        public OperatedStateEnum state { get; set; }
        private List<OperatedConfirmingItem> states;

        [Serializable]
        public class OperatedConfirmingItem
        {
            private OperatedConfirmingEnum confirming;
            private object[] parameters = new object[10];

            public OperatedConfirmingItem(OperatedConfirmingEnum confirming)
            {
                this.confirming = confirming;
            }
            public OperatedConfirmingItem(OperatedConfirmingEnum confirming, string confirmingStr)
            {
                this.confirming = confirming;
                this.parameters[0] = confirmingStr;
            }

            public OperatedConfirmingEnum getConfirmingType()
            {
                return this.confirming;
            }
            public object getConfimingData(int i)
            {
                return this.parameters[i];
            }

            public bool ifFatal()
            {
                switch (confirming)
                {
                    case OperatedConfirmingEnum.exception:
                    case OperatedConfirmingEnum.noRights:
                    case OperatedConfirmingEnum.cookieWrong:
                    case OperatedConfirmingEnum.stateWrong:
                    case OperatedConfirmingEnum.warrantNotFound:
                    case OperatedConfirmingEnum.warrantReturnMoreThanSell:
                    case OperatedConfirmingEnum.warrantNotEnoughDraft:
                    case OperatedConfirmingEnum.warrantTakeInAlreadyDeletedDraft:
                    case OperatedConfirmingEnum.warrantMultipleDraftInOneWarrant:
                    case OperatedConfirmingEnum.warrantReadException:
                    case OperatedConfirmingEnum.warrantPermitNotLegal:
                    case OperatedConfirmingEnum.warrantPayAmountNotLegal:
                    case OperatedConfirmingEnum.warrantWarrantTooOldToHandleDiscount:
                    case OperatedConfirmingEnum.warrantNotSoMuchPayCheckToReturn:
                    case OperatedConfirmingEnum.draftNotFound:
                    case OperatedConfirmingEnum.draftAlreadySubmitted:
                    case OperatedConfirmingEnum.draftNumNotZero:
                    case OperatedConfirmingEnum.draftCannotLoadPreviousInformation:
                    case OperatedConfirmingEnum.draftSpecialPriceTargetNotExists:
                    case OperatedConfirmingEnum.customerNotFound:
                    case OperatedConfirmingEnum.customerNameExists:
                    case OperatedConfirmingEnum.canOnlyAuthorize:
                    case OperatedConfirmingEnum.canOnlyLogin:
                    case OperatedConfirmingEnum.canNotLoginOrAuthorize:
                    case OperatedConfirmingEnum.rowCountNotRight:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public int Count
        {
            get
            {
                return states.Count;
            }
        }

        public OperatedState()
        {
            state = OperatedStateEnum.notSet;
            states = new List<OperatedConfirmingItem>();
        }

        public void addStates(OperatedConfirmingItem item)
        {
            this.state = OperatedStateEnum.notSuccess;
            states.Add(item);
        }

        public void addStates(OperatedConfirmingEnum state, string confirmingStr)
        {
            this.state = OperatedStateEnum.notSuccess;
            states.Add(new OperatedConfirmingItem(state, confirmingStr));
        }

        public void addStates(OperatedConfirmingEnum state)
        {
            this.state = OperatedStateEnum.notSuccess;
            states.Add(new OperatedConfirmingItem(state));
        }

        public void combine(OperatedState operatedState)
        {
            OperatedStateEnum state = this.state;
            foreach (OperatedConfirmingItem item in operatedState)
            {
                this.addStates(item);
            }
            if (this.state == OperatedStateEnum.notSuccess || operatedState.state == OperatedStateEnum.notSuccess)
                this.state = OperatedStateEnum.notSuccess;
            else if (this.state == OperatedStateEnum.success || operatedState.state == OperatedStateEnum.success)
                this.state = OperatedStateEnum.success;
            else
                this.state = OperatedStateEnum.normal;
        }

        public IEnumerator<OperatedConfirmingItem> GetEnumerator()
        { 
            return states.GetEnumerator();
        }

        public enum OperatedStateEnum
        {
            notSet = 0,
            normal = 8,
            success = 16,
            notSuccess = 24
        }

        public enum OperatedConfirmingEnum
        {
            exception,
            authorizeNeed,
            noRights,
            notEnoughRights,
            cookieWrong,
            stateWrong,
            warrantNotFound,
            warrantReturnMoreThanSell,
            warrantNotEnoughDraft,
            warrantLowerPriceAdjustThanPrice,           //error while sell
            warrantLowerPriceAdjustThanPriceIn,         //error while sell
            warrantTooMuchPriceChange,                  //error while in
            warrantNotSubmittedDraft,                   //error while sell
            warrantPriceAdjusted,                       //error while sell
            warrantNotPaid,                             //error while sell 
            warrantSubmitToLate,                        //error while sell
            warrantNotSupportDiscount,
            warrantTakeInAlreadyDeletedDraft,
            warrantMultipleDraftInOneWarrant,
            warrantReadException,
            warrantPermitNotLegal,
            warrantPayAmountNotLegal,
            warrantWarrantTooOldToHandleDiscount,
            warrantNotSoMuchPayCheckToReturn,
            warrantTypeNotMatched,
            draftNotFound,                              //error while draft
            draftAlreadySubmitted,                      //error while draft
            draftEditSubmitDraft,                       //error while draft
            draftEditSubmitDraftConsistentInfo,
            draftNumNotZero,
            draftCannotLoadPreviousInformation,
            draftSpecialPriceTargetNotExists,
            customerNotFound,
            customerNameExists,
            loginInformationWrong,
            canOnlyAuthorize,
            canOnlyLogin,
            canNotLoginOrAuthorize,
            rowCountNotRight,
            warrantTooMuchDiscount,
            draftEditSubmitDraftPriceChange,
        }
    }

    [Serializable]
    public class AWithOperatedState
    {
        public OperatedState operatedState { get; set; }
    }
}
