﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.Main.Transmission
{
    /// <summary>
    /// do not edit these numbers because it is equal to it in TSql
    /// </summary>
    public enum Instruction
    {
        NotSet,
        inWarrantList = 10,
        inWarrantSpecialOne = 11,
        inWarrantAdd = 12,
        inWarrantEdit = 13,
        inWarrantCancel = 14,
        inWarrantSubmit = 17,
        inWarrantPrint = 18,
        inWarrantMoney = 19,
        inWarrantComplete = 20,
        outWarrantList = 30,
        outWarrantSpecialOne = 31,
        outWarrantAdd = 32,
        outWarrantEdit = 33,
        outWarrantCancel = 34,
        outWarrantSubmitCustomerPage = 35,
        outWarrantPrintCustomerPage = 36,
        outWarrantSubmit = 37,
        outWarrantPrint = 38,
        outWarrantMoney = 39,
        outWarrantComplete = 40,
        outWarrantForcePass = 41,
        outWarrantTakeOut = 42,
        outWarrantReturn = 43,
        outWarrantSpecialPrice = 44,
        warrantViewDetail = 46,
        warrantViewOneCycle = 47,
        outWarrantDiscount = 50,
        outWarrantPayCheck = 51,
        outWarrantReturnPayCheck = 52,
        warrantAttendantsList = 53,
        customerInSourceList = 60,
        customerOutCustomerList = 61,
        customerAllCustomersList = 62,
        customerSpecialOneCustomer = 63,
        customerAdd = 64,
        customerEdit = 65,
        draftDisplayDraftList = 80,
        draftAdd = 81,
        draftEdit = 82,
        draftSpecialOne = 83,
        draftSubmit = 84,
        draftDel = 85,
        draftContainer1List = 86,
        draftContainer2List = 87,
        draftContainer3List = 88,
        draftDisplayDraftListSearch = 89,
        draftViewAll = 90,
        rightsAddUser = 100,
        askForNormalLogIn = 1,
        askForSpecialLogIn = 2,
        rights,
        test
    }

    public enum SecondaryInstruction
    {
        sendRequire,
        sendResponse,
        sendAuthorizeNeedResponse,
        sendAuthorizeResponse
    }
}
