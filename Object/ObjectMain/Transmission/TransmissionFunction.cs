﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransmissionReceiveMessage;

namespace SharedObject.Main.Transmission
{
    public class TransmissionFunction
    {

        public static byte[] createInstrData(SharedObject.Normal.SystemID systemID, Instruction instruction, SecondaryInstruction secondaryInstruction, int cookie)
        {
            return createInstrData(systemID, instruction, secondaryInstruction, cookie, new byte[] { });
        }
        public static byte[] createInstrData(SharedObject.Normal.SystemID systemID, Instruction instruction, SecondaryInstruction secondaryInstruction, int cookie, byte[] otherPara)
        {
            byte[] instrData = new byte[16];
            instrData[0] = 1;
            instrData[1] = (byte)systemID;
            instrData[2] = (byte)instruction;
            instrData[3] = (byte)secondaryInstruction;
            ReceiveMessage.convertIntToBytes(cookie, instrData, 4);
            int i = 8, j = 0;
            if (otherPara != null)
                while (j < otherPara.Length && i < 16)
                {
                    instrData[i] = otherPara[j];
                    i++; j++;
                }
            for (; i < 16; i++)
                instrData[i] = 0;
            return instrData;
        }
    }
}
