﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedObject.Main.Transmission;
using SharedObject.WareHouseInOut.Stuff;
using SharedObject.Identity.Stuff;

namespace SharedObject.WareHouseInOut.Transmission
{

    [Serializable]
    public class TSCDefaultWarrantList : AWithOperatedState
    {
        public List<DefaultWarrant> defaultWarrantList { get; set; }
    }

    [Serializable]
    public class TSCInWarrant : AWithOperatedState
    {
        public InWarrant inWarrant { get; set; }
    }

    [Serializable]
    public class TSCOutWarrant : AWithOperatedState
    {
        public OutWarrant outWarrant { get; set; }
    }

    [Serializable]
    public class TSCWarrantControl : AWithOperatedState
    {
        public WarrantCSControl warrant { get; set; }
    }

    [Serializable]
    public class TSCSpecialPrice : AWithOperatedState
    {
        public SpecialPriceSC specialPrice { get; set; }
    }

    [Serializable]
    public class TSCViewDetail : AWithOperatedState
    {
        public DetailAccount detailAccount { get; set; }
    }

    [Serializable]
    public class TSCViewOneCycle : AWithOperatedState
    {
        public ViewOneCycle viewOneCycle { get; set; }
    }

    [Serializable]
    public class TSCDiscountChange : AWithOperatedState
    {
        public DiscountItem discountItem { get; set; }
    }

    [Serializable]
    public class TSCPayCheckChange : AWithOperatedState
    {
        public PayCheckItem payCheckItem { get; set; }
    }

    [Serializable]
    public class TSCAttendantsList : AWithOperatedState
    {
        public List<DefaultAttendant> attendantsList { get; set; }
    }

    [Serializable]
    public class TCSDefaultWarrantsRequirement
    {
        public SharedObject.Normal.TCSDateRange dateRange { get; set; }
        public int target { get; set; }
    }

    [Serializable]
    public class TCSWarrantControl
    {
        public WarrantCSControl warrant { get; set; }
    }

    [Serializable]
    public class TCSInWarrantCreate
    {
        public InWarrantCSCreate inWarrant { get; set; }
    }

    [Serializable]
    public class TCSOutWarrantCreate
    {
        public OutWarrantCSCreate outWarrant { get; set; }
    }

    [Serializable]
    public class TCSOutWarrantReturnNumber : TCSWarrantControl
    {
        public List<DefaultAttendant> attendants { get; set; }
        public List<TransReturnNumberPair> transCSOutWarrantReturnNumberPairs { get; set; }
    }

    [Serializable]
    public class TCSGetSpecialPrice
    {
        public GetSpecialPriceCS getSpecialPriceInfo { get; set; }
    }

    [Serializable]
    public class TCSViewDetail
    {
        public int ID { get; set; }
        public SharedObject.Normal.TCSDateRange dateRange { get; set; }
    }

    [Serializable]
    public class TCSViewOneCycle
    {
        public DateTime date { get; set; }
    }

    [Serializable]
    public class TCSDiscountChange
    {
        public DiscountItem discountItem { get; set; }
        public WarrantCSControl warrant { get; set; }
    }

    [Serializable]
    public class TCSPayCheck
    {
        public PayCheckItem payCheckItem { get; set; }
        public WarrantCSControl warrant { get; set; }
    }
}
