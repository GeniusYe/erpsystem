﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.WareHouseInOut.Stuff
{
    public enum WarrantType
    {
        In,
        Out
    }

    [Serializable]
    public class DefaultWarrant
    {
        public int ID { get; set; }
        public string number { get; set; }
        public WarrantState state { get; set; }
        public OutWarrant.OutWarrantPayState payState { get; set; }
        public SpecificWarrantType type { get; set; }
        public int cookie { get; set; }
        public int? target { get; set; }
        public int? amountIn { get; set; }
        public int? amountTotal { get; set; }
        public int? amountCost { get; set; }
        public int? amountPermit { get; set; }
        public int? amountPaid { get; set; }
        public int? amountServiceFee { get; set; }
        public DateTime? submitCustomerPageDate { get; set; }
        public DateTime? submitDate { get; set; }

        public DefaultWarrant()
        {
            payState = OutWarrant.OutWarrantPayState.NotSuitable;
        }
        
        public virtual string stateString
        {
            get
            {
                switch (state)
                {
                    case WarrantState.Normal:
                        return "普通";
                    case WarrantState.Cancelled:
                        return "已作废";
                    case WarrantState.SubmittedCustomerPage:
                        return "销货提交";
                    case WarrantState.PrintedCustomerPage:
                        return "销货打印";
                    case WarrantState.TakenOut:
                        return "已出库";
                    case WarrantState.Returned:
                        return "已退件";
                    case WarrantState.Submitted:
                        return "已提交";
                    case WarrantState.Printed:
                        return "已打印";
                    case WarrantState.ForceCompleted:
                        return "强制终结";
                    case WarrantState.Completed:
                    case WarrantState.CompletedAndPaid:
                        return "终结";
                    default:
                        return "不正常";
                }
            }
        }

        public string payStateString
        {
            get
            {
                if (payState == OutWarrant.OutWarrantPayState.NotSuitable)
                    return "";
                switch (payState)
                {
                    case OutWarrant.OutWarrantPayState.NotPaid:
                        return "未付款";
                    case OutWarrant.OutWarrantPayState.Paid:
                        return "已付款";
                    case OutWarrant.OutWarrantPayState.PartPaid:
                        return "部分付款";
                    case OutWarrant.OutWarrantPayState.OverPaid:
                        return "超额付款";
                    case OutWarrant.OutWarrantPayState.ForcePass:
                        return "授权出库";
                    default:
                        return "不正常";
                }
            }
        }

        public static WarrantState IntToState(int? state)
        {
            if (state == null)
                return WarrantState.Abnormal;
            return (WarrantState)(state);
        }

        public static int StateToInt(WarrantState state)
        {
            return (int)state;
        }

        public static SpecificWarrantType IntToSpecificWarrantType(int? specificWarrantType)
        {
            if (specificWarrantType == null)
                return SpecificWarrantType.NotSuitable;
            return (SpecificWarrantType)(specificWarrantType);
        }

        public static int SpecificWarrantTypeToInt(SpecificWarrantType specificWarrantType)
        {
            return (int)specificWarrantType;
        }

        public enum SpecificWarrantType
        {
            withReceipt = 0,
            withoutReceipt = 1,
            Service = 2,
            In = 3,
            Gift = 4,
            AdjustIn = 5,
            AdjustOut = 6,
            NotSuitable = 100
        }

        public string specificWarrantTypeString
        {
            get
            {
                switch (type)
                {
                    case SpecificWarrantType.withReceipt:
                        return "有";
                    case SpecificWarrantType.withoutReceipt:
                        return "无";
                    case SpecificWarrantType.Service:
                        return "三包";
                    case SpecificWarrantType.AdjustOut:
                        return "调整";
                    case SpecificWarrantType.Gift:
                        return " A ";
                    case SpecificWarrantType.In:
                        return "普通";
                    case SpecificWarrantType.AdjustIn:
                        return "调整";
                    default:
                        return "有误";
                }
            }
        }

        public enum WarrantState
        {
            Normal = 0,                                 //in sell
            Cancelled = 2,                              //in sell       
            SubmittedCustomerPage = 4,                  //sell only
            PrintedCustomerPage = 5,                    //sell only
            TakenOut = 7,                               //sell only
            Returned = 8,                               //sell only
            Submitted = 9,                              //in sell
            Printed = 10,                               //in sell
            ForceCompleted = 13,                        //in sell
            Completed = 14,                             //in sell
            CompletedAndPaid = 20,                      //in sell
            Abnormal = 99
        }
    }

    [Serializable]
    public abstract class AbstractWarrant : DefaultWarrant
    {
        public string comment { get; set; }
        public List<WarrantOperationRecord> operationRecord { get; set; }

        public WarrantOperationRecord submitRecord
        {
            get
            {
                foreach (WarrantOperationRecord record in this.operationRecord)
                    if (record.operation == WarrantOperationEnum.submit)
                        return record;
                return null;
            }
        }

        public WarrantOperationRecord submitCustomerPageRecord
        {
            get
            {
                foreach (WarrantOperationRecord record in operationRecord)
                    if (record.operation == WarrantOperationEnum.submitCustomerPage)
                        return record;
                return null;
            }
        }

        public WarrantOperationRecord printRecord
        {
            get
            {
                for (int i = operationRecord.Count; i >= 0; i--)
                    if (operationRecord[i].operation == WarrantOperationEnum.print)
                        return operationRecord[i];
                return null;
            }
        }

        public WarrantOperationRecord printCustomerPageRecord
        {
            get
            {
                for (int i = operationRecord.Count - 1; i >= 0; i--)
                    if (operationRecord[i].operation == WarrantOperationEnum.printCustomerPage)
                        return operationRecord[i];
                return null;
            }
        }

        public int printTimes
        {
            get
            {
                int times = 0;
                for (int i = operationRecord.Count - 1; i >= 0; i--)
                    if (operationRecord[i].operation == WarrantOperationEnum.print)
                        times++;
                return times;
            }
        }

        public int printCustomerPageTimes
        {
            get
            {
                int times = 0;
                for (int i = operationRecord.Count - 1; i >= 0; i--)
                    if (operationRecord[i].operation == WarrantOperationEnum.printCustomerPage)
                        times++;
                return times;
            }
        }

        public AbstractWarrant()
        {
            this.operationRecord = new List<WarrantOperationRecord>();
        }
    }

    [Serializable]
    public class InWarrant : AbstractWarrant
    {
        public List<InWarrantItem> items { get; set; }
        public InWarrant()
        {
            items = new List<InWarrantItem>();
            this.type = SpecificWarrantType.In;
        }
    }

    [Serializable]
    public class OutWarrant : AbstractWarrant 
    {
        private List<DefaultAttendant> attendants;
        public string amountPermitReason { get; set; }
        public List<OutWarrantItem> items { get; set; }

        public OutWarrant()
        {
            items = new List<OutWarrantItem>();
            this.attendants = new List<DefaultAttendant>();
        }

        public void clearAttendants()
        {
            this.attendants.Clear();
        }

        public void addAttendants(DefaultAttendant attendant)
        {
            this.attendants.Add(attendant);
        }

        public List<DefaultAttendant> getAttendants()
        {
            return this.attendants;
        }

        public static OutWarrantPayState IntToPayState(int? payState)
        {
            if (payState == null)
                return OutWarrantPayState.Abnormal;
            return (OutWarrantPayState)(payState);
        }

        public static int PayStateToInt(OutWarrantPayState payState)
        {
            return (int)payState;
        }

        public enum OutWarrantPayState
        {
            NotPaid = 0,
            Paid = 1,
            ForcePass = 2,
            PartPaid = 3,
            OverPaid = 4,
            Abnormal = 19,
            NotSuitable = 20
        }
    }
}
