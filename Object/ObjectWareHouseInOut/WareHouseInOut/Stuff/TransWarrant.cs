﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.WareHouseInOut.Stuff
{
    /// <summary>
    /// used to transmission the core message of the warrant to the server
    /// used to submit, cancel, print
    /// </summary>
    [Serializable]
    public class WarrantCSControl
    {
        public int ID { get; set; }
        public int cookie { get; set; }

        public WarrantCSControl(DefaultWarrant warrant)
        {
            this.ID = warrant.ID;
            this.cookie = warrant.cookie;
        }
    }

    [Serializable]
    public class WarrantCSCreate : WarrantCSControl
    {
        public int? target { get; set; }
        public string comment { get; set; }
        public DefaultWarrant.SpecificWarrantType type { get; set; }

        public WarrantCSCreate(AbstractWarrant warrant)
            : base(warrant)
        {
            this.target = warrant.target;
            this.comment = warrant.comment;
            this.type = warrant.type;
        }
    }



    /// <summary>
    /// used to transmission the much message of the warrant to the server
    /// used to submit, cancel, print
    /// </summary>
    [Serializable]
    public class InWarrantCSCreate : WarrantCSCreate
    {
        public List<InWarrantItemCS> items { get; set; }

        public InWarrantCSCreate(InWarrant inWarrant)
            :base(inWarrant)
        {
            this.items = new List<InWarrantItemCS>();
            foreach (InWarrantItem item in inWarrant.items)
                items.Add(item.generateInWarrantItemCS());
        }
    }

    [Serializable]
    public class OutWarrantCSCreate : WarrantCSCreate
    {
        public List<OutWarrantItemCS> items { get; set; }
        public List<DefaultAttendant> attendants { get; set; }

        public OutWarrantCSCreate(OutWarrant outWarrant)
            :base(outWarrant)
        {
            this.items = new List<OutWarrantItemCS>();
            this.attendants = outWarrant.getAttendants();
            foreach (OutWarrantItem item in outWarrant.items)
                items.Add(item.generateOutWarrantItemCS());
        }
    }

    [Serializable]
    public class AbstractWarrantItemCS
    {
        public int ID { get; set; }
        public int IDdraft { get; set; }
        public int price { get; set; }          //the price value
        public int num { get; set; }
        public string remark { get; set; }

        public AbstractWarrantItemCS() { }
        public AbstractWarrantItemCS(AbstractWarrantItem warrantItem)
        {
            this.ID = warrantItem.ID;
            this.IDdraft = warrantItem.IDdraft;
            this.price = warrantItem.price;
            this.num = warrantItem.num;
            this.remark = warrantItem.remark;
        }

        public InWarrantItemCS convertToInWarrantItemCS()
        {
            return new InWarrantItemCS() { ID = ID, IDdraft = IDdraft, price = price, num = num, remark = remark };
        }
        public OutWarrantItemCS convertToOutWarrantItemCS()
        {
            return new OutWarrantItemCS() { ID = ID, IDdraft = IDdraft, price = price, num = num, remark = remark };
        }
    }

    [Serializable]
    public class InWarrantItemCS : AbstractWarrantItemCS
    {
        public InWarrantItemCS() { }
        public InWarrantItemCS(InWarrantItem inWarrantItem)
            : base(inWarrantItem)
        {
            priceWithTax = inWarrantItem.priceWithTax;
        }

        public int priceWithTax { get; set; }
    }

    [Serializable]
    public class OutWarrantItemCS : AbstractWarrantItemCS
    {
        private int? _priceAdjustMannual;
        public int? priceAdjustMannual
        {
            get { return this._priceAdjustMannual; }
            set { if (value == 0) this._priceAdjustMannual = null; else this._priceAdjustMannual = value; }
        }

        public OutWarrantItemCS() { }
        public OutWarrantItemCS(OutWarrantItem outWarrantItem)
            : base(outWarrantItem)
        {
            this.priceAdjustMannual = outWarrantItem.priceAdjustMannual;
        }
    }
}
