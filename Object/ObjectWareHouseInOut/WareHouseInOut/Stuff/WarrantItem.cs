﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.WareHouseInOut.Stuff
{
    [Serializable]
    public abstract class AbstractWarrantItem
    {
        public int ID { get; set; }
        public int IDdraft { get; set; }
        public string name { get; set; }
        public string specification { get; set; }
        public string hiddenSpecification { get; set; }
        public int IDWarrant { get; set; }
        public int price { get; set; }          //the price value
        public int num { get; set; }            //the number of the stuff
        public string unit { get; set; }
        public string remark { get; set; }
    }

    [Serializable]
    public class InWarrantItem : AbstractWarrantItem
    {
        public InWarrantItemCS generateInWarrantItemCS()
        {
            InWarrantItemCS item = new InWarrantItemCS(this);
            return item;
        }

        public int priceWithTax { get; set; }
    }

    [Serializable]
    public class OutWarrantItem : AbstractWarrantItem
    {
        public int numReturn { get; set; }
        //priceCostValue is totalPrice of all components of this item
        public int priceCostValue { get; set; }
        //priceAdjust priceOut are all single price
        public int priceAdjustAuto { get; set; }
        public int? priceAdjustMannual { get; set; }
        public int priceOutH { get; set; }
        public int priceOutL { get; set; }

        public int priceCost
        {
            get
            {
                return (num - numReturn) == 0 ? 0 : priceCostValue / (num - numReturn);
            }
        }

        public OutWarrantItemCS generateOutWarrantItemCS()
        {
            OutWarrantItemCS item = new OutWarrantItemCS(this);
            return item;
        }
    }

    [Serializable]
    public class WarrantOperationRecord
    {
        public int ID { get; set; }
        public WarrantType warrantType { get; set; }
        public int warrantId { get; set; }
        public int operationOperator { get; set; }
        public string operationOperatorName { get; set; }
        public WarrantOperationEnum operation { get; set; }
        public DateTime operatedTime { get; set; }
        public Object[] parameters { get; set; }

        public WarrantOperationRecord()
        {
            this.parameters = new Object[2];
        }
    }

    public enum WarrantOperationEnum
    {
        create = 0,
        edit = 1,
        cancel = 2,
        submitCustomerPage = 3,
        printCustomerPage = 4,
        forcePass = 5,
        takeOut = 6,
        returnOperation = 7,
        submit = 8,
        print = 9,
        finish = 10,
        payCheck = 11,
        discount = 12
    }

    [Serializable]
    public class TransReturnNumberPair
    {
        public int number1 { get; set; }
        public int number2 { get; set; }
    }
}
