﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedObject.WareHouseInOut.Stuff
{
    [Serializable]
    public class GetSpecialPriceCS
    {
        public int? target { get; set; }
        public List<Int32> draftID { get; set; }
    }

    [Serializable]
    public class SpecialPriceSC
    {
        [Serializable]
        public class DraftWithSpecialPrice
        {
            public int draftID { get; set; }
            public int? priceOutL { get; set; }
            public int? priceOutH { get; set; }
        }
        public List<DraftWithSpecialPrice> draftIDWithPrice { get; set; }
    }

    [Serializable]
    public class DetailAccount
    {
        public List<InDetail> inDetails { get; set; }
        public List<OutDetail> outDetails { get; set; }

        [Serializable]
        public class DefaultDetail
        {
            public DateTime dateTime { get; set; }
            public string name { get; set; }
            public string specification { get; set; }
            public int IDWarrant { get; set; }
            public string number { get; set; }
            public int num { get; set; }
            public int price { get; set; }
            public string remark { get; set; }
            public int currentValue { get; set; }
            public int currentNumAll { get; set; }
             
            public int currentPriceIn
            {
                get
                {
                    return currentNumAll == 0 ? 0 : currentValue / currentNumAll;
                }
            }
        }

        [Serializable]
        public class InDetail : DefaultDetail
        {
        }

        [Serializable]
        public class OutDetail : DefaultDetail
        {
            public int numReturn { get; set; }
            public int? priceAdjustMannual { get; set; }
            public int priceCost { get; set; }
        }
    }

    [Serializable]
    public class ViewOnceCycleInWarrant : DefaultWarrant
    {
        public string target_name { get; set; }
    }

    [Serializable]
    public class ViewOnceCycleOutWarrant : DefaultWarrant
    {
        public string attendant_names { get; set; }
        public string target_name { get; set; }
    }

    [Serializable]
    public class ViewOneCycle
    {
        public DateTime date { get; set; }
        public List<ViewOnceCycleInWarrant> inWarrantList { get; set; }
        public List<ViewOnceCycleOutWarrant> outWarrantList { get; set; }
    }

    [Serializable]
    public class DiscountItem
    {
        public int warrantId { get; set; }
        public int? amountPermit { get; set; }
        public string reason { get; set; }
    }

    [Serializable]
    public class PayCheckItem
    {
        public int warrantId { get; set; }
        public int amountPaid { get; set; }
    }

}
